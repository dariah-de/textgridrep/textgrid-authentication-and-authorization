# How to contribute to the TextGrid Authentication and Authorization Services

Please read the [RDD Technical Reference Manual](https://gitlab.gwdg.de/fe/technical-reference/-/releases) before proceeding!

If you want to contribute to the TextGrid Authentication and Authorization Services, please

1. Fork the GIT repository: `git clone https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization.git`

2. Do install the needed packages for commiting (you may to have use --force to avoid some unmatched dependencies): `npm i (--force)`

2. Create an new issue for your feature/fix in the [GitLab GUI issue page](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization/-/issues).

4. Create a new branch for your feature/fix.

5. Develop your feature/fix in the new branch and commit and push acording to the [sesmantic release rules](https://semantic-release.gitbook.io/semantic-release/)

6. Husky and commitizen installation should force you to provide correct commit messages.

7. Then file a [merge request in the GitLab GUI](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization/-/merge_requests/new) from your new branch (src) to the develop branch (dest).

8. If you are a maintainer, please take the [DARIAH-DE Release Workflow](https://wiki.de.dariah.eu/display/DARIAH3/DARIAH-DE+Release+Management#DARIAHDEReleaseManagement-Gitlabflow/Gitflow(develop,main,featurebranchesundtags)) into account.
