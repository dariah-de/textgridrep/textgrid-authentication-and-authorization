## [1.18.2](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization/compare/v1.18.1...v1.18.2) (2024-06-12)


### Bug Fixes

* configure logging to /var/log/dhrep/tgauth ([cd28adf](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization/commit/cd28adfd725818f3bfac3f0660686a79a7d36953))
* re-ignore online testing ([2335452](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization/commit/2335452a3b736e0f038bb3d08c7defb852ff1f14))

## [1.18.1](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization/compare/v1.18.0...v1.18.1) (2024-01-08)


### Bug Fixes

* fix gitlab ci ... again ([1c77bcf](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization/commit/1c77bcf95973c60e63788598a4ace22e8cebb269))

# [1.18.0](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization/compare/v1.17.2...v1.18.0) (2024-01-08)


### Bug Fixes

* add constructur again, remove .project file ([689e3f2](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization/commit/689e3f24a7cd93dfb02bb1e9116095c885c5887a))
* add java17 image to gitlab ci file ([c3967df](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization/commit/c3967dfbaeaeb072cca5ca44a22e40b9d6b4910c))
* arg! now I got it! :-) adapt constructor comment ([187904a](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization/commit/187904ac86b52d7fcc25f3a826d389f6e3f038f9))
* comment in ignoring the junit online tests ([cd9a031](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization/commit/cd9a0319d6a67597f0f7c470ae6373b8683c86c2))
* comment in log output for getsid html ([737c9e2](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization/commit/737c9e21584c70dfd93657b4b659ac9f5c6957e5))
* daasi mr changes ([46d6741](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization/commit/46d67417cee3aeac6284b49e89adddf5b8758186))
* do some exception things ([d7c34ff](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization/commit/d7c34ffce7e5278095a45d650a2fd3881e9eff26))
* fix WSDL files, add correct test server url ([8c6a8b6](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization/commit/8c6a8b66c40e45e70e05303a596c7957d6a8c967))
* fixed last typo, i hope... :-) ([2f437c5](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization/commit/2f437c5cd608b3a3de8f0cb9feb625536a4ee01d))
* fixes deployment of ALL tags to releases, only v1.2.3 like versions shall be released ([eb62010](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization/commit/eb62010079f851a65a39d8777a76d1fe1eaab356))
* revert some java8 things ([c92fbb2](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization/commit/c92fbb278e55340fc4629ba1483429be79680cb3))


### Features

* update to Java17 and CXF 4.0.x and other packages ([2e9e7c0](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization/commit/2e9e7c0d57b4e9db3d02bd50dc83dfd4eded4ec2))
* update to php8 (tested) ([19f4414](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization/commit/19f4414bb218d5e81f59ddd405828128681b61f2))

## [1.17.2](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization/compare/v1.17.1...v1.17.2) (2022-09-16)


### Bug Fixes

* adding new gitlab ci workflow ([0833ffb](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization/commit/0833ffb814c7c62e0840d4e95d5cd6bf2ac50c86))
* n ew SNAPSHOT version ([860c129](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization/commit/860c1296a2514aa234d07f04ba8108d34bd3452b))

# 1.16.0-SNAPSHOT

* Add new GitLab CI workflow
* Add version refs to pom files
* Remove unused code
* Add CONTRIBUTORS file
* Add CONTRIBUTING file
