/**
 * This software is copyright (c) 2023 by
 *
 * TextGrid Consortium (https://textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.middleware.tgauth.tgauthclient;

import java.net.URL;
import java.util.Map;
import info.textgrid.namespaces.middleware.tgauth.PortTgextra;
import info.textgrid.namespaces.middleware.tgauth.Tgextra;
import jakarta.xml.ws.BindingProvider;

/**
 * TODOLOG
 *
 **
 * CHANGELOG
 * 
 * 2023-09-18 - Funk - Upgrade to Java17 and CXF4.
 *
 * 2019-01-15 - Funk - Added timeout configuration.
 *
 * 2016-08-03 - Funk - Copied from TGCrudClientUtilities.
 */

/**
 * <p>
 * TG-auth client utility class to get appropriate TG-auth clients.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2023-09-18
 * @since 2016-08-03
 **/

public final class TGAuthClientUtilities {

  /**
   * <p>
   * Private constructor for class with static methods only. Makes no sense to be instantiated.
   * </p>
   */
  private TGAuthClientUtilities() {
    //
  }

  // **
  // MAIN CLASS
  // **

  /**
   * <p>
   * Creates a regular TG-extra stub.
   * </p>
   * 
   * @param theEndpoint The endpoint of the TG-auth service.
   * @return The TG-auth webservice port.
   */
  public static PortTgextra getTgextra(URL theEndpoint) {
    return getTgextra(theEndpoint, 0);
  }

  /**
   * <p>
   * Creates a regular TG-extra stub and sets given timeout.
   * </p>
   * 
   * @param theEndpoint The endpoint of the TG-auth service.
   * @param theTimeout The timeout.
   * @return The TG-auth webservice port.
   */
  public static PortTgextra getTgextra(URL theEndpoint, int theTimeout) {

    // Create TG-extra service stubs.
    Tgextra tgextra = new Tgextra((URL) null);
    PortTgextra portTgextra = tgextra.getPort(PortTgextra.class);
    BindingProvider bindingProvider = (BindingProvider) portTgextra;

    Map<String, Object> requestContext = bindingProvider.getRequestContext();
    requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, theEndpoint.toString());

    // Set timeout if configured.
    if (theTimeout > 0) {
      requestContext.put("com.sun.xml.ws.developer.JAXWSProperties.CONNECT_TIMEOUT", theTimeout);
      requestContext.put("com.sun.xml.ws.connect.timeout", theTimeout);
      requestContext.put("com.sun.xml.ws.internal.connect.timeout", theTimeout);
      requestContext.put("com.sun.xml.ws.request.timeout", theTimeout);
      requestContext.put("com.sun.xml.internal.ws.request.timeout", theTimeout);
      requestContext.put("javax.xml.ws.client.receiveTimeout", theTimeout);
      requestContext.put("javax.xml.ws.client.connectionTimeout", theTimeout);
    }

    return portTgextra;
  }

}
