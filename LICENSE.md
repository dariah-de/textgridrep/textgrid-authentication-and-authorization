# TextGrid Authentication and Authorization (tg-auth) Licenses

* [RBAC](./info.textgrid.middleware.tgauth.rbac/License.txt)
* [RBAC Stubs](./info.textgrid.middleware.tgauth.rbacstubs/License.txt)
* [TGExtra Client](./info.textgrid.middleware.tgauth.tgextra-client/LICENCE.txt)
* [TGExtra Client CRUD](./info.textgrid.middleware.tgauth.tgextra-client-crud/LICENCE.txt)
* [TGExtra Client Test](./info.textgrid.middleware.tgauth.tgextra-client-test/LICENCE.txt)
* [TGWebAuth](./info.textgrid.middleware.tgauth.webauth/License.txt)
