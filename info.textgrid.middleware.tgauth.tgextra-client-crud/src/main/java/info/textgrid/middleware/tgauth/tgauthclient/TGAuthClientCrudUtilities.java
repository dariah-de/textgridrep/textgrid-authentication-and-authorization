/**
 * This software is copyright (c) 2023 by
 *
 * TextGrid Consortium (https://textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.middleware.tgauth.tgauthclient;

import java.net.URL;
import java.util.Map;
import info.textgrid.namespaces.middleware.tgauth_crud.PortTgextraCrud;
import info.textgrid.namespaces.middleware.tgauth_crud.TgextraCrud;
import jakarta.xml.ws.BindingProvider;

/**
 * TODOLOG
 *
 **
 * CHANGELOG
 *
 * 2023-09-18 - Funk - Upgrade to Java17 and CXF4.
 * 
 * 2016-08-03 - Funk - Copied from TGAuthClientUtilities.
 */

/**
 * <p>
 * TG-auth CRUD client utility class to get appropriate TG-auth clients.
 * </p>
 *
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2023-09-18
 * @since 2016-08-03
 */

public final class TGAuthClientCrudUtilities {

  // **
  // MAIN CLASS
  // **

  /**
   * <p>
   * Creates a TG-extra CRUD stub.
   * </p>
   * 
   * @param theEndpoint The endpoint of the TG-auth service.
   * @return The TG-auth webservice port.
   */
  public static PortTgextraCrud getTgextraCrud(URL theEndpoint) {

    // Create TG-extra service stubs.
    TgextraCrud tgextraCrud = new TgextraCrud((URL) null);
    PortTgextraCrud portTgextraCrud = tgextraCrud.getPort(PortTgextraCrud.class);
    BindingProvider bindingProvider = (BindingProvider) portTgextraCrud;
    Map<String, Object> requestContext = bindingProvider.getRequestContext();
    requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, theEndpoint.toString());

    return portTgextraCrud;
  }

}
