# The TextGrid Authentication and Authorization Services

## Overview

The  TextGrid Authentication and Authorization Services is used for TextGrid Authentication and Authorization.

## Documentation

For service and API documentation please have a look at the [TG-auth Documentation](https://textgridlab.org/doc/services/submodules/tg-auth/docs/index.html).

## Installation

The service is provided with a Gitlab CI workflow to build .DEB files to be deployed and installed on TextGrid.

### Building TG-auth Clients from GIT using Maven

You can check out the TG-auth Service from our [GIT Repository](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization), develop or main branch, or get yourself a certain tag, and then use Maven to build the TG-auth Client JAR file:

    git clone https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization.git

... build the client JAR packagea using:

    mvn clean package

You will get TG-auth Client JAR files in the folders ./info.textgrid.middleware.tgauth.tgextra-client/target and ./info.textgrid.middleware.tgauth.tgextra-client-crud/target.

### Building TG-auth Service DEB File

The TG-auth Service DEB file is build using GitLab CI and will be deployed automatically to the DARIAH-DE [APTLY Repository](https://ci.de.dariah.eu/packages/)

## Links and References

* [TextGrid Authentication and Authorization Services](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization)
