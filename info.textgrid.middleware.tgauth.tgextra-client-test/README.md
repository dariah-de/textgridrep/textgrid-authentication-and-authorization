# The TextGrid Authentication and Authorization Services

## Testing the TG-auth

Just

1. clone the repository,
2. checkout the needeed branch,
3. copy the file from `src/test/resources/tgauth.test.properties` to a new file,
4. add that file to `src/test/resources/.gitignore`!
5. add that filename to the class `src/test/java/info/textgrid/middleware/tgauth/tgauthclient/TestTGAuthServiceOnline.java` as `PROPERTIES_FILE` value,
6. edit the file to match your test server, such as
    - `tgextraServiceEndpointUrl=https://test.textgridlab.org/1.0/tgauth/tgextra.php`
    - `tgextraCrudServiceEndpointUrl=https://dev.textgridlab.org/1.0/tgauth/tgextra-crud.php`
    - add the SessionID of user `crud.junit1@textgrid.de` and the crudSecret from the server you want to test.
7. out-comment the `@Ignore` annotation in the test class to get tests running (and do not push that commented-out code to the repo :-) we will fix this issue soon...)
8. finally do a `mvn clean test` in the root of the repo (where the `pom.xml` file is located) for JUnit online testing
