/**
 * This software is copyright (c) 2022 by
 *
 * TextGrid Consortium (https://textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.middleware.tgauth.tgauthclient;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * TODOLOG
 *
 **
 * CHANGELOG
 *
 * 2016-08-03 Funk Copied from TGCrudClientUtilities.
 */

/**
 * <p>
 * TG-auth client utility class to get appropriate TG-auth clients.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2023-12-15
 * @since 2018-03-06
 */

public final class TGAuthClientCommon {

  // **
  // STATIC FINALS
  // **

  public final static String OPERATION_CREATE = "create";
  public final static String OPERATION_DELETE = "delete";
  public final static String OPERATION_WRITE = "write";
  public final static String OPERATION_READ = "read";
  public final static String OPERATION_PUBLISH = "publish";

  public static String ROLE_EDITOR = "Bearbeiter";
  public static String ROLE_ADMIN = "Administrator";
  public static String ROLE_MANAGER = "Projektleiter";
  public static String ROLE_OBSERVER = "Beobachter";

  public final static String TABLINE = "\t----------------------------------------";

  // **
  // MAIN CLASS
  // **

  /**
   * <p>
   * Loads a resource.
   * </p>
   * 
   * TODO Put together with the method in TGCrudServiceUtilities! Maybe create a first build maven
   * module for util things.
   * 
   * @param {@link String} The resource to search for.
   * @return {@link File} The resource.
   * @throws IOException
   */
  public static File getResource(String resPart) throws IOException {

    File res;

    // If we have an absolute resPart, just return the file.
    if (resPart.startsWith(File.separator)) {
      return new File(resPart);
    }

    URL url = ClassLoader.getSystemClassLoader().getResource(resPart);
    if (url == null) {
      throw new IOException("Resource '" + resPart + "' not found");
    }
    try {
      res = new File(url.toURI());
    } catch (URISyntaxException ue) {
      res = new File(url.getPath());
    }

    return res;
  }

}
