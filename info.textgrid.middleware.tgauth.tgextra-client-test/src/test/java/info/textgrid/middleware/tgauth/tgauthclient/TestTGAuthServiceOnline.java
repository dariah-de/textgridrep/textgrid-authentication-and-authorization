/**
 * This software is copyright (c) 2023 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.middleware.tgauth.tgauthclient;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import info.textgrid.namespaces.middleware.tgauth.AddMemberRequest;
import info.textgrid.namespaces.middleware.tgauth.AuthenticateRequest;
import info.textgrid.namespaces.middleware.tgauth.AuthenticateResponse;
import info.textgrid.namespaces.middleware.tgauth.AuthenticationFault;
import info.textgrid.namespaces.middleware.tgauth.BooleanResponse;
import info.textgrid.namespaces.middleware.tgauth.CreateProjectRequest;
import info.textgrid.namespaces.middleware.tgauth.CreateProjectResponse;
import info.textgrid.namespaces.middleware.tgauth.DeactivateProjectRequest;
import info.textgrid.namespaces.middleware.tgauth.DeleteProjectRequest;
import info.textgrid.namespaces.middleware.tgauth.FilterBySidRequest;
import info.textgrid.namespaces.middleware.tgauth.FilterResponse;
import info.textgrid.namespaces.middleware.tgauth.Friend;
import info.textgrid.namespaces.middleware.tgauth.GetAllProjectsRequest;
import info.textgrid.namespaces.middleware.tgauth.GetAllProjectsResponse;
import info.textgrid.namespaces.middleware.tgauth.GetDeactivatedProjectsRequest;
import info.textgrid.namespaces.middleware.tgauth.GetDeactivatedProjectsResponse;
import info.textgrid.namespaces.middleware.tgauth.GetFriendsRequest;
import info.textgrid.namespaces.middleware.tgauth.GetFriendsResponse;
import info.textgrid.namespaces.middleware.tgauth.GetIDsRequest;
import info.textgrid.namespaces.middleware.tgauth.GetIDsResponse;
import info.textgrid.namespaces.middleware.tgauth.GetLeaderRequest;
import info.textgrid.namespaces.middleware.tgauth.GetMembersRequest;
import info.textgrid.namespaces.middleware.tgauth.GetMyUserAttributesRequest;
import info.textgrid.namespaces.middleware.tgauth.GetMyUserAttributesResponse;
import info.textgrid.namespaces.middleware.tgauth.GetNamesRequest;
import info.textgrid.namespaces.middleware.tgauth.GetNamesResponse;
import info.textgrid.namespaces.middleware.tgauth.GetNumberOfResourcesRequest;
import info.textgrid.namespaces.middleware.tgauth.GetNumberOfResourcesResponse;
import info.textgrid.namespaces.middleware.tgauth.GetObjectsRequest;
import info.textgrid.namespaces.middleware.tgauth.GetOwnerRequest;
import info.textgrid.namespaces.middleware.tgauth.GetOwnerResponse;
import info.textgrid.namespaces.middleware.tgauth.GetProjectDescriptionRequest;
import info.textgrid.namespaces.middleware.tgauth.GetProjectDescriptionResponse;
import info.textgrid.namespaces.middleware.tgauth.GetRightsRequest;
import info.textgrid.namespaces.middleware.tgauth.GetSidRequest;
import info.textgrid.namespaces.middleware.tgauth.GetSidResponse;
import info.textgrid.namespaces.middleware.tgauth.GetSupportedUserAttributesRequest;
import info.textgrid.namespaces.middleware.tgauth.GetSupportedUserAttributesResponse;
import info.textgrid.namespaces.middleware.tgauth.GetUserRoleRequest;
import info.textgrid.namespaces.middleware.tgauth.GetUserRoleResponse;
import info.textgrid.namespaces.middleware.tgauth.IsPublicRequest;
import info.textgrid.namespaces.middleware.tgauth.NotEmptyFault;
import info.textgrid.namespaces.middleware.tgauth.OperationsetResponse;
import info.textgrid.namespaces.middleware.tgauth.PortTgextra;
import info.textgrid.namespaces.middleware.tgauth.ProjectInfo;
import info.textgrid.namespaces.middleware.tgauth.RbacFault;
import info.textgrid.namespaces.middleware.tgauth.ReactivateProjectRequest;
import info.textgrid.namespaces.middleware.tgauth.ResourcesetResponse;
import info.textgrid.namespaces.middleware.tgauth.RolesetResponse;
import info.textgrid.namespaces.middleware.tgauth.SetNameRequest;
import info.textgrid.namespaces.middleware.tgauth.SetProjectFileRequest;
import info.textgrid.namespaces.middleware.tgauth.TgAddActiveRoleRequest;
import info.textgrid.namespaces.middleware.tgauth.TgAssignedProjectsRequest;
import info.textgrid.namespaces.middleware.tgauth.TgAssignedRolesRequest;
import info.textgrid.namespaces.middleware.tgauth.TgCheckAccessRequest;
import info.textgrid.namespaces.middleware.tgauth.TgDropActiveRoleRequest;
import info.textgrid.namespaces.middleware.tgauth.TgGrantPermissionRequest;
import info.textgrid.namespaces.middleware.tgauth.TgRevokePermissionRequest;
import info.textgrid.namespaces.middleware.tgauth.UnknownProjectFault;
import info.textgrid.namespaces.middleware.tgauth.UserAttribute;
import info.textgrid.namespaces.middleware.tgauth.UserDetail;
import info.textgrid.namespaces.middleware.tgauth.UserExistsRequest;
import info.textgrid.namespaces.middleware.tgauth.UserRole;
import info.textgrid.namespaces.middleware.tgauth.UsersetResponse;
import info.textgrid.namespaces.middleware.tgauth_crud.GetCSRRequest;
import info.textgrid.namespaces.middleware.tgauth_crud.GetCSRResponse;
import info.textgrid.namespaces.middleware.tgauth_crud.GetEPPNRequest;
import info.textgrid.namespaces.middleware.tgauth_crud.GetEPPNResponse;
import info.textgrid.namespaces.middleware.tgauth_crud.GetUUIDRequest;
import info.textgrid.namespaces.middleware.tgauth_crud.GetUUIDResponse;
import info.textgrid.namespaces.middleware.tgauth_crud.NearlyPublishRequest;
import info.textgrid.namespaces.middleware.tgauth_crud.PortTgextraCrud;
import info.textgrid.namespaces.middleware.tgauth_crud.PublishRequest;
import info.textgrid.namespaces.middleware.tgauth_crud.RegisterResourceRequest;
import info.textgrid.namespaces.middleware.tgauth_crud.TgCrudCheckAccessRequest;
import info.textgrid.namespaces.middleware.tgauth_crud.TgCrudCheckAccessResponse;
import info.textgrid.namespaces.middleware.tgauth_crud.UnknownResourceFault;
import info.textgrid.namespaces.middleware.tgauth_crud.UnregisterResourceRequest;
import jakarta.xml.ws.WebServiceException;
import jakarta.xml.ws.soap.SOAPFaultException;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2023-12-14 - Funk - Complete publish() tests.
 * 
 * 2023-12-05 - Funk - Add more tests.
 * 
 * 2023-12-04 - Funk - Add test for register and unregister resources.
 * 
 * 2023-09-18 - Funk - Upgrade to Java17 and CXF4.
 * 
 * 2022-05-30 - Funk - Add test for creating projects with certain UTF-8 chars such as „ and “.
 * 
 * 2020-02-14 - Funk - Add correct test for getNames (tgauth v1.10.0).
 * 
 * 2019-11-12 - Funk - Add GetAllProjects test for public projects.
 * 
 * 2019-01-28 - Funk - Make output pretty.
 * 
 * 2018-11-23 - Funk - Adding tests for issue #26471
 * 
 * 2016-08-11 - Funk - Using TGAuth client from TGAuth service utilities now!
 * 
 */

/**
 * <p>
 * This are TG-auth ONLINE tests for testing a running tgauth service to fulfil its duties.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2023-12-21
 * @since 2010-09-07
 */
@Ignore
public class TestTGAuthServiceOnline {

  // **
  // STATIC FINALS
  // **

  private static final String PROPERTIES_FILE = "tgauth.test.textgridlab-org.properties";
  // private static final String PROPERTIES_FILE = "tgauth.test.dev-textgridlab-org.properties";
  // private static final String PROPERTIES_FILE = "tgauth.test.test-textgridlab-org.properties";

  // DO NOT CHANGE THIS! :-O
  //
  private static final String PRODUCTIVE = "tgauth.test.textgridlab-org.properties";
  //
  // DO NOT CHANGE THIS! :-O

  private static final int TGEXTRA_CLIENT_TIMEOUT = 5000;
  private static final String ERROR = "\t??ERROR: ";
  private static final String OK = "\tOK";

  // **
  // STATICS
  // **

  private static PortTgextra tgextra;
  private static PortTgextraCrud tgextraCrud;
  private static String rbacSessionId;
  private static String rbacSessionIdInvalid;
  private static String crudSecret;
  private static String crudSecretInvalid;
  private static String logParameter;
  private static String projectId;
  private static String projectIdInvalid;
  private static String username;
  private static String usernameInvalid;
  private static String usernameSearch;
  private static String junitProjectName;
  private static String getMyUserAtrributesEmail;

  // **
  // PREPARATIONS
  // **

  /**
   * @throws java.lang.Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {

    // Load properties file.
    Properties p = new Properties();
    p.load(new FileInputStream(TGAuthClientCommon.getResource(PROPERTIES_FILE)));

    // Get and create the tgextra stubs.
    URL location = new URL(p.getProperty("tgextraServiceEndpointUrl"));
    URL locationCrud = new URL(p.getProperty("tgextraCrudServiceEndpointUrl"));

    System.out
        .println("WSDL location tgextra: " + location.getProtocol() + "://" + location.getHost()
            + ":" + (location.getPort() == -1 ? "80" : location.getPort()) + location.getPath());
    System.out.println("WSDL location tgextra-crud: " + locationCrud.getProtocol() + "://"
        + locationCrud.getHost() + ":"
        + (locationCrud.getPort() == -1 ? "80" : locationCrud.getPort()) + locationCrud.getPath());

    initTgextraService(location, TGEXTRA_CLIENT_TIMEOUT);
    initTgextraCrudService(locationCrud);

    // Create project name.
    File tempFile = File.createTempFile("JUNIT_", "_01");
    junitProjectName = tempFile.getName();
    boolean tempFileDeleted = tempFile.delete();
    if (!tempFileDeleted) {
      System.err
          .println(ERROR + "temp file '" + tempFile.getCanonicalPath() + "' could not be deleted");
    }
    // Get other needed things.
    rbacSessionId = p.getProperty("rbacSessionId");
    rbacSessionIdInvalid = p.getProperty("rbacSessionIdInvalid");
    logParameter = p.getProperty("logParameter");
    projectIdInvalid = p.getProperty("projectIdInvalid");
    crudSecret = p.getProperty("crudSecret");
    crudSecretInvalid = p.getProperty("crudSecretInvalid");
    username = p.getProperty("username");
    usernameInvalid = p.getProperty("usernameInvalid");
    usernameSearch = p.getProperty("usernameSearch");
    getMyUserAtrributesEmail = p.getProperty("getMyUserAtrributesEmail");

    addProject();
  }

  /**
   * @throws Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {

    System.out.println("DELETING PROJECT '" + junitProjectName + "' with ID '" + projectId + "'");

    deleteProject(junitProjectName, projectId);
  }

  // **
  // TEST BUGS
  // **

  /**
   * 
   */
  @Test
  public void testBug21896() {

    System.out.println("TESTING #BUG21896");

    //
    // Retrieve project information.
    //
    System.out.println("\tgetting project info for ID " + projectId);

    GetProjectDescriptionRequest req = new GetProjectDescriptionRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setProject(projectId);

    GetProjectDescriptionResponse res = tgextra.getProjectDescription(req);

    System.out.println("\tproject name:        " + res.getProject().getName());
    System.out.println("\tproject ID:          " + res.getProject().getId());
    System.out.println("\tproject Description: " + res.getProject().getDescription());
    System.out.println("\tproject File:        " + res.getProject().getFile());
  }

  // **
  // TGEXTRA TESTS
  // **

  /**
   * <p>
   * Test the userExists method with valid user.
   * </p>
   * 
   * @throws info.textgrid.namespaces.middleware.tgauth.AuthenticationFault
   */
  @Test
  public void testTgextraUserExists()
      throws info.textgrid.namespaces.middleware.tgauth.AuthenticationFault {

    System.out.println("TESTING #USEREXISTS with existing username");

    UserExistsRequest req = new UserExistsRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setUsername(username);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\tusername:      " + req.getUsername());
    System.out.println(TGAuthClientCommon.TABLINE);

    boolean result = tgextra.userExists(req).isResult();
    System.out.println("\tresult: " + result);

    if (!result) {
      assertTrue(false);
    }

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);
  }

  /**
   * <p>
   * Test the userExists method with valid user.
   * </p>
   * 
   * @throws info.textgrid.namespaces.middleware.tgauth.AuthenticationFault
   */
  @Test
  public void testTgextraUserExistsInvalidUsername()
      throws info.textgrid.namespaces.middleware.tgauth.AuthenticationFault {

    System.out.println("TESTING #USEREXISTS with invalid username");

    UserExistsRequest req = new UserExistsRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setUsername(usernameInvalid);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\tusername:      " + req.getUsername());
    System.out.println(TGAuthClientCommon.TABLINE);

    boolean result = tgextra.userExists(req).isResult();
    System.out.println("\tresult: " + result);

    if (result) {
      assertTrue(false);
    }

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);
  }

  /**
   * 
   */
  @Test
  public void testGetNames() {

    System.out.println("TESTING #GETNAMES");

    // Create getNames request.
    GetNamesRequest req = new GetNamesRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.getEPPN().add(username);
    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\tuser name:     " + req.getEPPN());
    System.out.println(TGAuthClientCommon.TABLINE);

    GetNamesResponse res = tgextra.getNames(req);

    if (res.getUserdetails().size() != 1) {
      assertTrue(false);
    }

    System.out.println("\tuser details list size = " + res.getUserdetails().size());
    System.out.println("\tuser details = " + res.getUserdetails().get(0).getEPPN() + ", "
        + res.getUserdetails().get(0).getMail() + ", " + res.getUserdetails().get(0).getName()
        + ", " + res.getUserdetails().get(0).getOrganisation());
    System.out.println(OK);
  }

  /**
   * @throws Exception
   */
  @Test
  @Ignore
  public void testGrantAndRevokePermission() throws Exception {

    // TODO Do we need this? I don't know how to use it, really!

    System.out.println("TESTING #GRANTANDREVOKEPERMISSION");

    String uri = "junit:" + System.currentTimeMillis();

    try {
      // Create registerResource request.
      System.out.println("\t--> RegisterResource()");

      registerResource(uri);

      // Grant delete permission for role observer.
      System.out.println("\t--> grantPermission()");

      assertTrue(grantPermission(uri, TGAuthClientCommon.OPERATION_WRITE));

      // Revoke delete permission for role observer.
      System.out.println("\t--> revokePermission()");

      assertTrue(revokePermission(uri, TGAuthClientCommon.OPERATION_WRITE));

    } finally {
      // Create unregisterResource request.
      System.out.println("\t--> UnregisterResource()");
      unregisterResource(uri);
    }
  }

  /**
   * @throws AuthenticationFault
   * @throws UnknownProjectFault
   */
  @Test
  public void testAddAndDropActiveRole() throws AuthenticationFault, UnknownProjectFault {

    // Role drop is not shown in second getUserRole() call, maybe we need a new SID?

    System.out.println("TESTING #ADDANDDROPACTIVEROLE");

    // Get user's roles.
    System.out.println("\t--> getUserRole()");

    getUserRole();

    // Drops a role for a session.
    System.out.println("\t--> dropActiveRole()");

    assertTrue(dropActiveRole(TGAuthClientCommon.ROLE_ADMIN));

    // Get user's roles.
    System.out.println("\t--> getUserRole()");

    getUserRole();

    // Add role for a session.
    System.out.println("\t--> addActiveRole()");

    assertTrue(addActiveRole(TGAuthClientCommon.ROLE_ADMIN));

    // Get user's roles.
    System.out.println("\t--> getUserRole()");

    getUserRole();
  }

  /**
   * <p>
   * Test searching for names and email addresses.
   * </p>
   * 
   * @throws AuthenticationFault
   */
  @Test
  public void testTgextraGetIDs() {

    System.out.println("TESTING #GETID name");

    // Create getEPPN request.
    GetIDsRequest req = new GetIDsRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setName(username);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\tusername:      " + req.getName());
    System.out.println(TGAuthClientCommon.TABLINE);

    GetIDsResponse res = tgextra.getIDs(req);

    // Get first (and only) entry for given ePPn.
    UserDetail det = res.getUserdetails().get(0);
    System.out.println(
        "\tuser details:  " + det.getEPPN() + " " + det.getMail() + " " + det.getName() + " "
            + det.getOrganisation() + " " + det.isAgreesearch() + " " + det.isUsersupplieddata());

    System.out.println(TGAuthClientCommon.TABLINE);

    if (!det.getEPPN().equals(username)) {
      System.out.println(ERROR);
      assertTrue(false);
    } else {
      System.out.println(OK);
    }
  }

  /**
   * <p>
   * Test searching for agreeSearch attribute.
   * </p>
   * 
   * @throws AuthenticationFault
   */
  @Test
  public void testTgextraGetIDsAgreeSearch() {

    System.out.println("TESTING #GETID agreeSearch");

    // Create getEPPN request.
    GetIDsRequest req = new GetIDsRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setName(username);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\tusername:      " + req.getName());
    System.out.println(TGAuthClientCommon.TABLINE);

    GetIDsResponse res = tgextra.getIDs(req);

    // Get first (and only) entry for given ePPn.
    UserDetail det = res.getUserdetails().get(0);
    System.out.println(
        "\tuser details:  " + det.getEPPN() + " " + det.getMail() + " " + det.getName() + " "
            + det.getOrganisation() + " " + det.isAgreesearch() + " " + det.isUsersupplieddata());

    System.out.println(TGAuthClientCommon.TABLINE);

    if (det.isAgreesearch() == null) {
      System.err.println(ERROR + "attribtue agreeSearch is not set");
      assertTrue(false);
    } else {
      System.out.println(OK);
    }
  }

  /**
   * <p>
   * Test searching for part of username, truncated!
   * </p>
   * 
   * @throws AuthenticationFault
   */
  @Test
  public void testTgextraGetIDsSearchUserTruncated() {

    System.out.println("TESTING #GETID search user truncated");

    // Create getEPPN request.
    GetIDsRequest req = new GetIDsRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    String trunk = "*" + usernameSearch + "*";
    req.setName(trunk);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\tusername:      " + req.getName());
    System.out.println(TGAuthClientCommon.TABLINE);

    GetIDsResponse res = tgextra.getIDs(req);

    int userListSize = res.getUserdetails().size();

    // Get first (and only) entry for given ePPn.
    int theFunkCount = 0;
    if (userListSize != 0) {
      System.out.println("\tuser details:");
      for (UserDetail det : res.getUserdetails()) {
        System.out.println("\t\t" + det.getEPPN() + " " + det.getMail() + " " + det.getName() + " "
            + det.getOrganisation() + " " + det.isAgreesearch() + " " + det.isUsersupplieddata());
        if (det.getMail().contains(getMyUserAtrributesEmail)) {
          theFunkCount++;
        }
      }
    } else {
      System.out.println("\tnothing found");
    }

    System.out.println(TGAuthClientCommon.TABLINE);

    if (userListSize == 0) {
      System.err.println(ERROR + "no users found containing " + trunk + "!");
      assertTrue(false);
    } else if (theFunkCount != 3) {
      System.err.println(ERROR + "theFunkCount must be 3!");
      assertTrue(false);
    } else {
      System.out.println(OK);
    }
  }

  /**
   * <p>
   * Test searching for part of username, not truncated!
   * </p>
   * 
   * @throws AuthenticationFault
   */
  @Test
  public void testTgextraGetIDsSearchUserNoTruncated() {

    System.out.println("TESTING #GETID search user not truncated");

    // Create getEPPN request.
    GetIDsRequest req = new GetIDsRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setName(usernameSearch);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\tusername:      " + req.getName());
    System.out.println(TGAuthClientCommon.TABLINE);

    GetIDsResponse res = tgextra.getIDs(req);

    int userListSize = res.getUserdetails().size();

    // Get first (and only) entry for given ePPn.
    int theFunkCount = 0;
    if (userListSize != 0) {
      System.out.println("\tuser details:");
      for (UserDetail det : res.getUserdetails()) {
        System.out.println("\t\t" + det.getEPPN() + " " + det.getMail() + " " + det.getName() + " "
            + det.getOrganisation() + " " + det.isAgreesearch() + " " + det.isUsersupplieddata());
        if (det.getMail().contains(getMyUserAtrributesEmail)) {
          theFunkCount++;
        }
      }
    } else {
      System.out.println("nothing found");
    }

    System.out.println(TGAuthClientCommon.TABLINE);

    if (userListSize == 0) {
      System.err.println(ERROR + "no users found containing " + usernameSearch + "!");
      assertTrue(false);
    } else if (theFunkCount != 3) {
      System.err
          .println(ERROR + "expecting 3 users with email " + getMyUserAtrributesEmail + "! Got "
              + theFunkCount);
      assertTrue(false);
    } else {
      System.out.println(OK);
    }
  }

  /**
   * <p>
   * Test getting all the objects from one project.
   * </p>
   */
  @Test
  public void testTgextraGetObjects() {

    System.out.println("TESTING #GETOBJECTS objects from project: " + projectId);

    GetObjectsRequest req = new GetObjectsRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setProject(projectId);

    try {
      ResourcesetResponse res = tgextra.getObjects(req);
      int size = res.getResource().size();

      System.out.println("\tproject has got " + size + " entr" + (size != 1 ? "ies" : "y"));
      System.out.println(OK);

    } catch (Exception e) {
      printError(e);
      assertTrue(false);
    }
  }

  /**
   * <p>
   * Test the tgCheckAccess method with access granted on project#CREATE.
   * </p>
   * 
   * @throws info.textgrid.namespaces.middleware.tgauth.UnknownResourceFault
   */
  @Test
  public void testTgextraTgCheckAccessProjectCreate()
      throws info.textgrid.namespaces.middleware.tgauth.UnknownResourceFault {

    System.out.println("TESTING #TGCHECKACCESS with access granted (project#CREATE)");

    TgCheckAccessRequest req = new TgCheckAccessRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setOperation(TGAuthClientCommon.OPERATION_CREATE);
    req.setResource(projectId);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\toperation:     " + req.getOperation());
    System.out.println("\tresource:      " + req.getResource());
    System.out.println(TGAuthClientCommon.TABLINE);

    try {
      System.out.println("\tresult: " + tgextra.tgCheckAccess(req).isResult());

      System.out.println(TGAuthClientCommon.TABLINE);
      System.out.println(OK);

    } catch (info.textgrid.namespaces.middleware.tgauth.UnknownResourceFault e) {
      printError(e);
      throw e;
    }
  }

  /**
   * <p>
   * Test the tgCheckAccess method with invalid SID.
   * </p>
   * 
   * @throws info.textgrid.namespaces.middleware.tgauth.UnknownResourceFault
   */
  @Test
  public void testTgextraTgCheckAccessDenied()
      throws info.textgrid.namespaces.middleware.tgauth.UnknownResourceFault {

    System.out.println("TESTING #TGCHECKACCESS with access denied (project#CREATE)");

    TgCheckAccessRequest req = new TgCheckAccessRequest();
    req.setAuth(rbacSessionIdInvalid);
    req.setLog(logParameter);
    req.setOperation(TGAuthClientCommon.OPERATION_CREATE);
    req.setResource(projectId);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\toperation:     " + req.getOperation());
    System.out.println("\tresource:      " + req.getResource());
    System.out.println(TGAuthClientCommon.TABLINE);

    try {
      boolean result = tgextra.tgCheckAccess(req).isResult();
      System.out.println("\tresult: " + result);

      if (result) {
        assertTrue(false);
      }

      System.out.println(TGAuthClientCommon.TABLINE);
      System.out.println(OK);

    } catch (info.textgrid.namespaces.middleware.tgauth.UnknownResourceFault e) {
      printError(e);
      throw e;
    }
  }

  /**
   * <p>
   * Test the tgCheckAccess method with invalid resource.
   * </p>
   * 
   * @throws info.textgrid.namespaces.middleware.tgauth.UnknownResourceFault
   */
  @Test(expected = info.textgrid.namespaces.middleware.tgauth.UnknownResourceFault.class)
  public void testTgextraTgCheckAccessUnknownResource()
      throws info.textgrid.namespaces.middleware.tgauth.UnknownResourceFault {

    System.out.println("TESTING #TGCHECKACCESS with unknown resource (project#CREATE)");

    TgCheckAccessRequest req = new TgCheckAccessRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setOperation(TGAuthClientCommon.OPERATION_CREATE);
    req.setResource(projectIdInvalid);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\toperation:     " + req.getOperation());
    System.out.println("\tresource:      " + req.getResource());
    System.out.println(TGAuthClientCommon.TABLINE);

    try {
      tgextra.tgCheckAccess(req).isResult();
    } catch (info.textgrid.namespaces.middleware.tgauth.UnknownResourceFault e) {
      System.out.println(OK);
      throw e;
    }
  }

  /**
   * <p>
   * Tests if a SID's user has assigned some roles.
   * </p>
   */
  @Test
  public void testTgextraTgAssignedRoles() {

    System.out.println("TESTING #TGASSIGNEDROLES");

    TgAssignedRolesRequest req = new TgAssignedRolesRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println(TGAuthClientCommon.TABLINE);

    RolesetResponse response = tgextra.tgAssignedRoles(req);

    for (String role : response.getRole()) {
      String data[] = role.split(",");
      if (data.length == 3) {
        System.out.println("\t" + data[0] + "\t" + data[1] + "\t" + data[2]);
      }
    }

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);
  }

  /**
   * 
   */
  @Test
  public void testTgextraTgAssignedProjects() {

    System.out.println("TESTING #TGASSIGNEDPROJECTS");

    TgAssignedProjectsRequest req = new TgAssignedProjectsRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println(TGAuthClientCommon.TABLINE);

    RolesetResponse response = tgextra.tgAssignedProjects(req);

    for (String role : response.getRole()) {
      String data[] = role.split(",");
      if (data.length == 3) {
        System.out.println("\t" + data[0] + "\t" + data[1] + "\t" + data[2]);
      }
    }

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);
  }

  /**
   * 
   */
  @Test
  public void testTgextraGetAllProjects() {

    System.out.println("TESTING #GETALLPROJECTS");

    //
    // Retrieve project information.
    //
    System.out.println("\tgetting project info for ID " + projectId);

    GetAllProjectsRequest req = new GetAllProjectsRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);

    GetAllProjectsResponse res = tgextra.getAllProjects(req);
    List<ProjectInfo> projectList = res.getProject();

    System.out.println("\tprojects found: " + projectList.size());

    for (ProjectInfo p : projectList) {
      System.out.println("\tNAME: " + p.getName() + " - ID: " + p.getId() + " - DESC: "
          + p.getDescription() + " - FILE: " + p.getFile());
    }

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);
  }

  /**
   * 
   */
  @Test
  public void testTgextraGetAllProjectsPublic() {

    System.out.println("TESTING PUBLIC #GETALLPROJECTS");

    GetAllProjectsRequest req = new GetAllProjectsRequest();
    req.setLog(logParameter);

    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println(TGAuthClientCommon.TABLINE);

    GetAllProjectsResponse response = tgextra.getAllProjects(req);

    for (ProjectInfo p : response.getProject()) {
      System.out.println("\t" + p.getId() + "\t" + p.getName() + "\t" + p.getDescription());
    }

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);
  }

  /**
   * @throws AuthenticationFault
   * @throws UnknownProjectFault
   * 
   */
  @Test
  public void testTgextraHandleDeactivationOfProjects()
      throws AuthenticationFault, UnknownProjectFault {

    System.out.println("TESTING #HANDLEDEACTIVATION");

    deactivateProject();

    getDeactivatedProjects();

    reactivateProject();
  }

  /**
   * 
   */
  @Test
  public void testTgextraGetLeader() {

    System.out.println("TESTING #GETLEADER");

    GetLeaderRequest req = new GetLeaderRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setProject(projectId);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\tproject:       " + req.getProject());
    System.out.println(TGAuthClientCommon.TABLINE);

    UsersetResponse res = tgextra.getLeader(req);

    List<String> users = res.getUsername();

    assertTrue(users.size() > 0);
    assertTrue(users.get(0).equals(username));

    System.out.println("\t users:        " + users);

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);
  }

  /**
   * @throws info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault
   * @throws Exception
   */
  @Test
  public void testTgextraFilterBySid()
      throws Exception {

    System.out.println("TESTING #FILTERBYSID");

    String uri = "junit:" + System.currentTimeMillis();

    try {
      // Create registerResource request.
      System.out.println("\t--> RegisterResource()");

      registerResource(uri);

      System.out.println("\t--> FilterBySid()");

      // Set uri as resource to be filtered.
      Collection<String> uris = new ArrayList<String>();
      uris.add(uri);

      // Do not modify incoming list, copy for retainAll operation.
      List<String> resource = new ArrayList<String>(uris);

      FilterBySidRequest req = new FilterBySidRequest();
      req.setAuth(rbacSessionId);
      req.setLog(logParameter);
      req.setOperation(TGAuthClientCommon.OPERATION_READ);
      req.getResource().addAll(resource);

      System.out.println("\tsession ID:    " + req.getAuth());
      System.out.println("\tlog parameter: " + req.getLog());
      System.out.println("\toperation:     " + req.getOperation());
      System.out.println("\tresource:      " + req.getResource());
      System.out.println(TGAuthClientCommon.TABLINE);

      System.out.println("\tAllowed objects:");

      FilterResponse response = tgextra.filterBySid(req);

      List<String> allowed = response.getResource();
      resource.retainAll(allowed);

      assertTrue("NO RESOURCES FOUND! Please check request or test!",
          resource.size() > 0);

      for (String r : resource) {
        System.out.println("\t" + r);
      }

    } finally {
      // Create unregisterResource request.
      System.out.println("\t--> UnregisterResource()");
      unregisterResource(uri);
    }

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);
  }

  /**
   * @throws Exception
   */
  @Test
  public void testTgextraGetOwner()
      throws Exception {

    System.out.println("TESTING #GETOWNDE");

    String uri = "junit:" + System.currentTimeMillis();

    try {
      // Create registerResource request.
      System.out.println("\t--> RegisterResource()");

      registerResource(uri);

      GetOwnerRequest req = new GetOwnerRequest();
      req.setAuth(rbacSessionId);
      req.setLog(logParameter);
      req.setResource(uri);

      System.out.println("\tsession ID:    " + req.getAuth());
      System.out.println("\tlog parameter: " + req.getLog());
      System.out.println("\tresource:      " + req.getResource());
      System.out.println(TGAuthClientCommon.TABLINE);

      GetOwnerResponse response = tgextra.getOwner(req);

      System.out.println("\towner of " + uri + ": " + response.getOwner());

    } finally {
      // Create unregisterResource request.
      System.out.println("\t--> UnregisterResource()");

      unregisterResource(uri);
    }

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);
  }

  /**
   * @throws AuthenticationFault
   */
  @Test
  public void testGetMembers() throws AuthenticationFault {

    System.out.println("TESTING #GETMEMBERS");

    GetMembersRequest req = new GetMembersRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setProject(projectId);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\tproject:       " + req.getProject());
    System.out.println(TGAuthClientCommon.TABLINE);

    UsersetResponse response = tgextra.getMembers(req);

    System.out.println("\tusernames for " + projectId + ": ");
    for (String user : response.getUsername()) {
      System.out.println("\t\t" + user);
    }

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);
  }

  /**
   * <p>
   * Gets all roles a user has on the project.
   * </p>
   * 
   * @throws info.textgrid.namespaces.middleware.tgauth.AuthenticationFault
   * @throws UnknownProjectFault
   */
  @Test
  public void testTgextraGetUserRole()
      throws info.textgrid.namespaces.middleware.tgauth.AuthenticationFault, UnknownProjectFault {

    System.out.println("TESTING #GETUSERROLE username (ePPN) only");

    getUserRole();
  }

  /**
   * 
   */
  @Test
  public void testTgextraGetFriends() {

    System.out.println("TESTING #GETFRIENDS");

    GetFriendsRequest req = new GetFriendsRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println(TGAuthClientCommon.TABLINE);

    GetFriendsResponse res = tgextra.getFriends(req);

    List<Friend> friends = res.getFriends();
    assertTrue(friends.size() > 0);

    for (Friend f : friends) {
      System.out.println("\tfriend:        " + f.getUsername() + " (" + f.getScore() + ")");
    }

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);
  }

  /**
   * <p>
   * Gets all rights a user has on the project.
   * </p>
   * 
   * @throws info.textgrid.namespaces.middleware.tgauth.AuthenticationFault
   */
  @Test
  public void testTgextraGetRightsWithProjectID()
      throws info.textgrid.namespaces.middleware.tgauth.AuthenticationFault {

    System.out.println("TESTING #GETRIGHTS");

    getRights(projectId);

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);
  }

  /**
   * 
   */
  @Test
  public void testTgextraIsPublic() {

    System.out.println("TESTING #ISPUBLIC");

    // Test using project ID here (must be FALSE).
    assertFalse(isPublic(projectId));
  }

  /**
   * 
   */
  @Test
  public void testTgextraGetNumberOfResources() {

    System.out.println("TESTING #GETNUMBEROFRESOURCES");

    GetNumberOfResourcesRequest req = new GetNumberOfResourcesRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setProject(projectId);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\tproject:       " + req.getProject());
    System.out.println(TGAuthClientCommon.TABLINE);

    GetNumberOfResourcesResponse res = tgextra.getNumberOfResources(req);

    System.out.println("\tall resources:    " + res.getAllresources());
    System.out.println("\tpublic resources: " + res.getPublicresources());

    // New user has got no resources here at all (or just one, if publish() has been tested
    // already).
    assertTrue(res.getAllresources() >= 0 && res.getPublicresources() >= 0);

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);
  }

  /**
   * 
   */
  @Test
  public void testTgextraGetProjectDescription() {

    System.out.println("TESTING #GETPROJECTDESCRIPTION");

    //
    // Retrieve project information.
    //
    System.out.println("\tgetting project info for ID " + projectId);

    GetProjectDescriptionRequest req = new GetProjectDescriptionRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setProject(projectId);

    GetProjectDescriptionResponse res = tgextra.getProjectDescription(req);

    System.out.println("\tproject name:        " + res.getProject().getName());
    System.out.println("\tproject ID:          " + res.getProject().getId());
    System.out.println("\tproject Description: " + res.getProject().getDescription());
    System.out.println("\tproject File:        " + res.getProject().getFile());
  }

  /**
   * 
   */
  @Test
  public void testTgextraGetSid() {

    System.out.println("TESTING #GETSID");

    GetSidRequest req = new GetSidRequest();
    GetSidResponse res = tgextra.getSid(req);

    String result = res.getSid();

    assertFalse(result == null || result.equals(""));

    System.out.println("\tSID=" + result);
  }

  /**
   * 
   */
  @Test
  public void testTgextraGetSupportedUserAttributes() {

    System.out.println("TESTING #GETSUPPORTEDUSERATTRIBUTES");

    GetSupportedUserAttributesRequest req = new GetSupportedUserAttributesRequest();

    System.out.println(TGAuthClientCommon.TABLINE);

    GetSupportedUserAttributesResponse res = tgextra.getSupportedUserAttributes(req);

    List<UserAttribute> attributeList = res.getAttribute();

    for (UserAttribute a : attributeList) {
      System.out.println(
          "\tattribute: " + a.getDisplayname() + " (" + a.getName() + " / " + a.getValue() + ")");
      System.out.println("\t\tdescription: " + a.getDescription());
    }

    if (attributeList.size() != 16) {
      System.err.println(ERROR + attributeList.size() + " != 16");
      assertTrue(false);
    }

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);
  }

  /**
   * @throws AuthenticationFault
   */
  @Test
  public void testTgextraGetMyUserAttributes() throws AuthenticationFault {

    System.out.println("TESTING #GETMYUSERATTRIBUTES");

    GetMyUserAttributesRequest req = new GetMyUserAttributesRequest();
    req.setAuth(rbacSessionId);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println(TGAuthClientCommon.TABLINE);

    GetMyUserAttributesResponse response = tgextra.getMyUserAttributes(req);

    List<UserAttribute> attributeList = response.getAttribute();

    String email = "";
    for (UserAttribute a : attributeList) {
      System.out.println(
          "\tattribute: " + a.getDisplayname() + " (" + a.getName() + " / " + a.getValue() + ")");
      System.out.println("\t\tdescription: " + a.getDescription());
      if (a.getName().equals("mail")) {
        email = a.getValue();
      }
    }

    if (attributeList.size() != 16 || !email.equals(getMyUserAtrributesEmail)) {
      System.err.println(ERROR + email + ": " + attributeList.size());
      assertTrue(false);
    }

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);
  }

  /**
   * <p>
   * Test the permission the delete a project.
   * </p>
   *
   * @throws info.textgrid.namespaces.middleware.tgauth.UnknownResourceFault
   * @throws UnknownProjectFault
   * @throws info.textgrid.namespaces.middleware.tgauth.AuthenticationFault
   */
  @Test
  public void testTgextraTgDeleteProjectPermission()
      throws info.textgrid.namespaces.middleware.tgauth.UnknownResourceFault,
      info.textgrid.namespaces.middleware.tgauth.AuthenticationFault, UnknownProjectFault {

    System.out.println("TESTING delete project permission");

    // Get roles first for project.
    GetUserRoleRequest req = new GetUserRoleRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setProject(projectId);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\tproject:       " + req.getProject());
    System.out.println(TGAuthClientCommon.TABLINE);

    GetUserRoleResponse response = tgextra.getUserRole(req);

    // Check if the user has the role Project Manager in the role project.
    boolean userHasRoleProjektleiter = false;
    for (UserRole role : response.getUserRole()) {
      if (role.getUsername().equals(username)) {
        for (String r : role.getRoles()) {
          if (r.equals(TGAuthClientCommon.ROLE_MANAGER)) {
            userHasRoleProjektleiter = true;
            System.out.println("\trole '" + TGAuthClientCommon.ROLE_MANAGER
                + "' found! deletion of project allowed!");
          }
        }
      }
    }

    if (!userHasRoleProjektleiter) {
      System.err.println(ERROR + "user is not allowed to delete project");
      assertTrue(false);
    }

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);
  }

  /**
   * @throws NotEmptyFault
   * @throws AuthenticationFault
   */
  @Test
  public void testTgextraCreateProject() throws AuthenticationFault, NotEmptyFault {

    System.out.println("TESTING project creation, getting description, and deletion");

    // Creating project.

    String projectName = "Huhu fugu!";
    String projectDescription = "fugu's JUNIT test project";

    CreateProjectResponse createProjectResponse =
        createProject(rbacSessionId, logParameter, projectName, projectDescription);
    String localProjectID = createProjectResponse.getProjectId();

    System.out.println("\tCREATE PROJECT");
    System.out.println("\t\tname:        " + projectName);
    System.out.println("\t\tdescription: " + projectDescription);

    // Getting project information.
    GetProjectDescriptionRequest getProjectDescriptionRequest = new GetProjectDescriptionRequest();
    getProjectDescriptionRequest.setAuth(rbacSessionId);
    getProjectDescriptionRequest.setLog(logParameter);
    getProjectDescriptionRequest.setProject(localProjectID);

    GetProjectDescriptionResponse getProjectDescriptionResponse =
        tgextra.getProjectDescription(getProjectDescriptionRequest);

    String projectNameFromTgauth = getProjectDescriptionResponse.getProject().getName();
    String projectIDFromTgauth = getProjectDescriptionResponse.getProject().getId();
    String projectDescriptionFromTgauth =
        getProjectDescriptionResponse.getProject().getDescription();
    String projectFileFromTgauth = getProjectDescriptionResponse.getProject().getFile();

    System.out.println("\tGET PROJECT DESCRIPTION");
    System.out.println("\t\tproject name:        " + projectNameFromTgauth);
    System.out.println("\t\tproject ID:          " + projectIDFromTgauth);
    System.out.println("\t\tproject description: " + projectDescriptionFromTgauth);
    System.out.println("\t\tproject file:        " + projectFileFromTgauth);

    // Checking...
    boolean failure = false;
    if (!projectDescriptionFromTgauth.equals(projectDescription)) {
      failure = true;
      System.out.println(projectDescriptionFromTgauth + " != " + projectDescription);
    }
    if (!projectNameFromTgauth.equals(projectName)) {
      failure = true;
      System.out.println(projectNameFromTgauth + " != " + projectName);
    }

    // Deleting project.
    System.out.println("\tDELETE PROJECT");

    deleteProject(projectName, localProjectID);

    // Fail if failure.
    if (failure) {
      assertTrue(false);
    }
  }

  /**
   * <p>
   * This bug is not yet fixed! Therefore we ignore the test!
   * </p>
   * 
   * @throws NotEmptyFault
   * @throws AuthenticationFault
   * @throws UnsupportedEncodingException
   */
  @Test
  @Ignore
  public void testTgextraCreateProjectSpecialChars()
      throws AuthenticationFault, NotEmptyFault, UnsupportedEncodingException {

    System.out.println(
        "TESTING project creation, getting description, and deletion with special chars in project name and description");

    // Creating project.
    String projectName = "Huhu „fugu“!";
    String projectDescription = "fugu's JUNIT „test“ project";

    CreateProjectResponse createProjectResponse =
        createProject(rbacSessionId, logParameter, projectName, projectDescription);
    String localProjectID = createProjectResponse.getProjectId();

    System.out.println("\tCREATE PROJECT");
    System.out.println("\t\tproject name:        " + projectName);
    System.out.println("\t\tproject description: " + projectDescription);

    // Getting project information.
    GetProjectDescriptionRequest getProjectDescriptionRequest = new GetProjectDescriptionRequest();
    getProjectDescriptionRequest.setAuth(rbacSessionId);
    getProjectDescriptionRequest.setLog(logParameter);
    getProjectDescriptionRequest.setProject(localProjectID);

    GetProjectDescriptionResponse getProjectDescriptionResponse =
        tgextra.getProjectDescription(getProjectDescriptionRequest);

    String projectNameFromTgauth = getProjectDescriptionResponse.getProject().getName();
    String projectIDFromTgauth = getProjectDescriptionResponse.getProject().getId();
    String projectDescriptionFromTgauth =
        getProjectDescriptionResponse.getProject().getDescription();
    String projectFileFromTgauth = getProjectDescriptionResponse.getProject().getFile();

    System.out.println("\tGET PROJECT DESCRIPTION");
    System.out.println("\t\tproject name:        " + projectNameFromTgauth);
    System.out.println("\t\tproject ID:          " + projectIDFromTgauth);
    System.out.println("\t\tproject description: " + projectDescriptionFromTgauth);
    System.out.println("\t\tproject file:        " + projectFileFromTgauth);

    // Checking...
    boolean failure = false;
    if (!projectDescriptionFromTgauth.equals(projectDescription)) {
      failure = true;
      System.out.println("\t\tERROR!  -->" + projectDescriptionFromTgauth + "<--  !=  -->"
          + projectDescription + "<--");
    }
    if (!projectNameFromTgauth.equals(projectName)) {
      failure = true;
      System.out.println(
          "\t\tERROR!  -->" + projectNameFromTgauth + "<--  !=  -->" + projectName + "<--");
    }

    // Deleting project.
    System.out.println("\tDELETE PROJECT '" + projectName + "' with ID '" + localProjectID + "'");

    deleteProject(projectName, localProjectID);

    // Fail if failure.
    if (failure) {
      assertTrue(false);
    }
  }

  // **
  // TGEXTRA CRUD TESTS
  // **

  /**
   * @throws UnknownResourceFault
   * @throws info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault
   */
  @Test
  public void testTgextraCRUDGetCSR() throws UnknownResourceFault,
      info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault {

    System.out.println("TESTING #GETCSR");

    // Create getEPPN request.
    GetCSRRequest req = new GetCSRRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println(TGAuthClientCommon.TABLINE);

    GetCSRResponse res = tgextraCrud.getCSR(req);

    assertFalse(res.getCsr().length == 0);

    System.out.println("\tcsr: " + res.getCsr() + " (" + res.getCsr().length + ")");

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);
  }

  /**
   * <p>
   * Test getting the ePPn. No secret needed anymore!
   * </p>
   * 
   * @throws AuthenticationFault
   */
  @Test
  public void testTgextraCRUDGetEppn()
      throws info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault {

    System.out.println("TESTING #TGCRUDGETEPPN");

    // Create getEPPN request.
    GetEPPNRequest req = new GetEPPNRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\tsecret:        " + req.getSecret());
    System.out.println(TGAuthClientCommon.TABLINE);

    try {
      GetEPPNResponse response = tgextraCrud.getEPPN(req);

      System.out.println("\teppn: " + response.getEppn());

      System.out.println(TGAuthClientCommon.TABLINE);
      System.out.println(OK);

    } catch (info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault e) {
      printError(e);
      throw e;
    }
  }

  /**
   * @throws info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault
   */
  @Test
  public void testTgextraCRUDGetUUID()
      throws info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault {

    System.out.println("TESTING #GETUUID");

    // Create getEPPN request.
    GetUUIDRequest req = new GetUUIDRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    // "Resource not found or no access right", correct with project ID, I suppose...
    req.setResource(projectId);
    req.setSecret(crudSecret);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\tresource:      " + req.getResource());
    System.out.println("\tsecret:        " + req.getSecret());
    System.out.println(TGAuthClientCommon.TABLINE);

    GetUUIDResponse res = tgextraCrud.getUUID(req);

    assertFalse(res.getUuid().length() == 0);

    System.out.println("\tuuid: " + res.getUuid());

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);
  }

  /**
   * <p>
   * Testing nearlyPublish with register first and unregister last.
   * </p>
   * 
   * @throws Exception
   */
  @Test
  public void testTgextraCRUDNearlyPublish()
      throws Exception {

    System.out.println("TESTING #NEARLYPUBLISH");

    String uri = "junit:" + System.currentTimeMillis();

    try {
      // Firstly register an object.
      System.out.println("\t--> registerResource()");

      registerResource(uri);

      // Then do a nearlyPublish.
      System.out.println("\t--> nearlyPublish()");

      nearlyPublish(uri);

      // Test for publicness.
      System.out.println("\t--> isPublic()");

      assertTrue(isPublic(uri));

    } finally {
      // Finally do unregister again.
      System.out.println("\t--> unregisterResource()");

      unregisterResource(uri);
    }
  }

  /**
   * <p>
   * Testing nearlyPublish with register first and unregister last.
   * </p>
   * 
   * @throws Exception
   */
  @Test
  public void testTgextraCRUDPublish()
      throws Exception {

    System.out.println("TESTING #PUBLISH");

    String uri = "junit:" + System.currentTimeMillis();

    try {
      // Firstly register an object.
      System.out.println("\t--> registerResource()");

      registerResource(uri);

      // Then do a publish.
      System.out.println("\t--> publish()");

      publish(uri);

      // Test for publicness.
      System.out.println("\t--> isPublic()");

      assertTrue(isPublic(uri));

    } finally {
      // Finally do unregister again.
      System.out.println("\t--> unregisterResource()");

      try {
        unregisterResource(uri);
      } catch (WebServiceException e) {

        System.out.println(
            "\texpected " + e.getClass().getName() + ": public object can not be unregistered!");

        assertTrue(e.getMessage().endsWith("SoapFault: 2"));
      }
    }
  }

  /**
   * <p>
   * Testing nearlyPublish with register first publish then, and unregister last.
   * </p>
   * 
   * @throws Exception
   */
  @Test
  public void testTgextraCRUDNearlyPublishThenPublish()
      throws Exception {

    System.out.println("TESTING #NEARLYPUBLISHTHENPUBLISH");

    String uri = "junit:" + System.currentTimeMillis();

    try {
      // Firstly register an object.
      System.out.println("\t--> 1. registerResource()");

      registerResource(uri);

      // Then do a nearlyPublish.
      System.out.println("\t--> 2. nearlyPublish()");

      nearlyPublish(uri);

      // Test for publicness.
      System.out.println("\t--> 3. isPublic()");

      assertTrue(isPublic(uri));

      // Get rights.
      System.out.println("\t--> 4. getRights()");

      getRights(uri);

      // Then do a publish.
      System.out.println("\t--> 5. publish()");

      publish(uri);

      // Test for publicness.
      System.out.println("\t--> 6. isPublic()");

      assertTrue(isPublic(uri));

      try {
        // Get rights.
        System.out.println("\t--> 7. getRights()");

        getRights(uri);

      } catch (AuthenticationFault e) {
        printError(e);
        throw e;
      }

    } finally {
      try {
        // Finally do unregister again.
        System.out.println("\t--> FIN. unregisterResource()");

        unregisterResource(uri);

      } catch (WebServiceException e) {
        printError(e);

        System.out.println(
            "\texpected " + e.getClass().getName() + ": public object can not be unregistered!");

        assertTrue(e.getMessage().endsWith("SoapFault: 2"));
      }
    }
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testTgextraCRUDPutCRT() {
    // No need for putting certificates anymore? We do not use grid anymore...
  }

  /**
   * @throws AuthenticationFault
   * @throws Exception
   */
  @Test
  public void testTgextraCrudRegisterUnregisterResource()
      throws Exception {

    System.out.println("TESTING #TGCRUDREGISTERUNREGISTERRESOURCE");

    String uri = "junit:" + System.currentTimeMillis();

    // Create registerResource request.
    System.out.println("\t--> RegisterResource()");

    registerResource(uri);

    // Check rights on resource.
    System.out.println("\t--> getRights");

    getRights(uri);

    // Create unregisterResource request.
    System.out.println("\t--> UnregisterResource()");

    unregisterResource(uri);
  }

  /**
   * <p>
   * Test the tgCrudCheckAccess with invalid resource.
   * </p>
   * 
   * @throws UnknownResourceFault
   * @throws info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault
   */
  @Test
  public void testTgextraCRUDTgCrudCheckAccessUnknownResource() throws UnknownResourceFault,
      info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault {

    System.out.println("TESTING #TGCRUDCHECKACCESS with unknown project resource (project#CREATE)");

    // Create checkAccess request.
    TgCrudCheckAccessRequest req = new TgCrudCheckAccessRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setOperation(TGAuthClientCommon.OPERATION_CREATE);
    req.setResource(projectIdInvalid);
    req.setSecret(crudSecret);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\toperation:     " + req.getOperation());
    System.out.println("\tresource:      " + req.getResource());
    System.out.println("\tsecret:        " + req.getSecret());
    System.out.println(TGAuthClientCommon.TABLINE);

    try {
      tgextraCrud.tgCrudCheckAccess(req);

    } catch (WebServiceException e) {
      printError(e);

      System.out.println("\texpected " + e.getClass().getName() + ": resource indeed is unknown!");

      assertTrue(e.getMessage().endsWith("The given resource is unknown to the system."));
    }
  }

  /**
   * <p>
   * Test the tgCrudCheckAccess with invalid TG-crud secret.
   * </p>
   * 
   * @throws AuthenticationFault
   * @throws UnknownResourceFault
   */
  @Test
  public void testTgextraCRUDTgCrudCheckAccessNoSecret()
      throws info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault,
      UnknownResourceFault {

    System.out.println("TESTING #TGCRUDCHECKACCESS with invalid tgcrud secret (project#CREATE)");

    // Create checkAccess request.
    TgCrudCheckAccessRequest req = new TgCrudCheckAccessRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setOperation(TGAuthClientCommon.OPERATION_CREATE);
    req.setResource(projectId);
    req.setSecret(crudSecretInvalid);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\toperation:     " + req.getOperation());
    System.out.println("\tresource:      " + req.getResource());
    System.out.println("\tsecret:        " + req.getSecret());
    System.out.println(TGAuthClientCommon.TABLINE);

    try {
      tgextraCrud.tgCrudCheckAccess(req);

    } catch (WebServiceException e) {
      printError(e);

      System.out
          .println("\texpected " + e.getClass().getName() + ": we indeed are not cruddy enough!");

      assertTrue(
          e.getMessage().endsWith("You are not cruddy enough - Go away and come back as CRUD!"));
    }
  }

  /**
   * <p>
   * Test the tgCheckAccess method with access granted on project#CREATE.
   * </p>
   * 
   * @throws UnknownResourceFault
   * @throws info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault
   */
  @Test
  public void testTgextraCRUDTgCrudCheckAccessProjectCreate() throws UnknownResourceFault,
      info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault {

    System.out.println("TESTING #TGCRUDCHECKACCESS with access granted (project#CREATE)");

    TgCrudCheckAccessRequest req = new TgCrudCheckAccessRequest();
    req.setAuth(rbacSessionId);
    req.setSecret(crudSecret);
    req.setLog(logParameter);
    req.setOperation(TGAuthClientCommon.OPERATION_CREATE);
    req.setResource(projectId);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tcrud secret:   " + req.getSecret());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\toperation:     " + req.getOperation());
    System.out.println("\tresource:      " + req.getResource());
    System.out.println(TGAuthClientCommon.TABLINE);

    try {
      TgCrudCheckAccessResponse response = tgextraCrud.tgCrudCheckAccess(req);
      System.out.println("\tresult: " + response.isResult());

      System.out.println(TGAuthClientCommon.TABLINE);
      System.out.println(OK);

    } catch (UnknownResourceFault
        | info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault e) {
      printError(e);
      throw e;
    }
  }

  /**
   * <p>
   * Test the tgCrudCheckAccess.
   * </p>
   * 
   * @throws UnknownResourceFault
   * @throws info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault
   */
  @Test
  public void testTgextraCRUDTgCrudCheckAccessGranted() throws UnknownResourceFault,
      info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault {

    System.out.println("TESTING #TGCRUDCHECKACCESS with access granted");

    // Create checkAccess request.
    TgCrudCheckAccessRequest req = new TgCrudCheckAccessRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setOperation(TGAuthClientCommon.OPERATION_CREATE);
    req.setResource(projectId);
    req.setSecret(crudSecret);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\toperation:     " + req.getOperation());
    System.out.println("\tresource:      " + req.getResource());
    System.out.println("\tsecret:        " + req.getSecret());
    System.out.println(TGAuthClientCommon.TABLINE);

    try {
      TgCrudCheckAccessResponse response = tgextraCrud.tgCrudCheckAccess(req);

      System.out.println("\tusername:  " + response.getUsername());
      System.out.println("\toperation: " + response.getOperation());
      System.out.println("\tproject:   " + response.getProject().getId() + " \""
          + response.getProject().getName() + "\"");
      System.out.println("\tresult:    " + response.isResult());
      System.out.println("\tisPublic:  " + response.isPublic());

      System.out.println(TGAuthClientCommon.TABLINE);
      System.out.println(OK);

    } catch (UnknownResourceFault
        | info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault e) {
      printError(e);
      throw e;
    }
  }

  /**
   * <p>
   * Test the tgCrudCheckAccess with invalid SID.
   * </p>
   * 
   * @throws UnknownResourceFault
   * @throws info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault
   */
  @Test
  public void testTgextraCRUDTgCrudCheckAccessDenied() throws UnknownResourceFault,
      info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault {

    System.out.println("TESTING #TGCRUDCHECKACCESS with invalid session ID");

    // Create checkAccess request.
    TgCrudCheckAccessRequest req = new TgCrudCheckAccessRequest();
    req.setAuth(rbacSessionIdInvalid);
    req.setLog(logParameter);
    req.setOperation(TGAuthClientCommon.OPERATION_CREATE);
    req.setResource(projectId);
    req.setSecret(crudSecret);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\toperation:     " + req.getOperation());
    System.out.println("\tresource:      " + req.getResource());
    System.out.println("\tsecret:        " + req.getSecret());
    System.out.println(TGAuthClientCommon.TABLINE);

    try {
      TgCrudCheckAccessResponse response = tgextraCrud.tgCrudCheckAccess(req);

      System.out.println("\tresult: " + response.isResult());

      if (response.isResult()) {
        assertTrue(false);
      }

      System.out.println(TGAuthClientCommon.TABLINE);
      System.out.println(OK);

    } catch (UnknownResourceFault
        | info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault e) {
      printError(e);
      throw e;
    }
  }

  /**
   * @throws AuthenticationFault
   */
  @Test(expected = AuthenticationFault.class)
  public void tgextraAuthenticate() throws AuthenticationFault {

    // Test implemented only for completeness, no SIDs are returned here (internal method due to
    // documentation! See
    // https://textgridlab.org/doc/services/submodules/tg-auth/docs/api/tg-authApi.html#authenticate.

    System.out.println("TESTING #AUTHENTICATE");

    AuthenticateRequest req = new AuthenticateRequest();
    req.setUsername("user");
    req.setPassword("pass");
    req.setLog(logParameter);

    System.out.println("\tuser:          " + req.getUsername());
    System.out.println("\tpass:          ***");
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println(TGAuthClientCommon.TABLINE);

    AuthenticateResponse res = tgextra.authenticate(req);

    System.out.println("\tauthenticated: " + res.getAuth());

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);
  }

  /**
   * @throws info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault
   * @throws Exception
   */
  @Test
  public void testSetProjectFile()
      throws Exception {

    System.out.println("TESTING #SETPROJECTFILE");

    String projectFileURI = "junit:projectfile-" + System.currentTimeMillis();

    try {
      // Register resource first.
      System.out.println("\t--> registerResource()");

      registerResource(projectFileURI);

      // Set project file then.
      System.out.println("\t--> setProjectFile()");

      assertTrue(setProjectFile(projectFileURI));

    } finally {
      // Unregister finally.
      System.out.println("\t--> unregisterResource()");

      unregisterResource(projectFileURI);
    }
  }

  /**
   * 
   */
  @Test
  public void testSetName() {

    System.out.println("TESTING #SETNAME");

    Boolean agreeSearch = false;

    SetNameRequest req = new SetNameRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setAgreeSearch(agreeSearch);
    req.setMail("mail@schubidu.net");
    req.setName("Nubma B. Kross");
    req.setOrganisation("Monster Inc.");
    // req.setWebAuthSecret("none");

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\tagreesearch:   " + agreeSearch);
    System.out.println("\tmail:          " + req.getMail());
    System.out.println("\torganization:  " + req.getOrganisation());
    System.out.println("\torganization:  " + req.getWebAuthSecret());
    System.out.println(TGAuthClientCommon.TABLINE);

    BooleanResponse res = tgextra.setName(req);

    System.out.println("\tboolean:       " + res.isResult());

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testDeleteMember() {

    System.out.println("TESTING #DELETEMEMBER");

  }

  //
  // ONLINE-TESTS: Please comment in if needed!
  //

  /**
   * <p>
   * ONLINE TEST: Test getting all the objects from large project.
   * </p>
   */
  @Test
  @Ignore
  public void testTgextraGetObjectsDigibib() {

    // Set project for specific TG instance.
    String digibib = "TGPR-372fe6dc-57f2-6cd4-01b5-2c4bbefcfd3c";
    String diginame = "Digitale Bibliothek";
    // String digibib = "TGPR-53fd0966-4bae-45e4-519f-57fcf48e3249";
    // String diginame = "fus tolles projekt";

    System.out.println("\tGetting all objects from project '" + diginame + "': " + digibib);

    GetObjectsRequest req = new GetObjectsRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setProject(digibib);

    ResourcesetResponse res = tgextra.getObjects(req);
    int size = res.getResource().size();

    System.out.println("Project has got " + size + " entr" + (size != 1 ? "ies" : "y"));
    System.out.println("\tOK");
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Adds a project for testing.
   * </p>
   * 
   * @throws RbacFault
   */
  private static void addProject() throws RbacFault {

    System.out.println("INITIALISING TESTS: CREATING PROJECT");
    System.out.println("\tgetting all projects");

    GetAllProjectsRequest allProjetsRequest = new GetAllProjectsRequest();
    allProjetsRequest.setAuth(rbacSessionId);
    allProjetsRequest.setLog(logParameter);

    long start = System.currentTimeMillis();
    List<ProjectInfo> allProjects = tgextra.getAllProjects(allProjetsRequest).getProject();
    long duration = System.currentTimeMillis() - start;

    System.out.println("\t" + allProjects.size() + " projects existing [" + duration + "ms]");

    System.out.println("\tchecking for junit test project '" + junitProjectName + "'");

    long overallStart = System.currentTimeMillis();

    boolean projectExisting = false;
    for (ProjectInfo pi : allProjects) {
      if (pi.getName().equals(junitProjectName)) {
        projectExisting = true;
        projectId = pi.getId();
      }
    }

    if (projectExisting) {
      System.out.println("\t'" + junitProjectName + "' already exists: " + projectId);
    } else {
      try {

        start = System.currentTimeMillis();

        CreateProjectResponse project1Response =
            createProject(rbacSessionId, logParameter, junitProjectName, junitProjectName);

        duration = System.currentTimeMillis() - start;

        projectId = project1Response.getProjectId();

        System.out.println(
            "\tproject " + junitProjectName + " created: " + projectId + " [" + duration + "ms]");

        addDefaultOwnerRoles();

      } catch (SOAPFaultException e) {
        System.err.println(ERROR + e.getClass().getName() + " --> " + e.getMessage() + " ["
            + (System.currentTimeMillis() - overallStart) + "ms]");
        throw e;
      } catch (RuntimeException e) {
        System.err.println(ERROR + e.getClass().getName() + " --> " + e.getMessage() + " ["
            + (System.currentTimeMillis() - overallStart) + "ms]");
        throw e;
      }
    }
  }

  /**
   * @param theSID The TextGrid RBAC session ID.
   * @param theLOG The log parameter.
   * @param theName The project name.
   * @param theDescription the Project description.
   * @return A create project response.
   */
  private static CreateProjectResponse createProject(final String theSID, final String theLOG,
      final String theName, final String theDescription) {

    CreateProjectRequest createProjectRequest = new CreateProjectRequest();
    createProjectRequest.setAuth(theSID);
    createProjectRequest.setLog(theLOG);
    createProjectRequest.setName(theName);
    createProjectRequest.setDescription(theDescription);

    return tgextra.createProject(createProjectRequest);
  }

  /**
   * <p>
   * Delete a project.
   * </p>
   * 
   * @param theName The project name.
   * @throws info.textgrid.namespaces.middleware.tgauth.AuthenticationFault
   * @throws NotEmptyFault
   */
  private static void deleteProject(final String theName, final String theProjectID)
      throws info.textgrid.namespaces.middleware.tgauth.AuthenticationFault, NotEmptyFault {

    DeleteProjectRequest req = new DeleteProjectRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setProject(theProjectID);

    try {
      BooleanResponse res = tgextra.deleteProject(req);

      if (!res.isResult()) {
        System.err.println(ERROR + "project could not be deleted");
      } else {
        System.out.println(OK);
      }
    } catch (WebServiceException e) {
      printError(e);

      System.out.println("\texpected " + e.getClass().getName() + ": project is not empty!");

      assertTrue(e.getMessage().endsWith("SoapFault: 7"));
    }
  }

  /**
   * <p>
   * Initialises a TG-extra stub with given timeout.
   * </p>
   * 
   * @param theEndpoint The TG-auth endpoint.
   * @param theTimeout The timeout.
   */
  private static void initTgextraService(URL theEndpoint, int theTimeout) {

    if (tgextra == null) {
      if (theTimeout > 0) {
        tgextra = TGAuthClientUtilities.getTgextra(theEndpoint, theTimeout);

        System.out.println("TGEXTRA stub created with timeout " + theTimeout + "ms");
      } else {
        tgextra = TGAuthClientUtilities.getTgextra(theEndpoint);

        System.out.println("TGEXTRA stub created");
      }
    } else {
      System.out.println("TGEXTRA stub existing");
    }
  }

  /**
   * <p>
   * Initialises the TG-extra CRUD stub.
   * </p>
   * 
   * @param theEndpoint The TG-auth crud endpoint.
   */
  private static void initTgextraCrudService(URL theEndpoint) {

    if (tgextraCrud == null) {
      tgextraCrud = TGAuthClientCrudUtilities.getTgextraCrud(theEndpoint);

      System.out.println("TGEXTRA_CRUD stub created");
    } else {
      System.out.println("TGEXTRA_CRUD stub existing");
    }
  }

  /**
   * @param e The exception to print out.
   */
  private static void printError(Exception e) {
    System.err.println(ERROR + e.getClass().getName() + " --> message: " + e.getMessage());
  }

  /**
   * @param theURI
   * @throws info.textgrid.namespaces.middleware.tgauth_crud.RbacFault
   * @throws info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault
   */
  private static void registerResource(String theURI)
      throws info.textgrid.namespaces.middleware.tgauth_crud.RbacFault,
      info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault {

    RegisterResourceRequest req = new RegisterResourceRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setProject(projectId);
    req.setSecret(crudSecret);
    req.setUri(theURI);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\tproject ID:    " + req.getProject());
    System.out.println("\tsecret:        " + req.getSecret());
    System.out.println("\turi:           " + req.getUri());
    System.out.println(TGAuthClientCommon.TABLINE);

    info.textgrid.namespaces.middleware.tgauth_crud.OperationsetResponse response =
        tgextraCrud.registerResource(req);

    System.out.println("\toperation: " + response.getOperation());

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);
  }

  /**
   * @param theURI
   * @throws UnknownResourceFault
   * @throws info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault
   */
  private static void unregisterResource(String theURI)
      throws info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault,
      UnknownResourceFault {

    UnregisterResourceRequest req = new UnregisterResourceRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setSecret(crudSecret);
    req.setUri(theURI);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\tsecret:        " + req.getSecret());
    System.out.println("\turi:           " + req.getUri());
    System.out.println(TGAuthClientCommon.TABLINE);

    info.textgrid.namespaces.middleware.tgauth_crud.BooleanResponse response =
        tgextraCrud.unregisterResource(req);

    System.out.println("\tboolean: " + response.isResult());

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);
  }

  /**
   * <p>
   * Add role Editor and Administrator to project.
   * </p>
   * 
   * @throws RbacFault
   */
  private static void addDefaultOwnerRoles() throws RbacFault {

    // <https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-python-clients/-/blob/main/src/tgclients/auth.py?ref_type=heads#L122>
    // if default_owner_roles:
    // eppn = self.get_eppn_for_sid(sid)
    // self.add_editor_to_project(sid, project_id, eppn)
    // self.add_admin_to_project(sid, project_id, eppn)

    // username actually IS the ePPN here, see config file.

    AddMemberRequest addEditorMemberReq = new AddMemberRequest();
    addEditorMemberReq.setAuth(rbacSessionId);
    addEditorMemberReq.setLog(logParameter);
    addEditorMemberReq
        .setRole(TGAuthClientCommon.ROLE_EDITOR + "," + projectId + ",Projekt-Teilnehmer");
    addEditorMemberReq.setUsername(username);

    BooleanResponse addEditionMemberRes = tgextra.addMember(addEditorMemberReq);
    assertTrue(addEditionMemberRes.isResult());

    AddMemberRequest addAdminMemberReq = new AddMemberRequest();
    addAdminMemberReq.setAuth(rbacSessionId);
    addAdminMemberReq.setLog(logParameter);
    addAdminMemberReq
        .setRole(TGAuthClientCommon.ROLE_ADMIN + "," + projectId + ",Projekt-Teilnehmer");
    addAdminMemberReq.setUsername(username);

    BooleanResponse addAdminMemberRes = tgextra.addMember(addAdminMemberReq);
    assertTrue(addAdminMemberRes.isResult());
  }

  /**
   * @throws AuthenticationFault
   */
  private static void deactivateProject() throws AuthenticationFault {

    System.out.println("\t--> deactivating project");

    DeactivateProjectRequest req = new DeactivateProjectRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setProject(projectId);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\tproject ID:    " + req.getProject());
    System.out.println(TGAuthClientCommon.TABLINE);

    BooleanResponse res = tgextra.deactivateProject(req);

    assertTrue(res.isResult());

    System.out.println(OK);
  }

  /**
   * 
   */
  private static void getDeactivatedProjects() {

    System.out.println("\t--> get deactivated projects");

    GetDeactivatedProjectsRequest req = new GetDeactivatedProjectsRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println(TGAuthClientCommon.TABLINE);

    GetDeactivatedProjectsResponse res = tgextra.getDeactivatedProjects(req);

    List<ProjectInfo> projects = res.getProject();

    assertTrue(projects.size() > 0);

    System.out.println("\tprojects:");

    for (ProjectInfo pi : projects) {
      System.out.println("\t\tname: " + pi.getName() + ", id: " + pi.getId());
    }

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);
  }

  /**
   * @throws UnknownProjectFault
   * @throws AuthenticationFault
   */
  private static void reactivateProject() throws AuthenticationFault, UnknownProjectFault {

    System.out.println("\t--> reactivating project");

    ReactivateProjectRequest req = new ReactivateProjectRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setProject(projectId);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\tproject ID:    " + req.getProject());
    System.out.println(TGAuthClientCommon.TABLINE);

    tgextra.reactivateProject(req);

    System.out.println(OK);
  }

  /**
   * @throws info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault
   * @throws UnknownResourceFault
   */
  private static void nearlyPublish(String theURI)
      throws info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault,
      UnknownResourceFault {

    NearlyPublishRequest req = new NearlyPublishRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setResource(theURI);
    req.setSecret(crudSecret);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\tresource:      " + req.getResource());
    System.out.println("\tsecret:        " + req.getSecret());
    System.out.println(TGAuthClientCommon.TABLINE);

    info.textgrid.namespaces.middleware.tgauth_crud.BooleanResponse res =
        tgextraCrud.nearlyPublish(req);

    assertTrue(res.isResult());

    System.out.println("\tresult: " + res.isResult());

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);
  }

  /**
   * @param theURI
   * @throws info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault
   */
  private static void publish(String theURI)
      throws info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault {

    if (PROPERTIES_FILE.equals(PRODUCTIVE)) {
      System.out.println("\tNO TESTING OF PUBLISH METHODS ON PRODUCTIVE SYSTEM!");
    } else {

      PublishRequest req = new PublishRequest();
      req.setAuth(rbacSessionId);
      req.setLog(logParameter);
      req.setResource(theURI);
      req.setSecret(crudSecret);

      System.out.println("\tsession ID:    " + req.getAuth());
      System.out.println("\tlog parameter: " + req.getLog());
      System.out.println("\tresource:      " + req.getResource());
      System.out.println("\tsecret:        " + req.getSecret());
      System.out.println(TGAuthClientCommon.TABLINE);

      info.textgrid.namespaces.middleware.tgauth_crud.BooleanResponse res =
          tgextraCrud.publish(req);

      assertTrue(res.isResult());

      System.out.println("\tresult: " + res.isResult());

      System.out.println(TGAuthClientCommon.TABLINE);
      System.out.println(OK);
    }
  }

  /**
   * 
   */
  private static boolean isPublic(String theURI) {

    IsPublicRequest req = new IsPublicRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setResource(theURI);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\tresource:      " + req.getResource());
    System.out.println(TGAuthClientCommon.TABLINE);

    BooleanResponse res = tgextra.isPublic(req);

    System.out.println("\tresult: " + res.isResult());
    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);

    return res.isResult();
  }

  /**
   * @param theResource
   * @throws AuthenticationFault
   */
  private static void getRights(String theResource) throws AuthenticationFault {

    GetRightsRequest req = new GetRightsRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setResource(theResource);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\tresource:      " + req.getResource());
    System.out.println(TGAuthClientCommon.TABLINE);

    OperationsetResponse response = tgextra.getRights(req);

    System.out.println("\trights:        " + response.getOperation());

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);
  }

  /**
   * @param theResource
   * @param theOperation
   * @return
   * @throws AuthenticationFault
   */
  private static boolean grantPermission(String theResource, String theOperation)
      throws AuthenticationFault {

    TgGrantPermissionRequest req = new TgGrantPermissionRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setOperation(theOperation);
    req.setResource(theResource);
    req.setRole(TGAuthClientCommon.ROLE_OBSERVER + "," + projectId + ",Projekt-Teilnehmer");

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\toperation:     " + req.getOperation());
    System.out.println("\tresource:      " + req.getResource());
    System.out.println("\trole:          " + req.getRole());
    System.out.println(TGAuthClientCommon.TABLINE);

    BooleanResponse res = tgextra.tgGrantPermission(req);

    System.out.println("\tgranted:       " + res.isResult());

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);

    return res.isResult();
  }

  /**
   * @param theResource
   * @param theOperation
   * @return
   * @throws AuthenticationFault
   */
  private static boolean revokePermission(String theResource, String theOperation)
      throws AuthenticationFault {

    TgRevokePermissionRequest req = new TgRevokePermissionRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setOperation(theOperation);
    req.setResource(theResource);
    req.setRole(TGAuthClientCommon.ROLE_OBSERVER + "," + projectId + ",Projekt-Teilnehmer");

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\toperation:     " + req.getOperation());
    System.out.println("\tresource:      " + req.getResource());
    System.out.println("\trole:          " + req.getRole());
    System.out.println(TGAuthClientCommon.TABLINE);

    BooleanResponse res = tgextra.tgRevokePermission(req);

    System.out.println("\trevoked:       " + res.isResult());

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);

    return res.isResult();
  }

  /**
   * @return
   * @throws AuthenticationFault
   */
  private static boolean addActiveRole(String theRole) throws AuthenticationFault {

    TgAddActiveRoleRequest req = new TgAddActiveRoleRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setRole(theRole + "," + projectId + ",Projekt-Teilnehmer");

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\trole:          " + req.getRole());
    System.out.println(TGAuthClientCommon.TABLINE);

    BooleanResponse res = tgextra.tgAddActiveRole(req);

    System.out.println("\tadded:         " + res.isResult());

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);

    return res.isResult();
  }

  /**
   * @return
   * @throws AuthenticationFault
   */
  private static boolean dropActiveRole(String theRole) throws AuthenticationFault {

    TgDropActiveRoleRequest req = new TgDropActiveRoleRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setRole(theRole + "," + projectId + ",Projekt-Teilnehmer");

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\trole:          " + req.getRole());
    System.out.println(TGAuthClientCommon.TABLINE);

    BooleanResponse res = tgextra.tgDropActiveRole(req);

    System.out.println("\tdropped:       " + res.isResult());

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);

    return res.isResult();
  }

  /**
   * @throws UnknownProjectFault
   * @throws AuthenticationFault
   */
  private static void getUserRole() throws AuthenticationFault, UnknownProjectFault {

    GetUserRoleRequest req = new GetUserRoleRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setProject(projectId);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\tproject:       " + req.getProject());
    System.out.println(TGAuthClientCommon.TABLINE);

    GetUserRoleResponse response = tgextra.getUserRole(req);

    for (UserRole role : response.getUserRole()) {
      System.out.print("\troles for " + role.getUsername() + ": ");
      for (String r : role.getRoles()) {
        System.out.print(r + " ");
      }
      System.out.println();
    }

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);
  }

  /**
   * @param theFile
   * @throws AuthenticationFault
   */
  private static boolean setProjectFile(String theFile) throws AuthenticationFault {

    SetProjectFileRequest req = new SetProjectFileRequest();
    req.setAuth(rbacSessionId);
    req.setLog(logParameter);
    req.setProject(projectId);
    req.setFile(theFile);

    System.out.println("\tsession ID:    " + req.getAuth());
    System.out.println("\tlog parameter: " + req.getLog());
    System.out.println("\tproject:       " + req.getProject());
    System.out.println("\tproject file:  " + req.getFile());
    System.out.println(TGAuthClientCommon.TABLINE);

    BooleanResponse res = tgextra.setProjectFile(req);

    System.out.println("\tboolean:       " + res.isResult());

    System.out.println(TGAuthClientCommon.TABLINE);
    System.out.println(OK);

    return res.isResult();
  }

}
