<?php
// ####################################################################
// Version: 0.2.1
// Autor: Markus Widmer
// Erstellungsdatum: 31.10.2007
// Letzte Aenderung: 21.06.2008



class RBAC {

  // ## Klassenvariablen ##############################################
  private $conf;
  private $objSystem = false;
  private $arrExtension = Array();
  private $arrEvent = Array();
  private $arrCallParameter = Array();




  // ## Konstruktor ###################################################
  public function __construct( $inConfigurationFile, $inBase = "./", $inLib = "../lib/" ) {

    $evalString = "";         // Temporary evaluation-string
    $rbacClassName = false;   // The name of the RBAC-System-class from the configuration file
    $arrExtension = Array();  // The configured extensions
    $file;                    // A file-handle
    $content = "";            // The content of the configuration file
    $xmlConf;                 // The XML-Parser
    $tmpExtObject = false;    // Temporary new extension object
    $i = 0;                   // Loop



    // If this RBAC-Framework is part of another framework
    // this may allready have loaded these interfaces and
    // classes. So we shouldn't do this again!
    if( !interface_exists( "iNode" ) ) {

      // Requiring these interfaces
      require_once( $inLib . "/iNode.interface.php" );
      require_once( $inLib . "/iXML.interface.php" );


      // Requiring these classes
      require_once( $inLib . "/XML.class.php" );
      require_once( $inLib . "/Node.class.php" );

    }


    // Read the system-configuration
    if( file_exists( $inConfigurationFile ) ) {

      $file = fopen( $inConfigurationFile, "r" );
      $content = fread( $file, filesize( $inConfigurationFile ) + 64 );

    }
    else {

      throw new Exception( "File not found: " . $inConfigurationFile );

    }


    // Read the configuration-file
    $xmlConf = new XML();
    $xmlConf->parse( $content, "conf" );
    $this->conf = $xmlConf->getRoot( "conf" );
    $this->conf = $this->conf[0];


    // This is the RBAC configuration file
    $rbacConfFile = $this->conf->getChild( "configuration", 0 )->getAttribute( "file" );
    if(    !preg_match( "/.+/", $rbacConfFile )
        || !file_exists( $rbacConfFile ) ) {

      throw new Exception( "RBAC::__construct() Given configuration-file " . $rbacConfFile
                           . " and/or default file does not exist: " . $rbacConfFile . "\n" );

    }


    // This RBAC-class is to be used.
    $xmlRbac = $this->conf->getChild( "rbac", 0 );
    $rbacClassName = $this->conf->getChild( "rbac", 0 )->getAttribute( "class" );


    // The class-name has to be set so that we can make an
    // instance of it later.
    if(    !$rbacClassName
        || !preg_match( "/.+/", $rbacClassName ) ) {

      throw new Exception( "RBAC::__construct() Missing configuration for RBAC-class\n" );

    }


    // Including all nevessarry classes and interfaces
    for( $i = 0; $i < $xmlRbac->countChilds( "require" ); $i++ ) {

      if( file_exists( $xmlRbac->getChild( "require", $i )->getAttribute( "file" ) ) ) {

        require_once( $xmlRbac->getChild( "require", $i )->getAttribute( "file" ) );

      }

    }


    if( class_exists( $rbacClassName ) ) {

      // Create an instance of the RBAC-System-class
      eval( "\$this->objSystem = new " . $rbacClassName . "( \$rbacConfFile, \$this );" );

    }
    else {

      throw new Exception( "RBAC::__construct() The configured RBAC-System-class " . $rbacClassName . " does not exists.\n" );

    }


    // These are the names of the extensions that are to be
    // used, for example "ssd" or "dsd"
    $arrExtension = $this->conf->searchChild( "extension", "class", "/.+/" );


    for( $i = 0; $i < sizeof( $arrExtension ); $i++ ) {

      if( !class_exists( $arrExtension[$i]->getAttribute( "class" ) ) ) {

        // To load the class, the file in which the class
        // is supposed to be has to exist
        if( file_exists( $arrExtension[$i]->getAttribute( "file" ) ) ) {

            require_once( $arrExtension[$i]->getAttribute( "file" ) );

        }
        else {

          throw new Exception( "Unable to load extension: \"" . $arrExtension[$i]->getAttribute( "file" ), 4 );

        }


        // Create an instance of the extension
        $tmpExtObject = false;
        $evalString  = "\$tmpExtObject = new ";
        $evalString .= $arrExtension[$i]->getAttribute( "class" ) . "( \$this->objSystem, \$this );";
        eval( $evalString );


        // Save the new instance under its name
        $this->arrExtension[get_class( $tmpExtObject )] = $tmpExtObject;
        $this->arrExtension[get_class( $tmpExtObject )]->registerEvents( $this );

      }

    }

  }




  // ## __call ########################################################
  public function __call( $inFunctionName, $inArrParameter ) {

    $evalString = "";
    $foundInExtension = false;
    $tmpClass = false;
    $tmpCall = false;
    $evalString = "";
    $i = 0;


    // First store the parameters becaus they are
    // later automatically attached to the context
    $this->arrCallParameter = $inArrParameter;


    // This will call directly functions that are defined
    // in extensions but not in RBAC. If the function exists
    // in RBAC, nothing will happen!
    foreach( $this->arrExtension as $className => $extension ) {

      if(    method_exists( $extension, $inFunctionName )
          && !method_exists( $this->objSystem, $inFunctionName ) ) {

        $result = call_user_func_array( Array( $this->arrExtension[$className], $inFunctionName ), $inArrParameter );
        $foundInExtension = true;
        break;

      }

    }


    // If no extension has defined the function, try to
    // call it in RBAC directly
    if( !$foundInExtension ) {

      if( method_exists( $this->objSystem, $inFunctionName ) ) {

        $result = call_user_func_array( Array( $this->objSystem, $inFunctionName ), $inArrParameter );


        if( isset( $this->arrEvent[$inFunctionName]['outputfilter'] ) ) {

          // Apply every filter that was registered for
          // this function.
          foreach( $this->arrEvent[$inFunctionName]['outputfilter'] as $index => $eventListener ) {

            $evalString = "\$result = " . $eventListener['class'] . "->" . $eventListener['call'] . "( " . $result . " );";
            eval( $evalString );

          }

        }

      }
      else {

        throw new Exception( "Unable to locate requested method\": " . $inFunctionName . "\"", 3 );

      }

    }


    return $result;

  }




  // ## registerEventListener #########################################
  public function registerEventListener( $inForFunction, $inEvent, $inCallClass, $inCallFunction ) {

    $methodExists = false;
    $className = get_class( $inCallClass );


    // Check if the method that has to be registered for
    // an event really exists
//    if( method_exists( $this->arrExtension[$inCallClass->getClassName()], $inCallFunction ) ) {
    if( method_exists( $this->arrExtension[$className], $inCallFunction ) ) {

      $methodExists = true;

    }


    // Store the class and it's listening function in the array
    // of listeners of the according function
    if( $methodExists ) {

      $this->arrEvent[$inForFunction][$inEvent][] = Array( "class" => get_class( $inCallClass ),
                                                           "call" => $inCallFunction );

    }
  }




  // ## event #########################################################
  public function event( $inForFunction, $inEvent, Context $inContext ) {

    $newContext = $inContext;


    // Append the parameters the function was
    // called with
    is_array( $this->arrCallParameter ) ? $newContext->setParameters( $this->arrCallParameter ) : false;
    $newContext->setType( $inEvent );


    if( isset( $this->arrEvent[$inForFunction][$inEvent] ) ) {

      foreach( $this->arrEvent[$inForFunction][$inEvent] as $index => $eventListener ) {

        $evalString = "\$newContext = \$this->arrExtension['" . $eventListener['class'] . "']->" . $eventListener['call'] . "( \$newContext );";
        eval( $evalString );


        if( !($newContext instanceof Context) ) {

          throw new Exception( "Return value from extension is not a \"Context\"", 6 );

        }

      }


      return $newContext;

    }
    else {

      return $newContext;

    }

  }

}
?>
