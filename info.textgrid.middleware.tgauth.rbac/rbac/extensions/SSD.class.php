<?php
// ####################################################################
// Version: 0.1.1
// Autor: Markus Widmer
// Erstellungsdatum: 31.10.2007
// Letzte Aenderung: 18.11.2007



class SSD extends RBACExtension {

  // ## Klassenvariablen ##############################################




  // ## Konstruktor ###################################################
  public function __construct( $inRBAC, RBAC $inRegistrar ) {

    // Save the instances of RBAC and grab the configuration
    // from it.
    $this->rbac = $inRBAC;
    $this->conf = $inRBAC->getConfiguration();


    // Get the user- and role connections from the
    // underlying RBAC-system
    $this->conn['user'] = $inRBAC->getConnection( "user" );
    $this->conn['role'] = $inRBAC->getConnection( "role" );


    // Add an own connection to the SSD
    $this->conn['ssd'] = new LDAP();
    $this->conn['ssd']->connect( $this->conf->getValue( "ssd", "host" ),
                                 $this->conf->getValue( "ssd", "port" ),
                                 $this->conf->getValue( "ssd", "version" ),
                                 preg_match( "/(^yes$)|(^true$)/i",
                                 $this->conf->getValue( "ssd", "tls" ) ) );
    $this->conn['ssd']->bind( $this->conf->getValue( "ssd", "binddn" ),
                              $this->conf->getValue( "ssd", "password" ) );


    // Let the extension do all the things
    // we dont't want to do
    parent::__construct( $inRBAC );

  }




  // ## registerEvents ################################################
  public function registerEvents( RBAC $inRegistrar ) {

    $inRegistrar->registerEventListener( "assignUser", "write", $this, "checkAssignUser" );
    $inRegistrar->registerEventListener( "addInheritance", "write", $this, "checkAddInheritance" );

  }




  // ## createSsdSet ##################################################
  public function createSsdSet( $inName, Array $inArrRole, $inCardinality ) {

    $filter = "";                     // An LDAP filter
    $arrSsd;                          // The SSD-entry if it allready exists
    $arrSsdEntry;                     // The new SSD-entry
    $arrRoleEntry;                    // Temporary role entry
    $allRoleExist = true;             // Check for existence of the given roles
    $ssdConstraintsSatisfied = true;  // Are all constraints satisfied
    $i = 0;                           // Loop


    if( $inCardinality >= 2 ) {

      // Construct a filter to search for the users entry
      $filter  = "(&" . $this->conf->getValue( "ssd", "filter" );
      $filter .= "(rbacname=" . $inName . "))";


      // Get the SSD entry
      $arrSsd = $this->conn['ssd']->search( $this->conf->getValue( "ssd", "base" ), $filter, "sub", Array( "rbacname" ) );


      if( $inArrRole && sizeof( $inArrRole ) > 1 ) {

        // Check if all the given roles exist
        for( $i = 0; $i < sizeof( $inArrRole ) && $allRoleExist; $i++ ) {

          // Make sure the role has internal representation
          !$this->rbac->isIntRepresentation( $inArrRole[$i] ) ? $inArrRole[$i] = $this->rbac->roleExtToInt( $inArrRole[$i] ) : false;


          // Get the role
          $arrRoleEntry = $this->conn['role']->getEntry( $inArrRole[$i] );


          // Keep in mind if still all roles exist
          $allRoleExist = $allRoleExist && isset( $arrRoleEntry['dn'] );

        }


        if( !isset( $arrSsd[0]['dn'] ) ) {

          if( $allRoleExist ) {

            if( $this->checkSSDConsistency( $inArrRole, $inCardinality ) ) {

              $arrSsdEntry['objectclass'][] = "rbacssd";
              $arrSsdEntry['rbacname'][0] = $inName;
              $arrSsdEntry['rbaccardinality'] = $inCardinality;
              $arrSsdEntry['rbacsdrole'] = $inArrRole;


              // Add the new SSD-Set to the directory
              if( $this->conn['ssd']->add( "rbacname=" . $inName . "," . $this->conf->getValue( "ssd", "base" ), $arrSsdEntry ) ) {

                return true;

              }
              else {

                throw new RBACException( $this->conf->getValue( "errorDescription", "LDAP_ERROR" ),
                                         $this->conf->getValue( "errorCode", "LDAP_ERROR" ) );

              }

            }
            else {

              return false;

            }

          }
          else {

            throw new RBACException( $this->conf->getValue( "errorDescription", "ROLE_UNKNOWN" ),
                                     $this->conf->getValue( "errorCode", "ROLE_UNKNOWN" ) );

          }

        }
        else {

          throw new RBACException( $this->conf->getValue( "errorDescription", "SD_ALLREADY_EXISTS" ),
                                   $this->conf->getValue( "errorCode", "SD_ALLREADY_EXISTS" ) );

        }

      }
      else {

        return false;

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "SD_CARDINALITY" ),
                               $this->conf->getValue( "errorCode", "SD_CARDINALITY" ) );

    }

  }




  // ## addSsdRoleMember ##############################################
  public function addSsdRoleMember( $inSsdName, $inRole ) {

    $filter = "";             // LDAP-filter
    $arrSsd = Array();        // The Ssd-set entry
    $arrRole = Array();       // The role-entry
    $arrTmpRole = Array();    // The temporary roles of the SSD-set


    // Make sure the role has internal representation
    !$this->rbac->isIntRepresentation( $inRole ) ? $inRole = $this->rbac->roleExtToInt( $inRole ) : false;


    // Create a filter to get the SSD-set
    $filter  = "(&" . $this->conf->getValue( "ssd", "filter" );
    $filter .= "(rbacName=" . $inSsdName . "))";


    // Ask the directory for the SSD-set
    $arrSsd = $this->conn['ssd']->search( $this->conf->getValue( "ssd", "base" ), $filter, "one" );


    // Get the role from the directory
    $arrRole = $this->conn->getEntry( $inRole );


    if( $arrSsd && sizeof( $arrSsd == 1 )
        && isset( $arrSsd[0]['dn'] ) ) {

      // Add the given role to the roles in the SSD-set
      // to test, if it would still be consistent
      isset( $arrSsd[0]['rbacsdrole'] ) ? $arrTmpRole = $arrSsd[0]['rbacsdrole'] : $arrTmpRole = Array();
      $arrTmpRole[] = $inRole;


      // The role has to exist
      if( isset( $arrRole['dn'] ) ) {

        if( $this->checkSSDConsistency( $arrTmpRole, $arrSsd[0]['rbaccardinality'][0] ) ) {

          // Store the new role into the SSD-set
          if( $this->conn['ssd']->modify( $arrSsd[0]['dn'], Array( "rbacsdrole" => $arrTmpRole ) ) ) {

            return true;

          }
          else {

            throw new RBACException( $this->conf->getValue( "errorDescription", "LDAP_ERROR" ),
                                     $this->conf->getValue( "errorCode", "LDAP_ERROR" ) );

          }

        }
        else {

          return false;

        }

      }
      else {

        throw new RBACException( $this->conf->getValue( "errorDescription", "ROLE_UNKNOWN" ),
                                 $this->conf->getValue( "errorCode", "ROLE_UNKNOWN" ) );

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "SD_UNKNOWN" ),
                               $this->conf->getValue( "errorCode", "SD_UNKNOWN" ) );

    }

  }




  // ## deleteSsdRoleMember ###########################################
  public function deleteSsdRoleMember( $inSsdName, $inRole ) {

    $filter = "";       // LDAP-filter
    $arrSsd = Array();  // The Ssd-set entry
    $i = 0;             // Loop


    // Make sure the role has internal representation
    !$this->rbac->isIntRepresentation( $inRole ) ? $inRole = $this->rbac->roleExtToInt( $inRole ) : false;


    // Create a filter to get the SSD-set
    $filter  = "(&" . $this->conf->getValue( "ssd", "filter" );
    $filter .= "(rbacName=" . $inSsdName . "))";


    // Ask the directory for the SSD-set
    $arrSsd = $this->conn['ssd']->search( $this->conf->getValue( "ssd", "base" ), $filter, "one" );


    if( $arrSsd && sizeof( $arrSsd ) == 1
        && isset( $arrSsd[0]['dn'] ) ) {

      if( $arrSsd[0]['rbaccardinality'][0] <= (sizeof( $arrSsd[0]['rbacsdrole'] ) - 1) ) {

        for( $i = 0; $i < sizeof( $arrSsd[0]['rbacsdrole'] ); $i++ ) {

          !preg_match( "/^" . $inRole . "$/i", $arrSsd[0]['rbacsdrole'][$i] ) ? $arrTmpRole[] = $arrSsd[0]['rbacsdrole'][$i] : false;

        }


        // Save the modifications to the directory
        if( $this->conn['ssd']->modify( $arrSsd[0]['dn'], Array( "rbacsdrole" => $arrTmpRole ) ) ) {

          return true;

        }
        else {

          throw new RBACException( $this->conf->getValue( "errorDescription", "LDAP_ERROR" ),
                                   $this->conf->getValue( "errorCode", "LDAP_ERROR" ) );

        }

      }
      else {

        throw new RBACException( $this->conf->getValue( "errorDescription", "SD_CARDINALITY" ),
                                 $this->conf->getValue( "errorCode", "SD_CARDINALITY" ) );

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "SD_UNKNOWN" ),
                               $this->conf->getValue( "errorCode", "SD_UNKNOWN" ) );

    }

  }




  // ## deleteSsdSet ##################################################
  public function deleteSsdSet( $inSsdName ) {

    $filter = "";       // LDAP-filter
    $arrSsd = Array();  // The Ssd-set entry


    // Create a filter to get the SSD-set
    $filter  = "(&" . $this->conf->getValue( "ssd", "filter" );
    $filter .= "(rbacName=" . $inSsdName . "))";


    // Ask the directory for the SSD-set
    $arrSsd = $this->conn['ssd']->search( $this->conf->getValue( "ssd", "base" ), $filter, "one" );


    if( $arrSsd && sizeof( $arrSsd ) == 1
        && isset( $arrSsd[0]['dn'] ) ) {

      if( $this->conn['ssd']->delete( $arrSsd[0]['dn'] ) ) {

        return true;

      }
      else {

        throw new RBACException( $this->conf->getValue( "errorDescription", "LDAP_ERROR" ),
                                 $this->conf->getValue( "errorCode", "LDAP_ERROR" ) );

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "SD_UNKNOWN" ),
                               $this->conf->getValue( "errorCode", "SD_UNKNOWN" ) );

    }

  }




  // ## setSsdSetCardinality ##########################################
  public function setSsdSetCardinality( $inSsdName, $inCardinality ) {

    $filter = "";       // LDAP-filter
    $arrSsd = Array();  // The Ssd-set entry


    // Create a filter to get the SSD-set
    $filter  = "(&" . $this->conf->getValue( "ssd", "filter" );
    $filter .= "(rbacName=" . $inSsdName . "))";


    // Ask the directory for the SSD-set
    $arrSsd = $this->conn['ssd']->search( $this->conf->getValue( "ssd", "base" ), $filter, "one" );


    if( $arrSsd && sizeof( $arrSsd ) == 1
        && isset( $arrSsd[0]['dn'] ) ) {

      if(    ($inCardinality >= 2)
          && ($inCardinality <= sizeof( $arrSsd[0]['rbacsdrole'] )) ) {

        if( $this->checkSSDConsistency( $arrSsd[0]['rbacsdrole'], $inCardinality ) ) {

          // Store the new role into the SSD-set
          if( $this->conn['ssd']->modify( $arrSsd[0]['dn'], Array( "rbaccardinality" => Array( $inCardinality ) ) ) ) {

            return true;

          }
          else {

            throw new RBACException( $this->conf->getValue( "errorDescription", "LDAP_ERROR" ),
                                     $this->conf->getValue( "errorCode", "LDAP_ERROR" ) );

          }

        }

      }
      else {

        return false;

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "SD_UNKNOWN" ),
                               $this->conf->getValue( "errorCode", "SD_UNKNOWN" ) );

    }

  }




  // ## ssdRoleSets ###################################################
  public function ssdRoleSets() {

    $arrSsd = Array();      // The Ssd-set entry
    $arrSetName = Array();  // The names of the SSD-sets
    $i = 0;                 // Loop


    // Ask the directory for the SSD-set
    $arrSsd = $this->conn['ssd']->search( $this->conf->getValue( "ssd", "base" ), $this->conf->getValue( "ssd", "filter" ),
                                          "one", Array( "rbacname" ) );


    for( $i = 0; $i < sizeof( $arrSsd ); $i++ ) {

      $arrSetName[] = $arrSsd[$i]['rbacname'][0];

    }


    return $arrSetName;

  }




  // ## ssdRoleSetRoles ###############################################
  public function ssdRoleSetRoles( $inSsdName, $inExtRepresentation = true ) {

    $filter = "";        // LDAP-filter
    $arrSsd = Array();   // The Ssd-set entry
    $arrRole = Array();  // The roles of the SSD-set
    $i = 0;              // Loop


    // Create a filter to get the SSD-set
    $filter  = "(&" . $this->conf->getValue( "ssd", "filter" );
    $filter .= "(rbacName=" . $inSsdName . "))";


    // Ask the directory for the SSD-set
    $arrSsd = $this->conn['ssd']->search( $this->conf->getValue( "ssd", "base" ), $filter, "one" );


    if( $arrSsd && sizeof( $arrSsd ) == 1
        && isset( $arrSsd[0]['dn'] ) ) {

      isset( $arrSsd[0]['rbacsdrole'] ) ? $arrRole = $arrSsd[0]['rbacsdrole'] : $arrRole = Array();

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "SD_UNKNOWN" ),
                               $this->conf->getValue( "errorCode", "SD_UNKNOWN" ) );

    }


    // For internal use this can be left out, so the
    // internal function that uses this one doesn't have
    // to convert every role back to internal representation
    if( $inExtRepresentation ) {

      for( $i = 0; $i < sizeof( $arrRole ); $i++ ) {

        $arrRole[$i] = $this->rbac->roleIntToExt( $arrRole[$i] );

      }

    }


    return $arrRole;

  }




  // ## ssdRoleSetCardinality #########################################
  public function ssdRoleSetCardinality( $inSsdName ) {

    $filter = "";        // LDAP-filter
    $arrSsd = Array();   // The Ssd-set entry
    $i = 0;              // Loop


    // Create a filter to get the SSD-set
    $filter  = "(&" . $this->conf->getValue( "ssd", "filter" );
    $filter .= "(rbacName=" . $inSsdName . "))";


    // Ask the directory for the SSD-set
    $arrSsd = $this->conn['ssd']->search( $this->conf->getValue( "ssd", "base" ), $filter, "one" );


    if( $arrSsd && sizeof( $arrSsd ) == 1
        && isset( $arrSsd[0]['dn'] ) ) {

      if( isset( $arrSsd[0]['rbaccardinality'][0] ) ) {

        return $arrSsd[0]['rbaccardinality'][0];

      }
      else {

        // The cardinality is a MUST-attribute, so if there is none
        // it must be a LDAP-error
        throw new RBACException( $this->conf->getValue( "errorDescription", "LDAP_ERROR" ),
                                 $this->conf->getValue( "errorCode", "LDAP_ERROR" ) );

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "SD_UNKNOWN" ),
                               $this->conf->getValue( "errorCode", "SD_UNKNOWN" ) );

    }

  }




  // ## checkAddInheritance ###########################################
  public function checkAddInheritance( Context $inContext ) {

    $arrSsd = Array();
    $arrParameter = $inContext->getParameters();  // The parameters the addInheritance-function got
    $ascendant = $arrParameter[0];
    $descendant = $arrParameter[1];
    $isAllowed = true;


    // Make sure the roles has internal representation
    !$this->rbac->isIntRepresentation( $ascendant ) ? $ascendant = $this->rbac->roleExtToInt( $ascendant ) : false;
    !$this->rbac->isIntRepresentation( $descendant ) ? $descendant = $this->rbac->roleExtToInt( $descendant ) : false;


    // Create a filter that gets all the SSD-sets
    // where the ascendant is present
    $filter  = "(&" . $this->conf->getValue( "ssd", "filter" );
    $filter .= "(rbacsdrole=" . $ascendant . "))";


    // Get these SSD-sets
    $arrSsd = $this->conn['ssd']->search( $this->conf->getValue( "ssd", "base" ), $filter, "one" );


    // The ascendant must not be in any SSD-set
    // because this set would be invalid afterwards
    if( $arrSsd && sizeof( $arrSsd ) == 0 ) {

      // Create a filter that gets all the SSD-sets
      // where the descendant is present
      $filter  = "(&" . $this->conf->getValue( "ssd", "filter" );
      $filter .= "(rbacsdrole=" . $descendant . "))";


      // Get these SSD-sets
      $arrSsd = $this->conn['ssd']->search( $this->conf->getValue( "ssd", "base" ), $filter, "one" );


      for( $i = 0; $i < sizeof( $arrSsd ) && $isAllowed; $i++ ) {

        $isAllowed = $isAllowed && $this->checkSSDConsistency( $arrSsd[$i]['rbacsdrole'], $arrSsd[0]['rbaccardinality'][0],
                                                               $ascendant, $descendant );

      }


      if( !$isAllowed ) {

        $inContext->changeSecurityChain( false, "Not all SSD-constraints would be satisfied" );

      }

    }
    else {

      $inContext->changeSecurityChain( false, "The ascendant-role is member of a SSD-set that would be inconsistent afterwards" );

    }


    return $inContext;

  }




  // ## checkAssignUser ###############################################
  public function checkAssignUser( Context $inContext ) {

    $arrParameter = $inContext->getParameters();  // The parameters the assignUser-function got
    $userMembership = 0;                          // The number of roles the user is assigned to in SSD-set
    $isAllowed = true;                            // Is it allowed to assign the user to the role
    $i = 0;                                       // Loop


    // Make sure the role has internal representation
    !$this->rbac->isIntRepresentation( $arrParameter[1] ) ? $arrParameter[1] = $this->rbac->roleExtToInt( $arrParameter[1] ) : false;


    // The user has these roles in internal
    // representation (last parameter false)
    $arrUserRole = $this->rbac->authorizedRoles( $arrParameter[0], false );


    // Create a filter that gets all the SSD-sets
    // where the role is present the user would be assigned to
    $filter  = "(&" . $this->conf->getValue( "ssd", "filter" );
    $filter .= "(rbacsdrole=" . $arrParameter[1] . "))";


    // Get these SSD-sets
    $arrSSD = $this->conn['ssd']->search( $this->conf->getValue( "ssd", "base" ), $filter, "one" );


    for( $i = 0; $i < sizeof( $arrSSD ); $i++ ) {

      $userMembership = 0;
      for( $j = 0; $j < sizeof( $arrUserRole ); $j++ ) {

        // This counts how many roles of the SSD-sed the
        // user is performer of
        for( $k = 0; $k < sizeof( $arrSSD[$i]['rbacsdrole'] ); $k++ ) {

          if( preg_match( "/^" . $arrUserRole[$j] . "$/i", $arrSSD[$i]['rbacsdrole'][$k] ) ) {

            $userMembership++;

          }

        }

      }


      // If one SSD-set disallows the assignment, it's enough
      $isAllowed = $isAllowed && ( ($userMembership + 1) < $arrSSD[$i]['rbaccardinality'][0] );

    }


    if( !$isAllowed ) {

      $inContext->changeSecurityChain( false, "Not all SSD-constraints would be satisfied" );

    }


    return $inContext;

  }




  // ## arrayIntersection #############################################
  public function arrayIntersection( Array $inArray ) {

    $arrHistogram = Array();
    $arrIntersection = Array();
    $i = 0;
    $j = 0;


    for( $i = 0; $i < sizeof( $inArray ); $i++ ) {

      for( $j = 0; $j < sizeof( $inArray[$i] ); $j++ ) {

        isset( $arrHistogram[$inArray[$i][$j]] ) ? $arrHistogram[$inArray[$i][$j]] = $arrHistogram[$inArray[$i][$j]] + 1
                                                 : $arrHistogram[$inArray[$i][$j]] = 1;

      }

    }


    foreach( $arrHistogram as $key => $value ) {

      if( $value > 1 ) {

        $arrIntersection[$key] = $value;

      }

    }


    return $arrIntersection;

  }




  // ## checkSSDConsistency ###########################################
  private function checkSSDConsistency( Array $inArrRole, $inCardinality, $inRoleAsc = Array(), $inRoleDesc = Array() ) {

    $ssdConstraintsSatisfied = true;  // The result of the check
    $arrMemberHistogram = Array();


    // All these vombinations are possible
    $arrRoleCombination = $this->createRoleCombinations( $inArrRole, $inCardinality );


    foreach( $arrRoleCombination as $index => $arrCombination ) {

      $arrMemberHistogram = Array();
      for( $i = 0; $i < sizeof( $arrCombination ); $i++ ) {

        // Get all authorized users for the role in the combination-set
        $arrAuthorizedUser = $this->rbac->authorizedUsers( $arrCombination[$i], true );


        // If an additional ascendant role is given, we have to
        // add its authorized users to these roles that match the
        // descendant role at any point of the hirarchy.
        if(    $inRoleAsc != null
            && $inRoleDesc != null
            && preg_match( "/" . $inRoleDesc . "$/i", $arrCombination[$i] ) ) {


          $arrAuthorizedUser = array_merge( $arrAuthorizedUser, $this->rbac->authorizedUsers( $inRoleAsc, true ) );

        }


        // If the user is performer of this role, than increase
        // by one
        for( $j = 0; $j < sizeof( $arrAuthorizedUser ); $j++ ) {

          isset( $arrMemberHistogram[$arrAuthorizedUser[$j]] ) ? $arrMemberHistogram[$arrAuthorizedUser[$j]]++
                                                               : $arrMemberHistogram[$arrAuthorizedUser[$j]] = 1;

        }


        // Each user may only perform less than inCardinality roles
        foreach( $arrMemberHistogram as $user => $count ) {

          if( $count >= $inCardinality ) {

            $ssdConstraintsSatisfied = false;
            break;

          }

        }

      }

    }


    return $ssdConstraintsSatisfied;

  }




  // ## createRoleCombinations ########################################
  public function createRoleCombinations( Array $inRole, $inCardinality ) {

    $arrTmp = Array();
    $arrResult = Array();


    for( $i = 1; $i < pow( 2, sizeof( $inRole ) ); $i++ ) {

      $bin = $i;
      $index = 0;
      $arrTmp = Array();


      if( $bin == 0 ) {

        $arrTmp[] = $inRole[$index];

      }
      else {

        while( $bin != 0 ) {

          if( $bin % 2 == 1 ) {

            $arrTmp[] = $inRole[$index];

          }


          $index++;
          $bin = floor( $bin / 2 );

        }

      }


      if($arrTmp && sizeof( $arrTmp ) == $inCardinality ) {

        $arrResult[] = $arrTmp;

      }

    }


    return $arrResult;

  }

}
?>
