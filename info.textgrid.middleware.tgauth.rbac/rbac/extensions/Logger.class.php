<?php
// ####################################################################
// Version: 0.1.0
// Autor: Markus Widmer
// Erstellungsdatum: 02.11.2007
// Letzte Aenderung: 11.06.2024 (fu)

$LOG_DIR = "/var/log/dhrep/tgauth/"; // This sould be configured via Puppet in dhrep module with write permission for www-data (apache2)!

class Logger extends RBACExtension {

  // ## Klassenvariablen ##############################################




  // ## Konstruktor ###################################################
  public function __construct( $inRBAC, $inRegistrar ) {

    // Let the extension do all the things
    // we dont't want to do
    parent::__construct( $inRBAC );

  }



  // ## registerEvents ################################################
  public function registerEvents( RBAC $inRegistrar ) {

    $inRegistrar->registerEventListener( "addUser", "write", $this, "logAddUserEvent" );

  }




  // ## createSsdSet ##################################################
  public function logAddUserEvent( Context $inContext ) {

    $file = fopen( $LOG_DIR . "addUser.log", "a+" );
    fclose( $file );


    return $inContext;

  }

}
?>
