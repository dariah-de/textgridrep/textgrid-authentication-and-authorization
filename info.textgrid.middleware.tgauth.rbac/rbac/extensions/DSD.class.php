<?php
// ####################################################################
// Version: 0.1.1
// Autor: Markus Widmer
// Erstellungsdatum: 07.11.2007
// Letzte Aenderung: 12.11.2007



class DSD extends RBACExtension {

  // ## Klassenvariablen ##############################################




  // ## Konstruktor ###################################################
  public function __construct( $inRBAC, RBAC $inRegistrar ) {

    // Save thsi instances of RBAC and grab the configuration
    // from it.
    $this->rbac = $inRBAC;
    $this->conf = $inRBAC->getConfiguration();


    // Get the user- and role connections from the
    // underlying RBAC-system
    $this->conn['session'] = $inRBAC->getConnection( "session" );
    $this->conn['role'] = $inRBAC->getConnection( "role" );


    // Add an own connection to the SSD
    $this->conn['dsd'] = new LDAP();
    $this->conn['dsd']->connect( $this->conf->getValue( "dsd", "host" ),
                                 $this->conf->getValue( "dsd", "port" ),
                                 $this->conf->getValue( "dsd", "version" ),
                                 preg_match( "/(^yes$)|(^true$)/i",
                                 $this->conf->getValue( "dsd", "tls" ) ) );
    $this->conn['dsd']->bind( $this->conf->getValue( "dsd", "binddn" ),
                              $this->conf->getValue( "dsd", "password" ) );


    // Let the extension do all the things
    // we dont't want to do
    parent::__construct( $inRBAC );

  }




  // ## registerEvents ################################################
  public function registerEvents( RBAC $inRegistrar ) {

    $inRegistrar->registerEventListener( "addActiveRole", "write", $this, "checkAddActiveRole" );
    $inRegistrar->registerEventListener( "createSession", "write", $this, "checkCreateSession" );

  }




  // ## createDsdSet ##################################################
  public function createDsdSet( $inName, Array $inArrRole, $inCardinality ) {

    $filter = "";                     // An LDAP filter
    $arrDsd;                          // The DSD-entry if it allready exists
    $arrDsdEntry;                     // The new DSD-entry
    $arrRoleENtry;                    // Temporary role entry
    $allRoleExist = true;             // Check for existence of the given roles
    $ssdConstraintsSatisfied = true;  // Are all constraints satisfied
    $i = 0;                           // Loop


    if( $inCardinality >= 2 ) {

      // Construct a filter to search for the users entry
      $filter  = "(&" . $this->conf->getValue( "dsd", "filter" );
      $filter .= "(rbacname=" . $inName . "))";


      // Get the DSD entry
      $arrDsd = $this->conn['dsd']->search( $this->conf->getValue( "dsd", "base" ), $filter, "sub", Array( "rbacname" ) );


      if( $inArrRole && sizeof( $inArrRole ) > 1 ) {

        // Check if all the given roles exist
        for( $i = 0; $i < sizeof( $inArrRole ) && $allRoleExist; $i++ ) {

          // Make sure the role has internal representation
          !$this->rbac->isIntRepresentation( $inArrRole[$i] ) ? $inArrRole[$i] = $this->rbac->roleExtToInt( $inArrRole[$i] ) : false;


          // Get the role
          $arrRoleEntry = $this->conn['role']->getEntry( $inArrRole[$i] );


          // Keep in mind if still all roles exist
          $allRoleExist = $allRoleExist && isset( $arrRoleEntry['dn'] );

        }


        if( !isset( $arrDsd[0]['dn'] ) ) {

          if( $allRoleExist ) {

            $arrDsdEntry['objectclass'][] = "rbacdsd";
            $arrDsdEntry['rbacname'][0] = $inName;
            $arrDsdEntry['rbaccardinality'] = $inCardinality;
            $arrDsdEntry['rbacsdrole'] = $inArrRole;


            // Add the new DSD-Set to the directory
            if( $this->conn['dsd']->add( "rbacname=" . $inName . "," . $this->conf->getValue( "dsd", "base" ), $arrDsdEntry ) ) {

              return true;

            }
            else {

              throw new RBACException( $this->conf->getValue( "errorDescription", "LDAP_ERROR" ),
                                       $this->conf->getValue( "errorCode", "LDAP_ERROR" ) );

            }

          }
          else {
  
            throw new RBACException( $this->conf->getValue( "errorDescription", "ROLE_UNKNOWN" ),
                                     $this->conf->getValue( "errorCode", "ROLE_UNKNOWN" ) );

          }

        }
        else {

          throw new RBACException( $this->conf->getValue( "errorDescription", "SD_ALLREADY_EXISTS" ),
                                   $this->conf->getValue( "errorCode", "SD_ALLREADY_EXISTS" ) );

        }

      }
      else {

        return false;

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "SD_CARDINALITY" ),
                               $this->conf->getValue( "errorCode", "SD_CARDINALITY" ) );

    }

  }




  // ## addDsdRoleMember ##############################################
  public function addDsdRoleMember( $inDsdName, $inRole ) {

    $filter = "";             // LDAP-filter
    $arrDsd = Array();        // The Dsd-set entry
    $arrTmpRole = Array();    // The temporary roles of the SSD-set


    // Make sure the role has internal representation
    !$this->rbac->isIntRepresentation( $inRole ) ? $inRole = $this->rbac->roleExtToInt( $inRole ) : false;


    // Create a filter to get the SSD-set
    $filter  = "(&" . $this->conf->getValue( "dsd", "filter" );
    $filter .= "(rbacName=" . $inDsdName . "))";


    // Ask the directory for the SSD-set
    $arrDsd = $this->conn['dsd']->search( $this->conf->getValue( "dsd", "base" ), $filter, "one" );


    // Get the role from the directory
    $arrRole = $this->conn->getEntry( $inRole );


    if(   $arrDsd &&  sizeof( $arrDsd == 1 )
        && isset( $arrDsd[0]['dn'] ) ) {

      // The role has to exist
      if( isset( $arrRole['dn'] ) ) {

        // Add the given role to the roles in the DSD-set
        // to test, if it would still be consistent
        isset( $arrDsd[0]['rbacsdrole'] ) ? $arrTmpRole = $arrDsd[0]['rbacsdrole'] : $arrTmpRole = Array();
        $arrTmpRole[] = $inRole;


        // Store the new role into the DSD-set
        if( $this->conn['dsd']->modify( $arrDsd[0]['dn'], Array( "rbacsdrole" => $arrTmpRole ) ) ) {

        return true;

        }
        else {

          throw new RBACException( $this->conf->getValue( "errorDescription", "LDAP_ERROR" ),
                                   $this->conf->getValue( "errorCode", "LDAP_ERROR" ) );

        }

      }
      else {

        throw new RBACException( $this->conf->getValue( "errorDescription", "ROLE_UNKNOWN" ),
                                 $this->conf->getValue( "errorCode", "ROLE_UNKNOWN" ) );

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "SD_UNKNOWN" ),
                               $this->conf->getValue( "errorCode", "SD_UNKNOWN" ) );

    }

  }




  // ## deleteDsdRoleMember ###########################################
  public function deleteDsdRoleMember( $inDsdName, $inRole ) {

    $filter = "";       // LDAP-filter
    $arrDsd = Array();  // The Ssd-set entry
    $i = 0;             // Loop


    // Make sure the role has internal representation
    !$this->rbac->isIntRepresentation( $inRole ) ? $inRole = $this->rbac->roleExtToInt( $inRole ) : false;


    // Create a filter to get the SSD-set
    $filter  = "(&" . $this->conf->getValue( "dsd", "filter" );
    $filter .= "(rbacName=" . $inDsdName . "))";


    // Ask the directory for the SSD-set
    $arrDsd = $this->conn['dsd']->search( $this->conf->getValue( "dsd", "base" ), $filter, "one" );


    if(   $arrDsd &&  sizeof( $arrDsd ) == 1
        && isset( $arrDsd[0]['dn'] ) ) {

      if( $arrDsd[0]['rbaccardinality'][0] <= (sizeof( $arrDsd[0]['rbacsdrole'] ) - 1) ) {

        for( $i = 0; $i < sizeof( $arrDsd[0]['rbacsdrole'] ); $i++ ) {

          !preg_match( "/^" . $inRole . "$/i", $arrDsd[0]['rbacsdrole'][$i] ) ? $arrTmpRole[] = $arrDsd[0]['rbacsdrole'][$i] : false;

        }


        // Save the modifications to the directory
        if( $this->conn['dsd']->modify( $arrDsd[0]['dn'], Array( "rbacsdrole" => $arrTmpRole ) ) ) {

          return true;

        }
        else {

          throw new RBACException( $this->conf->getValue( "errorDescription", "LDAP_ERROR" ),
                                   $this->conf->getValue( "errorCode", "LDAP_ERROR" ) );

        }

      }
      else {

        throw new RBACException( $this->conf->getValue( "errorDescription", "SD_CARDINALITY" ),
                                 $this->conf->getValue( "errorCode", "SD_CARDINALITY" ) );

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "SD_UNKNOWN" ),
                               $this->conf->getValue( "errorCode", "SD_UNKNOWN" ) );

    }

  }




  // ## deleteDsdSet ##################################################
  public function deleteDsdSet( $inDsdName ) {

    $filter = "";       // LDAP-filter
    $arrSsd = Array();  // The Ssd-set entry


    // Create a filter to get the SSD-set
    $filter  = "(&" . $this->conf->getValue( "dsd", "filter" );
    $filter .= "(rbacName=" . $inDsdName . "))";


    // Ask the directory for the DSD-set
    $arrDsd = $this->conn['dsd']->search( $this->conf->getValue( "dsd", "base" ), $filter, "one" );


    if( $arrDsd && sizeof( $arrDsd ) == 1
        && isset( $arrDsd[0]['dn'] ) ) {

      if( $this->conn['dsd']->delete( $arrDsd[0]['dn'] ) ) {

        return true;

      }
      else {

        throw new RBACException( $this->conf->getValue( "errorDescription", "LDAP_ERROR" ),
                                 $this->conf->getValue( "errorCode", "LDAP_ERROR" ) );

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "SD_UNKNOWN" ),
                               $this->conf->getValue( "errorCode", "SD_UNKNOWN" ) );

    }

  }




  // ## setDsdSetCardinality ##########################################
  public function setDsdSetCardinality( $inDsdName, $inCardinality ) {

    $filter = "";       // LDAP-filter
    $arrSsd = Array();  // The Ssd-set entry


    // Create a filter to get the SSD-set
    $filter  = "(&" . $this->conf->getValue( "dsd", "filter" );
    $filter .= "(rbacName=" . $inDsdName . "))";


    // Ask the directory for the DSD-set
    $arrDsd = $this->conn['dsd']->search( $this->conf->getValue( "dsd", "base" ), $filter, "one" );


    if( $arrDsd && sizeof( $arrDsd ) == 1
        && isset( $arrDsd[0]['dn'] ) ) {

      if(    ($inCardinality >= 2)
          && ($inCardinality <= sizeof( $arrDsd[0]['rbacsdrole'] )) ) {

        // Store the new role into the DSD-set
        if( $this->conn['dsd']->modify( $arrDsd[0]['dn'], Array( "rbaccardinality" => Array( $inCardinality ) ) ) ) {

          return true;

        }
        else {

          throw new RBACException( $this->conf->getValue( "errorDescription", "LDAP_ERROR" ),
                                   $this->conf->getValue( "errorCode", "LDAP_ERROR" ) );

        }

      }
      else {

        return false;

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "SD_UNKNOWN" ),
                               $this->conf->getValue( "errorCode", "SD_UNKNOWN" ) );

    }

  }




  // ## dsdRoleSets ###################################################
  public function dsdRoleSets() {

    $arrDsd = Array();      // The Dsd-set entry
    $arrSetName = Array();  // The names of the DSD-sets
    $i = 0;                 // Loop


    // Ask the directory for the DSD-set
    $arrDsd = $this->conn['dsd']->search( $this->conf->getValue( "dsd", "base" ), $this->conf->getValue( "dsd", "filter" ),
                                          "one", Array( "rbacname" ) );


    for( $i = 0; $i < sizeof( $arrDsd ); $i++ ) {

      $arrSetName[] = $arrDsd[$i]['rbacname'][0];

    }


    return $arrSetName;

  }




  // ## dsdRoleSetRoles ###############################################
  public function dsdRoleSetRoles( $inDsdName, $inExtRepresentation = true ) {

    $filter = "";        // LDAP-filter
    $arrSsd = Array();   // The Ssd-set entry
    $arrRole = Array();  // The roles of the SSD-set
    $i = 0;              // Loop


    // Create a filter to get the SSD-set
    $filter  = "(&" . $this->conf->getValue( "dsd", "filter" );
    $filter .= "(rbacName=" . $inDsdName . "))";


    // Ask the directory for the DSD-set
    $arrDsd = $this->conn['dsd']->search( $this->conf->getValue( "dsd", "base" ), $filter, "one" );


    if(  $arrDsd && sizeof( $arrDsd ) == 1
        && isset( $arrDsd[0]['dn'] ) ) {

      isset( $arrDsd[0]['rbacsdrole'] ) ? $arrRole = $arrDsd[0]['rbacsdrole'] : $arrRole = Array();

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "SD_UNKNOWN" ),
                               $this->conf->getValue( "errorCode", "SD_UNKNOWN" ) );

    }


    // For internal use this can be left out, so the
    // internal function that uses this one doesn't have
    // to convert every role back to internal representation
    if( $inExtRepresentation ) {

      for( $i = 0; $i < sizeof( $arrRole ); $i++ ) {

        $arrRole[$i] = $this->rbac->roleIntToExt( $arrRole[$i] );

      }

    }


    return $arrRole;

  }




  // ## dsdRoleSetCardinality #########################################
  public function dsdRoleSetCardinality( $inDsdName ) {

    $filter = "";        // LDAP-filter
    $arrDsd = Array();   // The Dsd-set entry
    $i = 0;              // Loop


    // Create a filter to get the SSD-set
    $filter  = "(&" . $this->conf->getValue( "dsd", "filter" );
    $filter .= "(rbacName=" . $inDsdName . "))";


    // Ask the directory for the DSD-set
    $arrDsd = $this->conn['dsd']->search( $this->conf->getValue( "dsd", "base" ), $filter, "one" );


    if(  $arrDsd && sizeof( $arrDsd ) == 1
        && isset( $arrDsd[0]['dn'] ) ) {

      if( isset( $arrDsd[0]['rbaccardinality'][0] ) ) {

        return $arrDsd[0]['rbaccardinality'][0];

      }
      else {

        // The cardinality is a MUST-attribute, so if there is none
        // it must be a LDAP-error
        throw new RBACException( $this->conf->getValue( "errorDescription", "LDAP_ERROR" ),
                                 $this->conf->getValue( "errorCode", "LDAP_ERROR" ) );

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "SSD_UNKNOWN" ),
                               $this->conf->getValue( "errorCode", "SSD_UNKNOWN" ) );

    }

  }



  // ## checkCreateSession ############################################
  public function checkCreateSession( Context $inContext ) {

    $arrParameter = $inContext->getParameters();  // The parameters the assignUser-function got
    $user = $arrParameter[0];                     // Function-parameter
    $arrRole = $arrParameter[1];                  // Function-parameter
    $session = $arrParameter[2];                  // Function-parameter
    $sessionMembership = 0;                       // A temporary count-variable
    $isAllowed = true;                            // Is it allowed to create the session
    $i = 0;                                       // Loop
    $j = 0;                                       // Loop
    $k = 0;                                       // Loop


    // Make sure the roles have internal representation
    for( $i = 0; $i < sizeof( $arrRole ); $i++ ) {

      !$this->rbac->isIntRepresentation( $arrRole[$i] ) ? $arrRole[$i] = $this->rbac->roleExtToInt( $arrRole[$i] ) : false;

    }


    // Create a filter that gets all the DSD-sets
    // where the role is present the user wants to activate
    $filter  = "(&" . $this->conf->getValue( "dsd", "filter" ) . "(|";

    for( $i = 0; $i < sizeof( $arrRole ); $i++ ) {

      $filter .= "(rbacsdrole=" . $arrRole[$i] . ")";

    }

    $filter .= "))";


    // Get these DSD-sets
    $arrDSD = $this->conn['dsd']->search( $this->conf->getValue( "dsd", "base" ), $filter, "one" );


    for( $i = 0; $i < sizeof( $arrDSD ); $i++ ) {

      $sessionMembership = 0;
      for( $j = 0; $j < sizeof( $arrRole ); $j++ ) {

        // This counts how many roles of the DSD-set would be active
        for( $k = 0; $k < sizeof( $arrDSD[$i]['rbacsdrole'] ); $k++ ) {

          if( preg_match( "/^" . $arrRole[$j] . "$/i", $arrDSD[$i]['rbacsdrole'][$k] ) ) {

            $sessionMembership++;

          }

        }

      }


      // If one SSD-set disallows the assignment, it's enough
      $isAllowed = $isAllowed && ( $sessionMembership < $arrDSD[$i]['rbaccardinality'][0] );

    }


    if( !$isAllowed ) {

      // Change the security-context of the calling function
      $inContext->changeSecurityChain( false, "Not all DSD-constraints would be satisfied" );

    }


    return $inContext;

  }




  // ## checkAddActiveRole ############################################
  public function checkAddActiveRole( Context $inContext ) {

    $arrParameter = $inContext->getParameters();  // The parameters the assignUser-function got
    $userMembership = 0;                          // The number of roles the user is assigned to in DSD-set
    $isAllowed = true;                            // Is it allowed to assign the user to the role
    $user = $arrParameter[0];                     // Function-parameter
    $session = $arrParameter[1];                  // Function-parameter
    $role = $arrParameter[2];                     // Function-parameter
    $sessionMembership = 0;                       // A temporary count-variable
    $i = 0;                                       // Loop


    // Make sure the role has internal representation
    !$this->rbac->isIntRepresentation( $role ) ? $role = $this->rbac->roleExtToInt( $role ) : false;

    // The session has these roles in internal
    // representation (last parameter false)
    $arrSessionRole = $this->rbac->sessionRoles( $session, false );


    // Create a filter that gets all the SSD-sets
    // where the role is present the user would be assigned to
    $filter  = "(&" . $this->conf->getValue( "dsd", "filter" );
    $filter .= "(rbacsdrole=" . $role . "))";


    // Get these SSD-sets
    $arrDSD = $this->conn['dsd']->search( $this->conf->getValue( "dsd", "base" ), $filter, "one" );


    for( $i = 0; $i < sizeof( $arrDSD ); $i++ ) {

      $sessionMembership = 0;
      for( $j = 0; $j < sizeof( $arrSessionRole ); $j++ ) {

        // This counts how many roles of the DSD-set the
        // session has active
        for( $k = 0; $k < sizeof( $arrDSD[$i]['rbacsdrole'] ); $k++ ) {

          if( preg_match( "/^" . $arrSessionRole[$j] . "$/i", $arrDSD[$i]['rbacsdrole'][$k] ) ) {

            $sessionMembership++;

          }

        }

      }


      // If one SSD-set disallows the assignment, it's enough
      $isAllowed = $isAllowed && ( ($sessionMembership + 1) < $arrDSD[$i]['rbaccardinality'][0] );

    }


    if( !$isAllowed ) {

      // Change the security-context of the calling function
      $inContext->changeSecurityChain( false, "Not all DSD-constraints would be satisfied" );

    }


    return $inContext;

  }

}
?>
