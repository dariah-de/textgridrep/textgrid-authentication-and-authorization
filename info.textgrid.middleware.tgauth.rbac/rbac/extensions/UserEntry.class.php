<?php
// ####################################################################
// Version: 0.1.0
// Autor: Markus Widmer
// Erstellungsdatum: 03.11.2007
// Letzte Aenderung: 03.11.2007



class UserEntry extends RBACExtension {

  // ## Klassenvariablen ##############################################




  // ## Konstruktor ###################################################
  public function __construct( $inRBAC ) {

    // Let the extension do all the things
    // we dont't want to do
    parent::__construct( $inRBAC );

  }



  // ## registerEvents ################################################
  public function registerEvents( RBAC $inRegistrar ) {

    $inRegistrar->registerEventListener( "addUser", "write", $this, "changeUserEntry" );

  }




  // ## changeUserEntry ###############################################
  public function changeUserEntry( Context $inContext ) {

    $entry = $inContext->getValue( "entry" );


    // Make your own definitions here
    $entry['givenName'][0] = "Foo";



    $inContext->setValue( "entry", $entry );

    return $inContext;

  }

}
?>