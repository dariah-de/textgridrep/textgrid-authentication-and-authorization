<?php
interface iContext {

  public function setParameters( Array $inArrParameter );
  public function getParameters();
  public function setType( $inType );
  public function getType();
  public function setValue( $inName, $inValue );
  public function getValue( $inName );
  public function changeSecurityChain( $inContinue, $inReason = null );
  public function getSecurityChain();
  public function getSecurityChainReason();

}
?>