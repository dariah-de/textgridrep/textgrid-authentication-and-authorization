<?php
// ####################################################################
// Version: 0.1.1
// Autor: Markus Widmer
// Erstellungsdatum: 02.11.2007
// Letzte Aenderung: 03.11.2007



class Context implements iContext {

  // ## Klassenvariablen ##############################################
  private $arrParameter = Array();
  private $arrData = Array();
  private $type;
  private $continue = true;
  private $securityChainReason;




  // ## Konstruktor ###################################################
  public function __construct() {

    $this->type = "none";

  }




  // ## setParameters #################################################
  public function setParameters( Array $inArrParameter ) {

    $this->arrParameter = $inArrParameter;

  }




  // ## getParameters #################################################
  public function getParameters() {

    if( is_array( $this->arrParameter ) ) {

      return $this->arrParameter;

    }
    else {

      return Array();

    }

  }




  // ## setType #######################################################
  public function setType( $inType ) {

    $this->type = $inType;

  }




  // ## getType #######################################################
  public function getType() {

    return $this->type;

  }




  // ## setValue ######################################################
  public function setValue( $inName, $inValue ) {

    $this->arrData[$inName] = $inValue;

  }




  // ## getValue ######################################################
  public function getValue( $inName ) {

    if( isset( $this->arrData[$inName] ) ) {

      return $this->arrData[$inName];

    }
    else {

      return null;

    }

  }




  // ## changeSecurityChain ###########################################
  public function changeSecurityChain( $inContinue, $inReason = null ) {

    if( is_bool( $inContinue ) ) {

      $this->continue = $inContinue;
      $inReason != null ? $this->securityChainReason = $inReason : false;


      return true;

    }
    else {

      return false;

    }

  }




  // ## getSecurityChain ##############################################
  public function getSecurityChain() {

    return $this->continue;

  }




  // ## getSecurityChainReason ########################################
  public function getSecurityChainReason() {

    return $this->securityChainReason;

  }

}
?>