<?php
// ####################################################################
// Version: 0.1.0
// Autor: Markus Widmer
// Erstellungsdatum: 31.07.2007
// Letzte Aenderung: 31.07.2007




class SimpleConfig {

  // ## Klassenvariablen ##############################################
  private $config;




  // ## Konstruktor ###################################################
  public function __construct( $inConfigurationFile ) {

    $file;          // File-handler
    $content = "";  // Content of the XML-file
    $xmlConf;
    $xmlSection;


    if( file_exists( $inConfigurationFile ) ) {

      $file = fopen( $inConfigurationFile, "r" );
      $content = fread( $file, filesize( $inConfigurationFile ) + 64 );

    }
    else {

      throw new Exception( "File not found: " . $inConfigurationFile );

    }


    // Read the configuration-file and store all the values
    $xmlConf = new XML();
    $xmlConf->parse( $content, "conf" );
    $xmlConf = $xmlConf->getRoot( "conf" );
    $xmlConf = $xmlConf[0];


    for( $i = 0; $i < $xmlConf->countChilds( "SECTION" ); $i++ ) {

      $xmlSection = $xmlConf->getChild( "SECTION", $i );


      for( $j = 0; $j < $xmlSection->countChilds( "VAR" ); $j++ ) {

        if( preg_match( "/^[a-z0-9_-]+$/i", $xmlSection->getChild( "VAR", $j )->getAttribute( "name" ) ) ) {

          $this->config[$xmlSection->getAttribute( "name" )]
                       [$xmlSection->getChild( "VAR", $j )->getAttribute( "name" )] = $xmlSection->getChild( "VAR", $j )->getValue();

        }

      }

    }

  }




  // ## getValue ######################################################
  public function getValue( $inSection, $inName ) {

    if( isset( $this->config[$inSection][$inName] ) ) {
    
      return $this->config[$inSection][$inName];

    }
    else {

      return false;

    }

  }




  // ## getDefined ####################################################
  public function getDefined( $inSection ) {

    $arrDef = Array();


    foreach( $this->config[$inSection] as $key => $value ) {

      $arrDef[] = $key;

    }


    return $arrDef;

  }

}
?>
