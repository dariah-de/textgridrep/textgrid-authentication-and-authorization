<?php
// ####################################################################
// Version: 0.1.0
// Autor: Markus Widmer
// Erstellungsdatum: 31.07.2007
// Letzte Aenderung: 31.07.2007




class RBACException extends Exception {

  // ## Klassenvariablen ##############################################




  // ## Konstruktor ###################################################
  public function __construct( $inMessage, $inCode ) {

    parent::__construct( $inMessage, $inCode );

  }




  // ## __toString ####################################################
  public function __toString() {

    return get_class() . ": [{$this->code}]: {$this->message}\n";

  }

}
?>
