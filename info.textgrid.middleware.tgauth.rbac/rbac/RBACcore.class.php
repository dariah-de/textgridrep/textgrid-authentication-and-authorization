<?php
// ####################################################################
// Version: 0.2.7
// Author: Markus Widmer
// Created: 31.07.2007
// Modified: 28.01.2019


// Requiring these interfaces if the RBAC-Framework
// is not existing. Otherwise the RBAC-Framework
// takes care of importing all nevessarry classes and
// interfaces.
if( !class_exists( "RBAC" ) ) {

  if(    defined( "RBAC_LIB_PATH" )
      && defined( "RBAC_PATH" ) ) {

    !interface_exists( "iHelper" ) ? require_once( RBAC_LIB_PATH . "/iHelper.interface.php" ) : false;
    !interface_exists( "iNode" ) ? require_once( RBAC_LIB_PATH . "/iNode.interface.php" ) : false;
    !interface_exists( "iXML" ) ? require_once( RBAC_LIB_PATH . "/iXML.interface.php" ) : false;
    !interface_exists( "iLDAP" ) ? require_once( RBAC_LIB_PATH . "/iLDAP.interface.php" ) : false;
    !interface_exists( "iCrypto" ) ? require_once( RBAC_LIB_PATH . "/iCrypto.interface.php" ) : false;

    !interface_exists( "iContext" ) ? require_once( RBAC_PATH . "/iContext.interface.php" ) : false;
    !interface_exists( "iRBACcore" ) ? require_once( RBAC_PATH . "/iRBACcore.interface.php" ) : false;


    !class_exists( "Helper" ) ? require_once( RBAC_LIB_PATH . "/Helper.class.php" ) : false;
    !class_exists( "Node" ) ? require_once( RBAC_LIB_PATH . "/Node.class.php" ) : false;
    !class_exists( "XML" ) ? require_once( RBAC_LIB_PATH . "/XML.class.php" ) : false;
    !class_exists( "LDAP" ) ? require_once( RBAC_LIB_PATH . "/LDAP.class.php" ) : false;
    !class_exists( "Crypto" ) ? require_once( RBAC_LIB_PATH . "/Crypto.class.php" ) : false;

    !class_exists( "SimpleConfig" ) ? require_once( RBAC_PATH . "/SimpleConfig.class.php" ) : false;
    !class_exists( "RBACException" ) ? require_once( RBAC_PATH . "/RBACException.class.php" ) : false;
    !class_exists( "RBACExtension" ) ? require_once( RBAC_PATH . "/RBACExtension.class.php" ) : false;
    !class_exists( "Context" ) ? require_once( RBAC_PATH . "/Context.class.php" ) : false;

  }
  else {

    exit( "\nYou have to define the constants RBAC_LIB_PATH and RBAC_PATH.\n" );

  }

}


class RBACcore implements iRBACcore {

  // ## Klassenvariablen ##############################################
  protected $conf;
  protected $conn;
  protected $arrEntryStorage = Array();
  protected $interceptor = false;




  // ## Konstruktor ###################################################
  public function __construct( $inConfigurationFile, RBAC $inInterceptor = null ) {

    $file;          // File-handler
    $content = "";  // Content of the XML-file


    if( $inInterceptor instanceof RBAC ) {

      $this->interceptor = $inInterceptor;

    }

    // Read the configuration
    $this->conf = new SimpleConfig( $inConfigurationFile );


    // Create a user connection
    $this->conn['user'] = new LDAP();
    $this->conn['user']->connect( $this->conf->getValue( "user", "host" ),
                                  $this->conf->getValue( "user", "port" ),
                                  $this->conf->getValue( "user", "version" ),
                                  preg_match( "/(^yes$)|(^true$)/i",
                                  $this->conf->getValue( "user", "tls" ) ) );
    $this->doBind( "user" );


    // Create a role connection
    if( $this->sameConnectionParams( "user", "role" ) ) {

      $this->conn['role'] = $this->conn['user'];

    }
    else {

      $this->conn['role'] = new LDAP();
      $this->conn['role']->connect( $this->conf->getValue( "role", "host" ),
                                    $this->conf->getValue( "role", "port" ),
                                    $this->conf->getValue( "role", "version" ),
                                    preg_match( "/(^yes$)|(^true$)/i",
                                    $this->conf->getValue( "role", "tls" ) ) );
      $this->doBind( "role" );

    }


    // Create a session connection
    if( $this->sameConnectionParams( "session", "user" ) ) {

      $this->conn['session'] = $this->conn['user'];

    }
    elseif( $this->sameConnectionParams( "session", "role" ) ) {

      $this->conn['session'] = $this->conn['role'];

    }
    else {

      $this->conn['session'] = new LDAP();
      $this->conn['session']->connect( $this->conf->getValue( "session", "host" ),
                                       $this->conf->getValue( "session", "port" ),
                                       $this->conf->getValue( "session", "version" ),
                                       preg_match( "/(^yes$)|(^true$)/i",
                                       $this->conf->getValue( "session", "tls" ) ) );
      $this->doBind( "session" );

    }


    // Create a resource connection
    if( $this->sameConnectionParams( "resource", "user" ) ) {

      $this->conn['resource'] = $this->conn['user'];

    }
    elseif( $this->sameConnectionParams( "resource", "role" ) ) {

      $this->conn['resource'] = $this->conn['role'];

    }
    elseif( $this->sameConnectionParams( "resource", "session" ) ) {

      $this->conn['resource'] = $this->conn['session'];

    }
    else {

      $this->conn['resource'] = new LDAP();
      $this->conn['resource']->connect( $this->conf->getValue( "resource", "host" ),
                                        $this->conf->getValue( "resource", "port" ),
                                        $this->conf->getValue( "resource", "version" ),
                                        preg_match( "/(^yes$)|(^true$)/i",
                                        $this->conf->getValue( "resource", "tls" ) ) );
      $this->doBind( "resource" );

    }

  }




  // ## __wakeup ######################################################
  private function __wakeup() {

    $this->doBind( "user" );


    if( !$this->sameConnectionParams( "role", "user" ) ) {

      $this->doBind( "role" );

    }


    if(    !$this->sameConnectionParams( "resource", "user" )
        && !$this->sameConnectionParams( "resource", "role" ) ) {

      $this->doBind( "resource" );

    }


    if(    !$this->sameConnectionParams( "session", "user" )
        && !$this->sameConnectionParams( "session", "role" )
        && !$this->sameConnectionParams( "session", "resource" ) ) {

      $this->doBind( "session" );

    }

  }




  // ## doBind ########################################################
  protected function doBind( $inConnectionName ) {

    $this->conn[$inConnectionName]->bind( $this->conf->getValue( $inConnectionName, "binddn" ),
                                          $this->conf->getValue( $inConnectionName, "password" ) );

  }




  // ## sameConnectionParams ##########################################
  protected function sameConnectionParams( $inNeedle, $inHaystack  ) {

    $same = true;  // The return value


    $same = $same & ( preg_match( "/^" . $this->conf->getValue( $inNeedle, "host" ) . "$/i",
                                  $this->conf->getValue( $inHaystack, "host" ) ) );

    $same = $same & ( preg_match( "/^" . $this->conf->getValue( $inNeedle, "port" ) . "$/i",
                                  $this->conf->getValue( $inHaystack, "port" ) ) );

    $same = $same & ( preg_match( "/^" . $this->conf->getValue( $inNeedle, "version" ) . "$/i",
                                  $this->conf->getValue( $inHaystack, "version" ) ) );

    $same = $same & ( preg_match( "/^" . $this->conf->getValue( $inNeedle, "tls" ) . "$/i",
                                  $this->conf->getValue( $inHaystack, "tls" ) ) );


    return $same;

  }




  // ## isIntepresentation ###########################################
  public function isIntRepresentation( $inRole ) {

    return preg_match( "/^\s*(.+=.+,\s*)+\s*" . $this->conf->getValue( "role", "base" ) . "\s*$/i",
                       $inRole );

  }



  // ## roleIntToExt ##################################################
  public function roleIntToExt( $inRole ) {

    $arrTmpSplit;  // Temporary var containing role-hirarchy
    $i = 0;        // Loop


    if( $this->isIntRepresentation( $inRole ) ) {

      // Cut off the base-DN
      $inRole = preg_replace( "/,\s*" . $this->conf->getValue( "role", "base" ) . "$/i", "", $inRole );


      // Split by comma
      $arrTmpSplit = preg_split( "/[,]/", $inRole );
      
      
      // Remove the naming-attribute
      for( $i = 0; $i < sizeof( $arrTmpSplit ); $i++ ) {

        $arrTmpSplit[$i] = trim( $arrTmpSplit[$i] );
        $arrTmpSplit[$i] = preg_replace( "/^" . $this->conf->getValue( "role", "namingattribute" )
                                              . "=/i",
                                         "", $arrTmpSplit[$i] );

      }


      // Reconstruct the role
      return join( ",", $arrTmpSplit );

    }
    else {

      return false;

    }

  }




  // ## roleExtToInt ##################################################
  public function roleExtToInt( $inRole ) {

    $arrTmpSplit;  // Temporary var containing role-hirarchy
    $i = 0;        // Loop


    if( !$this->isIntRepresentation( $inRole ) ) {

      // Split by comma
      $arrTmpSplit = preg_split( "/[,]/", $inRole );

     
      // Add the naming-attribute
      for( $i = 0; $i < sizeof( $arrTmpSplit ); $i++ ) {

        $arrTmpSplit[$i] = $this->conf->getValue( "role", "namingattribute" ) . "=" . $arrTmpSplit[$i];

      }


      return join( ",", $arrTmpSplit ) . "," . $this->conf->getValue( "role", "base" );

    }
    else {

      return false;

    }

  }




  // ## createSession #################################################
  public function createSession( $inUsername, Array $inRole, $inSession ) {

    $context = false;    // Possibly a Context-object
    $arrUser;            // The users entries
    $arrSession;         // The sessions entries
    $arrRole;            // The roles that the user is assigned to
    $sessionDn = "";     // The DN of the new session
    $arrSessionEntry;    // The sessions entry definition
    $roleOk = false;     // Temporary role-check
    $roleOkGlob = true;  // Are all roles ok
    $filter = "";        // Filterstring
    $continue = true;    // The session-creation is allowed by default
    $i = 0;              // Loop
    $j = 0;              // Loop

    
    // If an interceptor is defined, we need to
    // provide a context and events
    if( $this->interceptor ) {

      $context = new Context();

    }


    // Construct a filter to search for the users entry
    $filter  = "(&" . $this->conf->getValue( "user", "filter" );
    $filter .= "(" . $this->conf->getValue( "user", "namingattribute" ) . "=" . $inUsername . "))";


    // Get the users entry
    $arrUser = $this->conn['user']->search( $this->conf->getValue( "user", "base" ), $filter, "sub",
                                            Array( $this->conf->getValue( "user", "namingattribute" ) ) );


    // Construct a filter to search for the session entry
    $filter  = "(&" . $this->conf->getValue( "session", "filter" );
    $filter .= "(" . $this->conf->getValue( "session", "namingattribute" ) . "=" . $inSession . "))";


    // Get the sessions entry
    $arrSession = $this->conn['session']->search( $this->conf->getValue( "session", "base" ), $filter,
                                                  "one", Array( $this->conf->getValue( "session", "namingattribute" ) ) );


    // The user has to exist, but not the session
    if( $arrUser && sizeof( $arrUser ) == 1) {

      if((!$arrSession || sizeof( $arrSession ) == 0) && preg_match( "/.+/", $inSession ) ) {

        $arrRole = $this->authorizedRoles( $inUsername, false );


        // Make sure the given roles are in
        // internal representation
        for( $i = 0; $i < sizeof( $inRole ); $i++ ) {

          if( !$this->isIntRepresentation( $inRole[$i] ) ) {

            $inRole[$i] = $this->roleExtToInt( $inRole[$i] );

          }

        }


        // Check if the given roleset is ok
        for( $i = 0; $i < sizeof( $inRole ); $i++ ) {

          $roleOk = false;
          for( $j = 0; $j < sizeof( $arrRole ); $j++ ) {

            // The input-role has to match with one of the
            // users roles
            $roleOk = $roleOk || preg_match( "/^" . $arrRole[$j] . "$/i", $inRole[$i] );

          }


          // Every input-role has to have matched
          // at least once!
          $roleOkGlob = $roleOkGlob && $roleOk;

        }


        // If the roleset is ok
        if( $roleOkGlob ) {

          $sessionDn  = $this->conf->getValue( "session", "namingattribute" ) . "=" . $inSession . ",";
          $sessionDn .= $this->conf->getValue( "session", "base" );

          $arrSessionEntry[$this->conf->getValue( "session", "namingattribute" )][0] = $inSession;
          $arrSessionEntry['objectclass'][0] = "rbacSession";
          $arrSessionEntry['rbacSessionUser'][0] = $inUsername;
          $arrSessionEntry['rbacSessionCreationTimestamp'][0] = date( "YmdHis", time() ) . "Z";
          $inRole && sizeof( $inRole ) > 0 ? $arrSessionEntry['rbacSessionRole'] = $inRole : false;


          // -----------
          // -- EVENT --
          // The session-entry is defined and is going to be
          // added to the directory. Maybe someone wants to
          // change the entry itself or deny this action.
          // -----------
          if( $this->interceptor ) {

            // Create context
            $context->setValue( "entry", $arrSessionEntry );
            $context->setValue( "dn", $sessionDn );

            // Give away the context
            $context = $this->interceptor->event( "createSession", "write", $context );

            // Use the returned context
            $sessionDn = $context->getValue( "dn" );
            $arrSessionEntry = $context->getValue( "entry" );
            $continue = $context->getSecurityChain();

          }


          if( $continue ) {

            if( $this->conn['session']->add( $sessionDn, $arrSessionEntry ) ) {

              return true;

            }
            else {

              throw new RBACException( $this->conf->getValue( "errorDescription", "LDAP_ERROR" ),
                                       $this->conf->getValue( "errorCode", "LDAP_ERROR" ) );

            }

          }
          else {

            return false;

          }

        }
        else {

          throw new RBACException( $this->conf->getValue( "errorDescription", "USER_ROLE_ERROR" ),
                                   $this->conf->getValue( "errorCode", "USER_ROLE_ERROR" ) );

        }

      }
      else {

        throw new RBACException( $this->conf->getValue( "errorDescription", "SESSION_ALLREADY_EXISTS" ),
                                 $this->conf->getValue( "errorCode", "SESSION_ALLREADY_EXISTS" ) );

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "USER_UNKNOWN" ),
                               $this->conf->getValue( "errorCode", "USER_UNKNOWN" ) );

    }

  }


  // ## deleteSession #################################################
  public function deleteSession( $inUsername, $inSession ) {

    $arrSession;   // The sessions entries
    $filter = "";  // Filterstring


    // Construct a filter to search for the session entry
    $filter  = "(&" . $this->conf->getValue( "session", "filter" );
    $filter .= "(" . $this->conf->getValue( "session", "namingattribute" ) . "=" . $inSession . "))";


    // Get the sessions entry
    $arrSession = $this->conn['session']->search( $this->conf->getValue( "session", "base" ),
                                                  $filter, "one" );


    // The has has to exist
    if( $arrSession && sizeof( $arrSession ) == 1 && preg_match( "/.+/", $inSession ) ) {

      // The user has to be the owner of the session
      if( preg_match( "/^" . $arrSession[0]['rbacsessionuser'][0] . "$/i", $inUsername ) ) {

        // Delete the session
        if( $this->conn['session']->delete( $arrSession[0]['dn'] ) ) {

            return true;

        }
        else {

          throw new RBACException( $this->conf->getValue( "errorDescription", "LDAP_ERROR" ),
                                   $this->conf->getValue( "errorCode", "LDAP_ERROR" ) );

        }

      }
      else {

        throw new RBACException( $this->conf->getValue( "errorDescription", "SESSION_DOES_NOT_EXISTS" ),
                                 $this->conf->getValue( "errorCode", "SESSION_DOES_NOT_EXISTS" ) );

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "SESSION_DOES_NOT_EXISTS" ),
                               $this->conf->getValue( "errorCode", "SESSION_DOES_NOT_EXISTS" ) );

    }

  }




  // ## addActiveRole #################################################
  public function addActiveRole( $inUser, $inSession, $inRole ) {

    $context = false;  // Possibly a Context-object
    $arrSessionEntry;  // The session entry from the directory
    $arrRole;          // The users roles
    $sessionDn = "";   // The session-DN
    $roleOk = false;   // Is the role assigned to the user?
    $continue = true;  // It is allowed to add the role by default
    $i = 0;            // Loop


    // If an interceptor is defined, we need to
    // provide a context and events
    if( $this->interceptor ) {

      $context = new Context();

    }


    $sessionDn  = $this->conf->getValue( "session", "namingattribute" ) . "=" . $inSession . ",";
    $sessionDn .= $this->conf->getValue( "session", "base" );
    $arrSessionEntry = $this->conn['session']->getEntry( $sessionDn );
    

    if( isset( $arrSessionEntry['dn'] ) ) {

      // The given user has to be equal with the sessions
      // user.
//      if( preg_match( "/^" . $inUser . "$/i", $arrSessionEntry['rbacsessionuser'][0] ) ) {
      if( strtolower( $inUser ) == strtolower( $arrSessionEntry['rbacsessionuser'][0] ) ) {

        $arrRole = $this->authorizedRoles( $inUser, false );
        // Make sure the role has internal representation
        !$this->isIntRepresentation( $inRole ) ? $inRole = $this->roleExtToInt( $inRole ) : false;
        // Check the role really is assigned to the user
        for( $i = 0; $i < sizeof( $arrRole ); $i++ ) {

          $roleOk = $roleOk || preg_match( "/^" . $inRole . "$/i", $arrRole[$i] );

        }


        if( $roleOk ) {

          // Add the role to the roleset or create a new roleset.
          if( is_array( $arrSessionEntry['rbacsessionrole'] ) ) {

            // The role must not be in the roleset more than once.
            if( !in_array( $inRole, $arrSessionEntry['rbacsessionrole'] ) ) {

              $arrSessionEntry['rbacsessionrole'][] = $inRole;

            }

          }
          else {

            $arrSessionEntry['rbacsessionrole'] = Array( $inRole );

          }


          // -----------
          // -- EVENT --
          // The user-entry is defined and is going to be
          // added to the directory. Maybe someone wants to
          // change the entry itself or deny this action.
          // -----------
          if( $this->interceptor ) {

            // Create context
            $context->setValue( "arrSessionRole", $arrSessionEntry['rbacsessionrole'] );
            $context->setValue( "dn", $sessionDn );

            // Give away the context
            $context = $this->interceptor->event( "addActiveRole", "write", $context );

            // Use the returned context
            $sessionDn = $context->getValue( "dn" );
            $arrSessionEntry['rbacsessionrole'] = $context->getValue( "arrSessionRole" );
            $continue = $context->getSecurityChain();

          }


          if( $continue ) {

            // Commit the new roleset
            if( $this->conn['session']->modify( $sessionDn, Array( "rbacsessionrole" => $arrSessionEntry['rbacsessionrole'] ) ) ) {

              return true;

            }
            else {

              return false;

            }

          }
          else {

            return false;

          }

        }
        else {

          throw new RBACException( $this->conf->getValue( "errorDescription", "USER_ROLE_ERROR" ),
                                   $this->conf->getValue( "errorCode", "USER_ROLE_ERROR" ) );

        }

      }
      else {

        throw new RBACException( $this->conf->getValue( "errorDescription", "USER_SESSION_ERROR" ),
                                 $this->conf->getValue( "errorCode", "USER_SESSION_ERROR" ) );

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "SESSION_DOES_NOT_EXISTS" ),
                               $this->conf->getValue( "errorCode", "SESSION_DOES_NOT_EXISTS" ) );

    }

  }




  // ## dropActiveRole ################################################
  public function dropActiveRole( $inUser, $inSession, $inRole ) {

    $arrSessionEntry;  // The session entry from the directory
    $arrActiveRole = Array();    // The users roles
    $sessionDn = "";   // The session-DN
    $roleOk = false;   // Is the role assigned to the user?
    $i = 0;            // Loop


    $sessionDn  = $this->conf->getValue( "session", "namingattribute" ) . "=" . $inSession . ",";
    $sessionDn .= $this->conf->getValue( "session", "base" );


    $arrSessionEntry = $this->conn['session']->getEntry( $sessionDn );


    if( isset( $arrSessionEntry['dn'] ) ) {

      // Make sure the role has internal representation
      !$this->isIntRepresentation( $inRole ) ? $inRole = $this->roleExtToInt( $inRole ) : false;


      // Drop the role from the roleset.
      for( $i = 0; $i < sizeof( $arrSessionEntry['rbacsessionrole'] ); $i++ ) {

        if( !preg_match( "/^" . $inRole . "$/i", $arrSessionEntry['rbacsessionrole'][$i] ) ) {

          $arrActiveRole[] = $arrSessionEntry['rbacsessionrole'][$i];

        }

      }


      // Commit the new roleset
      if( $this->conn['session']->modify( $sessionDn, Array( "rbacsessionrole" => $arrActiveRole ) ) ) {

        return true;

      }
      else {

        return false;

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "SESSION_DOES_NOT_EXISTS" ),
                               $this->conf->getValue( "errorCode", "SESSION_DOES_NOT_EXISTS" ) );

    }

  }




  // ## assignedRoles #################################################
  public function assignedRoles( $inUsername, $inExtRepresentation = true ) {

    $filter = "";              // Filterstring
    $arrRole;                  // The roles that the user is assigned to
    $arrRoleReturn = Array();  // The roles in the correct format
    $i = 0;                    // Loop


    $filter  = "(&" . $this->conf->getValue( "role", "filter" );
    $filter .= "(" . $this->conf->getValue( "role", "assignedattribute" ) . "=" . $inUsername . "))";


    // Search only with scope "one" becaue we are in Core-RBAC
    $arrRole = $this->conn['role']->search( $this->conf->getValue( "role", "base" ), $filter,
                                            "sub", Array( $this->conf->getValue( "role", "namingattribute" ) ) );


    for( $i = 0; $i < sizeof( $arrRole ); $i++ ) {

      $arrRoleReturn[] = $arrRole[$i]['dn'];

    }


    // Convert the roles to the external representation
    if( $inExtRepresentation ) {

      for( $i = 0; $i < sizeof( $arrRoleReturn ); $i++ ) {

        $arrRoleReturn[$i] = $this->roleIntToExt( $arrRoleReturn[$i] );

      }

    }


    return $this->removeDuplicates( $arrRoleReturn );

  }




  // ## assignedUsers #################################################
  public function assignedUsers( $inRole, $inUseStoredRole = false ) {

    $filter = "";              // Filterstring
    $arrUser = Array();        // The users that are assigned to the role
    $arrRoleEntry;             // The roles entry
    $i = 0;                    // Loop

    // Make sure the role has internal representation
    !$this->isIntRepresentation( $inRole ) ? $inRole = $this->roleExtToInt( $inRole ) : false;


    if( $inUseStoredRole && isset( $this->arrEntryStorage['assignedusersrole'][$inRole] ) ) {

      $arrRoleEntry = $this->arrEntryStorage['assignedusersrole'][$inRole];

    }
    else {

      // Get the role-entry
      $arrRoleEntry = $this->conn['role']->getEntry( $inRole );
      
      $this->arrEntryStorage['assignedusersrole'][$inRole] = $arrRoleEntry;

    }


    if( isset( $arrRoleEntry['dn'] ) ) {

      $arrUser = $arrRoleEntry[$this->conf->getValue( "role", "assignedattribute" )];


    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "ROLE_UNKNOWN" ),
                               $this->conf->getValue( "errorCode", "ROLE_UNKNOWN" ) );

    }


    return $arrUser;

  }




  // ## rolePermissions ###############################################
  public function rolePermissions( $inRole ) {

    $arrResource;              // The resources the role has a permission on
    $arrPermission = Array();  // The permissions found
    $arrTmpSplit;              // Splitted string
    $filter = "";              // Filterstring
    $i = 0;                    // Loop
    $j = 0;                    // Loop

    // Make sure the role has internal representation
    !$this->isIntRepresentation( $inRole ) ? $inRole = $this->roleExtToInt( $inRole ) : false;


    // Get the role-entry
    $arrRole = $this->conn['role']->getEntry( $inRole );


    if( isset( $arrRole['dn'] ) ) {

      $filter  = "(&" . $this->conf->getValue( "resource", "filter" );
      $filter .= "(rbacpermission=" . $inRole . ":-:*))";


      $arrResource = $this->conn['resource']->search( $this->conf->getValue( "resource", "base" ),
                                                      $filter, "sub",
                                                      Array( $this->conf->getValue( "resource", "aliasattribute" ),
                                                             $this->conf->getValue( "resource", "namingattribute" ),
                                                             "rbacpermission" ) );


      for( $i = 0; $i < sizeof( $arrResource ); $i++ ) {

        for( $j = 0; $j < sizeof( $arrResource[$i]['rbacpermission'] ); $j++ ) {

          $arrTmpSplit = preg_split( "/:-:/", $arrResource[$i]['rbacpermission'][$j] );


          if( preg_match( "/^" . $arrTmpSplit[0] . "$/i", $inRole ) ) {

            $arrPermission[] = Array( "resource" => $arrResource[$i][$this->conf->getValue( "resource", "namingattribute" )][0],
                                      "alias" => $arrResource[$i][$this->conf->getValue( "resource", "aliasattribute" )],
                                      "operation" => $arrTmpSplit[1] );

          }

        }

      }


      return $arrPermission;

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "ROLE_UNKNOWN" ),
                               $this->conf->getValue( "errorCode", "ROLE_UNKNOWN" ) );

    }

  }




  // ## userPermissions ###############################################
  public function userPermissions( $inUsername ) {

    $arrRole;                      // The roles of the user
    $arrTmpPermission;             // Temporary permission of a role
    $arrUserPermission = Array();  // The permissions of the user
    $i = 0;                        // Loop

    // Get the users roles
    $arrRole = $this->authorizedRoles( $inUsername, false );


    for( $i = 0; $i < sizeof( $arrRole ); $i++ ) {

      $arrTmpPermission = $this->rolePermissions( $arrRole[$i] );


      $arrUserPermission = array_merge( $arrUserPermission, $arrTmpPermission );

    }


    return $arrUserPermission;

  }




  // ## sessionPermissions ############################################
  public function sessionPermissions( $inSession ) {

    $arrRole;                         // The roles of the session
    $arrTmpPermission;                // Temporary permission of a role
    $arrSessionPermission = Array();  // The permissions of the user
    $i = 0;                           // Loop

    // Get the session's roles
    try {

      $arrRole = $this->sessionRoles( $inSession, false );

    }
    catch( Exception $e ) {

      $arrRole = Array();

    }


    for( $i = 0; $i < sizeof( $arrRole ); $i++ ) {

      $arrTmpPermission = $this->rolePermissions( $arrRole[$i] );


      $arrSessionPermission = array_merge( $arrSessionPermission, $arrTmpPermission );

    }


    return $arrSessionPermission;

  }




  // ## roleOperationsOnObject ########################################
  public function roleOperationsOnObject( $inRole, $inResource ) {

    return $this->roleOperationsOnObjectInternal( $inRole, $inResource );

  }




  // ## roleOperationsOnObjectInternal ################################
  protected function roleOperationsOnObjectInternal( $inRole, $inResource, $inUseStoredResource = false ) {

    $arrRole;                 // The roles of the session
    $filter = "";             // Filter to search for the resource
    $arrTmpSplit;             // Temporary split of the permission-string
    $arrOperation = Array();  // The resulting operations
    $i = 0;                   // Loop


    // Make sure the role has internal representation
    !$this->isIntRepresentation( $inRole ) ? $inRole = $this->roleExtToInt( $inRole ) : false;


    // Get the users roles
    $arrRole = $this->conn['role']->getEntry( $inRole );


    if( isset( $arrRole['dn'] ) ) {

      // This functionality stores the resource and re-uses it
      // if said so. This saves an enourmous number of queries
      // to the ldap-directory especially if this method is
      // called from "OnObject".
      if( $inUseStoredResource && isset( $this->arrEntryStorage['roleoperationsonobjectinternal'] ) ) {

        $arrResource = $this->arrEntryStorage['roleoperationsonobjectinternal'];

      }
      else {

        // Create a filter to search for the resource
        $filter  = "(&" . $this->conf->getValue( "resource", "filter" );
        $filter .= "(|(" . $this->conf->getValue( "resource", "namingattribute" ) . "=" . $inResource . ")";
        $filter .= "  (" . $this->conf->getValue( "resource", "aliasattribute" ) . "=" . $inResource . ")))";


        // Ask the directory
        $arrResource = $this->conn['resource']->search( $this->conf->getValue( "resource", "base" ), $filter, Array( "rbacpermission" ) );


        // Store the entry
        is_array( $arrResource ) && sizeof( $arrResource ) == 1 ? $this->arrEntryStorage['roleoperationsonobjectinternal'] = $arrResource : false;

      }


      // There must not be more or less than 1 resource. If
      // there are more, the resource is unknown because it is
      // not uniqueue.
      if( is_array( $arrResource ) && sizeof( $arrResource ) == 1 ) {

        if( array_key_exists( "rbacpermission", $arrResource[0] ) ) {

          // Check all permissions if it is a permission of
          // the given role. If so, keep the operation in mind.
          for( $i = 0; $i < sizeof( $arrResource[0]['rbacpermission'] ); $i++ ) {

            $arrTmpSplit = preg_split( "/:-:/", $arrResource[0]['rbacpermission'][$i] );


            if( preg_match( "/^" . $arrTmpSplit[0] . "$/i", $inRole ) ) {

              $arrOperation[] = $arrTmpSplit[1];

            }

	  }

	}

      }
      else {

        throw new RBACException( $this->conf->getValue( "errorDescription", "RESOURCE_UNKNOWN" ),
                                 $this->conf->getValue( "errorCode", "RESOURCE_UNKNOWN" ) );

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "ROLE_UNKNOWN" ),
                               $this->conf->getValue( "errorCode", "ROLE_UNKNOWN" ) );

    }


    return $this->removeDuplicates( $arrOperation );

  }




  // ## userOperationsOnObject ########################################
  public function userOperationsOnObject( $inUser, $inResource ) {

    $context = false;           // Possibly a Context-object
    $arrAuthRole;               // The users authorized roles
    $arrOperation = Array();    // The resulting operations
    $arrTmpOperation = Array(); // Temporary array of operations
    $i = 0;                     // Loop

    // If an interceptor is defined, we need to
    // provide a context and events
    if( $this->interceptor ) {

      $context = new Context();

    }


    if( preg_match( "/.+/", $inUser ) ) {
    
      // Get the users authorized roles
      $arrAuthRole = $this->authorizedRoles( $inUser, false );


      if( is_array( $arrAuthRole ) ) {

        for( $i = 0; $i < sizeof( $arrAuthRole ); $i++ ) {

          // Get the operations the user has because of role i
          $arrTmpOperation = $this->roleOperationsOnObjectInternal( $arrAuthRole[$i], $inResource, true );

          // Only merge the result of the above call if it really is
          // an array.
          is_array( $arrTmpOperation ) ? $arrOperation = array_merge( $arrOperation, $arrTmpOperation ) : false;

        }

      }

    }

    // -----------
    // -- EVENT --
    // The operations are ready to return, but
    // maybe someone wants to add or remove one.
    // -----------
    if( $this->interceptor ) {
      // Create context
      $context->setValue( "arrOperation", $arrOperation );

      // Give away the context
      $context = $this->interceptor->event( "userOperationsOnObject", "finish", $context );

      // Use the returned context
      $arrOperation = $context->getValue( "arrOperation" );
      $continue = $context->getSecurityChain();

    }


    return $this->removeDuplicates( $arrOperation );

  }




  // ## sessionRoles ##################################################
  public function sessionRoles( $inSession, $inExtRepresentation = true ) {

    $arrRoleReturn = Array();  // The result
    $arrSessionEntry;          // The entry of the session
    $sessionDn = "";           // The sessions DN
    $filter = "";              // Filterstring
    $i = 0;                    // Loop

    if( preg_match( "/.+/", $inSession ) ) {

      $sessionDn  = $this->conf->getValue( "session", "namingattribute" ) . "=" . $inSession . ",";
      $sessionDn .= $this->conf->getValue( "session", "base" );


      $arrSessionEntry = $this->conn['session']->getEntry( $sessionDn );



      // The session has to exist to get its roles
      if( isset( $arrSessionEntry['dn'] ) ) {

        is_array( $arrSessionEntry['rbacsessionrole'] ) ? $arrRoleReturn = $arrSessionEntry['rbacsessionrole'] : $arrRoleReturn = Array();

      }
      else {

        throw new RBACException( $this->conf->getValue( "errorDescription", "SESSION_DOES_NOT_EXISTS" ),
                                 $this->conf->getValue( "errorCode", "SESSION_DOES_NOT_EXISTS" ) );

      }

      
      // Convert the roles to the external representation
      if($arrRoleReturn && $inExtRepresentation ) {

        for( $i = 0; $i < sizeof( $arrRoleReturn ); $i++ ) {

          $arrRoleReturn[$i] = $this->roleIntToExt( $arrRoleReturn[$i] );

        }

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "SESSION_DOES_NOT_EXISTS" ),
                               $this->conf->getValue( "errorCode", "SESSION_DOES_NOT_EXISTS" ) );

    }
    return $arrRoleReturn;

  }




  // ## checkAccess ###################################################
  public function checkAccess( $inSession, $inOperation, $inResource ) {
      
    $context = false;  // Possibly a Context-object
    $filter = "";      // Filterstring
    $arrSessionRole;   // The sessions active roleset
    $i = 0;            // Loop

    // If an interceptor is defined, we need to
    // provide a context and events
    if( $this->interceptor ) {

      $context = new Context();

    }

    try {
      $arrSessionRole = $this->sessionRoles( $inSession, false );

      if( $arrSessionRole && sizeof( $arrSessionRole ) > 0 ) {

        // Create a filter that only returns the resource if
        // it is allready clear, that access will be granted.
        // This filter includes not only the direct granted
        // permission, but also the indirectly granted permission
        // through role hirarchy.
        $filter  = "(&" . $this->conf->getValue( "resource", "filter" );
        $filter .= "(|(" . $this->conf->getValue( "resource", "namingattribute" ) . "=" . $inResource . ")";

        if( preg_match( "/.+/", $this->conf->getValue( "resource", "aliasattribute" ) ) ) {

          $filter .= "  (" . $this->conf->getValue( "resource", "aliasattribute" ) . "=" . $inResource . ")";

        }

        $filter .= ")(|";


        for( $i = 0; $i < sizeof( $arrSessionRole ); $i++ ) {

          $filter .= "(rbacpermission=" . $arrSessionRole[$i] . ":-:" . $inOperation . ")";

        }


        $filter .= "))";

        // Ask the directory
        $arrResource = $this->conn['resource']->search( $this->conf->getValue( "resource", "base" ), $filter, "sub",
                                                        Array( $this->conf->getValue( "resource", "namingattribute" ) ) );
	
        // -----------
        // -- EVENT --
        // The decision is known an given away to an other
        // function. This may decide if we grant permission
        // or not.
        // -----------
	
	if( $this->interceptor ) {

          // Create context
          $context->setValue( "decision", sizeof( $arrResource ) == 1 );


          // Give away the context
	  $context = $this->interceptor->event( "checkAccess", "finish", $context );

          // Use the returned context
          $continue = $context->getSecurityChain();

        }

        // If there is an interceptor, we allow other
        // functions to completely decide what to do.
        if(    $this->interceptor
            && $context->getValue( "decision" ) ) {
          return true;

        }
        elseif(    !$this->interceptor
                && $arrResource && sizeof( $arrResource ) == 1 ) {

          return true;

        }
        else {
	
          return false;

        }

      }
      else {

        return false;

      }

    }
    catch( Exception $e ) {

      $exceptionResult = false;


      // -----------
      // -- EVENT --
      // There was an exception. Maybe an Extension can handle
      // this. At this point we tend to return FALSE.
      // -----------
      if( $this->interceptor ) {

        // Create context
        $context->setValue( "exception", $e );
        $context->setValue( "decision", $exceptionResult );

        // Give away the context
        $context = $this->interceptor->event( "checkAccess", "exception", $context );

        // Use the returned context
        $exceptionResult = $context->getValue( "decision" );

      }


      return $exceptionResult;

    }

  }




  // ## addUser #######################################################
  public function addUser( $inUsername, $inPassword, $inSubtree = "" ) {

    $context = false;          // Possibly a Context-object
    $crypto = new Crypto();    // The crypto-class to build hashes
    $arrUserEntry;             // The new user-entry
    $namingAttribute = "uid";  // The users naming attribute
    $userDn = "";              // The users DN
    $continue = true;          // The possible change in security chain


    // If an interceptor is defined, we need to
    // provide a context and events
    if( $this->interceptor ) {

      $context = new Context();

    }


    // If a valid definition is given then use it.
    if( preg_match( "/(^uid$)|(^cn$)|(^sn$)/i", $this->conf->getValue( "user", "namingattribute" ) ) ) {

      $namingAttribute = $this->conf->getValue( "user", "namingattribute" );

    }


    // Create filter to search for the user. It must not exist in
    // the directory.
    $filter  = "(&" . $this->conf->getValue( "user", "filter" );
    $filter .= "(" . $namingAttribute . "=" . $inUsername . "))";


    // Ask the directory
    $arrUserEntry = $this->conn['user']->search( $this->conf->getValue( "user", "base" ), $filter,
                                                 "sub", Array( $namingAttribute ) );


    // The user does not exist
    if( $arrUserEntry && sizeof( $arrUserEntry ) == 0 ) {

      // The users DN
      $userDn  = $namingAttribute . "=" . $inUsername . ",";
      $userDn .= preg_match( "/^(.+=.+\s*,\s*)*(.+=.+\s*)$/i", $inSubtree ) ? $inSubtree . "," : "";
      $userDn .= $this->conf->getValue( "user", "base" );


      // Create the entry
      $arrUserEntry['objectclass'][0] = "inetorgperson";
      $arrUserEntry[$namingAttribute][0] = $inUsername;


      // ## SECURITY RISK ################################
      // Attention!
      // This is a temporary fix to get things working on
      // the GWDG-server where mhash could not be installed.
      // The change here results in a security hole because
      // the password is stored in cleartext. For the
      // TextGrid project it is not relevant, but you should
      // not do this in other projects.
      $arrUserEntry['userpassword'][0] = $inPassword; //$crypto->ssha( $inPassword );
      // #################################################


      // One of these attributes is allready set!
      preg_match( "/(^uid$)|(^sn$)/i", $namingAttribute ) ? $arrUserEntry['cn'][0] = $inUsername : false;
      preg_match( "/(^uid$)|(^cn$)/i", $namingAttribute ) ? $arrUserEntry['sn'][0] = $inUsername : false;
      preg_match( "/(^sn$)|(^cn$)/i", $namingAttribute ) ? $arrUserEntry['uid'][0] = $inUsername : false;


      // -----------
      // -- EVENT --
      // The user-entry is defined and is going to be
      // added to the directory. Maybe someone wants to
      // change the entry itself or deny this action.
      // -----------
      if( $this->interceptor ) {

        // Create context
        $context->setValue( "entry", $arrUserEntry );
        $context->setValue( "dn", $userDn );

        // Give away the context
        $context = $this->interceptor->event( "addUser", "write", $context );

        // Use the returned context
        $userDn = $context->getValue( "dn" );
        $arrUserEntry = $context->getValue( "entry" );
        $continue = $context->getSecurityChain();

      }


      if( $continue ) {

        if( $this->conn['user']->add( $userDn, $arrUserEntry ) ) {

          return true;

        }
        else {

          throw new RBACException( $this->conf->getValue( "errorDescription", "LDAP_ERROR" ),
                                   $this->conf->getValue( "errorCode", "LDAP_ERROR" ) );

        }

      }
      else {

        return false;

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "USER_ALLREADY_EXISTS" ),
                               $this->conf->getValue( "errorCode", "USER_ALLREADY_EXISTS" ) );

    }

  }




  // ## deleteUser ####################################################
  public function deleteUser( $inUsername ) {

    $arrUserEntry;             // The new user-entry
    $namingAttribute = "uid";  // The standard naming-attribute
    $filter = "";              // The search filter


    // If a valid definition is given then use it.
    if( preg_match( "/(^uid$)|(^cn$)|(^sn$)/i", $this->conf->getValue( "user", "namingattribute" ) ) ) {

      $namingAttribute = $this->conf->getValue( "user", "namingattribute" );

    }


    // Construct the filter to search for the given user
    $filter  = "(&" . $this->conf->getValue( "user", "filter" );
    $filter .= "(" . $namingAttribute . "=" . $inUsername . "))";


    // Ask the directory
    $arrUserEntry = $this->conn['user']->search( $this->conf->getValue( "user", "base" ), $filter, "sub",
                                                 Array( $namingAttribute ) );


    if( $arrUserEntry && sizeof( $arrUserEntry ) == 1 ) {

      if( $this->conn['user']->delete( $arrUserEntry[0]['dn'] ) ) {

        return true;

      }
      else {

        throw new RBACException( $this->conf->getValue( "errorDescription", "LDAP_ERROR" ),
                                 $this->conf->getValue( "errorCode", "LDAP_ERROR" ) );

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "USER_UNKNOWN" ),
                               $this->conf->getValue( "errorCode", "USER_UNKNOWN" ) );

    }

  }




  // ## addRole #######################################################
  public function addRole( $inRole ) {

    $context = false;       // Possibly a Context-object
    $arrRoleEntry;          // The new role-entry
    $roleNamingValue = "";  // The value of the naming-attribute
    $continue = true;       // Adding the role is permitted by default


    // If an interceptor is defined, we need to
    // provide a context and events
    if( $this->interceptor ) {

      $context = new Context();

    }


    // Make sure the role has internal representation
    !$this->isIntRepresentation( $inRole ) ? $inRole = $this->roleExtToInt( $inRole ) : false;

    // Try to get the role from the directory
    $arrRoleEntry = $this->conn['role']->getEntry( $inRole );


    // The role must not exist.
    if( !isset( $arrRoleEntry['dn'] ) ) {

      // Extract the naming-attribute from the role
      $roleNamingValue = preg_split( "/[,]/", $inRole );
      $roleNamingValue = preg_split( "/[=]/", $roleNamingValue[0] );
      $roleNamingValue = $roleNamingValue[1];


      // Create the entry
      $arrRoleEntry = Array();
      $arrRoleEntry['objectclass'][0] = "rbacrole";
      $arrRoleEntry[$this->conf->getValue( "role", "namingattribute" )][0] = $roleNamingValue;


      // -----------
      // -- EVENT --
      // The role-entry is defined and ready. But maybe
      // someone wants to change it or the creation
      // of the role is not permitted
      // -----------
      if( $this->interceptor ) {

        // Create context
        $context->setValue( "entry", $arrRoleEntry );
        $context->setValue( "dn", $inRole );

        // Give away the context
        $context = $this->interceptor->event( "addRole", "write", $context );

        // Use the returned context
        $inRole = $context->getValue( "dn" );
        $arrRoleEntry = $context->getValue( "entry" );
        $continue = $context->getSecurityChain();

      }


      if( $continue ) {

        if( $this->conn['role']->add( $inRole, $arrRoleEntry ) ) {

          return true;

        }
        else {

          throw new RBACException( $this->conf->getValue( "errorDescription", "LDAP_ERROR" ),
                                   $this->conf->getValue( "errorCode", "LDAP_ERROR" ) );

        }

      }
      else {

        return false;

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "ROLE_ALLREADY_EXISTS" ),
                               $this->conf->getValue( "errorCode", "ROLE_ALLREADY_EXISTS" ) );

    }

  }




  // ## deleteRole ####################################################
  public function deleteRole( $inRole ) {

    $arrRoleEntry;          // The new role-entry
    $roleNamingValue = "";  // The value of the naming-attribute


    // Make sure the role has internal representation
    !$this->isIntRepresentation( $inRole ) ? $inRole = $this->roleExtToInt( $inRole ) : false;


    // Try to get the role from the directory
    $arrRoleEntry = $this->conn['user']->getEntry( $inRole );


    // The role must not exist.
    if( isset( $arrRoleEntry['dn'] ) ) {

      if( $this->conn['role']->delete( $inRole ) ) {

        return true;

      }
      else {

        throw new RBACException( $this->conf->getValue( "errorDescription", "LDAP_ERROR" ),
                                 $this->conf->getValue( "errorCode", "LDAP_ERROR" ) );

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "ROLE_UNKNOWN" ),
                               $this->conf->getValue( "errorCode", "ROLE_UNKNOWN" ) );

    }

  }




  // ## assignUser ####################################################
  public function assignUser( $inUsername, $inRole ) {

    $continue = true;          // The possible change in security chain
    $context = false;          // Possibly a Context-object
    $arrUserEntry;             // The user-entry that has to exist
    $arrRoleEntry;             // The role-entry that will be modified
    $userIsInEntry = false;    // Is the user allready assigned to the role
    $roleDn = "";              // The roles DN
    $roleAssAttr = "";         // The attribute
    $roleNamingValue = "";     // The value of the naming-attribute
    $filter = "";              // Filter to search for user / role
    $i = 0;                    // Loop


    // If an interceptor is defined, we need to
    // provide a context and events
    if( $this->interceptor ) {

      $context = new Context();

    }


    // Make sure the role has internal representation
    !$this->isIntRepresentation( $inRole ) ? $inRole = $this->roleExtToInt( $inRole ) : false;


    // Create filter to search for the user.
    $filter  = "(&" . $this->conf->getValue( "user", "filter" );
    $filter .= "(" . $this->conf->getValue( "user", "namingattribute" ) . "=" . $inUsername . "))";

    // Ask the directory
    $arrUserEntry = $this->conn['user']->search( $this->conf->getValue( "user", "base" ), $filter,
                                                 "sub", Array( $namingAttribute ) );

    // The user has to exist
    if( $arrUserEntry && sizeof( $arrUserEntry ) == 1 ) {

      // Extract the naming-attribute from the role
      $roleNamingValue = preg_split( "/[,]/", $inRole );
      $roleNamingValue = preg_split( "/[=]/", $roleNamingValue[0] );
      $roleNamingValue = $roleNamingValue[1];


      $arrRoleEntry = $this->conn['role']->getEntry( $inRole );


      if( isset( $arrRoleEntry['dn'] ) ) {

        $roleDn = $arrRoleEntry['dn'];
        $roleAssAttr = $this->conf->getValue( "role", "assignedattribute" );

	if (isset( $arrRoleEntry[$roleAssAttr])){
        // Check if the user is allready assigned to this role
          for( $i = 0; $i < sizeof( $arrRoleEntry[$roleAssAttr] ); $i++ ) {

            $userIsInEntry = $userIsInEntry
                             || preg_match( "/^" . $inUsername . "$/i", $arrRoleEntry[$roleAssAttr][$i] );

          }

	}
        // If the role exists, the user exists and the user is
        // not allready assigned to the role do the assignement
        if( !$userIsInEntry ) {

          if( $arrRoleEntry[$roleAssAttr] && sizeof( $arrRoleEntry[$roleAssAttr] ) > 0 ) {

            $arrRoleEntry[$roleAssAttr][] = $inUsername;

          }
          else {

            $arrRoleEntry[$roleAssAttr][0] = $inUsername;

          }


          // -----------
          // -- EVENT --
          // The role-entry is defined and ready. But maybe
          // someone wants to change it or the assignment
          // of the user to the role is not permitted
          // -----------
          if( $this->interceptor ) {

            // Create context
            $context->setValue( "performer", $arrRoleEntry[$roleAssAttr] );
            $context->setValue( "dn", $roleDn );

            // Give away the context
            $context = $this->interceptor->event( "assignUser", "write", $context );

            // Use the returned context
            $roleDn = $context->getValue( "dn" );
            $arrRoleEntry[$roleAssAttr] = $context->getValue( "performer" );
            $continue = $context->getSecurityChain();

          }


          if( $continue ) {

            if( $this->conn['role']->modify( $roleDn, Array( $roleAssAttr => $arrRoleEntry[$roleAssAttr] ) ) ) {

              return true;

            }
            else {

              throw new RBACException( $this->conf->getValue( "errorDescription", "LDAP_ERROR" ),
                                       $this->conf->getValue( "errorCode", "LDAP_ERROR" ) );

            }

          }
          else {

            return false;

          }

        }
        else {


          return false;

        }

      }
      else {

        throw new RBACException( $this->conf->getValue( "errorDescription", "ROLE_UNKNOWN" ),
                                 $this->conf->getValue( "errorCode", "ROLE_UNKNOWN" ) );

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "USER_UNKNOWN" ),
                               $this->conf->getValue( "errorCode", "USER_UNKNOWN" ) );

    }

  }




  // ## deassignUser ##################################################
  public function deassignUser( $inUsername, $inRole ) {

    $arrAssigned;              // The users that stay assigned to the role
    $arrUserEntry;             // The user-entry that has to exist
    $arrRoleEntry;             // The role-entry that will be modified
    $userIsInEntry = false;    // Is the user allready assigned to the role
    $userDn = "";              // The users DN
    $roleAssAttr = "";         // The attribute
    $filter = "";              // Filter to search for user / role
    $i = 0;                    // Loop


    // Make sure the role has internal representation
    !$this->isIntRepresentation( $inRole ) ? $inRole = $this->roleExtToInt( $inRole ) : false;


    // Create filter to search for the user.
    $filter  = "(&" . $this->conf->getValue( "user", "filter" );
    $filter .= "(" . $this->conf->getValue( "user", "namingattribute" ) . "=" . $inUsername . "))";


    // Ask the directory
    $arrUserEntry = $this->conn['user']->search( $this->conf->getValue( "user", "base" ), $filter,
                                                 "sub", Array( $namingAttribute ) );


    // The user has to exist
    if( $arrUserEntry && sizeof( $arrUserEntry ) == 1 ) {

      // Extract the naming-attribute from the role
      $roleNamingValue = preg_split( "/[,]/", $inRole );
      $roleNamingValue = preg_split( "/[=]/", $roleNamingValue[0] );
      $roleNamingValue = $roleNamingValue[1];


      // Looking for the role
      $arrRoleEntry = $this->conn['role']->getEntry( $inRole );

      if( isset( $arrRoleEntry['dn'] ) ) {

        $roleAssAttr = $this->conf->getValue( "role", "assignedattribute" );


        // Check if the user is allready assigned to this role. At the same
        // time re-add all the other users assigned to that role.
        $arrAssigned = Array();
        for( $i = 0; $i < sizeof( $arrRoleEntry[$roleAssAttr] ); $i++ ) {

//          if( !preg_match( "/^" . $inUsername . "$/i", $arrRoleEntry[$roleAssAttr][$i] ) ) {
          if( strtolower( $inUsername ) != strtolower( $arrRoleEntry[$roleAssAttr][$i] ) ) {

            $arrAssigned[] = $arrRoleEntry[$roleAssAttr][$i];

          }
          else {

            $userIsInEntry = true;

          }

        }


        // If the role exists, the user exists and the user is
        // not allready assigned to the role do the assignement
        if( $userIsInEntry ) {

          if( $this->conn['role']->modify( $arrRoleEntry['dn'], Array( $roleAssAttr => $arrAssigned ) ) ) {

            return true;

          }
          else {

            throw new RBACException( $this->conf->getValue( "errorDescription", "LDAP_ERROR" ),
                                     $this->conf->getValue( "errorCode", "LDAP_ERROR" ) );

          }

        }
        else {

          return false;

        }

      }
      else {

        throw new RBACException( $this->conf->getValue( "errorDescription", "ROLE_UNKNOWN" ),
                                 $this->conf->getValue( "errorCode", "ROLE_UNKNOWN" ) );

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "USER_UNKNOWN" ),
                               $this->conf->getValue( "errorCode", "USER_UNKNOWN" ) );

    }

  }




  // ## grantPermission ###############################################
  public function grantPermission( $inResource, $inOperation, $inRole ) {

    $arrRoleEntry;       // The roles entry
    $arrResourceEntry;   // The resources entry
    $arrPermission;      // The permissions that have to be set
    $filter = "";        // Filter to search for role and resource
    $isValidOp = false;  // Is the given operation valid
    $isInEntry = false;  // Is the permission allready granted
    $i = 0;              // Loop

    // Make sure the role has internal representation
    !$this->isIntRepresentation( $inRole ) ? $inRole = $this->roleExtToInt( $inRole ) : false;


    // Create a filter to get the resource
    $filter  = "(&" . $this->conf->getValue( "resource", "filter" );
    $filter .= "(|(" . $this->conf->getValue( "resource", "namingattribute" ) . "=" . $inResource . ")";

    if( preg_match( "/.+/", $this->conf->getValue( "resource", "aliasattribute" ) ) ) {

      $filter .= "  (" . $this->conf->getValue( "resource", "aliasattribute" ) . "=" . $inResource . ")";

    }

    $filter .= "))";


    // Get the resource
    $arrResourceEntry = $this->conn['resource']->search( $this->conf->getValue( "resource", "base" ),
                                                         $filter, "sub" );


    // Get the role
    $arrRoleEntry = $this->conn['role']->getEntry( $inRole );


    // The resource has to exist and has to be uniqueue
    if( $arrResourceEntry && sizeof( $arrResourceEntry ) == 1 ) {

      if (isset($arrResourceEntry[0]['rbacoperation'])) {
         // Check if the operation is valid
        for( $i = 0; $i < sizeof( $arrResourceEntry[0]['rbacoperation'] ); $i++ ) {

          $isValidOp = $isValidOp | preg_match( "/^" . $inOperation . "$/i", $arrResourceEntry[0]['rbacoperation'][$i] );

        }
      }
      if (isset($arrResourceEntry[0]['rbacpermission'])) {
      // Check if the permission allready exists
      for( $i = 0; $i < sizeof( $arrResourceEntry[0]['rbacpermission'] ); $i++ ) {

        $isInEntry = $isInEntry | preg_match( "/^" . $inRole . ":-:" . $inOperation . "$/i", $arrResourceEntry[0]['rbacpermission'][$i] );

      }
}

      // The role has to exist
      if( isset( $arrRoleEntry['dn'] ) ) {

        // The operation has to be valid
        if( $isValidOp ) {

          // The permission must not allready be set
          if( !$isInEntry ) {

            $arrPermission = $arrResourceEntry[0]['rbacpermission'];


            $arrPermission[] = $inRole . ":-:" . $inOperation;


            if( $this->conn['resource']->modify( $arrResourceEntry[0]['dn'], Array( "rbacpermission" => $arrPermission ) ) ) {

              return true;

            }
            else {

              throw new RBACException( $this->conf->getValue( "errorDescription", "LDAP_ERROR" ),
                                       $this->conf->getValue( "errorCode", "LDAP_ERROR" ) );

            }

          }
          else {

            return false;

          }

        }
        else {

          throw new RBACException( $this->conf->getValue( "errorDescription", "RESOURCE_OPERATION_ERROR" ),
                                   $this->conf->getValue( "errorCode", "RESOURCE_OPERATION_ERROR" ) );

        }

      }
      else {

        throw new RBACException( $this->conf->getValue( "errorDescription", "ROLE_UNKNOWN" ),
                                 $this->conf->getValue( "errorCode", "ROLE_UNKNOWN" ) );

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "RESOURCE_UNKNOWN" ),
                               $this->conf->getValue( "errorCode", "RESOURCE_UNKNOWN" ) );

    }

  }




  // ## revokePermission ##############################################
  public function revokePermission( $inOperation, $inResource, $inRole ) {

    $arrResourceEntry;   // The resources entry
    $arrPermission;      // The permissions that have to be set
    $filter = "";        // Filter to search for role and resource
    $isValidOp = false;  // Is the given operation valid
    $isInEntry = false;  // Is the permission allready granted
    $i = 0;              // Loop

    // Make sure the role has internal representation
    !$this->isIntRepresentation( $inRole ) ? $inRole = $this->roleExtToInt( $inRole ) : false;


    // Create a filter to get the resource
    $filter  = "(&" . $this->conf->getValue( "resource", "filter" );
    $filter .= "(|(" . $this->conf->getValue( "resource", "namingattribute" ) . "=" . $inResource . ")";

    if( preg_match( "/.+/", $this->conf->getValue( "resource", "aliasattribute" ) ) ) {

      $filter .= "(" . $this->conf->getValue( "resource", "aliasattribute" ) . "=" . $inResource . ")";

    }

    $filter .= "))";


    // Get the resource
    $arrResourceEntry = $this->conn['resource']->search( $this->conf->getValue( "resource", "base" ),
                                                         $filter, "sub" );


    // The resource has to exist and has to be uniqueue
    if( $arrResrouceEntry && sizeof( $arrResourceEntry ) == 1 ) {

      // Check if the permission really exists
      for( $i = 0; $i < sizeof( $arrResourceEntry[0]['rbacpermission'] ); $i++ ) {

        $isInEntry = $isInEntry | preg_match( "/^" . $inRole . ":-:" . $inOperation . "$/i", $arrResourceEntry[0]['rbacpermission'][$i] );

      }


      // The permission must not allready be set
      if( $isInEntry ) {

        for( $i = 0; $i < sizeof( $arrResourceEntry[0]['rbacpermission'] ); $i++ ) {

          if( !preg_match( "/^" . $inRole . ":-:" . $inOperation . "$/i", $arrResourceEntry[0]['rbacpermission'][$i] ) ) {

            $arrPermission[] = $arrResourceEntry[0]['rbacpermission'][$i];

          }

        }


        if( $this->conn['resource']->modify( $arrResourceEntry[0]['dn'], Array( "rbacpermission" => $arrPermission ) ) ) {

          return true;

        }
        else {

          throw new RBACException( $this->conf->getValue( "errorDescription", "LDAP_ERROR" ),
                                   $this->conf->getValue( "errorCode", "LDAP_ERROR" ) );

        }

      }
      else {

        return false;

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "RESOURCE_UNKNOWN" ),
                               $this->conf->getValue( "errorCode", "RESOURCE_UNKNOWN" ) );

    }

  }




  // ## authorizedRoles ###############################################
  public function authorizedRoles( $inUsername, $inExtRepresentation = true ) {

    return $this->assignedRoles( $inUsername, $inExtRepresentation );

  }




  // ## authorizedUsers ###############################################
  public function authorizedUsers( $inRole, $inUseStoredRole = false ) {

    return $this->assignedUsers( $inRole, $inUseStoredRole );

  }




  // ## sessionUser ###################################################
  public function sessionUser( $inSession ) {

    $arrSessionEntry;  // The entry of the session
    $sessionDn = "";   // The sessions DN
    $filter = "";      // Filterstring
    $username = "";    // The owner of the session
    $i = 0;            // Loop

    if( preg_match( "/.+/", $inSession ) ) {

      $sessionDn  = $this->conf->getValue( "session", "namingattribute" ) . "=" . $inSession . ",";
      $sessionDn .= $this->conf->getValue( "session", "base" );


      $arrSessionEntry = $this->conn['session']->getEntry( $sessionDn );

      
      // The session has to exist to get its roles
      if( isset( $arrSessionEntry['dn'] ) ) {

        $username = $arrSessionEntry['rbacsessionuser'][0];

      }
      else {

        throw new RBACException( $this->conf->getValue( "errorDescription", "SESSION_DOES_NOT_EXISTS" ),
                                 $this->conf->getValue( "errorCode", "SESSION_DOES_NOT_EXISTS" ) );

      }

    }


    return $username;

  }




  // ## removeDuplicates ##############################################
  public final function removeDuplicates( Array $inArray ) {

    return array_values( array_unique( $inArray ) );

  }




  // ## getConfiguration ##############################################
  public function getConfiguration() {

    return $this->conf;

  }




  // ## getConnection #################################################
  public function getConnection( $inName ) {

    if(    isset( $this->conn[$inName] )
        && $this->conn[$inName]->hasConnection() ) {

      return $this->conn[$inName];

    }

  }

}
?>
