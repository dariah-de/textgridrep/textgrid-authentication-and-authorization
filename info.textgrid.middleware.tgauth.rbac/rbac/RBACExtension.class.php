<?php
// ####################################################################
// Version: 0.1.0
// Autor: Markus Widmer
// Erstellungsdatum: 02.11.2007
// Letzte Aenderung: 02.11.2007



abstract class RBACExtension {

  // ## Klassenvariablen ##############################################
  protected $rbac;
  protected $conn;
  protected $conf;




  // ## Konstruktor ###################################################
  public function __construct( $inRBAC ) {

    // Save thsi instances of RBAC and grab the configuration
    // from it.
    $this->rbac = $inRBAC;
    $this->conf = $inRBAC->getConfiguration();


    // Get the user- and role connections from the
    // underlying RBAC-system
    $this->conn['user'] = $inRBAC->getConnection( "user" );
    $this->conn['role'] = $inRBAC->getConnection( "role" );
    $this->conn['resource'] = $inRBAC->getConnection( "resource" );
    $this->conn['session'] = $inRBAC->getConnection( "session" );

  }



  // ## registerEvents ################################################
  abstract public function registerEvents( RBAC $inRegistrar );

}
?>