<?php
// #######################################################
// Author: Markus Widmer
// Created: 07.04.2008
// Modified: 10.04.2008
// Version: 0.2.0
// #######################################################

// Set this variable to the appropriate
// path to RBAC.
$path_to_rbac = "<PATH_TO_RBAC>";


require_once( $path_to_rbac . "/RBAC.class.php" );
require_once( "XACML.class.php" );


// Dont be so verbose with messages and notices.
error_reporting( E_ERROR | E_USER_ERROR );


// #############################################################
// Starting SOAP-Server
// #############################################################
$server = new SoapServer( "http://<PATH_TO_WSDL>/xacml.wsdl" );
$server->setClass( "XACML", "../conf/system.conf", $path_to_rbac );


$server->handle();
?>
