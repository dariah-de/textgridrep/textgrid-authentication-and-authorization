<?php
// #######################################################
// Author: Markus Widmer
// Created: 03.04.2008
// Modified: 10.08.2008
// Version: 0.1.0
// #######################################################



// -----------------------------------------------------
// You need this service
// -----------------------------------------------------
$soapXACML = new SoapClient( "http://<PATH_TO_WSDL>/xacml.wsdl", Array( "trace" => 1 ) );


echo "<BODY><HTML>";


if( isset( $_POST['session'] ) ) {

  // -----------------------------------------------------
  // The XACMLAuthzDecicionQuery consists of a complex
  // structure that is build up here. 
  // -----------------------------------------------------

  $regReq = new stdClass();

  $regReq->Version = "2.0";
  $regReq->ID = time();
  $regReq->ReturnContext = true;
  $regReq->Request = new stdClass();
  $regReq->Request->Subject = new stdClass();
  $regReq->Request->Resource = new stdClass();
  $regReq->Request->Action = new stdClass();
  $regReq->Request->Environment = new stdClass();

  $regReq->Request->Subject->Attribute = new stdClass();
  $regReq->Request->Subject->Attribute->AttributeId = "urn:oasis:names:tc:xacml:1.0:subject:subject-id";
  $regReq->Request->Subject->Attribute->DataType = "http://www.w3.org/2001/XMLSchema#string";
  $regReq->Request->Subject->Attribute->AttributeValue = new stdClass();
  $regReq->Request->Subject->Attribute->AttributeValue->any = $_POST['session'];

  $regReq->Request->Resource->Attribute = new stdClass();
  $regReq->Request->Resource->Attribute->AttributeId = "urn:oasis:names:tc:xacml:1.0:resource:resource-id";
  $regReq->Request->Resource->Attribute->DataType = "http://www.w3.org/2001/XMLSchema#anyURI";
  $regReq->Request->Resource->Attribute->AttributeValue = new stdClass();
  $regReq->Request->Resource->Attribute->AttributeValue->any = $_POST['resource'];

  $regReq->Request->Action->Attribute = new stdClass();
  $regReq->Request->Action->Attribute->AttributeId = "urn:oasis:names:tc:xacml:1.0:action:action-id";
  $regReq->Request->Action->Attribute->DataType = "http://www.w3.org/2001/XMLSchema#string";
  $regReq->Request->Action->Attribute->AttributeValue = new stdClass();
  $regReq->Request->Action->Attribute->AttributeValue->any = $_POST['operation'];


  echo "<BR>";
  echo "Checking for access...<BR/><BR/>";
  echo "Look at the HTML-code to see what happens, if you are interested in the SOAP documents!<BR/><BR/>";


  try {

    $caResponse = $soapXACML->checkXACMLaccess( $regReq );

    echo "\n\n" . $soapXACML->__getLastRequest();
    echo "\n\n" . $soapXACML->__getLastResponse() . "\n\n";

    if( preg_match( "/^permit$/i", $caResponse->Response->Result->Decision ) ) {

      echo "<BR><HR><BR>Granted: YES.<BR><HR><BR>";

    }
    else {

      echo "<BR><HR><BR>Granted: NO.<BR><HR><BR>";

    }

  }
  catch( SoapFault $f ) {

    echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

  }

}


echo "<FORM action=\"xacmlCheckAccess.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
echo "Session: <INPUT type=\"text\" name=\"session\" value=\"\"><BR>\n";
echo "Resource: <INPUT type=\"text\" name=\"resource\" value=\"\"><BR>\n";
echo "Operation: <INPUT type=\"text\" name=\"operation\" value=\"\"><BR>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";
?>
