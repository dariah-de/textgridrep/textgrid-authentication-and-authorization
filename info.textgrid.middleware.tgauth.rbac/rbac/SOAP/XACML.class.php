<?php
// #######################################################
// Author: Markus Widmer
// Created: 07.04.2008
// Modified: 10.04.2008
// Version: 0.2.0
// #######################################################



class XACML {

  // Global variables
  protected $rbac;



  // -----------------------------------------------------
  // Constructor
  // Input: none
  // Output: object RBACcore
  // Description:
  //   Creates initial connections to the LDAP-server and
  //   sets some configuration parameters.
  // -----------------------------------------------------
  public function __construct( $inRbacConfFile, $inRbacBase ) {

    $this->rbac = new RBAC( $inRbacConfFile, $inRbacBase );

  }




  // -----------------------------------------------------
  // Function: checkXACMLaccess
  // Input: inRequest / urn:oasis:xacml:2.0:saml:protocol:schema:os:XACMLAuthzDecisionQuery
  // Output: result / urn:oasis:xacml:2.0:saml:assertion:schema:os:XACMLAuthzDecisionStatement
  // Description
  //   Decides if the access to the resource may be
  //   granted. To do so the function takes the Subject,
  //   Action and Resource and passes them to the RBAC system.
  // -----------------------------------------------------
  function checkXACMLaccess( $inRequest ) {

    $result = new stdClass();  // The response


    $result->Response = new stdClass();
    $result->Response->Result = new stdClass();


    if( preg_match( "/^2\.0$/", $version ) ) {

      try {

        if( $this->rbac->checkAccess( $inRequest->Request->Subject->Attribute->AttributeValue->any,
                                      $inRequest->Request->Action->Attribute->AttributeValue->any,
                                      $inRequest->Request->Resource->Attribute->AttributeValue->any ) ) {

          $result->Response->Result->Decision = "Permit";

        }
        else {

          $result->Response->Result->Decision = "Deny";

        }

      }
      catch( Exception $e ) {

        $result->Response->Result->Decision = "Indeterminate";

      }



      // Return the request if the flag is set to TRUE
      if( $inRequest->ReturnContext ) {

        $result->Request = new stdClass();

        isset( $inRequest->Request->Subject ) ? $result->Request->Subject = $inRequest->Request->Subject
                                              : $result->Request->Subject = new sdtClass();


        isset( $inRequest->Request->Resource ) ? $result->Request->Resource = $inRequest->Request->Resource
                                               : $result->Request->Resource = new stdClass();


        isset( $inRequest->Request->Action ) ? $result->Request->Action = $inRequest->Request->Action
                                             : $result->Request->Action = new stdClass();


        isset( $inRequest->Request->Environment ) ? $result->Request->Environment = $inRequest->Request->Environment
                                                  : $result->Request->Environment = new stdClass();

      }

    }
    else {

      $result->Response->Result->Decision = "NotApplicable";

    }


    return $result;

  }

}
?>
