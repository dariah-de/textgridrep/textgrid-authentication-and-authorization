<?php
// ####################################################################
// Version: 0.2.0
// Autor: Markus Widmer
// Erstellungsdatum: 15.08.2007
// Letzte Aenderung: 19.12.2007


// Requiring these interfaces if the RBAC-Framework
// is not existing. Otherwise the RBAC-Framework
// takes care of importing all nevessarry classes and
// interfaces.
if( !class_exists( "RBAC" ) ) {

  if(    defined( "RBAC_LIB_PATH" )
      && defined( "RBAC_PATH" ) ) {

    !interface_exists( "iHelper" ) ? require_once( RBAC_LIB_PATH . "/iHelper.interface.php" ) : false;
    !interface_exists( "iNode" ) ? require_once( RBAC_LIB_PATH . "/iNode.interface.php" ) : false;
    !interface_exists( "iXML" ) ? require_once( RBAC_LIB_PATH . "/iXML.interface.php" ) : false;
    !interface_exists( "iLDAP" ) ? require_once( RBAC_LIB_PATH . "/iLDAP.interface.php" ) : false;
    !interface_exists( "iCrypto" ) ? require_once( RBAC_LIB_PATH . "/iCrypto.interface.php" ) : false;

    !interface_exists( "iContext" ) ? require_once( RBAC_PATH . "/iContext.interface.php" ) : false;
    !interface_exists( "iRBACcore" ) ? require_once( RBAC_PATH . "/iRBACcore.interface.php" ) : false;
    !interface_exists( "iRBAClimitedHirarchical" ) ? require_once( RBAC_PATH . "/iRBAClimitedHirarchical.interface.php" ) : false;


    !class_exists( "Helper" ) ? require_once( RBAC_LIB_PATH . "/Helper.class.php" ) : false;
    !class_exists( "Node" ) ? require_once( RBAC_LIB_PATH . "/Node.class.php" ) : false;
    !class_exists( "XML" ) ? require_once( RBAC_LIB_PATH . "/XML.class.php" ) : false;
    !class_exists( "LDAP" ) ? require_once( RBAC_LIB_PATH . "/LDAP.class.php" ) : false;
    !class_exists( "Crypto" ) ? require_once( RBAC_LIB_PATH . "/Crypto.class.php" ) : false;

    !class_exists( "SimpleConfig" ) ? require_once( RBAC_PATH . "/SimpleConfig.class.php" ) : false;
    !class_exists( "RBACException" ) ? require_once( RBAC_PATH . "/RBACException.class.php" ) : false;
    !class_exists( "RBACExtension" ) ? require_once( RBAC_PATH . "/RBACExtension.class.php" ) : false;
    !class_exists( "Context" ) ? require_once( RBAC_PATH . "/Context.class.php" ) : false;
    !class_exists( "RBACcore" ) ? require_once( RBAC_PATH . "/RBACcore.class.php" ) : false;

  }
  else {

    exit( "\nYou have to define the constants RBAC_LIB_PATH and RBAC_PATH.\n" );

  }


/*

  require_once( "iNode.interface.php" );
  require_once( "iXML.interface.php" );
  require_once( "iLDAP.interface.php" );
  require_once( "iCrypto.interface.php" );
  require_once( "iConvert.interface.php" );
  require_once( "iContext.interface.php" );
  require_once( "iRBACcore.interface.php" );
  require_once( "iRBAClimitedHirarchical.interface.php" );


  require_once( "SimpleConfig.class.php" );
  require_once( "XML.class.php" );
  require_once( "Node.class.php" );
  require_once( "LDAP.class.php" );
  require_once( "Crypto.class.php" );
  require_once( "Convert.class.php" );
  require_once( "RBACException.class.php" );
  require_once( "RBACExtension.class.php" );
  require_once( "Context.class.php" );
  require_once( "RBACcore.class.php" );
*/
}


class RBAClimitedHirarchical extends RBACcore implements iRBAClimitedHirarchical {

  // ## Klassenvariablen ##############################################




  // ## Konstruktor ###################################################
  public function __construct( $inConfigurationFile, RBAC $inInterceptor = null ) {

    parent::__construct( $inConfigurationFile, $inInterceptor );

  }




  // ## rolePermissions ###############################################
  public function rolePermissions( $inRole ) {

    $arrResource;              // The resources the role has a permission on
    $arrPermission = Array();  // The permissions found
    $arrTmpSplit;              // Splitted string
    $filter = "";              // Filterstring
    $i = 0;                    // Loop
    $j = 0;                    // Loop


    // Make sure the role has internal representation
    !$this->isIntRepresentation( $inRole ) ? $inRole = $this->roleExtToInt( $inRole ) : false;


    // Get the role-entry
    $arrRole = $this->conn['role']->getEntry( $inRole );


    if( isset( $arrRole['dn'] ) ) {

      // Create the filter to search for all the roles in the
      // role-hirarchy. This is the start.
      $filter  = "(&" . $this->conf->getValue( "resource", "filter" ) . "(|";


      // Split the role-DN to get informations about the hirarchy. But first
      // remove the role-base.
      $arrTmpSplit = preg_split( "/[,]/", preg_replace( "/\s*,\s*" . $this->conf->getValue( "role", "base" ) . "\s*$/i", "", $inRole ) );


      // Add each role in the hirarchy to the filter
      while( sizeof( $arrTmpSplit ) > 0 ) {

        $filter .= "(rbacpermission=" . join( ",", $arrTmpSplit ) . "," . $this->conf->getValue( "role", "base" ) . ":-:*)";


        // Cut off the first element
        array_shift( $arrTmpSplit );

      }



      $filter .= "))";


      $arrResource = $this->conn['resource']->search( $this->conf->getValue( "resource", "base" ),
                                                      $filter, "sub",
                                                      Array( $this->conf->getValue( "resource", "namingattribute" ),
                                                             $this->conf->getValue( "resource", "aliasattribute" ),
                                                             "rbacpermission" ) );


      for( $i = 0; $i < sizeof( $arrResource ); $i++ ) {

        for( $j = 0; $j < sizeof( $arrResource[$i]['rbacpermission'] ); $j++ ) {

          $arrTmpSplit = preg_split( "/:-:/", $arrResource[$i]['rbacpermission'][$j] );


          if( preg_match( "/" . $arrTmpSplit[0] . "$/i", $inRole ) ) {

            $arrPermission[] = Array( "resource" => $arrResource[$i][$this->conf->getValue( "resource", "namingattribute" )][0],
                                      "alias" => $arrResource[$i][$this->conf->getValue( "resource", "aliasattribute" )],
                                      "operation" => $arrTmpSplit[1] );

          }

        }

      }


      return $arrPermission;

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "ROLE_UNKNOWN" ),
                               $this->conf->getValue( "errorCode", "ROLE_UNKNOWN" ) );

    }

  }




  // ## addInheritance ################################################
  public function addInheritance( $inAscendant, $inDescendant ) {

    $context = false;  // Possibly a Context-object
    $arrAscendant;     // Role entry of the ascendant
    $arrDescendant;    // Role entry of the descendant
    $tmpString = "";   // Temporary string
    $arrTmpSplit;      // The splitted tmpString
    $filter = "";      // Filterstring
    $continue = true;  // The possible change in security chain


    // If an interceptor is defined, we need to
    // provide a context and events
    if( $this->interceptor ) {

      $context = new Context();

    }


    // Make sure the roles have internal representation
    !$this->isIntRepresentation( $inAscendant ) ? $inAscendant = $this->roleExtToInt( $inAscendant ) : false;
    !$this->isIntRepresentation( $inDescendant ) ? $inDescendant = $this->roleExtToInt( $inDescendant ) : false;


    // Get the role-entries
    $arrAscendant = $this->conn['role']->getEntry( $inAscendant );
    $arrDescendant = $this->conn['role']->getEntry( $inDescendant );


    // Both roles have to exist in the directory
    if( isset( $arrAscendant['dn'] ) && isset( $arrDescendant['dn'] ) ) {

      // The ascendant does not have any descendant yet. To find out, cut
      // the base-DN. The result should be the role RDN.
      $tmpString = $inAscendant;
      $tmpString = preg_replace( "/\s*,\s*" . $this->conf->getValue( "role", "base" ) . "$/i", "", $tmpString );
      $arrTmpSplit = preg_split( "/[,]/", $tmpString );


      // The ascendants new DN will be
      $ascendantNewDn = $tmpString . "," . $inDescendant;


      // If there are not more commas there are no descendants and
      // the tmpString is the role RDN. So the preg_split-function
      // returns an array containing only one entry, the role RDN
      // itself.
      if( sizeof( $arrTmpSplit ) <= 1 ) {

        // -----------
        // -- EVENT --
        // The inheritance is going to be added to
        // the RBAC-system. But maybe someone wants to
        // check for consistency.
        // -----------
        if( $this->interceptor ) {

          // Create context
          $context->setValue( "ascendantNewDn", $tmpString . "," . $inDescendant );

          // Give away the context
          $context = $this->interceptor->event( "addInheritance", "write", $context );

          // Use the returned context
          $userDn = $context->getValue( "dn" );
          $arrUserEntry = $context->getValue( "entry" );
          $continue = $context->getSecurityChain();

        }


        if( $continue ) {

          if( $this->conn['role']->copy( $inAscendant, $tmpString . "," . $inDescendant, true ) ) {

            if( $this->conn['role']->delete( $inAscendant, true ) ) {

              return true;

            }
            else {

              throw new RBACException( $this->conf->getValue( "errorDescription", "UNKNOWN_ERROR" ),
                                       $this->conf->getValue( "errorCode", "UNKNOWN_ERROR" ) );

            }

          }
          else {

            throw new RBACException( $this->conf->getValue( "errorDescription", "UNKNOWN_ERROR" ),
                                     $this->conf->getValue( "errorCode", "UNKNOWN_ERROR" ) );

          }

        }
        else {

          return false;

        }

      }
      else {

        return false;

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "ROLE_UNKNOWN" ),
                               $this->conf->getValue( "errorCode", "ROLE_UNKNOWN" ) );

    }

  }




  // ## deleteInheritance #############################################
  public function deleteInheritance( $inAscendant, $inDescendant ) {

    $arrAscendant;    // Role entry of the ascendant
    $arrDescendant;   // Role entry of the descendant
    $tmpString = "";  // Temporary string
    $arrTmpSplit;     // The splitted tmpString
    $filter = "";     // Filterstring


    // Make sure the roles have internal representation
    !$this->isIntRepresentation( $inAscendant ) ? $inAscendant = $this->roleExtToInt( $inAscendant ) : false;
    !$this->isIntRepresentation( $inDescendant ) ? $inDescendant = $this->roleExtToInt( $inDescendant ) : false;


    // Get the role-entries
    $arrAscendant = $this->conn['role']->getEntry( $inAscendant );
    $arrDescendant = $this->conn['role']->getEntry( $inDescendant );


    // Both roles have to exist in the directory
    if( isset( $arrAscendant['dn'] ) && isset( $arrDescendant['dn'] ) ) {

      // The ascendant has to be a direct ascendant of the descendant. To
      // find out, cut the descendant from the ascendant. This should be the
      // same as taking the RDN of the ascendant.
      $tmpString = $inAscendant;
      $tmpString = preg_replace( "/\s*,\s*" . $inDescendant . "/i", "", $tmpString );
      $arrTmpSplit = preg_split( "/[,]/", $inAscendant );


      if( preg_match( "/[\s]*" . $tmpString . "[\s]*/i", $arrTmpSplit[0] ) ) {

        // Move the ascendant to the role base
        if( $this->conn['role']->copy( $inAscendant, $arrTmpSplit[0] . "," . $this->conf->getValue( "role", "base" ), true ) ) {

          if( $this->conn['role']->delete( $inAscendant, true ) ) {

            return true;

          }
          else {

            throw new RBACException( $this->conf->getValue( "errorDescription", "UNKNOWN_ERROR" ),
                                     $this->conf->getValue( "errorCode", "UNKNOWN_ERROR" ) );

          }

        }
        else {

          throw new RBACException( $this->conf->getValue( "errorDescription", "UNKNOWN_ERROR" ),
                                   $this->conf->getValue( "errorCode", "UNKNOWN_ERROR" ) );

        }

      }
      else {

        return false;

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "ROLE_UNKNOWN" ),
                               $this->conf->getValue( "errorCode", "ROLE_UNKNOWN" ) );

    }

  }




  // ## addAscendant ##################################################
  public function addAscendant( $inAscendant, $inDescendant ) {

    $context = false;       // Possibly a Context-object
    $arrAscEntry;           // The new role-entry
    $arrDescEntry;          // The descendant role-entry
    $ascNamingValue = "";   // The value of the naming-attribute
    $ascDn = "";            // The DN of the ascendant
    $continue = true;       // Adding the role is permitted by default


    // If an interceptor is defined, we need to
    // provide a context and events
    if( $this->interceptor ) {

      $context = new Context();

    }


    // Make sure the roles have internal representation
    !$this->isIntRepresentation( $inAscendant ) ? $inAscendant = $this->roleExtToInt( $inAscendant ) : false;
    !$this->isIntRepresentation( $inDescendant ) ? $inDescendant = $this->roleExtToInt( $inDescendant ) : false;


    // Extract the naming-attribute from the ascendant
    $ascNamingValue = preg_split( "/[,]/", $inAscendant );
    $ascNamingValue = preg_split( "/[=]/", $ascNamingValue[0] );
    $ascNamingValue = $ascNamingValue[1];


    // The ascendant-entry will be directly under the descendant-entry
    $ascDn = $this->conf->getValue( "role", "namingattribute" ) . "=" . $ascNamingValue . "," . $inDescendant;


    // Try to get the role from the directory
    $arrAscEntry = $this->conn['role']->getEntry( $ascDn );
    $arrDescEntry = $this->conn['role']->getEntry( $inDescendant );


    // The descendant has to exist, while ascendant must not.
    // The ascendant has to be directly under the descendant!
    if(    !isset( $arrAscEntry['dn'] )
        && isset( $arrDescEntry['dn'] ) ) {

      // Create the entry
      $arrAscEntry = Array();
      $arrAscEntry['objectclass'][0] = "rbacrole";
      $arrAscEntry[$this->conf->getValue( "role", "namingattribute" )][0] = $ascNamingValue;


      // -----------
      // -- EVENT --
      // The ascendant-entry is defined and ready. But maybe
      // someone wants to change it or the creation
      // of the ascendant is not permitted
      // -----------
      if( $this->interceptor ) {

        // Create context
        $context->setValue( "entry", $arrAscEntry );
        $context->setValue( "dn", $ascDn );

        // Give away the context
        $context = $this->interceptor->event( "addAscendant", "write", $context );

        // Use the returned context
        $inAscendant = $context->getValue( "dn" );
        $arrAscEntry = $context->getValue( "entry" );
        $continue = $context->getSecurityChain();

      }


      if( $continue ) {

        if( $this->conn['role']->add( $ascDn, $arrAscEntry ) ) {

          // -----------
          // -- EVENT --
          // The ascendant-entry has been added. Is there
          // anything else to do by somebody?
          // -----------
          if( $this->interceptor ) {

            // Create context
            $context->setValue( "entry", $arrAscEntry );
            $context->setValue( "dn", $ascDn );

            // Give away the context
            $context = $this->interceptor->event( "addAscendant", "finished", $context );

          }


          return true;

        }
        else {

          throw new RBACException( $this->conf->getValue( "errorDescription", "LDAP_ERROR" ),
                                   $this->conf->getValue( "errorCode", "LDAP_ERROR" ) );

        }

      }
      else {

        return false;

      }

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "ROLE_ALLREADY_EXISTS" ),
                               $this->conf->getValue( "errorCode", "ROLE_ALLREADY_EXISTS" ) );

    }

  }




  // ## addDescendant #################################################
  public function addDescendant( $inAscendant, $inDescendant ) {

    if( $this->addRole( $inDescendant ) ) {

      return $this->addInheritance( $inAscendant, $inDescendant );

    }

  }




  // ## authorizedUsers ###############################################
  public function authorizedUsers( $inRole, $inUseStoredRole = false ) {

    $arrRole;           // The roles ldap-entry
    $arrRoleAscendant;  // The roles ascendants
    $arrUser;           // The authorized users
    $filter = "";       // Filter-string

    // Make sure the role has internal representation
    !$this->isIntRepresentation( $inRole ) ? $inRole = $this->roleExtToInt( $inRole ) : false;


    if( $inUseStoredRole && isset( $this->arrEntryStorage['assignedusersrole'][$inRole] ) ) {
      $arrRole = $this->arrEntryStorage['assignedusersrole'][$inRole];

    }
    else {

      // Get the role-entry
      $arrRole = $this->conn['role']->getEntry( $inRole );

      $this->arrEntryStorage['assignedusersrole'][$inRole] = $arrRole;

    }

    if( isset( $arrRole['dn'] ) ) {

      // The filter is only the role-filter because every
      // ascendant is wanted.
      $filter = $this->conf->getValue( "role", "filter" );


      // Get all the ascendants
      $arrRoleAscendant = $this->conn['role']->search( $arrRole['dn'], $filter, "sub",
                                                       Array( $this->conf->getValue( "role", "namingattribute" ),
                                                              $this->conf->getValue( "role", "assignedattribute" ) ) );


//    $file = fopen ("/tmp/xxxAU.log", "w+");
//    fwrite ($file, serialize ($arrRoleAscendant) ."\n");
//    fclose ($file);

      if ($arrRoleAscendant) {
      for( $i = 0; $i < sizeof( $arrRoleAscendant ); $i++ ) {
      	$attribute = $this->conf->getValue( "role", "assignedattribute" );
	if ( $arrRoleAscendant[$i][$attribute]) {
        for( $j = 0; $j < sizeof( $arrRoleAscendant[$i][$attribute] ); $j++ ) {
           
          $arrUser[] = $arrRoleAscendant[$i][$this->conf->getValue( "role", "assignedattribute" )][$j];

        }
	}
}
      }

      return $this->removeDuplicates( $arrUser );

    }
    else {

      throw new RBACException( $this->conf->getValue( "errorDescription", "ROLE_UNKNOWN" ),
                               $this->conf->getValue( "errorCode", "ROLE_UNKNOWN" ) );

    }

  }




  // ## authorizedRoles ###############################################
  public function authorizedRoles( $inUsername, $inExtRepresentation = true ) {

    $arrTmpSplit;                  // Splitted string
    $arrAssignedRole = Array();    // All the assigned roles for the user
    $arrAuthorizedRole = Array();  // All the authorized roles for the user
    $i = 0;                        // Loop


    // Get the directly assigned roles
    $arrAssignedRole = $this->assignedRoles( $inUsername, false );


    for( $i = 0; $i < sizeof( $arrAssignedRole ); $i++ ) {

      // Split the role-DN to get informations about the hirarchy. But first
      // remove the role-base.
      $arrTmpSplit = preg_split( "/[,]/",
                                 preg_replace( "/\s*,\s*" . $this->conf->getValue( "role", "base" ) . "\s*$/i",
                                               "", $arrAssignedRole[$i] ) );


      // Add each role in the hirarchy to the list
      while( sizeof( $arrTmpSplit ) > 0 ) {

        $arrAuthorizedRole[] = join( ",", $arrTmpSplit ) . "," . $this->conf->getValue( "role", "base" );


        // Cut off the first element
        array_shift( $arrTmpSplit );

      }

    }


    $arrAuthorizedRole = $this->removeDuplicates( $arrAuthorizedRole );


    // For internal use this can be left out, so the
    // internal function that uses this one doesn't have
    // to convert every role back to internal representation
    if( $inExtRepresentation ) {

      for( $i = 0; $i < sizeof( $arrAuthorizedRole ); $i++ ) {

        $arrAuthorizedRole[$i] = $this->roleIntToExt( $arrAuthorizedRole[$i] );

      }

    }


    return $arrAuthorizedRole;

  }




  // ## roleOperationsOnObject ########################################
  public function roleOperationsOnObject( $inRole, $inResource, $inUseStoredResource = false ) {

    $arrOperation = Array();  // All the operations the role is authorized for
    $arrRoleSplit = Array();
    $roleWithoutBase = "";
    $roleTmp = "";
    $i = 0;


    // Make sure the roles have internal representation
    !$this->isIntRepresentation( $inRole ) ? $inRole = $this->roleExtToInt( $inRole ) : false;


    // Remove the role-base
    $roleWithoutBase = preg_replace( "/\s*,\s*" . $this->conf->getValue( "role", "base" ) . "\s*$/i", "", $inRole );


    // Split the role hirarchy
    $arrRoleSplit = preg_split( "/[,]/", $roleWithoutBase );


    $roleTmp = $this->conf->getValue( "role", "base" );

    for( $i = sizeof( $arrRoleSplit ) - 1; $i >= 0; $i-- ) {

      $roleTmp = $arrRoleSplit[$i] . "," . $roleTmp;

      $arrOperation = array_merge( $arrOperation, parent::roleOperationsOnObjectInternal( $roleTmp, $inResource, $inUseStoredResource ) );

    }


    return $this->removeDuplicates( $arrOperation );

  }

}
?>
