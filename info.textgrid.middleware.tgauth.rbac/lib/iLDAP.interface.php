<?php
interface iLDAP {

  public function connect( $inHost, $inPort, $inVersion, $inTls = false );
  public function bind( $inDn, $inPassword );
  public function reconnect();
  public function rebind();
  public function getEntry( $inDn );
  public function search( $inBase, $inFilter, $inScope = "sub", $inArrAttribute = null );
  public function copy( $inDnFrom, $inDnTo, $inRecursiv = false );
  public function delete( $inDn, $inRecursiv = false );
  public function modify( $inDn, Array $inAttribute );
  public function add( $inDn, Array $inAttribute );
  public function getConnection();
  public function hasConnection();

}
?>
