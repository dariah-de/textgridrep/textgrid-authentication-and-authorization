<?php
// ####################################################################
// Version: 0.2.0
// Autor: Markus Widmer
// Erstellungsdatum: 16.09.2007
// Letzte Aenderung: 21.01.2008




class Date implements iDate {

  // ## Klassenvariablen ##############################################
  private $language = "";
  private $timestamp;
  private $arrDate;




  // ## Konstruktor ###################################################
  public function __construct( $inTimestamp, $inLanguage = false ) {

    $inTimestamp ? $this->timestamp = $inTimestamp : $this->timestamp = time();
    $inLanguage ? $this->language = $inLanguage : $this->language = "en";


    if( $inTimestamp != 0 ) {

      $this->timestamp = $inTimestamp;
      $this->fillArrDate();

    }
    else {

      $arrDate = Array();

    }

  }




  // ## parseGeneralizedTime ##########################################
  public function parseGeneralizedTime( $inGeneralizedTime ) {

    $year;    // Jahr
    $month;   // Monat
    $day;     // Tag
    $minute;  // Minute
    $hour;    // Stunde
    $second;  // Sekunde


    if( preg_match( "/[0-9]{14}Z/i", $inGeneralizedTime ) ) {

      $year = substr( $inGeneralizedTime, 0, 4 );
      $month = substr( $inGeneralizedTime, 4, 2 );
      $day = substr( $inGeneralizedTime, 6, 2 );
      $hour = substr( $inGeneralizedTime, 8, 2 );
      $minute = substr( $inGeneralizedTime, 10, 2 );
      $second = substr( $inGeneralizedTime, 12, 2 );


      $this->timestamp = mktime( $hour, $minute, $second, $month, $day, $year );
      $this->fillArrDate();


      return true;

    }
    else {

      return false;

    }

  }




  // ## parseMySQLDatetime ###########################################
  public function parseMySQLDatetime( $inDatetime ) {

    $pregString = "";
    $tmp = Array();


    // Datumsformat mit Zeitangabe in der MySQL Datenbank
    $pregString .= "/^[0-9]{4}[-]{1}[0-9]{1,2}[-]{1}[0-9]{1,2}";
    $pregString .= "[\s]+[0-9]{1,2}([:]{1}[0-9]{1,2}){2}$/";


    if( preg_match( $pregString, $inDatetime ) ) {

      // Leerzeichen so lange entfernen, bis nur noch
      // einzelne Leerzeichen vorhanden sind.
      while( preg_match( "/[\s]{2}/", $inDatetime ) ) {

        $inDatetime = preg_replace( "/[\s]{2}/", " ", $inDatetime );

      }


      $tmp = preg_split( "/[\s]/", $inDatetime );


      if( is_array( $tmp ) ) {

        isset( $tmp[0] ) ? $tmpDate = preg_split( "/[-]/", $tmp[0] ) : $tmpDate = Array();
        isset( $tmp[1] ) ? $tmpTime = preg_split( "/[:]/", $tmp[1] ) : $tmpTime = Array();

        if( sizeof( $tmpDate ) == 3 ) {

          $this->timestamp = mktime( $tmpTime[0], $tmpTime[1], $tmpTime[2], $tmpDate[1], $tmpDate[2], $tmpDate[0] );
          $this->fillArrDate();


          return true;

        }
        else {

          return false;

        }

      }


      return true;

    }
    else {

      return false;

    }

  }




  // ## parseMySQLDatetime ###########################################
  public function parseMySQLDate( $inDate ) {

    $pregString = "";
    $tmp = Array();


    // Datumsformat ohne Zeitangabe in der MySQL Datenbank
    $pregString .= "/^[0-9]{4}[-]{1}[0-9]{1,2}[-]{1}[0-9]{1,2}$/";


    if( preg_match( $pregString, $inDate ) ) {

      // Alle Leerzeichen entfernen.
      $inDate = preg_replace( "/[\s]/", "", $inDate );


      $tmpDate = preg_split( "/[-]/", $inDate );

      if( sizeof( $tmpDate ) == 3 ) {

        $this->timestamp = mktime( 0, 0, 0, $tmpDate[1], $tmpDate[2], $tmpDate[0] );
        $this->fillArrDate();


        return true;

      }
      else {

        return false;

      }

    }

  }




  // ## setLanguage ###################################################
  public function setLanguage( $inLanguage ) {

    $this->language = $inLanguage;

  }




  // ## setTimestamp ##################################################
  public function setTimestamp( $inTimestamp ) {

    if( is_int( $inTimestamp ) ) {

      $this->timestamp = $inTimestamp;
      return true;

    }
    else {

      return false;

    }

  }




  // ## parseDateFirst ################################################
  public function parseDateFirst( $inText ) {

    $tmp = Array();
    $tmpDate = Array();
    $tmpTime = Array( "12", "0", "0" );
    $arrDateAndTime = Array();
    $pregString = "";
    $dateTime = "";


    if( preg_match( "/^de$/i", $this->language ) ) {

      // Deutsches Datumsformat
      $pregString .= "/^[0-9]{1,2}[.]{1}[0-9]{1,2}[.]{1}[0-9]{4}";
      $pregString .= "([\s]+[0-9]{1,2}([:]{1}[0-9]{1,2}){1,2}?)?$/";


      if( preg_match( $pregString, $inText ) ) {

        while( preg_match( "/[\s]{2}/", $inText ) ) {

          $inText = preg_replace( "/[\s]{2}/", " ", $inText );

        }


        $tmp = preg_split( "/[\s]/", $inText );

      }
      else {

        return false;

      }


      if( is_array( $tmp ) ) {

        isset( $tmp[0] ) ? $tmpDate = preg_split( "/[.]/", $tmp[0] ) : false;
        isset( $tmp[1] ) ? $tmpTime = preg_split( "/[:]/", $tmp[1] ) : false;

        if( sizeof( $tmpDate ) == 3 ) {

          $this->timestamp = mktime( $tmpTime[0], $tmpTime[1], $tmpTime[2], $tmpDate[1], $tmpDate[0], $tmpDate[2] );
          $this->fillArrDate();


          return true;

        }
        else {

          return false;

        }

      }

    }
    else {

      return false;

    }

  }




  // ## getArray ######################################################
  public function getArray() {

    return $this->arrDate;

  }




  // ## getDateText ###################################################
  public function getDateText() {

    if( preg_match( "/^de$/i", $this->language ) ) {

      return $this->arrDate['day'] . "." . $this->arrDate['month'] . "." . $this->arrDate['year'];

    }
    else {

      return $this->arrDate['year'] . "-" . $this->arrDate['month'] . "-" . $this->arrDate['day'];

    }

  }




  // ## getDateTimeText ###############################################
  public function getDateTimeText() {

    if( preg_match( "/^de$/i", $this->language ) ) {

      return $this->arrDate['day'] . "." . $this->arrDate['month'] . "." . $this->arrDate['year'] . " " . $this->arrDate['hour']
             . ":" . $this->arrDate['minute'] . ":" . $this->arrDate['second'];

    }
    else {

      return $this->arrDate['year'] . "-" . $this->arrDate['month'] . "-" . $this->arrDate['day'] . " " . $this->arrDate['hour']
             . ":" . $this->arrDate['minute'] . ":" . $this->arrDate['second'];

    }

  }




  // ## getMySQLDate ##################################################
  public function getMySQLDate() {

    return $this->arrDate['year'] . "-" . $this->arrDate['month'] . "-" . $this->arrDate['day'];

  }




  // ## getMySQLDatetime ##############################################
  public function getMySQLDatetime() {

    return $this->arrDate['year'] . "-" . $this->arrDate['month'] . "-" . $this->arrDate['day'] . " " . $this->arrDate['hour']
           . ":" . $this->arrDate['minute'] . ":" . $this->arrDate['second'];

  }




  // ## getWeek #######################################################
  public function getWeek() {

    return $this->arrDate['week'];

  }




  // ## fillArrDate ###################################################
  private function fillArrDate() {

    $this->arrDate['year'] = date( "Y", $this->timestamp );
    $this->arrDate['month'] = date( "m", $this->timestamp );
    $this->arrDate['week'] = date( "W", $this->timestamp );
    $this->arrDate['day'] = date( "d", $this->timestamp );
    $this->arrDate['hour'] = date( "H", $this->timestamp );
    $this->arrDate['minute'] = date( "i", $this->timestamp );
    $this->arrDate['second'] = date( "s", $this->timestamp );

  }

}
?>
