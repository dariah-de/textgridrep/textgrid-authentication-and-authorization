<?php
// ####################################################################
// Version: 0.2.4
// Author: Markus Widmer
// Created: 12.10.2006
// Modified: 09.04.2008




class XML implements iXML {

  // ## Klassenvariablen ##############################################
  private $rootNode = Array();




  // ## Konstruktor ###################################################
  public function __construct() {
  }




  // ## getRoot #######################################################
  public function getRoot( $inName = "default" ) {

    if( isset( $this->rootNode[$inName] ) ) {

      return $this->rootNode[$inName];

    }
    else {

      return Array();

    }

  }




  // ## parseFile #####################################################
  public function parseFile( $inFilename, $inName = "default" ) {

    if( file_exists( $inFilename ) ) {

      $this->parse( file_get_contents( $inFilename ), $inName );

    }
    else {

      throw new Exception( "XML::parseFile() File does not exists: " . $inFilename );

    }

  }




  // ## parse #########################################################
  public function parse( $inData, $inName = "default" ) {

    $parser = xml_parser_create();  // Der Parser
    $arrVal = Array();              // Hier werden die erhaltenen Werte abgelegt
    $arrValClean = Array();         // Werte ohne CDATA
    $index = Array();               // Ein Index des Inhalts
    $node = Array();                // Zum Ablegen der erzeugten Knoten
    $tmpNode = new Node();          // Knoten zum Anhaengen der Ergebnisknoten
    $i = 0;                         // Schleifenvariable


    // Um den XML-Parser benutzen zu koennen wird ein Dummy-Knoten
    // um die Daten herum gebaut und hinterher wieder entfernt. Dies
    // dient dazu auch nicht korrekte XML-Dateien, bei denen es mehrere
    // Root-Knoten gibt, parsen zu koennen.
    $inData = "<DDUUMMYY>" . $inData . "</DDUUMMYY>";


    // Der XML-Parser versteht den XML-Kopf mit Angaben
    // zur Version und zum verwendeten Zeichensatz nicht.
    // Dieser muss hier manuell ausgewertet werden.
    $arrXmlHeader = Array();
    preg_match( "/<\?\s*xml\s.+\?>/i", $inData, $arrXmlHeader );


    if( sizeof( $arrXmlHeader ) == 1 ) {

      $inData = preg_replace( "/<\?\s*xml\s.+\?>/i", "", $inData );

    }


    // The data needs only to encoded if it is not
    // already. In any case they are parsed into an array.
    // The decision is made funded on the information given
    // in the XML file itself. If no information is given,
    // we do not encode but use the data as it is.
    if(    (sizeof( $arrXmlHeader ) == 1)
        && !preg_match( "/encoding\s*=\s*.?utf[-]?8/i", $arrXmlHeader[0] ) ) {

      xml_parse_into_struct( $parser, utf8_encode( $inData ), $arrVal, $index );

    }
    else {

      xml_parse_into_struct( $parser, $inData, $arrVal, $index );

    }


    xml_parser_free( $parser );


    // Alle CDATA-Informationen aus dem Array entfernen, um die
    // weitere Bearbeitung zu beschleunigen.
    for( $i = 0; $i < sizeof( $arrVal ); $i++ ) {

      if( $arrVal[$i]['type'] != "cdata" ) {

        $arrValClean[] = $arrVal[$i];

      }

    }


    // Den Array auswerten und Objekte daraus erzeugen
    $node = $this->parseArrayToNode( $arrValClean, 1 );


    // Etwas Speicher freigeben
    unset( $vals );
    unset( $index );


    // Entfernen des Dummy-Knotens
    $node = $node[0]->getChildArray();


    // Besteht der Array "node" nur aus einem Eintrag, so kann
    // der Knoten uebernommen werden. Besteht er aus mehreren,
    // so wird er unterhalb eines neu erzeugten Knotens
    // abgelegt. Ist der Array leer,
    // so ist ein Fehler aufgetreten und es wird nichts gespeichert.
    if( !$node || sizeof( $node ) <= 0 ) {

      return false;

    }
    else {

      $this->rootNode[$inName] = $node;


      return true;

    }

  }




  // ## parseArrayToNode ##############################################
  private function parseArrayToNode( Array $inArray, $inLevel, $inArrNamespace = null ) {

    $node = new Node();       // Zum Anlegen neuer Knoten
    $arrNode = Array();       // Array der neu angelegten Knoten
    $arrAttribute = Array();  // Zwischenspeicher fuer die Attribute
    $arrSplit = Array();
    $flagFound = false;       // Flag, das zur Suche nach dem Knotenende verwendet wird
    $i = 0;                   // Schleifenvariable
    $j = 0;                   // Schleifenvariable
    $c = 0;                   // Schleifenvariable


    // Setting the array to be an empty array if it is
    // not already set.
    $inArrNamespace == null ? $inArrNamespace = Array() : false;


    $i = 0;
    while( $i < sizeof( $inArray ) ) {

      if(    ($inArray[$i]['type'] == "complete")
          && ($inArray[$i]['level'] == $inLevel) ) {

        $node = new Node();


        if( !isset( $inArray[$i]['value'] ) ) {

          $inArray[$i]['value'] = "";

        }


        $node->setValue( trim( $inArray[$i]['value'] ) );


        // Every attribute except the xmlns attributes
        // are stored as normal attributes. The xmlns
        // is stored as special value in the node.
        if( isset( $inArray[$i]['attributes'] ) ) {

          $arrAttribute = $inArray[$i]['attributes'];

          foreach( $arrAttribute as $key => $value ) {

            $arrMatch = Array();
            if( preg_match( "/^xmlns:(.+)$/i", $key, $arrMatch ) ) {

              $inArrNamespace[$arrMatch[1]] = $value;

            }
            elseif( preg_match( "/^xmlns$/i", $key ) ) {

              $inArrNamespace['defaultNamespace'] = $value;

            }
            else {

              $node->setAttribute( strtolower( $key ), $value );

            }

          }

        }


        // To open the possibility to use the alias instead of the
        // namespace while asking a node for childs, the aliases
        // in the XML file are passed as well to every node.
        foreach( $inArrNamespace as $key => $value ) {

          $node->setNamespaceAlias( strtolower( $key ), $value );

        }


        $arrSplit = preg_split( "/[:]/", $inArray[$i]['tag'] );

        if(    (sizeof( $arrSplit ) > 1)
            && (isset( $inArrNamespace[$arrSplit[0]] )) ) {

          $node->setName( $arrSplit[1], $inArrNamespace[$arrSplit[0]] );

        }
        else {

          if( isset( $inArrNamespace['defaultNamespace'] ) ) {

            $node->setName( $inArray[$i]['tag'], $inArrNamespace['defaultNamespace'] );

          }
          else {

            $node->setName( $inArray[$i]['tag'] );

          }

        }


        // Add the new node to the array
        $arrNode[] = $node;

      }
      elseif(    preg_match( "/open/i", $inArray[$i]['type'] )
              && ($inArray[$i]['level'] == $inLevel) ) {

        # Ende des Knoten finden
        $j = $i;
        $flagFound = false;
        while(    ($j < sizeof( $inArray ))
               && !$flagFound ) {

          if(    preg_match( "/close/i", $inArray[$j]['type'] )
              && ($inArray[$j]['level'] == $inArray[$i]['level'])
              && preg_match( "/" . $inArray[$i]['tag'] . "/i", $inArray[$j]['tag'] ) ) {

            $flagFound = true;

          }
          else {

            $j++;

          }

        }


        # Create a new node
        $node = new Node();


        // Every attribute except the xmlns attributes
        // are stored as normal attributes. The xmlns
        // is stored as special value in the node.
        if( isset( $inArray[$i]['attributes'] ) ) {

          $arrAttribute = $inArray[$i]['attributes'];

          foreach( $arrAttribute as $key => $value ) {

            $arrMatch = Array();
            if( preg_match( "/^xmlns:(.+)$/i", $key, $arrMatch ) ) {

              $inArrNamespace[$arrMatch[1]] = $value;

            }
            elseif( preg_match( "/^xmlns$/i", $key ) ) {

              $inArrNamespace['defaultNamespace'] = $value;

            }
            else {

              $node->setAttribute( strtolower( $key ), $value );

            }

          }

        }


        // To open the possibility to use the alias instead of the
        // namespace while asking a node for childs, the aliases
        // in the XML file are passed as well to every node.
        foreach( $inArrNamespace as $key => $value ) {

          $node->setNamespaceAlias( strtolower( $key ), $value );

        }


        $arrSplit = preg_split( "/[:]/", $inArray[$i]['tag'] );

        if(    (sizeof( $arrSplit ) > 1)
            && (isset( $inArrNamespace[$arrSplit[0]] )) ) {

          $node->setName( $arrSplit[1], $inArrNamespace[$arrSplit[0]] );

        }
        else {

          if( isset( $inArrNamespace['defaultNamespace'] ) ) {

            $node->setName( $inArray[$i]['tag'], $inArrNamespace['defaultNamespace'] );

          }
          else {

            $node->setName( $inArray[$i]['tag'] );

          }

        }


        $child = $this->parseArrayToNode( array_slice( $inArray, $i + 1, $j - $i - 1 ), $inLevel + 1, $inArrNamespace );


        for( $c = 0; $c < sizeof( $child ); $c++ ) {

          $node->addChild( $child[$c] );

        }


        // Add the new node to the array
        $arrNode[] = $node;

      }
      elseif(    ($inArray[$i]['type'] == "cdata")
              && ($inArray[$i]['level'] == $inLevel) ) {

      }


      $i++;

    }


    return $arrNode;

  }

}
?>
