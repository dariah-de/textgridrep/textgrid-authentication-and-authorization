<?php
// ####################################################################
// Version: 0.1.0
// Autor: Markus Widmer
// Erstellungsdatum: 13.12.2006
// Letzte Aenderung: 13.12.2006




class Crypto implements iCrypto {

  // ## Klassenvariablen ##############################################




  // ## Konstruktor ###################################################
  function __construct() {
  }




  // ## Destruktor ####################################################
  function __destruct() {
  }




  // ## ssha ##########################################################
  function ssha( $inPassword ) {

    $hash = "";  // Der Hash des Passworts
    $salt = "";  // Das Salt


    // Zufallszahl erzeugen
    mt_srand( (double)microtime()*1000000 );


    // "Salz" erzeugen
    $salt = mhash_keygen_s2k( MHASH_SHA1, $inPassword, substr( pack( 'h*', md5( mt_rand() ) ), 0, 8), 4);


    // Den "Hash" erzeugen
    $hash = "{SSHA}" . base64_encode( mhash( MHASH_SHA1, $inPassword . $salt ) . $salt);


    return $hash;

  }




  // ## md5 ###########################################################
  function md5( $inPassword ) {

    return "{MD5}" . md5( $inPassword );

  }




  // ## ntPassword ####################################################
  function ntPassword( $inPassword ) {

    return strtoupper( hash( "md4", mb_convert_encoding( $inPassword, "UCS-2LE" ) ) );

  }




  // ## validatePassword ##############################################
  function validatePassword( $inPassword, $inHash ) {

    $hash = "";          // Der Hash ohne "{Was bin ich}"
    $newHash = "";       // Das verschluesselte Referenzpasswort
    $originalHash = "";  // Der SSHA-Hash ohne Salt
    $salt = "";          // Das Salt bei SSHA


    if( preg_match( "/^\{ssha\}/i", $inHash ) ) {

      // "{SSHA}" am Anfang abschneiden
      $hash = preg_replace( "/^\{ssha\}/i", "", $inHash );


      $hash = base64_decode( $hash );


      // Hash und Salt voneinander trennen
      $originalHash = substr( $hash, 0, 20 );
      $salt = substr( $hash, 20 );


      // Neuen Hash erzeugen um ihn mit dem alten zu vergleichen
      $newHash = "{SSHA}" . base64_encode( mhash( MHASH_SHA1, $inPassword . $salt ) . $salt);


      if( strcmp( $inHash, $newHash ) == 0 ) {

        return true;

      }
      else {

        return false;

      }

    }
    elseif( preg_match( "/^\{md5\}/i", $inHash ) ) {

      $hash = preg_replace( "/^\{md5\}/i", "", $inHash );
      $newHash = preg_replace( "/^\{md5\}/i", "", $this->md5( $inPassword ) );

      return preg_match( "/^" . $newHash . "$/", $hash );

    }
    else {

      return false;

    }

  }

}
?>
