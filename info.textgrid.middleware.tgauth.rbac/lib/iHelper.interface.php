<?php
interface iHelper {

//  public function numberFormat( $inNumber, $inAfterComma, $inLanguage, $inRound = false );
//  public function expandNumber( $inNumber, $inDigit );
  public function isUtf8( $inString );
  public function flatAddressToArray( $inAddress, $inSplit );
  public function extractFilename( $inPath );
//  public function generalizedtimeToTextDate( $inDate, $inCountry );
//  public function textDateToGeneralizedtime( $inText, $inCountry );
//  public function generalizedtimeToTextDateTime( $inText, $inCountry );
//  public function mysqlDatetimeToArray( $inDate, $inCountry );

}
?>