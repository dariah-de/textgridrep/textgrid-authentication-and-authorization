<?php
interface iXML {

  public function parse( $inData, $inName = "default" );
  public function parseFile( $inFilename, $inName = "default" );
  public function getRoot( $inName = "default" );

}
?>
