<?php
// ####################################################################
// Version: 0.1.0
// Author: Markus Widmer
// Created: 01.04.2008
// Modified: 01.04.2008




class Number implements iNumber {

  // ## Klassenvariablen ##############################################




  // ## Konstruktor ###################################################
  public function __construct() {
  }




  // ## format ########################################################
  function format( $inNumber, $inAfterComma, $inLanguage, $inRound = false ) {

    if( $inRound ) {

      $inNumber = round( $inNumber, $inRound + 1 );

    }


    switch( $inLanguage ) {

      case "de": return number_format( $inNumber, $inAfterComma, ",", "." );
                 break;
      case "en": return number_format( $inNumber, $inAfterComma, ".", "," );
                 break;
      default  : return $inNumber;

    }

  }




  // ## expand ########################################################
  function expand( $inNumber, $inDigit ) {

    settype( $inNumber, "string" );


    while( strlen( $inNumber ) < $inDigit ) {

      $inNumber = "0" . $inNumber;

    }


    return $inNumber;

  }

}
?>
