<?php
// ####################################################################
// Version: 0.3.1
// Autor: Markus Widmer
// Erstellungsdatum: 11.10.2006
// Letzte Aenderung: 12.07.2008



class Node implements iNode {

  // ## Klassenvariablen ##############################################
  private $value = false;
  private $name = "NODE";
  private $namespace = "";
  private $arrNamespaceAlias = Array();
  private $child = Array();
  private $attribute = Array();




  // ## Konstruktor ###################################################
  public function __construct() {
  }




  // ## Destruktor ####################################################
  public function __destruct() {
  }




  // ## setName #######################################################
  public function setName( $inName, $inNamespace = null ) {

    // The name must not be an array or an object to be
    // stored.
    if(    !is_object( $inName )
        && !is_array( $inName ) ) {

      $this->name = strtolower( $inName );


      // If a namespace is given, store it as well
      if(    ($inNamespace != null)
          && !is_array( $inNamespace )
          && !is_object( $inNamespace ) ) {

        $this->namespace = $inNamespace;

      }


      return true;

    }
    else {

      return false;

    }

  }




  // ## getName #######################################################
  public function getName() {

    return $this->name;

  }




  // ## setAttribute ##################################################
  public function setAttribute( $inName, $inValue ) {

    // Der Name darf weder ein Objekt, ein Array oder eine
    // ausfuehrbare Funktion sein. Ebenso der Wert.
    if(    !is_object( $inName )
        && !is_array( $inName )
        && !is_object( $inValue )
        && !is_array( $inValue ) ) {

      $this->attribute[$inName] = strval( $inValue );


      return true;

    }
    else {

      return false;

    }

  }




  // ## getAttribute ##################################################
  public function getAttribute( $inName ) {

    // Das Attribut muss einen Wert haben
    if( isset( $this->attribute[$inName] ) ) {

      return $this->attribute[$inName];

    }

    // Sonst wird ein leerer String zurueckgegeben
    else {

      return "";

    }

  }



  // ## setValue ######################################################
  public function setValue( $inValue ) {

    // Ein Knoten kann nur dann einen Wert haben, wenn
    // er keine Kinderknoten hat.
    if( !$this->child || sizeof( $this->child ) == 0 ) {

      // Der Wert darf weder ein Objekt, ein Array oder eine
      // ausfuehrbare Funktion sein, um gespeichert zu werden.
      if(    !is_object( $inValue )
          && !is_array( $inValue ) ) {

        $this->value = strval( $inValue );


        return true;

      }
      else {

        return false;

      }

    }
    else {

      return false;

    }

  }




  // ## getValue ######################################################
  public function getValue() {

    return $this->value;

  }




  // ## setNamespace ##################################################
  public function setNamespace( $inNamespace ) {

    if(    !is_object( $inNamespace )
        && !is_array( $inNamespace ) ) {

      $this->namespace = $inNamespace;

    }

  }




  // ## getNamespace ##################################################
  public function getNamespace() {

    return $this->namespace;

  }




  // ## setNamespaceAlias #############################################
  public function setNamespaceAlias( $inAlias, $inNamespace ) {

    $this->arrNamespaceAlias[$inAlias] = $inNamespace;

  }




  // ## addChild ######################################################
  public function addChild( iNode $inNode ) {

    // Ein Knoten kann nur dann Kinderknoten haben, wenn er
    // nicht bereits einen Wert hat. Der uebergebene Wert
    // muss zudem auch wirklich ein Knoten sein.
    if(    ($this->value == false)
        && ($inNode instanceof iNode) ) {

      $this->child[] = $inNode;


      return true;

    }
    else {

      return false;

    }

  }




  // ## getChildArray #################################################
  public function getChildArray() {

    if( is_array( $this->child ) ) {

      return $this->child;

    }
    else {

      return Array();

    }

  }




  // ## getChild ######################################################
  public function getChild( $inName, $inNumber, $inNamespace = null ) {

    // Es wird auf jeden Fall ein korrekter Knoten
    // zurueckgegeben, auch wenn keiner gefunden
    // wurde.
    $wantedNode = new Node();
    $wantedNode->setName( $inName );


    // Start the search for the node by looking at every child
    $i = 0;
    $n = 0;
    $flagFound = false;
    while(    ($i < sizeof( $this->child ))
           && !$flagFound ) {

      // Get one child node
      $child = $this->child[$i];


      // Name is correct...
      if( preg_match( "/^" . $child->getName() . "$/i", $inName ) ) {

        // If a namespace is given and the node has this
        // namespace then it is considered to be the
        // one we are looking for. Otherwise it is ignored.
        if(    (    $inNamespace != null
                 && (    hash( "md5", $child->getNamespace() ) == hash( "md5", $inNamespace ) )
                      || (    isset( $this->arrNamespaceAlias[strtolower($inNamespace)] )
                           && hash( "md5", $child->getNamespace() ) == hash( "md5", $this->arrNamespaceAlias[strtolower($inNamespace)] ) ) )
            || ($inNamespace == null) ) {

          // Number is korrekt...
          if( $inNumber == $n ) {

            $flagFound = true;
            $wantedNode = $child;

          }
          else {

            $n++;

          }

        }

      }


      // Schleifenvariable hochzaehlen
      $i++;

    }


    return $wantedNode;

  }




  // ## searchChild ###################################################
  public function searchChild( $inName, $inAttribute, $inRegex, $inNamespace = null ) {

    $arrNode = Array();   // Der Rueckgabewert
    $child = new Node();  // Temporaerer Knoten
    $i = 0;               // Schleifenvariable


    for( $i = 0; $i < $this->countChilds( $inName ); $i++ ) {

      $child = $this->getChild( $inName, $i, $inNamespace );


      if( preg_match( $inRegex, $child->getAttribute( $inAttribute ) ) ) {

        $arrNode[] = $child;

      }

    }


    return $arrNode;

  }




  // ## countChilds ###################################################
  public function countChilds( $inName, $inNamespace = null ) {

    $number = 0;  // Anzahl der Knoten
    $i = 0;       // Schleifenvariable


    // Alle Knoten durchgehen
    for( $i = 0; $i < sizeof( $this->child ); $i++ ) {

      if( preg_match( "/^" . $this->child[$i]->getName() . "$/i", $inName ) ) {

        $number++;

      }

    }


    return $number;

  }

}
?>