<?php
// ####################################################################
// Version: 0.1.0
// Author: Markus Widmer
// Created: 19.01.2007
// Modified: 07.10.2007




class Authentication {

  // ## Class-variables ###############################################
  private $connection;




  // ## Constructor ###################################################
  public function __construct( CConnection $inConnection ) {

    // To use the connection it has to have an established
    // connection to the database.
    if( $inConnection->handle->hasConnection() ) {

      $this->connection = $inConnection;

    }
    else {

      throw new Exception( "Authentication::__construct() Unable to use this connection!" );

    }

  }




  // ## ldap ##########################################################
  public function ldap( $inUsername, $inPassword, $inAttribute = "uid", Array $inArrProvide = null ) {

    $log = new Log();
    $filter = "";     // The LDAP filter to search for the user
    $dn = "";         // The DN of the user
    $result = false;  // The default return value is false


    // The type of connection has to be a LDAP connection
    if( $this->connection instanceof CLDAPConnection ) {

      // If the username is just a name, search for it
      // in the directory before trying to bind.
      if( !preg_match( "/^(.+=.+)+,(.+=.+)$/", $inUsername ) ) {

        // Search for a user that fits the username
        $log->debug( $this->connection->filter . "\n" );
        $filter  = "(&" . $this->connection->filter;
        $filter .= "(" . $inAttribute . "=" . $inUsername . "))";


        // Ask the directory
        $arrUserEntry = $this->connection->handle->search( $this->connection->base, $filter, "sub", Array( $inAttribute ) );


        if( $arrUserEntry && sizeof( $arrUserEntry ) == 1 ) {

          if( $this->connection->handle->bind( $arrUserEntry[0]['dn'], $inPassword ) ) {

            $result = $arrUserEntry[0]['dn'];

          }

        }

      }
      else {

        if( $this->connection->handle->bind( $inUsername, $inPassword ) ) {

          $result = $inUsername;

        }

      }

    }
    else {

      throw new Exception( "Authentication::setMethod() Cannot use this connection in LDAP-mode." );

    }


    return $result;

  }

}
?>
