<?php
// ####################################################################
// Version: 0.6.3
// Author: Markus Widmer
// Created: 28.11.2006
// Modified: 27.04.2008




class Helper implements iHelper {

  // ## Klassenvariablen ##############################################




  // ## Konstruktor ###################################################
  public function __construct() {
  }




  // ## extractFilename ###############################################
  public function extractFilename( $inPath ) {

    $arrComponent = Array();  // Die einzelnen Komponentn des Pfads


    $arrComponent = preg_split( "/\//", $inPath );


    return $arrComponent[(sizeof( $arrComponent ) - 1)];

  }




  // ## isUtf8 ########################################################
  public function isUtf8( $inString ) {

    return (utf8_encode( utf8_decode( $inString ) ) == $inString);

  }




  // ## ensureUtf8 ####################################################
  public function ensureUtf8( $inString ) {

    return ( $this->isUtf8( $inString ) ? $inString : utf8_encode( $inString ) );

  }




  // ## flatAddressToArray ############################################
  public function flatAddressToArray( $inAddress, $inSplit ) {

    $arrRawAddress = Array();  // Die erste Bearbeitung der Adresse
    $arrAddress = Array();     // Endversion
    $zipIndex = false;         // Stelle im Array, an der die PLZ steht
    $i = 0;                    // Schleifenvariable


    $arrAddress['raw'] = $inAddress;
    $arrRawAddress = preg_split( "/[" . $inSplit . "]/", $inAddress );


    // Im letzten Eintrag muss der Laendercode stehen
    $arrAddress['countrycode'] = trim( strtolower( array_pop( $arrRawAddress ) ) );


    if( preg_match( "/^de$/i", trim( $arrAddress['countrycode'] ) ) ) {

      // Herausfinden, an welcher Stelle die Postleitzahl steht
      for( $i = 0; $i < sizeof( $arrRawAddress ); $i++ ) {

        if( preg_match( "/[0-9]{5}/", trim( $arrRawAddress[$i] ) ) ) {

          $zipIndex = $i;
          $arrAddress['zip'] = trim( $arrRawAddress[$i] );

        }

      }


      if( $zipIndex ) {

        // Alle Eintraege davor gehoeren zur Anschrift
        for( $i = 0; $i < $zipIndex; $i++ ) {

          $arrAddress['address'][$i] = trim( $arrRawAddress[$i] );

        }


        // Der Eintrag nach nach der PLZ ist die Stadt
        if($arrRawAddress && sizeof( $arrRawAddress ) > $zipIndex + 1 ) {

          $arrAddress['town'] = trim( $arrRawAddress[$zipIndex+1] );

        }


        // Land
        $arrAddress['country'] = trim( $arrAddress['countrycode'] );

      }
      else {

        // Alle Eintraege gehoeren zur Anschrift
        for( $i = 0; $i < sizeof( $arrRawAddress ); $i++ ) {

          $arrAddress['address'][$i] = trim( $arrRawAddress[$i] );

        }

      }

    }


    return $arrAddress;

  }




  // ## extractLanguage ###############################################
  public function extractLanguage( $inServerString ) {

    $arrCode = Array();         // Alle gefunden moeglichen Codes
    $arrCombination = Array();  // Alle Kombinationen aus Sprache-Land
    $arrBlacklist = Array();    // Diese Codes nicht verwenden, da sie ein Land bezeichnen
    $arrReturn = Array();       // Die gueltigen gefundenen Sprachen
    $i = 0;                     // Schleifenvariable


    preg_match_all( "/[a-z]{2}/i", $inServerString, $arrCode );
    preg_match_all( "/[a-z]{2}-[a-z]{2}/i", $inServerString, $arrCombination );


    // Nur der erste Array-Eintrag ist von Interesse in diesem Fall
    $arrCode = $arrCode[0];
    $arrCombination = $arrCombination[0];


    for( $i = 0; $i < sizeof( $arrCombination ); $i++ ) {

      $arrTmp = split( "-", $arrCombination[$i] );


      // Der hintere Teil bezieht sich auf das Land. Wenn aber hinterer und vorderer
      // Teil gleich sind, darf es trotzdem nicht auf die Blacklist.
      !preg_match( "/" . $arrTmp[0] . "/", $arrTmp[1] ) ? array_push( $arrBlacklist, $arrTmp[1] ) : false;

    }


    for( $i = 0; $i < sizeof( $arrCode ); $i++ ) {

      if(    !in_array( $arrCode[$i], $arrReturn )
          && !in_array( $arrCode[$i], $arrBlacklist ) ) {

        array_push( $arrReturn, strtolower( $arrCode[$i] ) );

      }

    }


    return $arrReturn;

  }




  // ## stripQuotes ###################################################
  public function stripQuotes( $inString, $inReplace = "" ) {

    return rawurldecode( preg_replace( "/(%5C%22)|(%5C%27)/", $inReplace, rawurlencode( $inString ) ) );

  }



/*
  // ## generalizedtimeToTextDateTime #################################
  public function generalizedtimeToTextDateTime( $inDate, $inCountry ) {

    $datestring = "";  // Formatiertes Datum mit Uhrzeit


    if( $inDate != "" ) {

      if( preg_match( "/de/i", $inCountry ) ) {

        $datestring .= substr( $inDate, 6, 2 ) . "." . substr( $inDate, 4, 2 ) . "." . substr( $inDate, 0, 4 ) . " / ";
        $datestring .= substr( $inDate, 8, 2 ) . ":" . substr( $inDate, 10, 2 ) . ":" . substr( $inDate, 12, 2 );

      }
      elseif( preg_match( "/en/i", $inCountry ) ) {

        $datestring .= substr( $inDate, 6, 2 ) . "/" . substr( $inDate, 4, 2 ) . "/" . substr( $inDate, 0, 4 ) . " - ";
        $datestring .= substr( $inDate, 8, 2 ) . ":" . substr( $inDate, 10, 2 ) . ":" . substr( $inDate, 12, 2 );

      }
      else {

        $datestring = $inDate;

      }

    }


    return $datestring;

  }
*/


/*
  // ## generalizedtimeToTextDate #####################################
  public function generalizedtimeToTextDate( $inDate, $inCountry ) {

    $datestring = "";  // Formatiertes Datum


    if( $inDate != "" ) {

      if( preg_match( "/de/i", $inCountry ) ) {

        $datestring .= substr( $inDate, 6, 2 ) . "." . substr( $inDate, 4, 2 ) . "." . substr( $inDate, 0, 4 );

      }
      elseif( preg_match( "/en/i", $inCountry ) ) {

        $datestring .= substr( $inDate, 6, 2 ) . "/" . substr( $inDate, 4, 2 ) . "/" . substr( $inDate, 0, 4 );        

      }
      else {

        $datestring = $inDate;

      }

    }


    return $datestring;

  }
*/


/*
  // ## textDateToGeneralizedtime #####################################
  public function textDateToGeneralizedtime( $inText, $inCountry ) {

    $datestring = "";          // Datum im Format generalizedtime
    $convert = new Convert();  // Konvertierungsklasse


    if( $inText != "" ) {

      if( preg_match( "/de/i", $inCountry ) ) {

        $tmp = split( "\.", $inText );


        if( sizeof( $tmp ) == 3 ) {

          $datestring .= $convert->expandNumber( trim( $tmp[2] ), 4 );
          $datestring .= $convert->expandNumber( trim( $tmp[1] ), 2 );
          $datestring .= $convert->expandNumber( trim( $tmp[0] ), 2 ) . "000000Z";

        }
        else {

          $datestring = $inText;

        }

      }
      elseif( preg_match( "/en/i", $inCountry ) ) {

        $tmp = split( "/", $inText );


        if( sizeof( $tmp ) == 3 ) {

          $datestring .= $convert->expandNumber( trim( $tmp[2] ), 4 );
          $datestring .= $convert->expandNumber( trim( $tmp[1] ), 2 );
          $datestring .= $convert->expandNumber( trim( $tmp[0] ), 2 ) . "000000Z";

        }
        else {

          $datestring = $inText;

        }

      }
      else {

        $datestring = $inText;

      }

    }


    return $datestring;

  }
*/


/*
  // ## textToMysqlDatetime ###########################################
  public function textToMysqlDatetime( $inText, $inCountry ) {

    $tmp = Array();
    $tmpDate = Array();
    $tmpTime = Array();
    $dateTime = "";


    if( preg_match( "/^de$/i", $inCountry ) ) {

      if( preg_match( "/^[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{4}(\s[0-9]{1,2}(:[0-9]{1,2}){1,2})?$/", $inText ) ) {

        $tmp = split( " ", $inText );

      }
      else {

        $tmp = false;

      }


      if( is_array( $tmp ) ) {

        isset( $tmp[0] ) ? $tmpDate = split( "\.", $tmp[0] ) : $tmpDate = Array();
        isset( $tmp[1] ) ? $tmpTime = split( ":", $tmp[1] ) : $tmpTime = Array();

        sizeof( $tmpDate ) == 3 ? $dateTime .= $tmpDate[2] . "-" . $tmpDate[1] . "-" . $tmpDate[0] : false;
        sizeof( $tmpTime ) >= 2 ? $dateTime .= " " . $tmpTime[0] . ":" . $tmpTime[1] : false;
        sizeof( $tmpTime ) == 3 ? $dateTime .= ":" . $tmpTime[2] : false;


        return $dateTime;

      }
      else {

        return false;

      }

    }
    elseif( preg_match( "/^en$/i", $inCountry ) ) {

      return false;

    }
    else {

      return false;

    }

  }
*/


/*
  // ## mysqlDatetimeToArray ##########################################
  public function mysqlDatetimeToArray( $inDate, $inCountry ) {

    $tmp = Array();
    $tmpDate = Array();
    $tmpTime = Array();
    $arrDateAndTime = Array();


    if( $inDate != "" ) {

      // Aufteilen des Datums in seine Bestandteile
      $tmp = split( " ", $inDate );
      $tmpDate = split( "-", $tmp[0] );
      $tmpTime = split( ":", $tmp[1] );


      if( preg_match( "/^de$/i", $inCountry ) ) {

        // Das Datum zusammensetzen
        if( sizeof( $tmpDate ) == 3 ) {

          $arrDateAndTime['date'] = trim( $tmpDate[2] ) . "." . trim( $tmpDate[1] ) . "." . trim( $tmpDate[0] );
          $arrDateAndTime['day'] = $tmpDate[2];
          $arrDateAndTime['month'] = $tmpDate[1];
          $arrDateAndTime['year'] = $tmpDate[0];

        }
        else {

          $arrDateAndTime['date'] = $tmp[0];

        }


        // Die Uhrzeit kann einfach uebernommen werden
        $arrDateAndTime['time'] = join( ":", $tmpTime );
        isset( $tmpTime[0] ) ? $arrDateAndTime['hour'] = $tmpTime[0] : $arrDateAndTime['hour'] = 0;
        isset( $tmpTime[1] ) ? $arrDateAndTime['minute'] = $tmpTime[1] : $arrDateAndTime['minute'] = 0;
        isset( $tmpTime[2] ) ? $arrDateAndTime['second'] = $tmpTime[2] : $arrDateAndTime['second'] = 0;

      }
      elseif( preg_match( "/^en$/i", $inCountry ) ) {

        // Das Datum zusammensetzen
        if( sizeof( $tmpDate ) == 3 ) {

          $arrDateAndTime['date'] = trim( $tmpDate[0] ) . "/" . trim( $tmpDate[1] ) . "/" . trim( $tmpDate[2] );
          $arrDateAndTime['day'] = $tmpDate[2];
          $arrDateAndTime['month'] = $tmpDate[1];
          $arrDateAndTime['year'] = $tmpDate[0];

        }
        else {

          $arrDateAndTime['date'] = $tmp[0];

        }


        // Die Uhrzeit kann einfach uebernommen werden
        $arrDateAndTime['time'] = join( ":", $tmpTime );
        isset( $tmpTime[0] ) ? $arrDateAndTime['hour'] = $tmpTime[0] : $arrDateAndTime['hour'] = 0;
        isset( $tmpTime[1] ) ? $arrDateAndTime['minute'] = $tmpTime[1] : $arrDateAndTime['minute'] = 0;
        isset( $tmpTime[2] ) ? $arrDateAndTime['second'] = $tmpTime[2] : $arrDateAndTime['second'] = 0;

      }
      else {

        $arrDateAndTime['date'] = $tmp[0];
        $arrDateAndTime['time'] = $tmp[1];

      }

    }
    else {

     $datestring = "";

    }


    return $arrDateAndTime;

  }
*/
}
?>
