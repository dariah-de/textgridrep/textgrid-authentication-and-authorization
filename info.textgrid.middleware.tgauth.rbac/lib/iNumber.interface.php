<?php
interface iNumber {

  public function format( $inNumber, $inAfterComma, $inLanguage, $inRound = false );
  public function number( $inNumber, $inDigit );

}
?>
