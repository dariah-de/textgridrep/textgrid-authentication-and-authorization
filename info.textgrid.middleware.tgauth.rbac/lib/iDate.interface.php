<?php
interface iDate {

  public function __construct( $inTimestamp, $inLanguage = false );
  public function parseMySQLDatetime( $inDatetime );
  public function parseMySQLDate( $inDate );
  public function parseGeneralizedTime( $inGeneralizedTime );
  public function setLanguage( $inLanguage );
  public function setTimestamp( $inTimestamp );
  public function parseDateFirst( $inText );
  public function getArray();
  public function getDateText();
  public function getDateTimeText();
  public function getMySQLDate();
  public function getMySQLDatetime();
  public function getWeek();

}
?>
