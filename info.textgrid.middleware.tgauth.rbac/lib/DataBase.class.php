<?php
// ####################################################################
// Version: 0.4.0
// Author: Markus Widmer
// Created: 09.11.2006
// Modified: 12.07.2008




class DataBase implements iDataBase {

  // ## Class-variables ###############################################
  private $host = "";
  private $database = "";
  private $arrData = Array();
  private $arrFieldname = Array();
  private $username = "";
  private $password = "";
  private $hasConnection = false;
  private $connection;




  // ## Constructor ###################################################
  public function __construct( $inHost, $inDatabase ) {

    $this->host = $inHost;
    $this->database = $inDatabase;

  }




  // ## Wakeup ########################################################
  public function __wakeup() {

    if( $this->hasConnection() ) {

      $this->reconnect();

    }

  }




  // ## connect #######################################################
  public function connect( $inUsername, $inPassword ) {

    $this->username = $inUsername;
    $this->password = $inPassword;

    if( $mysqlHandler = mysql_connect( $this->host, $this->username, $this->password ) ) {

      $this->hasConnection = true;
      $this->connection = $mysqlHandler;


      return true;

    }
    else {

      $this->hasConnection = false;


      return false;

    }

  }




  // ## reconnect #####################################################
  public function reconnect() {

    return $this->connect( $this->username, $this->password );

  }




  // ## hasConnection #################################################
  public function hasConnection() {

    return $this->hasConnection;

  }




  // ## get ###########################################################
  public function get( $inTable, $inOrder, $inFilter = "", $inCol = "*" ) {

    $queryString = "";         // Abfragestring, der an die Datenbank gesendet wird
    $mysqlQuery = false;       // Das Ergebnis der Abfrage
    $mysqlHandler = false;     // Verbindungs-Handler
    $strCol = "";              // Angabe zu den Spalten
    $result = Array();         // Rueckgabe
    $helper = new Helper();    // Konvertierungsklasse


/*
    if( $helper->isUtf8( $inFilter ) ) {

      $inFilter = utf8_decode( $inFilter );

    }
*/

    // Eventuell eine Liste von Spalten zusammenstellen,
    // die zurueckgegeben werden sollen
    if( is_array( $inCol ) ) {

      for( $i = 0; $i < sizeof( $inCol ); $i++ ) {

        if( $i == 0 ) {

          $strCol = $inCol[0];

        }
        else {

          $strCol .= ", " . $inCol[$i];

        }

      }

    }
    else {

      $strCol = $inCol;

    }


    // Verbindung mit der Datenbank aufbauen
    if( $this->hasConnection ) {

      // Abfrage starten
      if( $inFilter != "" ) {

        $queryString = "select " . $strCol . " from " . $this->database . "." . $inTable
                                 . " where " . $inFilter . " order by " . $inOrder . ";";


        trigger_error( "Query: " . $queryString . "\n", E_USER_NOTICE );


        $mysqlQuery = mysql_query( $queryString );

      }
      else {

        $queryString = "select " . $strCol . " from " . $this->database . "." . $inTable . " order by " . $inOrder . ";";


        trigger_error( "Query: " . $queryString . "\n", E_USER_NOTICE );


        $mysqlQuery = mysql_query( $queryString );

      }


      $result = Array();
      while( $mysqlResult = mysql_fetch_array( $mysqlQuery, MYSQL_ASSOC ) ) {

        foreach( $mysqlResult as $key => $value ) {

          if( !$helper->isUtf8( $mysqlResult[$key] ) ) {

            $mysqlResult[$key] = utf8_encode( $value );

          }

        }


        $result[] = $mysqlResult;

      }

    }
    else {

      trigger_error( "Not connected to database", E_USER_ERROR );

    }


    return $result;

  }




  // ## getColumns ####################################################
  public function getColumns( $inTable ) {

    $queryString = "";         // Abfragestring, der an die Datenbank gesendet wird
    $mysqlQuery = false;       // Das Ergebnis der Abfrage
    $mysqlHandler = false;     // Verbindungs-Handler
    $result = Array();         // Die Rueckgabe
    $helper = new Helper();    // Konvertierungsklasse


    // Verbindung mit der Datenbank aufbauen
    if( $this->hasConnection ) {

      $queryString = "show columns from " . $this->database . "." . $inTable . ";";
      $mysqlQuery = mysql_query( $queryString );


      trigger_error( "Query: " . $queryString . "\n", E_USER_NOTICE );


      while( $mysqlResult = mysql_fetch_array( $mysqlQuery, MYSQL_ASSOC ) ) {

        foreach( $mysqlResult as $key => $value ) {

          if( !$helper->isUtf8( $mysqlResult[$key] ) ) {

            $mysqlResult[$key] = utf8_encode( $value );

          }

        }


        $result[] = $mysqlResult;

      }

    }
    else {

      trigger_error( "Not connected to database", E_USER_ERROR );

    }


    return $result;

  }



  // ## store #########################################################
  public function store( $inTable, $inData ) {

    $helper = new Helper();                   // Konvertierungsklasse
    $keyInData = true;                        // Flag, ob ein primaerer Schluessel in den Daten vorkommt
    $checkIfExists = "";                      // Abfragestring, um zu ueberpruefen, ob ein Eintrag bereits existiert
    $whereClause = " where ";                 // Bedingungs-String, an dem noch weiter angebaut werden muss
    $allreadyExists = false;                  // Flag, ob ein Eintrag bereits existiert
    $columnAutoincrement = "";                // 
    $columnPrimaryKey = Array();              // Alle primaeren Schluessel, die in der Tabelle definiert sind
    $column = $this->getColumns( $inTable );  // Die vorhandenen Spalten der Tabelle
    $checkResult = Array();                   // Ergebnis der Abfrage, ob der Datensatz bereits existiert
    $returnResult = Array();                  // Rueckgabe
    $lastInsertResult = Array();              // Ergebnis der Abfrage nach dem zuletzt eingefuegten Datensatz
    $mysqlQuery = false;                      // Antwort der Datenbank auf eine Anfrage
    $mysqlResult = Array();                   // Ein Datensatz der Antwort der Datenbank
    $queryString = "";                        // Abfrage, die an die Datenbank geschickt wird
    $i = 0;                                   // Schleifenvariable
    $useUtf8 = false;                         // UTF8 zur Uebertragung verwenden



    // An diesem String muss noch weiter angebaut werden im Verlauf
    // der Funktion
    $checkIfExists = "select * from " . $this->database . "." . $inTable . " where";


    // Alle Spalten anschauen, ob
    // 1. Die Spalte ein primaerer Schluessel ist
    // 2. Dieser Schluessel in den Eingabedaten vorkommt
    // 3. Diese Spalte vielleicht ein "auto_increment" besitzt
    for( $i = 0; $i < sizeof( $column ); $i++ ) {

      if( $column[$i]['Key'] == "PRI" ) {

        // Es wird darauf geprueft, ob wirklich alle Felder, die einen
        // primaeren Schluessel darstellen, angegeben sind.
        $keyInData = $keyInData && isset( $inData[$column[$i]['Field']] ) && preg_match( "/.+/", $inData[$column[$i]['Field']] );
        $columnPrimaryKey[] = $column[$i]['Field'];


        if( $keyInData ) {

          $checkIfExists .= " " . $column[$i]['Field'] . "=\"" . $inData[$column[$i]['Field']] . "\" and";
          $whereClause .= " " . $column[$i]['Field'] . "=\"" . $inData[$column[$i]['Field']] . "\" and";

        }

      }


      if( $column[$i]['Extra'] == "auto_increment" ) {

        $columnAutoincrement = $column[$i]['Field'];

      }

    }


    // Wenn alle Schluesselwerte uebergeben wurden, dann pruefen, ob der
    // Datensatz bereits existiert
    if( $keyInData ) {

      // Verbindung mit der Datenbank aufbauen
      if( $this->hasConnection ) {

        // Abschneiden des letzten "and"
        $checkIfExists = substr( $checkIfExists, 0, sizeof( $checkIfExists ) - 4 ) . ";";
        $mysqlQuery = mysql_query( $checkIfExists );


        $checkResult = Array();
        while( $mysqlResult = mysql_fetch_array( $mysqlQuery, MYSQL_ASSOC ) ) {

          $checkResult[] = $mysqlResult;

        }


        $allreadyExists = $checkResult && sizeof( $checkResult ) > 0;

      }
      else {

        trigger_error( "Not connected to database", E_USER_ERROR );

      }

    }



    // Wenn sowohl alle Schluessel angegeben sind in den Daten und der Datensatz schon
    // existiert, dann wird ein "update"-Befehl abgesetzt. Sonst ein "insert"-Befehl
    if( $keyInData && $allreadyExists ) {

      $queryString = "update " . $this->database . "." . $inTable . " set ";

      foreach( $inData as $key => $value ) {

        $queryString .= " " . $key . "=\"" . $value . "\", ";


        if(    $helper->isUtf8( $key )
            || $helper->isUtf8( $value ) ) {

          $useUtf8 = true;

        }

      }

      $queryString = substr( $queryString, 0, strlen( $queryString ) - 2 ) . substr( $whereClause, 0, strlen( $whereClause ) - 3 ) . ";";

    }
    else {

      $queryString = "insert into " . $this->database . "." . $inTable . " ( ";

      foreach( $inData as $key => $value ) {

        $queryString .= " " . $key . ", ";


        if(    $helper->isUtf8( $key ) ) {

          $useUtf8 = true;

        }

      }

      $queryString = substr( $queryString, 0, strlen( $queryString ) - 2 ) . " ) values ( ";

      foreach( $inData as $key => $value ) {

        $queryString .= " \"" . $value . "\", ";


        if( $helper->isUtf8( $value ) ) {

          $useUtf8 = true;

        }

      }

      $queryString = substr( $queryString, 0, strlen( $queryString ) - 2 ) . " );";

    }


    // Verbindung mit der Datenbank aufbauen
    if( $this->hasConnection ) {

      // Es muss gelten (entweder oder)
      // 1. In den Daten sind alle notwendigen primaeren Schluessel eingetragen und es
      //    ist keine Spalte dabei, die ein "auto_increment" enthaelt.
      // 2. Es sind alle Schluessel vorhanden und der Datensatz existiert bereits in der
      //    Tabelle
      if( ($keyInData && ($columnAutoincrement == "")) || ($keyInData && $allreadyExists) ) {

        // Wenn es sich um einen "insert"-Befehl handelt, dann soll die Datenbank
        // gesperrt werden, um anschliessend den neu eingefuegten Datensatz wieder
        // auslesen zu koennen. 
        if( !$keyInData ) {

          $mysqlQuery = mysql_query( "lock tables " . $this->database . "." . $inTable . " write;" );

        }


        trigger_error( "DataBase::store() Query: " . $queryString . "\n", E_USER_NOTICE );


        // Sollte ein Wert UTF8-codiert sein, so muss dies der Datenbank
        // gesagt werden
        if( $useUtf8 ) {

          trigger_error( "DataBase::store() Using UTF8 for submission.\n", E_USER_NOTICE );

          mysql_query( "SET CHARACTER SET \"utf8\"" );

        }


        // Den zusammengesetzten Query-String an die Datenbank senden
        $mysqlQuery = mysql_query( $queryString );


        // Es war ein insert-Befehl und es muss erausgefunden werden, welcher
        // Schluessel dabei erzeugt wurde. Ausserdem werden die eingefuegten Daten
        // abgefragt, um als Rueckgabe der Funktion zu dienen.
        if( !$keyInData ) {

          // Herausfinden, welcher Schluessel erzeugt wurde
          $queryString = "select distinct last_insert_id() from " . $this->database . "." . $inTable . ";";
          $mysqlQuery = mysql_query( $queryString );


          $lastInsertResult = Array();
          while( $mysqlResult = mysql_fetch_array( $mysqlQuery, MYSQL_ASSOC ) ) {

            $lastInsertResult[] = $mysqlResult;

          }


          $queryString  = "select * from " . $this->database . "." . $inTable . " where ";
          $queryString .= $columnAutoincrement . "=\"" . $lastInsertResult[0]['last_insert_id()'] . "\";";
          $mysqlQuery = mysql_query( $queryString );

        }
        else {

          $queryString  = "select * from " . $this->database . "." . $inTable . " where ";
          $queryString .= $columnPrimaryKey[0] . "=\"" . $inData[$columnPrimaryKey[0]] . "\";";
          $mysqlQuery = mysql_query( $queryString );

        }


        while( $mysqlResult = mysql_fetch_array( $mysqlQuery, MYSQL_ASSOC ) ) {

          $returnResult[] = $mysqlResult;

        }


        // Datenbank wieder freigeben
        if( !$keyInData ) {

          $mysqlQuery = mysql_query( "unlock tables;" );

        }

      }
      else {

        trigger_error( "Cannot insert data into table without having auto_increment or primary key in data", E_USER_ERROR );

      }

    }
    else {

      trigger_error( "Not connected to database", E_USER_ERROR );

    }


    if( $returnResult && sizeof( $returnResult ) > 0 ) {

      return $returnResult;

    }
    else {

      return false;

    }

  }




  // ## query #########################################################
  public function query( $inQuery ) {

    $queryString = "";         // Abfragestring, der an die Datenbank gesendet wird
    $mysqlQuery = false;       // Das Ergebnis der Abfrage
    $mysqlHandler = false;     // Verbindungs-Handler
    $helper = new Helper();    // Konvertierungsklasse
    $returnResult = true;      // Die Rueckgabe


    if( $helper->isUtf8( $inQuery ) ) {

      mysql_query( "SET CHARACTER SET 'utf8'" );
      //$inQuery = utf8_decode( $inQuery );

    }


    if( $this->hasConnection ) {

      trigger_error( "Query: " . $inQuery . "\n", E_USER_NOTICE );


      $mysqlQuery = mysql_db_query( $this->database, $inQuery );


      // Sollte es sich nicht um eine delete-Anweisung handeln, dann das
      // Abfrageergebnis uebernehmen
      if( !preg_match( "/(^[\s]*delete[\s]*from[\s]*.*)|(^[\s]*update[\s]*.*)/i", $inQuery ) ) {

        $returnResult = Array();
        while( $mysqlResult = mysql_fetch_array( $mysqlQuery, MYSQL_ASSOC ) ) {

          foreach( $mysqlResult as $key => $value ) {

            $mysqlResult[$key] = utf8_decode( $value );

          }


          $returnResult[] = $mysqlResult;

        }

      }

    }
    else {

      trigger_error( "Not connected to database", E_USER_ERROR );


      $returnResult = false;

    }


    return $returnResult;

  }




  // ## dateToText ####################################################
  public function dateToText( $inDate, $inCountry ) {

    $tmp = Array();


    if( $inDate != "" ) {

      // Aufteilen des Datums in seine Bestandteile
      $tmp = split( "-", $inDate );


      if( preg_match( "/^de$/i", $inCountry ) ) {

        if( sizeof( $tmp ) == 3 ) {

          $datestring = trim( $tmp[2] ) . "." . trim( $tmp[1] ) . "." . trim( $tmp[0] );

        }
        else {

          $datestring = $inDate;

        }

      }
      elseif( preg_match( "/^en$/i", $inCountry ) ) {

        if( sizeof( $tmp ) == 3 ) {

          $datestring = trim( $tmp[2] ) . "/" . trim( $tmp[1] ) . "/" . trim( $tmp[0] );

        }
        else {

          $datestring = $inDate;

        }

      }
      else {

        $datestring = $inDate;

      }

    }
    else {

     $datestring = "";

    }


    return $datestring;

  }




  // ## textToDate ####################################################
  public function textToDate( $inText, $inCountry ) {

    if( $inText != "" ) {

      if( preg_match( "/de/i", $inCountry ) ) {

        $tmp = split( "\.", $inText );


        if( sizeof( $tmp ) == 3 ) {

          $datestring = trim( $tmp[2] ) . "-" . trim( $tmp[1] ) . "-" . trim( $tmp[0] );

        }
        else {

          $datestring = $inText;

        }

      }
      elseif( preg_match( "/en/i", $inCountry ) ) {

        $tmp = split( "/", $inText );


        if( sizeof( $tmp ) == 3 ) {

          $datestring = trim( $tmp[2] ) . "-" . trim( $tmp[1] ) . "-" . trim( $tmp[0] );

        }
        else {

          $datestring = $inText;

        }

      }
      else {

        $datestring = $inText;

      }

    }
    else {

     $datestring = "";

    }


    return $datestring;

  }




  // ## mysqlToFloat ##################################################
  public function mysqlToFloat( $inFloat, $inCountry ) {

    if( $inFloat != "" ) {

      if( preg_match( "/de/i", $inCountry ) ) {

        $float = str_replace( ".", ",", $inFloat );

      }

    }
    else {

      $float = 0;

    }


    return $float;

  }




  // ## floatToMysql ##################################################
  public function floatToMysql( $inFloat, $inCountry ) {

    if( $inFloat != "" ) {

      if( preg_match( "/de/i", $inCountry ) ) {

        $float = str_replace( ".", "", $inFloat );
        $float = str_replace( ",", ".", $float );

      }

    }
    else {

      $float = 0;

    }


    return $float;

  }




  // ## floatrangeToMysql #############################################
  public function floatrangeToMysql( $inFloatrange, $inCountry ) {

    if( $inFloatrange != "" ) {

      if( preg_match( "/de/i", $inCountry ) ) {

        $floatrange = str_replace( ".", "", $inFloatrange );
        $floatrange = str_replace( " ", "", $floatrange );
        $floatrange = str_replace( ",", ".", $floatrange );
        $floatrange = str_replace( "-", " - ", $floatrange );

      }

    }
    else {

      $floatrange = "";

    }


    return $floatrange;

  }




  // ## mysqlToFloatrange #############################################
  public function mysqlToFloatrange( $inFloatrange, $inCountry ) {

    if( $inFloatrange != "" ) {

      if( preg_match( "/de/i", $inCountry ) ) {

        $floatrange = str_replace( ".", ",", $inFloatrange );

      }

    }
    else {

      $floatrange = "";

    }


    return $floatrange;

  }

}
?>
