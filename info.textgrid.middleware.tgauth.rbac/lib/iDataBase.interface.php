<?php
interface iDataBase {

  public function __construct( $inHost, $inDatabase );


  public function connect( $inUsername, $inPassword );
  public function reconnect();
  public function hasConnection();
  public function get( $inTable, $inOrder, $inFilter = "", $inColumn = "*" );
  public function getColumns( $inTable );
  public function store( $inTable, $inData );
  public function query( $inQuery );
  public function dateToText( $inDate, $inCountry );
  public function textToDate( $inText, $inCountry );
  public function mysqlToFloatrange( $inFloatrange, $inCountry );
  public function floatrangeToMysql( $inFloatrange, $inCountry );
  public function floatToMysql( $inFloat, $inCountry );
  public function mysqlToFloat( $inFloat, $inCountry );

}
?>
