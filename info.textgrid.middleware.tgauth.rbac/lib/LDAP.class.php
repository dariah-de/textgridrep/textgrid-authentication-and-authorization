<?php
// ####################################################################
// Version: 0.4.1
// Author: Markus Widmer
// Created: 12.10.2006
// Modified: 02.04.2008



class LDAP implements iLDAP {

  // ## Class-variables ###############################################
  private $connection;
  private $hasConnection;
  private $lastConnectHost;
  private $lastConnectPort;
  private $lastConnectVersion;
  private $lastConnectTls;
  private $lastBindDn;
  private $lastBindPw;
  private $cache;




  // ## Constructor ###################################################
  public function __construct() {

    $this->hasConnection = false;

  }




  // ## Destructor ####################################################
  public function __destruct() {
  }




  // ## Wakeup ########################################################
  public function __wakeup() {

    if( $this->hasConnection() ) {

      $this->reconnect();

    }

  }




  // ## connect #######################################################
  public function connect( $inHost, $inPort, $inVersion, $inTls = false ) {

    $connection = false;  // Verbindungstest ueber einen Socket
    $socket = false;      // Socket-Handler


    // Verbindungsdaten fuer ein "reconnect" speichern
    $this->lastConnectHost = $inHost;
    $this->lastConnectPort = $inPort;
    $this->lastConnectVersion = $inVersion;
    $this->lastConnectTls = $inTls;


    // Die Verbindung wird als nicht vorhanden eingestuft
    $this->connection = false;
    $this->hasConnection = false;


    // Testen, ob eine Verbindung moeglich ist. Dies wird durch
    // den Aufbau eines Sockets geleistet, da die Funktion
    // "ldap_connect" in jedem Fall den Wert TRUE liefert und
    // so nicht festgestellt werden kann ob eine Verbindung
    // moeglich ist. Dieser Test gibt jedoch keine Auskunft
    // darueber, ob wirklich ein LDAP-Server antwortet!
    $socket = socket_create( AF_INET, SOCK_STREAM, SOL_TCP );
    $connection = @socket_connect( $socket, $inHost, $inPort );
    socket_close( $socket );


    if( $connection ) {

      // Es wird nun davon ausgegangen, dass auch der
      // Verbindungsaufbau klappt und das Skript daher
      // eine Verbindung hat.
      $this->hasConnection = true;


      // Verbindungsaufbau
      $this->connection = ldap_connect( $inHost, $inPort ) or die( "ERROR!" );


      // Optionen setzen
      ldap_set_option( $this->connection, LDAP_OPT_PROTOCOL_VERSION, $inVersion );

      if( $inTls ) {

       ldap_start_tls( $this->connection );

      }

    }
    else {

      trigger_error( "LDAP::connect(): Connection to host " . $inHost . " on port . " . $inPort . " failed.\n", E_USER_ERROR );

    }


    return $this->connection;

  }




  // ## reconnect #####################################################
  public function reconnect() {

    $this->hasConnection = false;
    return $this->connect( $this->lastConnectHost, $this->lastConnectPort, $this->lastConnectVersion, $this->lastConnectTls );

  }




  // ## rebind ########################################################
  public function rebind() {

    return $this->bind( $this->lastBindDn, $this->lastBindPw );

  }




  // ## bind ##########################################################
  public function bind( $inDn, $inPassword ) {

    // Verbindungsdaten fuer ein "rebind" speichern
    $this->lastBindDn = $inDn;
    $this->lastBindPw = $inPassword;


    // Am LDAP binden funktioniert nur ueber eine
    // vorhandene Verbindung.
    if( $this->hasConnection() ) {

      if( !ldap_bind( $this->connection, $inDn, $inPassword ) ) {

        trigger_error( "LDAP::bind(): Bind for " . $inDn . " not ok\n", E_USER_NOTICE );

        return false;

      }
      else {

        return true;

      }

    }
    else {

      return false;

    }

  }

/*
  // ## getBinaryValues ###########################
  public function getBinaryValues ( $inDn, $attribute ) {

    $arrResult = Array();    // Wanted search result

    // Im LDAP suchen funktioniert nur ueber eine
    // vorhandene Verbindung.
    if( $this->hasConnection ) {

      $arrResult = ldap_get_values_len( $this->connection, $inDn, $attribute );

    }

    return $arrResult;

  }
*/


  // ## getEntry ######################################################
  public function getEntry( $inDn, Array $inArrAttribute = null , $wantBinaryAttribute = false) {

    $ldapSearch = false;     // Handle
    $ldapEntries = Array();  // Unmodified search results
    $arrResult = Array();    // Wanted search result
    $i = 0;                  // Loop


    // Im LDAP suchen funktioniert nur ueber eine
    // vorhandene Verbindung.
    if( $this->hasConnection ) {

      if( isset( $this->cache[$inDn] ) && !$wantBinaryAttribute ) {

        return $this->cache[$inDn];

      }
      else {

        trigger_error( "LDAP::getEntry(): Searching for: " . $inDn . "\n", E_USER_NOTICE );


        if( $inArrAttribute != null ) {
	    
          $ldapSearch  = @ldap_read( $this->connection, $inDn, "(objectClass=*)", $inArrAttribute );

        }
        else {

          $ldapSearch  = @ldap_read( $this->connection, $inDn, "(objectClass=*)" );
        }

	if( $ldapSearch ) {
	  if ($wantBinaryAttribute) {
            $ldapEntry = ldap_first_entry( $this->connection, $ldapSearch );
	    if ($inArrAttribute) {
            for ( $i = 0 ; $i < sizeof ($inArrAttribute); $i++) {
              $binEntries = ldap_get_values_len ($this->connection, $ldapEntry, $inArrAttribute[$i] );
 
              unset ( $binEntries['count'] );       
 
              $arrResult[$inArrAttribute[$i]] =  $binEntries ;
            }
	    }
          } else {

            $ldapEntries = ldap_get_entries( $this->connection, $ldapSearch );

          }

        }

	
        // Free some memory
	if ($ldapSearch) {
       	  ldap_free_result( $ldapSearch );
	}
	unset( $ldapSearch );

        if (!$wantBinaryAttribute) {
 	  // Ueberfluessige Eintraege entfernen
          $ldapEntries = $this->clean( $ldapEntries );

          // Sollte nur ein einziger oder kein Eintrag sein! Dieser
          // Eine (oder Keine) wird uebernommen.
	  if ($ldapEntries) {
            for( $i = 0; $i < sizeof( $ldapEntries ); $i++ ) {

              // Das Ergebnis in den Cache schreiben
              $this->cache[$ldapEntries[$i]['dn']] = $ldapEntries[$i];


              if( preg_match( "/^" . $ldapEntries[$i]['dn'] . "$/i", $inDn ) ) {

                $arrResult = $ldapEntries[$i];

              }
 
            }
	  }
        }

      }

    }
	
    return $arrResult;

  }




  // ## search ########################################################
  public function search( $inBase, $inFilter, $inScope = "sub", $inArrAttribute = null ) {

    $ldapSearch = false;       // Such-Handler
    $ldapEntries = false;      // Unbearbeitete Ergebnisse
    $arrResult = Array();      // Suchergebnisse
    $i = 0;                    // Schleifenvariable

    // Im LDAP suchen funktioniert nur ueber eine
    // vorhandene Verbindung.
    if( $this->hasConnection() ) {

      trigger_error( "LDAP::search(): Searching for: " . $inFilter . " with scope " . $inScope . "\n", E_USER_NOTICE );


      // Je nach "scope" muss eine andere Funktion aufgerufen werden.
      if( $inScope == "one" ) {

        if( is_array( $inArrAttribute ) ) {

          $ldapSearch = ldap_list( $this->connection, $inBase, $inFilter, $inArrAttribute );

        }
        else {

          $ldapSearch = ldap_list( $this->connection, $inBase, $inFilter );

        }

      }
      else {

        if( is_array( $inArrAttribute ) ) {

          $ldapSearch = ldap_search( $this->connection, $inBase, $inFilter, $inArrAttribute );

        }
        else {

          $ldapSearch = ldap_search( $this->connection, $inBase, $inFilter );

        }

      }


      $ldapEntries = ldap_get_entries( $this->connection, $ldapSearch );

      // Free some memory
      if ($ldapSearch) {
        ldap_free_result( $ldapSearch );
      }
      unset( $ldapSearch );

      // Ueberfluessige Eintraege entfernen
      $arrResult = $this->clean( $ldapEntries );

      // Die gefundenen Eintraege in den Cache schreiben,
      // sofern die Abfrage nicht auf wenige Attribute
      // beschraenkt wurde!
      if( !$inArrAttribute == null ) {
        for( $i = 0; $i < sizeof( $arrResult ); $i++ ) {

          $this->cache[$arrResult[$i]['dn']] = $arrResult[$i];

        }

      }

    }


    return $arrResult;

  }




  // ## clean #########################################################
  private function clean( $inResult, $inDecode = false ) {

    $helper = new Helper();   // Hilfsklasse mit verschiedenen Funktionen
    $attributeName = "";      // Temporaerer Attributname
    $arrAttribute = Array();  // Temporaerer Speicher fuer Attribute
    $arrResAttr = Array();    // Temporaerer Speicher fuer Attribute

    $j = 0;                   // Schleifenvariable
    $m = 0;                   // Schleifenvariable
    $o = 0;                   // Schleifenvariable
    $p = 0;                   // Schleifenvariable
    $arrResult = Array();     // Rueckgabe


    // Alle Suchergebnisse durchgehen. Dabei ist es wichtig
    // das letzte Element nicht zu beachten, da eine
    // LDAP-Abfrage immer ein Ergebniss mehr liefert als
    // tatsaechlich vorhanden sind.
    for( $i = 0; $i < sizeof( $inResult ) - 1; $i++ ) {

      // Nur Eintraege betrachten, die eine DN mit Wert haben.
      if( $inResult[$i]['dn'] != "" ) {

        // DN uebernehmen
        $arrResult[$j]['dn'] = $inResult[$i]['dn'];


        // Jedes Attribut ansehen
        for( $m = 0; $m < $inResult[$i]['count']; $m++ ) {

          $attributeName = $inResult[$i][$m];


          $o = 0;
          $arrAttribute = Array();
          $arrResAttr = $inResult[$i][$attributeName];
          for( $p = 0; $p < sizeof( $arrResAttr ) - 1; $p++ ) {

            if( $arrResAttr[$p] != "" ) {

              // Daten decodieren
              if( $inDecode ) {

                $arrAttribute[$o] = utf8_decode( $arrResAttr[$p] );

              }
              else {

                if( !$helper->isUtf8( $arrResAttr[$p] ) ) {

                  $arrAttribute[$o] = utf8_encode( $arrResAttr[$p] );

                }
                else {

                  $arrAttribute[$o] = $arrResAttr[$p];

                }

              }


              $o++;

            }

          }


          // Uebernehmen des bereinigten Attribut-Arrays
          $arrResult[$j][$attributeName] = $arrAttribute;

        }


        $j++;

      }

    }


    return $arrResult;

  }




  // ## copy ##########################################################
  public final function copy( $inDnFrom, $inDnTo, $inRecursiv = false ) {

    $ldapSearch = false;    // Such-Handler
    $ldapResult = Array();  // Unbehandelte Suchergebnisse
    $tmp = "";              // Temporaerer String
    $oldDn = "";            // Alte DN zwischenspeichern
    $newRdn = "";           // Neues Ziel bei Rekursion
    $attrNameOld = "";      // Alter Attributname
    $attrNameNew = "";      // Neuer Attributname
    $attrValueOld = "";     // Alter Wert des Attributs
    $attrValueNew = "";     // Neuer Wert des Attributs
    $i = 0;                 // Schleifenvariable
    $treeCopy = false;      // Rekursives Kopieren
    $baseCopy = false;      // Kopieren des aktuellen DN


    if( $this->hasConnection() ) {

      // Abfragen des Eintrags
      $ldapSearch = ldap_search( $this->connection, $inDnFrom, "(objectClass=*)" );
      $ldapResult = ldap_get_entries( $this->connection, $ldapSearch );


      // Entfernen von ueberfluessigen Eintragungen im Array
      $ldapResult = $this->clean( $ldapResult, false );


      // Zunaechst nur das erste Element bearbeiten
      $ldapResult = $ldapResult[0];


      // Entfernen des DN nach Zwischenspeicherung
      $oldDn = $ldapResult['dn'];
      unset( $ldapResult['dn'] );


      // Attribut umbenennen, damit kein Namens-Konflikt entsteht
      $tmp = split( ",", $oldDn );
      $tmp = split( "=", $tmp[0] );
      $attrNameOld = $tmp[0];
      $attrValueOld = $tmp[1];
      $tmp = split( ",", $inDnTo );
      $tmp = split( "=", $tmp[0] );
      $attrNameNew = $tmp[0];
      $attrValueNew = $tmp[1];

      
    
      for( $i = 0; $i < sizeof( $ldapResult[$attrNameOld] ); $i++ ) {

        if( $ldapResult[$attrNameOld][$i] == $attrValueOld ) {

          // Alle Eintraege muessen UTF8 codiert sein
          $ldapResult[$attrNameNew][$i] = utf8_encode( $attrValueNew );

        }

      }


      // Einfuegen an neuer DN
      $baseCopy = ldap_add( $this->connection, utf8_encode( $inDnTo ), $ldapResult );


      // Rekursion
      if( $inRecursiv ) {

        // Abfragen der darunterliegenden Eintraege
        $ldapSearch = ldap_list( $this->connection, $inDnFrom, "(objectClass=*)" );
        $ldapResult = ldap_get_entries( $this->connection, $ldapSearch );


        // Entfernen von ueberfluessigen Eintragungen im Array
        $ldapResult = $this->clean( $ldapResult );


        for( $i = 0; $i < sizeof( $ldapResult ); $i++ ) {

          $oldDn = $ldapResult[$i]['dn'];
          unset( $ldapResult[$i]['dn'] );


          // Name des Knotens herausfinden
          $newRdn = split( ",", $oldDn );
          $newRdn = $newRdn[0];


          // Rekursiver Aufruf
          $treeCopy = $this->copy( $oldDn, $newRdn . "," . $inDnTo, true );

        }

      }


      if( $inRecursive ) {

        return ( $baseCopy && $treeCopy );

      }
      else {

        return $baseCopy;

      }

    }
    else {

      return false;

    }

  }




  // ## delete ########################################################
  public final function delete( $inDn, $inRecursiv = false ) {

    $treeDelete;  // Erfolg des rekursiven Loeschens


    if( $this->hasConnection() ) {

      // Rekursion
      if( $inRecursiv ) {

        $treeDelete = true;


        // Abfragen der darunterliegenden Eintraege
        $ldapSearch = ldap_list( $this->connection, $inDn, "(objectClass=*)", Array( "objectClass" ) );
        $ldapResult = ldap_get_entries( $this->connection, $ldapSearch );


        // Entfernen von ueberfluessigen Eintragungen im Array
        $ldapResult = $this->clean( $ldapResult );


        for( $i = 0; $i < sizeof( $ldapResult ); $i++ ) {

          unset( $this->cache[$ldapResult[$i]['dn']] );
          $treeDelete = $treeDelete && $this->delete( $ldapResult[$i]['dn'], true );

        }

      }
      else {

        $treeDelete = true;

      }


      // Loeschen der aktuellen DN
      unset( $this->cache[$inDn] );
      return ( $treeDelete && ldap_delete( $this->connection, $inDn ) );

    }
    else {

      return false;

    }

  }




  // ## modify ########################################################
  public function modify( $inDn, Array $inAttribute ) {

    if( $this->hasConnection() ) {

      unset( $this->cache[$inDn] );
      return ldap_modify( $this->connection, $inDn, $inAttribute );

    }
    else {

      return false;

    }

  }




  // ## removeAttribute ###############################################
  public function removeAttribute( $inDn, $inAttribute ) {

    if( $this->hasConnection() ) {

      unset( $this->cache[$inDn] );
      return ldap_mod_del( $this->connection, $inDn, Array( $inAttribute => Array() ) );

    }
    else {

      return false;

    }

  }




  // ## add ###########################################################
  public function add( $inDn, Array $inAttribute ) {

    if( $this->hasConnection() ) {

      unset( $this->cache[$inDn] );
      return ldap_add( $this->connection, $inDn, $inAttribute );

    }
    else {

      return false;

    }

  }




  // ## getConnection #################################################
  public final function getConnection() {

    return $this->connection;

  }




  // ## getConnection #################################################
  public final function hasConnection() {

    return $this->hasConnection;

  }

}
?>
