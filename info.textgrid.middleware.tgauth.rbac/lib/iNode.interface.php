<?php
interface iNode {

  public function __construct();
  public function __destruct();


  public function setName( $inName );
  public function getName();

  public function setValue( $inValue );
  public function getValue();

  public function addChild( iNode $inChild );
  public function getChild( $inName, $inNumber );
  public function searchChild( $inName, $inAttribute, $inRegex );

  public function setAttribute( $inName, $inValue );
  public function getAttribute( $inName );

  public function getChildArray();

  public function countChilds( $inName );

}
?>
