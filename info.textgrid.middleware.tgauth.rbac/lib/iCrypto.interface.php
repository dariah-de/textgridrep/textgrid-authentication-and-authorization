<?php
interface iCrypto {

  public function __construct();
  public function __destruct();

  public function ssha( $inPassword );
  public function md5( $inPassword );
  public function validatePassword( $inPassword, $inHash );

}
?>
