<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 17.07.2007
// Modification date: 01.11.2007
// Version: 0.1.3
// #######################################################



class XACML {

  // Global variables
  protected $rbac;
  protected $config;
  protected $connection;



  // -----------------------------------------------------
  // Constructor
  // Input: none
  // Output: object RBACcore
  // Description:
  //   Creates initial connections to the LDAP-server and
  //   sets some configuration parameters.
  // -----------------------------------------------------
  public function __construct( $inConfigurationFilename, $inRbacConfFile, $inRbacBase ) {

    $this->rbac = new RBAC( $inRbacConfFile, $inRbacBase );


    $this->config = new SimpleConfig( $inConfigurationFilename );

  }




  // -----------------------------------------------------
  // Function: checkXACMLaccess
  // Input: auth / xsd:string
  //        log / xsd:string
  //        username / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Checks if a given user exists in the LDAP directory.
  // -----------------------------------------------------
  function checkXACMLaccess( $inRequest ) {

    $version = false;          // The version of the XACML-SAML-Request
    $result = new stdClass();  // The response


    $version = $inRequest->Version;
    $id = $inRequest->ID;


    if( preg_match( "/^2\.0$/", $version ) ) {

      $result->Version = "2.0";
      $result->ID = $id;

      $result->Response = new stdClass();
      $result->Response->Result = new stdClass();


      try {

        if( $this->rbac->checkAccess( $inRequest->Request->Subject->Attribute->AttributeValue->any,
                                      $inRequest->Request->Action->Attribute->AttributeValue->any,
                                      $inRequest->Request->Resource->Attribute->AttributeValue->any ) ) {

          $result->Response->Result->Decision = "Permit";

        }
        else {

          $result->Response->Result->Decision = "Deny";

        }

      }
      catch( Exception $e ) {

        $result->Response->Result->Decision = "NotApplicable";

      }



      // Return the request if the flag is set to TRUE
      if( $inRequest->ReturnContext ) {

        $result->Request = new stdClass();

        isset( $inRequest->Request->Subject ) ? $result->Request->Subject = $inRequest->Request->Subject
                                              : $result->Request->Subject = new sdtClass();


        isset( $inRequest->Request->Resource ) ? $result->Request->Resource = $inRequest->Request->Resource
                                               : $result->Request->Resource = new stdClass();


        isset( $inRequest->Request->Action ) ? $result->Request->Action = $inRequest->Request->Action
                                             : $result->Request->Action = new stdClass();


        isset( $inRequest->Request->Environment ) ? $result->Request->Environment = $inRequest->Request->Environment
                                                  : $result->Request->Environment = new stdClass();

      }

    }


    return $result;

  }

}
?>
