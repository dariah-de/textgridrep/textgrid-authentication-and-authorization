<?php
class authenticateRequest {

  public $username;
  public $password;
  public $log;

}

class authenticateResponse {

  public $sid;

}

class getSidResponse {

  public $sid;

}

class checkAccessRequest {

  public $intSid;
  public $operation;
  public $resource;
  public $sid;

}

class tgCrudCheckAccessRequest {

  public $auth;
  public $log;
  public $secret;
  public $operation;
  public $resource;
  public $sid;

}

class tgCrudCheckAccessResponse {

  public $result;
  public $public;
  public $projectInfo;
  public $username;
  public $operation;

}

class tgCheckAccessRequest {

  public $auth;
  public $log;
  public $operation;
  public $resource;
  public $sid;

}

class tgGrantPermissionRequest {

  public $auth;
  public $log;
  public $role;
  public $resource;
  public $operation;

}

class tgRevokePermissionRequest {

  public $auth;
  public $log;
  public $role;
  public $resource;
  public $operation;

}

class getOwnerRequest {

  public $auth;
  public $log;
  public $resource;

}

class getOwnerResponse {

  public $owner;

}


class getUUIDRequest {

  public $auth;
  public $log;
  public $resource;

}

class getUUIDResponse {

  public $uuid;

}

class getMembersRequest {

  public $auth;
  public $log;
  public $project;

}

class deactivateProjectRequest {

  public $auth;
  public $log;
  public $project;

}

class getDeactivatedProjectsRequest {
  public $auth;
  public $log;
}


class reactivateProjectRequest {

  public $auth;
  public $log;
  public $project;

}

class deleteProjectRequest {

  public $auth;
  public $log;
  public $project;

}

class getRightsRequest {

  public $auth;
  public $log;
  public $resource;
  public $username;

}

class publishRequest {

  public $auth;
  public $log;
  public $resource;

}

class isPublicRequest {

  public $auth;
  public $log;
  public $resource;

}

class hasPublicResourcesRequest {

  public $auth;
  public $log;
  public $project;

}

class getProjectDescriptionRequest {

  public $auth;
  public $log;
  public $project;

}

class getProjectDescriptionResponse {

  public $project;

}

class createSessionRequest {
  public $intSid;
  public $username;
  public $roleset;
  public $sid;

}

class tgAddActiveRoleRequest {

  public $auth;
  public $log;
  public $role;

}

class tgAssignedRolesRequest {

  public $auth;
  public $log;
  public $username;

}

class tgAssignedProjectsRequest {

  public $auth;
  public $log;
  public $level;

}

class deleteSessionRequest {

  public $intSid;
  public $username;
  public $sid;

}

class addActiveRoleRequest {

  public $role;
  public $auth;

}

class dropActiveRoleRequest {

  public $role;
  public $auth;

}

class addUserRequest {

  public $intSid;
  public $username;
  public $password;

}

class deleteUserRequest {

  public $intSid;
  public $username;

}

class addInheritanceRequest {

  public $intSid;
  public $ascendant;
  public $descendant;

}

class deleteInheritanceRequest {

  public $intSid;
  public $ascendant;
  public $descendant;

}

class addAscendantRequest {

  public $intSid;
  public $ascendant;
  public $descendant;

}

class addDescendantRequest {

  public $intSid;
  public $ascendant;
  public $descendant;

}

class addRoleRequest {

  public $intSid;
  public $role;

}

class deleteRoleRequest {

  public $intSid;
  public $role;

}

class grantPermissionRequest {

  public $intSid;
  public $resource;
  public $operation;
  public $role;

}

class revokePermissionRequest {

  public $intSid;
  public $resource;
  public $operation;
  public $role;

}

class assignUserRequest {

  public $intSid;
  public $username;
  public $role;

}

class deassignUserRequest {

  public $intSid;
  public $username;
  public $role;

}

class sessionRolesRequest {

  public $intSid;
  public $sid;

}

class assignedRolesRequest {

  public $intSid;
  public $username;

}

class authorizedRolesRequest {

  public $intSid;
  public $username;

}

class roleOperationsOnObjectRequest {

  public $intSid;
  public $role;
  public $resource;

}

class userOperationsOnObjectRequest {

  public $intSid;
  public $user;
  public $resource;

}

class operationsetResponse {

  public $operationset;

}

class assignedUsersRequest {

  public $intSid;
  public $role;

}

class authorizedUsersRequest {

  public $intSid;
  public $role;

}

class usersetResponse {

  public $username;

}

class rolePermissionsRequest {

  public $intSid;
  public $role;

}

class userPermissionsRequest {

  public $intSid;
  public $username;

}

class getLeaderRequest {

  public $auth;
  public $log;
  public $project;

}

class getObjectsRequest {

  public $auth;
  public $log;
  public $project;

}

class sessionPermissionsRequest {

  public $intSid;
  public $sid;

}

class rolesetResponse {

  public $role;

}

class permissionsetResponse {

  public $permissionset;

}

class resourcesetResponse {

  public $resource;

}

class createProjectRequest {

  public $auth;
  public $log;
  public $name;
  public $description;
  public $file;

}

class setProjectFileRequest {

  public $auth;
  public $log;
  public $project;
  public $file;

}

class setNameRequest {

  public $auth;
  public $log;
  public $webAuthSecret;
  public $name;
  public $mail;
  public $organisation;
  public $agreeSearch;

}

class registerResourceRequest {

  public $auth;
  public $log;
  public $project;
  public $uri;

}

class unregisterResourceRequest {

  public $auth;
  public $log;
  public $uri;

}

class addMemberRequest {

  public $auth;
  public $log;
  public $role;
  public $username;

}

class deleteMemberRequest {

  public $auth;
  public $log;
  public $role;
  public $username;

}

class createProjectResponse {

  public $projectId;

}

class getAllProjectsResponse {

  public $project;

}

class getUserRoleResponse {

  public $userrole;

}

class getNamesRequest {
  public $auth;
  public $log;
  public $ePPN;
}

class getNamesResponse {

  public $userdetails;

}

class getIDsRequest {
  public $auth;
  public $log;
  public $name;
  public $mail;
}

class getIDsResponse {

  public $userdetails;

}


class getFriendsResponse {

  public $friends;

}

class getFriendsRequest {

  public $auth;
  public $log;

}

class getAllProjectsRequest {

  public $log;

}

class userExistsRequest {

  public $auth;
  public $log;
  public $username;

}


class booleanResponse {

  public $result;
  public $errorCode;
  public $errorDescription;

}

class filterBySidRequest {

  public $auth;
  public $log;
  public $resource;
  public $operation;

}

class filterResponse {

  public $resource;

}


class permission {

  public $resource;
  public $operation;


  public function __construct( $inOperation, $inResource ) {

    $this->operation = $inOperation;
    $this->resource = $inResource;

  }

}

class projectInfo {

  public $id;
  public $description;
  public $name;
  public $file;


  public function __construct( $inId, $inName, $inDescription, $inFile ) {

    $this->id = $inId;
    $this->description = $inDescription;
    $this->name = $inName;
    $this->file = $inFile;

  }

}

class userRole {

    public $username;
    public $roles;

    public function __construct( $inUsername, $inRoles ) {

	$this->username = $inUsername;
	$this->roles = $inRoles;

    }

}

class friend {
  public $username;
  public $score;

  public function __construct( $inUsername, $inScore ) {

	$this->username = $inUsername;
	$this->score = $inScore;
  }
}

class userAttribute {
  public $name;
  public $value;
  public $description;
  public $mandatory;
  public $ldapname;
  public $inclass;
  public $displayname;

  public function __construct ( $inname,  $invalue,  $indescription,  $inmandatory,  $inldapname,  $ininclass,  $indisplayname) {
    $this->name = $inname;
    $this->value = $invalue;
    $this->description = $indescription;
    $this->mandatory = $inmandatory;
    $this->ldapname = $inldapname;
    $this->inclass = $ininclass;
    $this->displayname = $indisplayname;
  }
}

class userDetail {
  public $ePPN;
  public $name;
  public $mail;
  public $organisation;
  public $agreesearch;
  public $usersupplieddata;

  public function __construct( $inePPN, $inname, $inmail, $inorganisation, $inagreesearch, $inusersupplieddata ) {

	$this->ePPN = $inePPN;
	$this->name = $inname;
	if (!is_null ($inmail)) { $this->mail = $inmail;}
	if (!is_null ($inorganisation)) { $this->organisation = $inorganisation;}
	if (!is_null ($inagreesearch)) { $this->agreesearch = $inagreesearch;}
	if (!is_null ($inusersupplieddata)) { $this->usersupplieddata = $inusersupplieddata;}

  }
}



class checkXACMLaccessRequest {

  public $request;

}

class getSLCRequest {

  public $auth;
  public $log;
  public $secret;

}

class getSLCResponse {

  public $slc;

}



?>
