<?php
// #######################################################
// Author: Markus Widmer & Martin Haase, DAASI International GmbH
// Creation date: 17.07.2007
// Modification date: 15.10.2010
// Version: 0.3.0
// #######################################################

class TgExtraCrud {

  // Global variables
  protected $rbac;
  protected $config;
  protected $connection;

  // -----------------------------------------------------
  // Constructor
  // Input: none
  // Output: object RBACcore
  // Description:
  //   Creates initial connections to the LDAP-server and
  //   sets some configuration parameters.
  // -----------------------------------------------------
  public function __construct( $inConfigurationFilename, $inRbacConfFile, $inRbacBase ) {
    $this->rbac = new RBAC( $inRbacConfFile, $inRbacBase );


    $this->config = new SimpleConfig( $inConfigurationFilename );


    // Create connection
    $this->connection['user'] = new LDAP();
    $this->connection['user']->connect( $this->config->getValue( "authentication", "host" ),
                                        $this->config->getValue( "authentication", "port" ),
                                        $this->config->getValue( "authentication", "version" ),
                                        preg_match( "/yes/i", $this->config->getValue( "authentication", "tls" ) ) ? true : false );
    $this->connection['user']->bind( $this->config->getValue( "authentication", "binddn" ),
                                     $this->config->getValue( "authentication", "password" ) );


    $this->connection['counter'] = new LDAP();
    $this->connection['counter']->connect( $this->config->getValue( "counter", "host" ),
                                           $this->config->getValue( "counter", "port" ),
                                           $this->config->getValue( "counter", "version" ),
                                           preg_match( "/yes/i", $this->config->getValue( "counter", "tls" ) ) ? true : false );
    $this->connection['counter']->bind( $this->config->getValue( "counter", "binddn" ),
                                        $this->config->getValue( "counter", "password" ) );


    $this->connection['resource'] = $this->rbac->getConnection( "resource" );
    $this->connection['role'] = $this->rbac->getConnection( "role" );
    $this->connection['session'] = $this->rbac->getConnection( "session" );

  }



  // -----------------------------------------------------
  // Function: nearlyPublish
  // Input: auth / xsd:string
  //        log / xsd:string
  //        secret / xsd:string
  //        resource / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Returns a list of operations allowed by the user
  //   on a specific resource.
  // -----------------------------------------------------
  public function nearlyPublish( $inRequest ) {

    $arrResource = Array();
    $arrModify = Array();
    $filter = "";
    $result = new booleanResponse();  // The return-result
    $objFaultDetail = new stdClass();

    if( $inRequest->secret !== $this->config->getValue( "crud", "secret" ) ) {

      $objFaultDetail->faultNo = $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" );
      $objFaultDetail->faultMessage = $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" );
      $objFaultDetail->cause = "Only CRUD can nearly publish resources.";

      return new SoapFault( "authenticationFault",
                            "Only CRUD can nearly publish resources.",
                            get_class( $this ),
                            $objFaultDetail );
    }

    // ELSE (presented secret is correct)

    $filter  = "(&" . $this->rbac->getConfiguration()->getValue( "resource", "filter" );
    $filter .= "(|(" . $this->rbac->getConfiguration()->getValue( "resource", "namingattribute" ) . "=" . $inRequest->resource . ")";
    $filter .= "  (" . $this->rbac->getConfiguration()->getValue( "resource", "aliasattribute" ) . "=" . $inRequest->resource . ")))";


    // Get the resource
    $arrResource = $this->connection['resource']->search( $this->rbac->getConfiguration()->getValue( "resource", "base" ), $filter, "sub",
                                                          Array( "rbacpermission", "tgispublic", "tgprojectid" ) );


    if( $this->rbac->checkAccess( $inRequest->auth, "publish", $inRequest->resource ) ) {

      $arrModify['rbacpermission'] = Array();


      for( $loPermission = 0; $loPermission < sizeof( $arrResource[0]['rbacpermission'] ); $loPermission++ ) {

        if(    preg_match( "/:-:publish$/i", $arrResource[0]['rbacpermission'][$loPermission] )
            || preg_match( "/:-:delete$/i", $arrResource[0]['rbacpermission'][$loPermission] ) ) {

          $arrModify['rbacpermission'][] = $arrResource[0]['rbacpermission'][$loPermission];

        } 

      }


      $arrModify['tgispublic'][] = "TRUE";
      $result->result = $this->connection['resource']->modify( $arrResource[0]['dn'], $arrModify );

    }
    else {
      // somehow checkAccess returned False. Debug it now.
      $secondtry = $this->rbac->checkAccess( $inRequest->auth, "publish", $inRequest->resource );

      $objFaultDetail->faultNo = $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" );
      $objFaultDetail->faultMessage = $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" );
      $objFaultDetail->cause = "You do not have the permission publish the resource " . $inRequest->resource . ".";


      return new SoapFault( "authenticationFault",
                             $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                             get_class( $this ),
                             $objFaultDetail );

    }


    return $result;

  }


  // -----------------------------------------------------
  // Function: registerResource
  // Input: auth / xsd:string
  //        log / xsd:string
  //        project / xsd:string
  //        uri / xsd:string
  //        uuid / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Tries to add a resource to the directory. There for
  //   a user has to have the permission "create" on the
  //   resource "project".
  // -----------------------------------------------------
  function registerResource( $inRequest ) {

    $connection = false;              // The connection for resources
    $registered = false;              // The result of the registration-tries
    $registerTry = 10;                // The number of tries of registering the resource
    $resourceNamingAttribute = "";    // The naming-attribute of the resource
    $arrCounter = false;              // The counter entry
    $askedDaemon = false;
    $result = new operationsetResponse();  // The return-result
    $objFaultDetail = new stdClass();


    // Create a new uuid for the new project
    $strRbacResourceUuid = $this->uuidV4();	


    if( $inRequest->secret === $this->config->getValue( "crud", "secret" ) ) {


      // The TextGrid-resource naming-attribute
      $resourceNamingAttribute = $this->rbac->getConfiguration()->getValue( "resource", "namingattribute" );


      if( $this->rbac->checkAccess( $inRequest->auth, "create", $inRequest->project ) ) {

        // As long as the resource is not registered and the number
        // of tries has not been reached, try to register
        while(    !$registered
               && ($registerTry > 0) ) {

          // This is the resource-entry that will be
          // added to the directory
          $arrEntry = Array();
          $arrEntry['objectclass'][] = "textgridResource";
          $arrEntry['objectclass'][] = "rbacResource";
          $arrEntry['rbacoperation'][] = "read";
          $arrEntry['rbacoperation'][] = "write";
          $arrEntry['rbacoperation'][] = "delegate";
          $arrEntry['rbacoperation'][] = "delete";
          $arrEntry['rbacoperation'][] = "publish";
          $arrEntry['tgresourceuri'][] = $inRequest->uri;
	  if (isset($inRequest->uuid)) {
	          $arrEntry['tgresourceuuid'][] = $inRequest->uuid;
	  }
          $arrEntry['tgprojectid'][] = $inRequest->project;
          $arrEntry['tgispublic'][] = "FALSE";
          $arrEntry[$resourceNamingAttribute][] = "TGRS-" . $strRbacResourceUuid; //($freeNumber);
          $arrEntry['tgresourceowner'][] = $this->rbac->sessionUser( $inRequest->auth );

          // Add the default permissions to the resource
          $arrEntry['rbacpermission'][] = $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=Projektleiter,"
                                          . $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=" . $inRequest->project . ","
                                          . $this->rbac->getConfiguration()->getValue( "project", "base" ) . ":-:delegate";
          $arrEntry['rbacpermission'][] = $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=Projektleiter,"
                                          . $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=" . $inRequest->project . ","
                                          . $this->rbac->getConfiguration()->getValue( "project", "base" ) . ":-:publish";
          $arrEntry['rbacpermission'][] = $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=Projektleiter,"
                                          . $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=" . $inRequest->project . ","
                                          . $this->rbac->getConfiguration()->getValue( "project", "base" ) . ":-:read";
          $arrEntry['rbacpermission'][] = $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=Administrator,"
                                          . $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=" . $inRequest->project . ","
                                          . $this->rbac->getConfiguration()->getValue( "project", "base" ) . ":-:delete";
          $arrEntry['rbacpermission'][] = $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=Administrator,"
                                          . $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=" . $inRequest->project . ","
                                          . $this->rbac->getConfiguration()->getValue( "project", "base" ) . ":-:read";
          $arrEntry['rbacpermission'][] = $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=Bearbeiter,"
                                          . $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=" . $inRequest->project . ","
                                          . $this->rbac->getConfiguration()->getValue( "project", "base" ) . ":-:read";
          $arrEntry['rbacpermission'][] = $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=Bearbeiter,"
                                          . $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=" . $inRequest->project . ","
                                          . $this->rbac->getConfiguration()->getValue( "project", "base" ) . ":-:write";
          $arrEntry['rbacpermission'][] = $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=Beobachter,"
                                          . $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=" . $inRequest->project . ","
                                          . $this->rbac->getConfiguration()->getValue( "project", "base" ) . ":-:read";

          // Try to add the resource with the appropriate number. If
          // this fails, we will try again (10 times).
          $registered = $this->connection['resource']->add( $this->rbac->getConfiguration()->getValue( "resource", "namingattribute" ) . "=TGRS-"
                                                            . $strRbacResourceUuid . "," . $this->config->getValue( "textGridResource", "base" ),
                                                            $arrEntry );


          $registerTry--;
        }

	if (!$registered) {

          $objFaultDetail->faultNo = 4096;
 	  $objFaultDetail->faultMessage = "An LDAP-Error occured, see description";
          $objFaultDetail->cause = "Could not register the resource";

      	  return new SoapFault( "rbacFault",
                            "Could not register the resource",
                            get_class( $this ),
                            $objFaultDetail );

	}

        // Create the set of operations the user has on
        // the new object.

        $result->operation = $this->rbac->userOperationsOnObject( $this->rbac->sessionUser( $inRequest->auth ), "TGRS-" . $strRbacResourceUuid );

      }
      else {

        $objFaultDetail->faultNo = $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" );
        $objFaultDetail->faultMessage = $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" );
        $objFaultDetail->cause = "You are not allowed to create resources in this project.";


        return new SoapFault( "authenticationFault",
                              "You are not allowed to create resources in this project.",
                              get_class( $this ),
                              $objFaultDetail );

      }


      return $result;

    }
    else {

      $objFaultDetail->faultNo = $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" );
      $objFaultDetail->faultMessage = $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" );
      $objFaultDetail->cause = "You are not cruddy enough - Go away and come back as CRUD!";


      return new SoapFault( "authenticationFault",
                            "You are not cruddy enough - Go away and come back as CRUD!",
                            get_class( $this ),
                            $objFaultDetail );

    }

  }





  // -----------------------------------------------------
  // Function: unregisterResource
  // Input: auth / xsd:string
  //        log / xsd:string
  //        project / xsd:string
  //        uri / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Tries to remove a resource from the directory. There for
  //   a user has to have the permission "delete" on the
  //   resource.
  // -----------------------------------------------------
  function unregisterResource( $inRequest ) {
      
    $resourceNamingAttribute = "";    // The naming-attribute of the resource
    $resourceAliasAttribute = "";     // The alias-attribute of the resource
    $filter = "";                     // The LDAP-filter to find the resource
    $result = new booleanResponse();  // The return-result
    $objFaultDetail = new stdClass();


    if( $inRequest->secret === $this->config->getValue( "crud", "secret" ) ) {

      if( $this->rbac->checkAccess( $inRequest->auth, "delete", $inRequest->uri ) ) {

        // The TextGrid-resource naming-attribute
        $resourceNamingAttribute = $this->rbac->getConfiguration()->getValue( "resource", "namingattribute" );
        $resourceAliasAttribute = $this->rbac->getConfiguration()->getValue( "resource", "aliasattribute" );

        $filter  = "(&" . $this->rbac->getConfiguration()->getValue( "resource", "filter" );
        $filter .= "(|(" . $resourceNamingAttribute . "=" . $inRequest->uri . ")";
        $filter .= "  (" . $resourceAliasAttribute . "=" . $inRequest->uri . ")))";

        $arrResource = $this->connection['resource']->search( $this->rbac->getConfiguration()->getValue( "resource", "base" ), $filter, "sub" );


        if( is_array($arrResource) && $arrResource && sizeof( $arrResource ) == 1 ) {

          $result->result = $this->connection['resource']->delete( $arrResource[0]['dn'] );

        }
        else {

          $objFaultDetail->faultNo = $this->config->getValue( "errorCode", "RESOURCENOTFOUND_ERROR" );
          $objFaultDetail->faultMessage = $this->config->getValue( "errorDescription", "RESOURCENOTFOUND_ERROR" );
          $objFaultDetail->cause = "The specified resource " . $inRequest->resource . " could not be found.";


          return new SoapFault( "unknownResourceFault",
                                $this->config->getValue( "errorCode", "RESOURCENOTFOUND_ERROR" ),
                                get_class( $this ),
                                $objFaultDetail );

        }

      }
      else {

        $objFaultDetail->faultNo = $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" );
        $objFaultDetail->faultMessage = $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" );
        $objFaultDetail->cause = "You do not have the permission to remove " . $inRequest->resource . ".";


        return new SoapFault( "authenticationFault",
                               $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                               get_class( $this ),
                               $objFaultDetail );

      }

      return $result;

    }
    else {

      $objFaultDetail->faultNo = $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" );
      $objFaultDetail->faultMessage = $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" );
      $objFaultDetail->cause = "You are not cruddy enough - Go away and come back as CRUD!";


      return new SoapFault( "authenticationFault",
                            "You are not cruddy enough - Go away and come back as CRUD!",
                            get_class( $this ),
                            $objFaultDetail );

    }

  }





  // -----------------------------------------------------
  // Function: tgCrudCheckAccess
  // Input: auth / xsd:string
  //        log / xsd:string
  //        operation / xsd:string
  //        resource / xsd:string
  //        secret / xsd:string
  // Output: 
  //        result / xsd:boolean
  //        public / xsd:boolean
  //        project / tns:projectinfo
  //        username / xsd:string
  //        operation / xsd:string
  // Description
  //   Searches for the given resource. If it's existing the
  //   method starts the checkAccess query and returns the
  //   result. Otherwise a Fault is generated that sais that
  //   the resource does not exist.
  // -----------------------------------------------------
  public function tgCrudCheckAccess( $inRequest ) {

    // Must check in the following order:
    // 1. Check if the tgcrud secret is correctly set, if not > authenticationFault
    // 2. Check if the resource is existing, if not > unknownResourceFault
    // 2.5 Check for isPublic flag, if set to TRUE let the user READ the ressource.
    // 3. Check the session ID, if not valid > set result to false and return
    // 4. Set the other results and return true

    $result = new tgCrudCheckAccessResponse();
    $objProjectInfo = new stdClass();
    $objFaultDetail = new stdClass();
    $arrResource = Array();
    $arrProject = Array();
    $filter = "";


    if( $inRequest->secret === $this->config->getValue( "crud", "secret" ) ) {

      $filter  = "(&(objectClass=rbacResource)";
      $filter .= "(|(" . $this->rbac->getConfiguration()->getValue( "resource", "aliasattribute" ) . "=" . $inRequest->resource . ")";
      $filter .= "(" . $this->rbac->getConfiguration()->getValue( "resource", "namingattribute" ) . "=" . $inRequest->resource . ")))";


      // Search for the resource.
      $arrResource = $this->connection['resource']->search( $this->rbac->getConfiguration()->getValue( "resource", "base" ), $filter, "sub" );


      if( $arrResource && sizeof( $arrResource ) > 0 ) {

        // Add the PDP result
        $result->result = $this->rbac->checkAccess( $inRequest->auth, $inRequest->operation, $inRequest->resource );
	if( !isset( $arrResource[0]['tgispublic'] ) ) {
	  $result->public = false;
	} else {
	  preg_match( "/^true$/i", $arrResource[0]['tgispublic'][0] ) ? $result->public = true : $result->public = false;
	}

        // See if the object is a project role...
        for( $loObjectclass = 0; $loObjectclass < sizeof( $arrResource[0]['objectclass'] ); $loObjectclass++ ) {

          // If this matches the resource is a project and the
          // work has already been done.
          if( preg_match( "/TextGridProject/i", $arrResource[0]['objectclass'][$loObjectclass] ) ) {

            $arrProject = $arrResource;
            break;

          }

        }


	// ------------------------------------------------------------------------
        // Check here, if we have got a public ressource and operation is READ!
        // If so, set result to TRUE. May be fixed above or better! (Ask Mr Haase!)
        // ------------------------------------------------------------------------
        // fu 2013-03-21 -- solves TG-1929
        // ------------------------------------------------------------------------
        if( $inRequest->operation == "read" && $result->public == true ) {
              $result->result = true;
        }
        // ------------------------------------------------------------------------


        // If the resource isn't a project the project corresponding to the
        // has to be searched now.
        if( !$arrProject && isset( $arrResource['tgprojectid'] ) ) {

          $filter  = "(&(objectClass=rbacResource)";
          $filter .= "(|(" . $this->rbac->getConfiguration()->getValue( "resource", "aliasattribute" ) . "=" . $arrResource['tgprojectid'][0] . ")";
          $filter .= "(" . $this->rbac->getConfiguration()->getValue( "resource", "namingattribute" ) . "=" . $arrResource['tgprojectid'][0] . ")))";


          // Search for the project.
          $arrProject = $this->connection['resource']->search( $this->rbac->getConfiguration()->getValue( "resource", "base" ), $filter, "sub" );

        }


        if( $arrProject && sizeof( $arrProject ) > 0 ) {

          $result->project = new ProjectInfo( $arrProject[0][$this->rbac->getConfiguration()->getValue( "role", "namingattribute" )][0],
                                              $arrProject[0]['tgprojectname'][0], $arrProject[0]['tgprojectdescription'][0],
                                              $arrProject[0]['tgprojectfile'][0] );

        }


        // Finally add the username and allowed
        // operations for the current session.
	// have to check for anonymous access (or invalid SID)
	try {
	    $result->username = $this->rbac->sessionUser( $inRequest->auth );
	} catch ( Exception $e ) {
	    $result->username = "--invalid--";
	}
	

        try {

          $result->operation = $this->rbac->userOperationsOnObject( $result->username, $inRequest->resource );


          // Make sure that if the operation was "read" and the user
          // has access but has this right due to an extension the
          // permissionset contains the read operation.
          if( $inRequest->operation == "read" && $result->result && !in_array( "read", $result->operation ) ) {

            $result->operation[] = "read";

          }


          return $result;

        }
        catch( Exception $e ) {

          $objFaultDetail->faultNo = $this->config->getValue( "errorCode", "RESOURCENOTFOUND_ERROR" );
          $objFaultDetail->faultMessage = $this->config->getValue( "errorDescription", "RESOURCENOTFOUND_ERROR" );
          $objFaultDetail->cause = "Object does not exist";


          return new SoapFault( "unknownResourceFault",
                                $this->config->getValue( "errorDescription", "RESOURCENOTFOUND_ERROR" ),
                                get_class( $this ),
                                $objFaultDetail );

        }

      }
      else {

        $objFaultDetail->faultNo = $this->config->getValue( "errorCode", "RESOURCENOTFOUND_ERROR" );
        $objFaultDetail->faultMessage = $this->config->getValue( "errorDescription", "RESOURCENOTFOUND_ERROR" );
        $objFaultDetail->cause = "Object does not exist";


        return new SoapFault( "unknownResourceFault",
                              $this->config->getValue( "errorDescription", "RESOURCENOTFOUND_ERROR" ),
                              get_class( $this ),
                              $objFaultDetail );

      }

    }
    else {

      $objFaultDetail->faultNo = $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" );
      $objFaultDetail->faultMessage = $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" );
      $objFaultDetail->cause = "You are not cruddy enough - Go away and come back as CRUD!";


      return new SoapFault( "authenticationFault",
                            "You are not cruddy enough - Go away and come back as CRUD!",
                            get_class( $this ),
                            $objFaultDetail );

    }

  }




  // -----------------------------------------------------
  // Function: getEPPN
  // Input: auth / xsd:string
  //        log / xsd:string
  //        secret / xsd:string
  // Output: eppn / xsd:string
  // Description
  //   Returns the TextGrid ID (ePPN) of a session user
  // -----------------------------------------------------
  public function getEPPN ( $inRequest ) {

    $result = new StdClass();  // The return-result

    // Secret is not needed for getEPPN anymore, it is contained in any metadata anyway.
    // So we do not check it! *fu* 20150113
    // if ($inRequest->secret !== $this->config->getValue( "SIDcheck", "secret" ) ) { 
    //   return new SoapFault( "authenticationFault",
    //                        $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
    //                        get_class( $this ),
    //                        $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" ) );
    // }

    try {
      $result->eppn = $this->rbac->sessionUser( $inRequest->auth );
    } catch (Fault $f) {
      return new SoapFault( "authenticationFault",
                            $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" ) );
    }

    return $result;
  }



  // -----------------------------------------------------
  // Function: getUUID
  // Input: auth / xsd:string
  //        log / xsd:string
  //        resource / xsd:string
  // Output: result / xsd:string
  // Description
  //   Returns the UUID of a resource, possibly nothing if unset.
  // -----------------------------------------------------
  public function getUUID( $inRequest ) {

    $result = new getUUIDResponse();  // The return-result
    $filter = "";                      // RBAC-filter
    $arrResource = Array();            // Resoult of the RBAC-search


    // Create a filter that searches for the
    // given resource.
    $filter  = "(&" . $this->rbac->getConfiguration()->getValue( "resource", "filter" );
    $filter .= "(|(" . $this->rbac->getConfiguration()->getValue( "resource", "namingattribute" ) . "=" . $inRequest->resource . ")";
    $filter .= "  (" . $this->rbac->getConfiguration()->getValue( "resource", "aliasattribute" ) . "=" . $inRequest->resource . ")))";


    if( $this->rbac->checkAccess( $inRequest->auth, "read", $inRequest->resource ) ) {

      $arrResource = $this->connection['resource']->search( $this->rbac->getConfiguration()->getValue( "resource", "base" ),
                                                            $filter, "sub",
                                                            Array( "tgresourceuuid" ) );


      if( $arrResource &&  sizeof( $arrResource ) == 1 ) {

	if (isset($arrResource[0]['tgresourceuuid'][0])) {

          $result->uuid = $arrResource[0]['tgresourceuuid'][0];

	} else {

          $result->uuid = "No UUID defined"; // will be a fault later

	}

      } else {

        $result->uuid = "Resource not found or no access right";	// will be a fault later

      }

    } else {

      $result->uuid = "Resource not found or no access right";	// will be a fault later

    }

    return $result;

  }


  // -----------------------------------------------------
  // Function: publish
  // Input: auth / xsd:string
  //        log / xsd:string
  //        secret / xsd:string
  //        resource / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Returns a list of operations allowed by the user
  //   on a specific resource.
  // -----------------------------------------------------
  public function publish( $inRequest ) {

    $arrResource = Array();
    $arrModify = Array();
    $filter = "";
    $result = new booleanResponse();  // The return-result
    $objFaultDetail = new stdClass();

    if( $inRequest->secret !== $this->config->getValue( "crud", "secret" ) ) {

      $objFaultDetail->faultNo = $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" );
      $objFaultDetail->faultMessage = $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" );
      $objFaultDetail->cause = "Only CRUD can publish resources.";

      return new SoapFault( "authenticationFault",
                            "Only CRUD can publish resources.",
                            get_class( $this ),
                            $objFaultDetail );
    }


    $filter  = "(&" . $this->rbac->getConfiguration()->getValue( "resource", "filter" );
    $filter .= "(|(" . $this->rbac->getConfiguration()->getValue( "resource", "namingattribute" ) . "=" . $inRequest->resource . ")";
    $filter .= "  (" . $this->rbac->getConfiguration()->getValue( "resource", "aliasattribute" ) . "=" . $inRequest->resource . ")))";


    // Get the resource
    $arrResource = $this->connection['resource']->search( $this->rbac->getConfiguration()->getValue( "resource", "base" ), $filter, "sub",
                                                          Array( "tgispublic", "tgprojectid" ) );


    if( $this->rbac->checkAccess( $inRequest->auth, "publish", $inRequest->resource ) ) {

      $arrModify['tgispublic'][] = "TRUE";
      $arrModify['rbacpermission'] = Array();
      $result->result = $this->connection['resource']->modify( $arrResource[0]['dn'], $arrModify );

    }
    else {

      $objFaultDetail->faultNo = $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" );
      $objFaultDetail->faultMessage = $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" );
      $objFaultDetail->cause = "You do not have the permission publish the resource " . $inRequest->resource . ".";


      return new SoapFault( "authenticationFault",
                             $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                             get_class( $this ),
                             $objFaultDetail );

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: getSLC
  // Input: auth / xsd:string
  //        log / xsd:string
  //        secret / xsd:string
  // Output: slc / xsd:base64binary
  // Description
  // CRUD may use this to retrieve a Short-Lived Certificate Key pair for the user
  // -----------------------------------------------------
  function getSLC ( $inRequest ) {

    if ($inRequest->secret !== $this->config->getValue( "crud", "secret" ) ) { 
      return new SoapFault( "authenticationFault",
                            $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                            get_class( $this ),
		            $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" ) );
    }

    // Search for Session entry
    $filter = "(" . $this->rbac->getConfiguration()->getValue( "session", "namingattribute" ) .
      "=" . $inRequest->auth . ")";
    $arrSessionEntry = $this->connection['user']->search(
           $this->rbac->getConfiguration()->getValue( "session", "base" ),
	   $filter,	   "sub",           Array( "rbacSessionUser" ) );

    // retrieve rbacSessionUser attribute (ePPN)
    if(    isset( $arrSessionEntry[0] ) && isset( $arrSessionEntry[0]['dn'] ) ) {
      $eppn = $arrSessionEntry[0]['rbacsessionuser'][0];
    } else {
      // echo ("Could not find Session entry");
      return new SoapFault( "authenticationFault",
			    $this->config->getValue( "errorCode", "AUTHENTICATION_ERROR" ),
			    get_class( $this ),
			    $this->config->getValue( "errorDescription", "AUTHENTICATION_ERROR" ) );

    }

    // Search for user entry
    $filter = "(" . $this->config->getValue( "authentication", "namingattribute" ) . "=" . $eppn . ")";
    $arrUserEntry = $this->connection['user']->search( 
       $this->config->getValue( "authentication", "base" ),
       $filter, "sub", Array( "dn" ) );

    
    // Retrieve userPKCS12 attribute (certificate + key)
    if(  isset( $arrUserEntry[0] ) && isset( $arrUserEntry[0]['dn'] ) ) {
      // have to set binary mode to TRUE...
      $certArr = $this->connection['user']->getEntry( $arrUserEntry[0]['dn'], array("userpkcs12"), TRUE);
      $pkcs12 = $certArr['userpkcs12'][0];
    } else {
      //echo ("Failed finding the user entry");
      return new SoapFault( "authenticationFault",
			    $this->config->getValue( "errorCode", "AUTHENTICATION_ERROR" ),
			    get_class( $this ),
			    $this->config->getValue( "errorDescription", "AUTHENTICATION_ERROR" ) );

    }

    // find the passphrase for the p12 from the mapSIDtoePassphrase demon 
    $sock = socket_create( AF_UNIX, SOCK_STREAM, 0 );
    $response = new stdClass();
    $socketfilename = $this->config->getValue( "SLCPassphraseMap", "socketfile"  );

    if( socket_connect( $sock, $socketfilename ) ) {
	socket_write ( $sock, "getPassphrase\n");
        socket_write ( $sock, $inRequest->auth . "\n" );
	socket_write ( $sock, "\n" . '>>>EOF<<<' . "\n");

	$result = socket_read( $sock, 4096 , PHP_NORMAL_READ );
	
	if ( strpos ($result, "getPassphraseresult") == 0 ){
   	    $passphrase = chop(socket_read( $sock, 4096 , PHP_NORMAL_READ ));
	} else {
            // no passphrase returned
            return new SoapFault( "unknownResourceFault",
                            $this->config->getValue( "errorCode", "RESOURCENOTFOUND_ERROR" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "RESOURCENOTFOUND_ERROR" ) );
	}

        socket_shutdown( $sock, 2 );
        socket_close( $sock );

    } else {
        // could not connect to socket
        return new SoapFault( "unknownResourceFault",
                            $this->config->getValue( "errorCode", "RESOURCENOTFOUND_ERROR" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "RESOURCENOTFOUND_ERROR" ) );

    }
    // Encode certificate into PEM format
    $both = '';
    if ( openssl_pkcs12_read($pkcs12, $allpem, $passphrase) ) {
      $both = $allpem['cert'] .  $allpem['pkey'];
    } else {
      //echo ("Failed reading the user's certificate");
      return new SoapFault( "unknownResourceFault",
			    $this->config->getValue( "errorCode", "RESOURCENOTFOUND_ERROR" ),
			    get_class( $this ),
			    $this->config->getValue( "errorDescription", "RESOURCENOTFOUND_ERROR" ) );

    }  

    $result = new getSLCResponse();
    $result->slc =  base64_encode($both);
    
    return $result;

  }


  // -----------------------------------------------------
  // Function: getCSR
  // Input: auth / xsd:string
  //        log / xsd:string
  // Output: csr / xsd:base64binary
  // Description
  // Returns a dummy certificate signing request, where the contacted deamon will hold the key in memory
  // -----------------------------------------------------
  function getCSR ( $inRequest ) {

    # TODO should check whether auth is valid
    $sock = socket_create( AF_UNIX, SOCK_STREAM, 0 );
    $response = new stdClass();
    $socketfilename = $this->config->getValue( "SLCPassphraseMap", "socketfile"  );

    if( socket_connect( $sock, $socketfilename ) ) {

        socket_write ( $sock, "getCSR\n");
	socket_write ( $sock, $inRequest->auth . "\n" );
        $result = socket_read( $sock, 80, PHP_NORMAL_READ );
        if ( strpos ($result , "getCSRresult") == 0 ) {
            $result = socket_read( $sock, 80, PHP_NORMAL_READ );
            $csr = "";
            while ( $result && !(strpos ($result, '>>>EOF<<<') === 0 )) {
                $csr .= $result;
                $result = socket_read( $sock, 80, PHP_NORMAL_READ );
            }

   	    $response->csr =  $csr;
    
	return $response;
	} else {
            return new SoapFault( "unknownResourceFault",
                            $this->config->getValue( "errorCode", "RESOURCENOTFOUND_ERROR" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "RESOURCENOTFOUND_ERROR" ) );

	}
	socket_shutdown( $sock, 2 );
        socket_close( $sock );

    } else {
    $response->csr = base64_encode(socket_strerror(socket_last_error()));
    return $response;

        return new SoapFault( "unknownResourceFault",
                            $this->config->getValue( "errorCode", "RESOURCENOTFOUND_ERROR" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "RESOURCENOTFOUND_ERROR" ) );

    }
}


  // -----------------------------------------------------
  // Function: putCRT
  // Input: auth / xsd:string
  //        log / xsd:string
  //        crt / xsd:string 
  // Output: success / xsd:boolean
  // Description
  // Returns a dummy certificate signing request
  // -----------------------------------------------------
  function putCRT ( $inRequest ) {

    # TODO should check whether auth is valid
    $sock = socket_create( AF_UNIX, SOCK_STREAM, 0 );
    $response = new stdClass();
    $socketfilename = $this->config->getValue( "SLCPassphraseMap", "socketfile"  );

    if( socket_connect( $sock, $socketfilename ) ) {

        socket_write ( $sock, "putCRT\n");
	socket_write ( $sock, $inRequest->auth . "\n" );
	socket_write ( $sock, $inRequest->crt );
        socket_write ( $sock, "\n" . '>>>EOF<<<' . "\n");

        $result = socket_read( $sock, 4096, PHP_NORMAL_READ );
	if ( strpos ($result, "putCRTresult") == 0 ) {
	    $success  = chop(socket_read( $sock, 4096 , PHP_NORMAL_READ));
	    if ($success == "true") {
		$response->success = true;
                return $response;
	    }
	}
	socket_shutdown( $sock, 2 );
        socket_close( $sock );
    } 
    $response->success = false;
    return $response;
  }


  function uuidV4() {

    $strResult = "";
    $strRandom = "";


    $strRandom = uniqid( md5( rand() ) );

    $strResult .= substr( $strRandom, 0, 8 ) . "-";
    $strResult .= substr( $strRandom, 8, 4 ) . "-";
    $strResult .= substr( $strRandom, 12, 4 ) . "-";
    $strResult .= substr( $strRandom, 16, 4 ) . "-";
    $strResult .= substr( $strRandom, 32, 12 );


    return $strResult;

  }

}
?>
