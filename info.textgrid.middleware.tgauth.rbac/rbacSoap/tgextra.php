<?php 
/* Set internal character encoding to UTF-8 */
mb_internal_encoding("UTF-8");
// #######################################################
// Author: Markus Widmer
// Creation date: 07.07.2007
// Modification date: 13.11.2007
// Version: 1.0.0
// #######################################################

// testcomment

require_once( "soapTypes.inc.php" );
require_once( "../rbac/RBAC.class.php" );
require_once( "TgExtra.class.php" );


// Dont be so verbose with messages and notices.
error_reporting( E_ERROR | E_USER_ERROR );


// #############################################################
// Starting SOAP-Server
// #############################################################
$server = new SoapServer( "./wsdl/tgextra.wsdl" );
$server->setClass( "TgExtra", "../conf/rbacSoap.conf", "../conf/system.conf", "../rbac/" );


$server->handle();
/*
$tge = new TgExtra( "../conf/rbacSoap.conf.xml", "../conf/system.conf.xml", "../rbac/" );
$createProjectRequest = new CreateProjectRequest();
$tge->createProject( $createProjectRequest );
*/
?>
