<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 18.07.2007
// Modification date: 13.11.2007
// Version: 0.1.6
// #######################################################


class TgReview {

  // Global variables
  protected $rbac;
  protected $config;



  // -----------------------------------------------------
  // Constructor
  // Input: none
  // Output: object TgReview
  // Description:
  //   Sets the configuration and creates an instance of
  //   the RBAC-class.
  // -----------------------------------------------------
  public function __construct( $inConfigurationFilename, $inRbacConfFile, $inRbacBase ) {
    $this->rbac = new RBAC( $inRbacConfFile, $inRbacBase );


    $this->config = new SimpleConfig( $inConfigurationFilename );

  }




  // -----------------------------------------------------
  // Function: sessionRoles
  // Input: intSid / xsd:string
  //        sid / xsd:string
  // Output: result / array of xsd:string
  // Description
  //   If the internal session has appropriate access the
  //   function returns a list of all roles that are activ
  //   for the given session.
  // -----------------------------------------------------
  public function sessionRoles( $inRequest ) {

    $arrRoleResult = Array();         // The roles of the session
    $result = new rolesetResponse();  // The result


    // Test if the user has apropriate rights
    if( $this->rbac->checkAccess( $inRequest->intSid, "review", "session_base" ) ) {

      try {

        $arrRoleResult = $this->rbac->sessionRoles( $inRequest->sid );


        $result->role = $arrRoleResult;

      }
      catch( RBACException $e ) {

        return new SoapFault( "rbacFault", $e->getCode(), get_class( $this->rbac ), $e->getMessage() );

      }

    }        
    else {

      return new SoapFault( "authenticationFault",
                            $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" ) );

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: assignedRoles
  // Input: intSid / xsd:string
  //        username / xsd:string
  // Output: result / array of xsd:string
  // Description
  //   If the internal session has appropriate access the
  //   function returns a list of all roles the user is
  //   assigned to.
  // -----------------------------------------------------
  public function assignedRoles( $inRequest ) {

    $userDomain = "";                 // The domain-component of the user
    $arrTmpDomain = Array();          // Temporary array
    $arrRoleResult = Array();         // The roles of the user
    $result = new rolesetResponse();  // The result


    if( preg_match( "/^.+[@]{1}.+$/", $inRequest->username ) ) {

      $arrTmpDomain = preg_split( "/[@]/", $inRequest->username );
      $userDomain = $arrTmpDomain[1];


      // Test if the user has apropriate rights
      if(    $this->rbac->checkAccess( $inRequest->intSid, "review", "user_" . $userDomain )
          || $this->rbac->checkAccess( $inRequest->intSid, "review", "user_base" ) ) {

        try {

          $arrRoleResult = $this->rbac->assignedRoles( $inRequest->username );


          $result->role = $arrRoleResult;

        }
        catch( RBACException $e ) {

          return new SoapFault( "rbacFault", $e->getCode(), get_class( $this->rbac ), $e->getMessage() );

        }

      }
      else {

        return new SoapFault( "authenticationFault",
                              $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                              get_class( $this ),
                              $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" ) );

      }

    }
    else {

      return new SoapFault( "formatFault",
                            $this->config->getValue( "errorCode", "INVALID_USER_FORMAT" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "INVALID_USER_FORMAT" ) );

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: assignedUsers
  // Input: intSid / xsd:string
  //        role / xsd:string
  // Output: result / array of xsd:string
  // Description
  //   If the internal session has appropriate access the
  //   function returns a list of all users that are
  //   assigned to the role.
  // -----------------------------------------------------
  public function assignedUsers( $inRequest ) {

    $arrUserResult = Array();         // The users assigned to the role
    $result = new usersetResponse();  // The result


    // Test if the user has apropriate rights to list the users
    // that are assigned to the role
    if(    $this->rbac->checkAccess( $inRequest->intSid, "review", $inRequest->role )
        || $this->rbac->checkAccess( $inRequest->intSid, "review", "role_base" ) ) {

      try {

        $arrUserResult = $this->rbac->assignedUsers( $inRequest->role );


        $result->username = $arrUserResult;

      }
      catch( RBACException $e ) {

        return new SoapFault( "rbacFault", $e->getCode(), get_class( $this->rbac ), $e->getMessage() );

      }

    }
    else {

      return new SoapFault( "authenticationFault",
                            $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" ) );

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: rolePermissions
  // Input: intSid / xsd:string
  //        role / xsd:string
  // Output: result / array of tns:permission
  // Description
  //   If the internal session has appropriate access the
  //   function returns all permissions a role has.
  // -----------------------------------------------------
  public function rolePermissions( $inRequest ) {

    $arrPermission = Array();               // The permissions of the role
    $result = new permissionsetResponse();  // The result


    // Test if the user has apropriate rights
    if( $this->rbac->checkAccess( $inRequest->intSid, "rolePermissions", "resource_top" ) ) {

      try {

        $arrPermission = $this->rbac->rolePermissions( $inRequest->role );
        $result->permissionset = Array();


        for( $i = 0; $i < sizeof( $arrPermission ); $i++ ) {

          array_push( $result->permissionset, new permission( $arrPermission[$i]['operation'],
                                                               $arrPermission[$i]['resource'] ) );

        }

      }
      catch( RBACException $e ) {

        return new SoapFault( "rbacFault", $e->getCode(), get_class( $this->rbac ), $e->getMessage() );

      }

    }
    else {

      return new SoapFault( "authenticationFault",
                            $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" ) );

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: roleOperationsOnObject
  // Input: intSid / xsd:string
  //        role / xsd:string
  //        resource / xsd:string
  // Output: result / array of xsd:string
  // Description
  //   If the internal session has appropriate access the
  //   function returns all operations a role has on a specific
  //   resource.
  // -----------------------------------------------------
  public function roleOperationsOnObject( $inRequest ) {

    $arrOperation = Array();               // The operations of the role on the given resource
    $result = new operationsetResponse();  // The result


    // Test if the user has apropriate rights
    if( $this->rbac->checkAccess( $inRequest->intSid, "review", "resource_top" ) ) {

      try {

        $arrOperation = $this->rbac->roleOperationsOnObject( $inRequest->role, $inRequest->resource );


        $result->operationset = $arrOperation;

      }
      catch( RBACException $e ) {

        return new SoapFault( "rbacFault", $e->getCode(), get_class( $this->rbac ), $e->getMessage() );

      }

    }
    else {

      return new SoapFault( "authenticationFault",
                            $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" ) );

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: userOperationsOnObject
  // Input: intSid / xsd:string
  //        user / xsd:string
  //        resource / xsd:string
  // Output: result / array of xsd:string
  // Description
  //   If the internal session has appropriate access the
  //   function returns all operations a user has on a specific
  //   resource.
  // -----------------------------------------------------
  public function userOperationsOnObject( $inRequest ) {

    $arrOperation = Array();               // The operations of the role on the given resource
    $result = new operationsetResponse();  // The result


    // Test if the user has apropriate rights
    if( $this->rbac->checkAccess( $inRequest->intSid, "top", "resource_top" ) ) {

      try {

        $arrOperation = $this->rbac->userOperationsOnObject( $inRequest->user, $inRequest->resource );


        $result->operationset = $arrOperation;

      }
      catch( RBACException $e ) {

        return new SoapFault( "rbacFault", $e->getCode(), get_class( $this->rbac ), $e->getMessage() );

      }

    }
    else {

      return new SoapFault( "authenticationFault",
                            $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" ) );

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: userPermissions
  // Input: intSid / xsd:string
  //        username / xsd:string
  // Output: result / array of tns:permission
  // Description
  //   If the internal session has appropriate access the
  //   function returns all permissions a user has by
  //   beeing assigned to roles.
  // -----------------------------------------------------
  public function userPermissions( $inRequest ) {

    $arrPermission = Array();               // The permissions of the user
    $result = new permissionsetResponse();  // The result


    // Test if the user has apropriate rights
    if( $this->rbac->checkAccess( $inRequest->intSid, "review", "resource_top" ) ) {

      try {

        $arrPermission = $this->rbac->userPermissions( $inRequest->username );
        $result->permissionset = Array();


        for( $i = 0; $i < sizeof( $arrPermission ); $i++ ) {

          array_push( $result->permissionset, new permission( $arrPermission[$i]['operation'],
                                                               $arrPermission[$i]['resource'] ) );

        }

      }
      catch( RBACException $e ) {

        return new SoapFault( "rbacFault", $e->getCode(), get_class( $this->rbac ), $e->getMessage() );

      }

    }
    else {

      return new SoapFault( "authenticationFault",
                            $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" ) );

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: sessionPermissions
  // Input: intSid / xsd:string
  //        sid / xsd:string
  // Output: result / array of tns:permission
  // Description
  //   If the internal session has appropriate access the
  //   function returns all permissions a session has
  //   because of the roles that are active.
  // -----------------------------------------------------
  public function sessionPermissions( $inRequest ) {

    $arrPermission = Array();               // The permissions of the user
    $result = new permissionsetResponse();  // The result


    // Test if the user has apropriate rights
    if( $this->rbac->checkAccess( $inRequest->intSid, "review", "resource_top" ) ) {

      try {

        $arrPermission = $this->rbac->sessionPermissions( $inRequest->sid );
        $result->permissionset = Array();


        for( $i = 0; $i < sizeof( $arrPermission ); $i++ ) {

          array_push( $result->permissionset, new permission( $arrPermission[$i]['operation'],
                                                               $arrPermission[$i]['resource'] ) );

        }

      }
      catch( RBACException $e ) {

        return new SoapFault( "rbacFault", $e->getCode(), get_class( $this->rbac ), $e->getMessage() );

      }

    }
    else {

      return new SoapFault( "authenticationFault",
                            $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" ) );

    }



    return $result;

  }




  // -----------------------------------------------------
  // Function: authorizedRoles
  // Input: intSid / xsd:string
  //        username / xsd:string
  // Output: result / array of xsd:string
  // Description
  //   If the internal session has appropriate access the
  //   function returns a list of all roles the user is
  //   authorized for.
  // -----------------------------------------------------
  public function authorizedRoles( $inRequest ) {

    $userDomain = "";                 // The domain-component of the user
    $arrTmpDomain = Array();          // Temporary array
    $arrRoleResult = Array();         // The roles of the user
    $result = new rolesetResponse();  // The result

   
    if( preg_match( "/^.+[@]{1}.+$/", $inRequest->username ) ) {

      $arrTmpDomain = preg_split( "/[@]/", $inRequest->username );
      $userDomain = $arrTmpDomain[1];
      // Test if the user has apropriate rights
      if(    $this->rbac->checkAccess( $inRequest->intSid, "review", "user_" . $userDomain )
          || $this->rbac->checkAccess( $inRequest->intSid, "review", "user_base" ) ) {

        try {


          $arrRoleResult = $this->rbac->authorizedRoles( $inRequest->username );
	  

          $result->role = $arrRoleResult;

        }
        catch( RBACException $e ) {
	  
          return new SoapFault( "rbacFault", $e->getCode(), get_class( $this->rbac ), $e->getMessage() );

        }

      }
      else {

        return new SoapFault( "authenticationFault",
                              $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                              get_class( $this ),
                              $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" ) );

      }

    }
    else {

      return new SoapFault( "formatFault",
                            $this->config->getValue( "errorCode", "INVALID_USER_FORMAT" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "INVALID_USER_FORMAT" ) );

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: authorizedUsers
  // Input: intSid / xsd:string
  //        role / xsd:string
  // Output: result / array of xsd:string
  // Description
  //   If the internal session has appropriate access the
  //   function returns a list of all users that are
  //   authorized for this role.
  // -----------------------------------------------------
  public function authorizedUsers( $inRequest ) {

    $arrUserResult = Array();         // The users assigned to the role
    $result = new usersetResponse();  // The result


    // Test if the user has apropriate rights to list the users
    // that are authorized to that role
    if(    $this->rbac->checkAccess( $inRequest->intSid, "review", $inRequest->role )
        || $this->rbac->checkAccess( $inRequest->intSid, "review", "role_base" ) ) {

      try {

        $arrUserResult = $this->rbac->authorizedUsers( $inRequest->role );


        $result->username = $arrUserResult;

      }
      catch( RBACException $e ) {

        return new SoapFault( "rbacFault", $e->getCode(), get_class( $this->rbac ), $e->getMessage() );

      }

    }
    else {

      return new SoapFault( "authenticationFault",
                            $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" ) );

    }


    return $result;

  }

}
?>
