<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 18.07.2007
// Modification date: 13.08.2007
// Version: 0.1.4
// #######################################################


class TgAdministration {

  // Global variables
  protected $rbac;
  protected $config;
  protected $connection;



  // -----------------------------------------------------
  // Constructor
  // Input: none
  // Output: object RBACcore
  // Description:
  //   Sets the configuration and creates an instance of
  //   the RBAC-class.
  // -----------------------------------------------------
  public function __construct( $inConfigurationFilename, $inRbacConfFile, $inRbacBase ) {

    $this->rbac = new RBAC( $inRbacConfFile, $inRbacBase );


    $this->config = new SimpleConfig( $inConfigurationFilename );


    // Create connection
    $this->connection['user'] = new LDAP();
    $this->connection['user']->connect( $this->config->getValue( "authentication", "host" ),
                                        $this->config->getValue( "authentication", "port" ),
                                        $this->config->getValue( "authentication", "version" ),
                                        preg_match( "/yes/i", $this->config->getValue( "authentication", "tls" ) ) ? true : false );
    $this->connection['user']->bind( $this->config->getValue( "authentication", "binddn" ),
                                     $this->config->getValue( "authentication", "password" ) );

  }




  // -----------------------------------------------------
  // Function: addUser
  // Input: intSid / xsd:string
  //        username / xsd:string
  //        password / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Tries to authorize the user. If this is
  //   successful the user is added to the system.
  // -----------------------------------------------------
  function addUser( $inRequest ) {

    $userDomain = "";                 // The domain-component of the user
    $arrTmpDomain = Array();          // Temporary array
    $userTreeDn = "";                 // The tree of the directory where to add the user
    $addUserResult = false;           // The result of the RBAC-call
    $result = new booleanResponse();  // The result


    if( preg_match( "/^.+[@]{1}.+$/", $inRequest->username ) ) {

      $arrTmpDomain = preg_split( "/[@]/", $inRequest->username );
      $userDomain = $arrTmpDomain[1];


      // Test if the user has apropriate rights
      if(    $this->rbac->checkAccess( $inRequest->intSid, "administer", "user_" . $userDomain )
          || $this->rbac->checkAccess( $inRequest->intSid, "administer", "user_base" ) ) {

        // Construct the sub-dn under which the user will be added. The base-DN is
        // not given because the RBAC-system uses this allways as base and simply starts
        // to act from there. To give a dn to the RBAC-function is optional and not
        // documented or supported by the ANSI-standard! It is an implementation-specific
        // addition.
        $userTreeDn = $this->config->getValue( "user", "userTreeAttribute" ) . "=" . $userDomain;


        // If the internal user tree does not already exist,
        // it has to be created to avoid unnecessarry faults.
        $filter = "(" . $this->config->getValue( "user", "userTreeAttribute" ) . "=" . $userDomain . ")";
        $treeResult = $this->connection['user']->search( $this->config->getValue( "authentication", "base" ), $filter, "one" );


        if( $treeResult && sizeof( $treeResult ) < 1 ) {

          $arrTree['objectclass'][] = "organizationalunit";
          $arrTree['objectclass'][] = "rbacresource";
          $arrTree['ou'][] = $userDomain;

//          $file = fopen( "/tmp/debug.log", "a+" );
//          fwrite( $file, $userTreeDn . "," . $this->config->getValue( "authentication", "base" ) );
//          fclose( $file );
          $this->connection['user']->add( $userTreeDn . "," . $this->config->getValue( "authentication", "base" ), $arrTree );

        }


        try {

          $addUserResult = $this->rbac->addUser( $inRequest->username, $inRequest->password, $userTreeDn );


          $result->result = $addUserResult;

        }
        catch( RBACException $e ) {

          return new SoapFault( "rbacFault", $e->getCode(), get_class( $this->rbac ), $e->getMessage() );

        }

      }
      else {

        return new SoapFault( "authenticationFault",
                              $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                              get_class( $this ),
                              $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" ) );

      }

    }
    else {

      return new SoapFault( "formatFault",
                            $this->config->getValue( "errorCode", "INVALID_USER_FORMAT" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "INVALID_USER_FORMAT" ) );

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: deleteUser
  // Input: intSid / xsd:string
  //        username / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Tries to authorize the user. If this is
  //   successful the given user is removed from the system.
  // -----------------------------------------------------
  function deleteUser( $inRequest ) {

    $userDomain = "";                 // The domain-component of the user
    $arrTmpDomain = Array();          // Temporary array
    $deleteUserResult = false;        // Result of the RBAC-call
    $result = new booleanResponse();  // The result


    if( preg_match( "/^.+[@]{1}.+$/", $inRequest->username ) ) {

      $arrTmpDomain = preg_split( "/[@]/", $inRequest->username );
      $userDomain = $arrTmpDomain[1];


      // Test if the user has apropriate rights
      if(    $this->rbac->checkAccess( $inRequest->intSid, "administer", "user_" . $userDomain )
          || $this->rbac->checkAccess( $inRequest->intSid, "administer", "user_base" ) ) {

        try {

          $deleteUserResult = $this->rbac->deleteUser( $inRequest->username );


          $result->result = $deleteUserResult;

        }
        catch( RBACException $e ) {

          return new SoapFault( "rbacFault", $e->getCode(), get_class( $this->rbac ), $e->getMessage() );

        }

      }
      else {

        return new SoapFault( "authenticationFault",
                              $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                              get_class( $this ),
                              $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" ) );

      }

    }
    else {

      return new SoapFault( "formatFault",
                            $this->config->getValue( "errorCode", "INVALID_USER_FORMAT" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "INVALID_USER_FORMAT" ) );

    }


    return $result;

  }



  // -----------------------------------------------------
  // Function: addRole
  // Input: intSid / xsd:string
  //        role / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Tries to authorize the user. If this is
  //   successful the given role is added to the system.
  //   This function creates roles without a hirarchy, so it
  //   only has to check if there is access to the "role_base".
  // -----------------------------------------------------
  function addRole( $inRequest ) {

    $addRoleResult = false;           // The result of the RBAC-call
    $result = new booleanResponse();  // The result


    // Test if the user has apropriate rights
    if( $this->rbac->checkAccess( $inRequest->intSid, "administer", "role_base" ) ) {

      try {

        $addRoleResult = $this->rbac->addRole( $inRequest->role );


        $result->result = $addRoleResult;

      }
      catch( RBACException $e ) {

        return new SoapFault( "rbacFault", $e->getCode(), get_class( $this->rbac ), $e->getMessage() );

      }

    }
    else {

      return new SoapFault( "authenticationFault",
                            $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" ) );

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: deleteRole
  // Input: intSid / xsd:string
  //        role / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Tries to authorize the user. If this is
  //   successful the given role is removed from the system.
  //   This function removes roles without a hirarchy, so it
  //   only has to check if there is access to the "role_base".
  // -----------------------------------------------------
  function deleteRole( $inRequest ) {

    $deleteRoleResult = false;        // The result of the RBAC-call
    $result = new booleanResponse();  // The result


    // Test if the user has apropriate rights
    if( $this->rbac->checkAccess( $inRequest->intSid, "administer", "role_base" ) ) {

      try {

        $deleteRoleResult = $this->rbac->deleteRole( $inRequest->role );


        $result->result = $deleteRoleResult;

      }
      catch( RBACException $e ) {

        return new SoapFault( "rbacFault", $e->getCode(), get_class( $this->rbac ), $e->getMessage() );

      }

    }
    else {

      return new SoapFault( "authenticationFault",
                            $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" ) );

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: assignUser
  // Input: intSid / xsd:string
  //        username / xsd:string
  //        role / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Tries to authorize the user. If this is
  //   successful the given user is assigned to the
  //   role.
  // -----------------------------------------------------
  function assignUser( $inRequest ) {

    $assignUserResult = false;        // The result of the RBAC-call
    $result = new booleanResponse();  // The result


    // Test if the user has apropriate rights to assign a user
    // to the given role. The user may have directly the right
    // to modify the given role or the user may have the right to
    // modify the whole role-tree.
    if(    $this->rbac->checkAccess( $inRequest->intSid, "delegate", $inRequest->role )
        || $this->rbac->checkAccess( $inRequest->intSid, "administer", "role_base" ) ) {

      try {

        $assignUserResult = $this->rbac->assignUser( $inRequest->username, $inRequest->role );


        $result->result = $assignUserResult;

      }
      catch( RBACException $e ) {

        return new SoapFault( "rbacFault", $e->getCode(), get_class( $this->rbac ), $e->getMessage() );

      }

    }
    else {

      return new SoapFault( "authenticationFault",
                            $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" ) );

    }


    return $result;

  }



  // -----------------------------------------------------
  // Function: deassignUser
  // Input: intSid / xsd:string
  //        username / xsd:string
  //        role / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Tries to authorize the user. If this is
  //   successful the given user is deassigned from
  //   the role.
  // -----------------------------------------------------
  function deassignUser( $inRequest ) {

    $deassignUserResult = false;      // The result of the RBAC-call
    $result = new booleanResponse();  // The result


    // Test if the user has apropriate rights to assign a user
    // to the given role. The user may have directly the right
    // to modify the given role or the user may have the right to
    // modify the whole role-tree.
    if(    $this->rbac->checkAccess( $inRequest->intSid, "delegate", $inRequest->role )
        || $this-rbac-checkAccess( $inRequest->intSid, "administer", "role_base" ) ) {

      try {

        $deassignUserResult = $this->rbac->deassignUser( $inRequest->username, $inRequest->role );


        $result->result = $deassignUserResult;

      }
      catch( RBACException $e ) {

        return new SoapFault( "rbacFault", $e->getCode(), get_class( $this->rbac ), $e->getMessage() );

      }

    }
    else {

      return new SoapFault( "authenticationFault",
                            $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" ) );

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: grantPermission
  // Input: intSid / xsd:string
  //        resource / xsd:string
  //        operation / xsd:string
  //        role / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Tries to authorize the user. If this is
  //   successful the given user is deassigned from
  //   the role.
  // -----------------------------------------------------
  function grantPermission( $inRequest ) {

    $grantPermissionResult = false;   // The result of the RBAC-call
    $result = new booleanResponse();  // The result


    // Test if the user has apropriate rights to grant a permission
    // to the given role. The user may have directly the right
    // to modify the given permission or the user may have the right to
    // modify the whole permission (resource)-tree.
    if( $this->rbac->checkAccess( $inRequest->intSid, "delegate", $inRequest->resource ) ) {

      try {

        $grantPermissionResult = $this->rbac->grantPermission( $inRequest->resource, $inRequest->operation, $inRequest->role );


        $result->result = $grantPermissionResult;

      }
      catch( RBACException $e ) {

        return new SoapFault( "rbacFault", $e->getCode(), get_class( $this->rbac ), $e->getMessage() );

      }

    }
    else {

      return new SoapFault( "authenticationFault",
                            $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" ) );

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: revokePermission
  // Input: intSid / xsd:string
  //        operation / xsd:string
  //        resource / xsd:string
  //        role / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Tries to authorize the user. If this is
  //   successful the role loses 
  // -----------------------------------------------------
  function revokePermission( $inRequest ) {

    $revokePermissionResult = false;   // The result of the RBAC-call
    $result = new booleanResponse();   // The result


    // Test if the user has apropriate rights to grant a permission
    // to the given role. The user may have directly the right
    // to modify the given permission or the user may have the right to
    // modify the whole permission (resource)-tree.
    if(    $this->rbac->checkAccess( $inRequest->intSid, "delegate", $inRequest->resource ) ) {

      try {

        $revokePermissionResult = $this->rbac->revokePermission( $inRequest->operation, $inRequest->resource, $inRequest->role );


        $result->result = $revokePermissionResult;

      }
      catch( RBACException $e ) {

        return new SoapFault( "rbacFault", $e->getCode(), get_class( $this->rbac ), $e->getMessage() );

      }

    }
    else {

      return new SoapFault( "authenticationFault",
                            $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" ) );

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: addInheritance
  // Input: intSid / xsd:string
  //        ascendant / xsd:string
  //        descendant / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Tries to authorize the user. If this is
  //   successful the the role inheritance between
  //   the ascendance and the descendance is established.
  //   After this, the descendant has all the rights of
  //   the ascendant.
  // -----------------------------------------------------
  function addInheritance( $inRequest ) {

    $addInheritanceResult = false;    // The result of the RBAC-call
    $result = new booleanResponse();  // The result


    // Test if the user has apropriate rights to add an inheritance
    // between the given roles. The user may have directly the right
    // to add an inheritance to the descendant or he is allowed to
    // modify all of the roles inheritances.
    if(    $this->rbac->checkAccess( $inRequest->intSid, "administer", "role_base" ) ) {

      try {

        $addInheritanceResult = $this->rbac->addInheritance( $inRequest->ascendant, $inRequest->descendant );


        $result->result = $addInheritanceResult;

      }
      catch( RBACException $e ) {

        return new SoapFault( "rbacFault", $e->getCode(), get_class( $this->rbac ), $e->getMessage() );

      }

    }
    else {

      return new SoapFault( "authenticationFault",
                            $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" ) );

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: deleteInheritance
  // Input: intSid / xsd:string
  //        ascendant / xsd:string
  //        descendant / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Tries to authorize the user. If this is
  //   successful the role inheritance between
  //   the ascendance and the descendance is removed.
  //   After this, the descendant no longer has the rights of
  //   the ascendant.
  // -----------------------------------------------------
  function deleteInheritance( $inRequest ) {

    $deleteInheritanceResult = false; // The result of the RBAC-call
    $result = new booleanResponse();  // The result


    // Test if the user has apropriate rights to add an inheritance
    // between the given roles. The user may have directly the right
    // to delete the inheritance or he is allowed to
    // modify all of the roles inheritances.
    if(    $this->rbac->checkAccess( $inRequest->intSid, "administer", "role_base" ) ) {

      try {

        $deleteInheritanceResult = $this->rbac->deleteInheritance( $inRequest->ascendant, $inRequest->descendant );


        $result->result = $deleteInheritanceResult;

      }
      catch( RBACException $e ) {

        return new SoapFault( "rbacFault", $e->getCode(), get_class( $this->rbac ), $e->getMessage() );

      }

    }
    else {

      return new SoapFault( "authenticationFault",
                            $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" ) );

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: addAscendant
  // Input: intSid / xsd:string
  //        ascendant / xsd:string
  //        descendant / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Tries to authorize the user. If this is
  //   successful the new role "ascendant" is added and
  //   the inheritance between the ascendance and the
  //   descendance is established. After this, the descendant
  //   has all the rights of the ascendant.
  // -----------------------------------------------------
  function addAscendant( $inRequest ) {

    $addAscendantResult = false;      // The result of the RBAC-call
    $result = new booleanResponse();  // The result


    // Test if the user has apropriate rights to add an ascendant.
    // The user may have directly the right to add the ascendant
    // or he is allowed to modify all of the roles.
    if(    $this->rbac->checkAccess( $inRequest->intSid, "delegate", $inRequest->descendant )
        || $this->rbac->checkAccess( $inRequest->intSid, "administer", "role_base" ) ) {

      try {

        $addAscendantResult = $this->rbac->addAscendant( $inRequest->ascendant, $inRequest->descendant );


        $result->result = $addAscendantResult;

      }
      catch( RBACException $e ) {

        return new SoapFault( "rbacFault", $e->getCode(), get_class( $this->rbac ), $e->getMessage() );

      }

    }
    else {

      return new SoapFault( "authenticationFault",
                            $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" ) );

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: addDescendant
  // Input: intSid / xsd:string
  //        ascendant / xsd:string
  //        descendant / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Tries to authorize the user. If this is
  //   successful the new role "descendant" is added and
  //   the inheritance between the ascendance and the
  //   descendance is established. After this, the descendant
  //   has all the rights of the ascendant.
  // -----------------------------------------------------
  function addDescendant( $inRequest ) {

    $addDescendantResult = false;     // The result of the RBAC-call
    $result = new booleanResponse();  // The result


    // Test if the user has apropriate rights to add an descendant.
    // The user may have directly the right to add the ascendant
    // or he is allowed to modify all of the roles.
    if(    $this->rbac->checkAccess( $inRequest->intSid, "administer", "role_base" ) ) {

      try {

        $addDescendantResult = $this->rbac->addDescendant( $inRequest->ascendant, $inRequest->descendant );


        $result->result = $addDescendantResult;

      }
      catch( RBACException $e ) {

        return new SoapFault( "rbacFault", $e->getCode(), get_class( $this->rbac ), $e->getMessage() );

      }

    }
    else {

      return new SoapFault( "authenticationFault",
                            $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" ) );

    }


    return $result;

  }

}
?>
