<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 17.07.2007
// Modification date: 09.10.2007
// Version: 0.2.2
// #######################################################



class TgSystem {

  // Global variables
  protected $rbac;
  protected $config;



  // -----------------------------------------------------
  // Constructor
  // Input: none
  // Output: object RBACcore
  // Description:
  //   Creates initial connections to the LDAP-server and
  //   sets some configuration parameters.
  // -----------------------------------------------------
  public function __construct( $inConfigurationFilename, $inRbacConfFile, $inRbacBase ) {

    $this->rbac = new RBAC( $inRbacConfFile, $inRbacBase );


    $this->config = new SimpleConfig( $inConfigurationFilename );

  }




  // -----------------------------------------------------
  // Function: createSession
  // Input: intSid / xsd:string
  //        username / xsd:string
  //        roleset / xsd:string
  //        sid / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Creates a session for a user. But first the user who
  //   wants to create a session for another user has to
  //   be authenticated and authorised.
  // -----------------------------------------------------
  function createSession( $inRequest ) {

    $arrRole = Array();               // The initial roleset for the new session
    $result = new booleanResponse();  // The result of the session-creation
    $createSessionResult = false;     // The result of the rbac-call
    // Test if the user has apropriate rights
    if( $this->rbac->checkAccess( $inRequest->intSid, "administer", "session_base" ) ) {

      // Only if there is more than one role given, the soap-engine of
      // PHP creates an array!
      if( isset( $inRequest->roleset ) ) {

        is_array( $inRequest->roleset ) ? $arrRole = $inRequest->roleset : $arrRole[] = $inRequest->roleset;
    }

      // Try to create the session
      try{
      
        $createSessionResult = $this->rbac->createSession( $inRequest->username, $arrRole, $inRequest->sid );

        $result->result = true;

      }
      catch( RBACException $e ) {

        return new SoapFault( "rbacFault", $e->getCode(), get_class( $rbac ), $e->getMessage() );

      }

    }
    else {

      return new SoapFault( "authenticationFault",
                            $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" ) );

    }

    return $result;

  }



  // -----------------------------------------------------
  // Function: deleteSession
  // Input: intSid / xsd:string
  //        username / xsd:string
  //        sid / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Deletes a user's session. But first the user who
  //   wants to delte the session for another user has to
  //   be authenticated and authorised.
  // -----------------------------------------------------
  function deleteSession( $inRequest ) {

    $result = new booleanResponse();  // The result of the session-creation
    $deleteSessionResult = false;     // The result of the rbac-call


    // Test if the user has apropriate rights
    if( $this->rbac->checkAccess( $inRequest->intSid, "administer", "session_base" ) ) {

      // Try to create the session
      try{

        $deleteSessionResult = $this->rbac->deleteSession( $inRequest->username, $inRequest->sid );

        $result->result = true;

      }
      catch( RBACException $e ) {

        return new SoapFault( "rbacFault", $e->getCode(), get_class( $rbac ), $e->getMessage() );

      }

    }
    else {

      return new SoapFault( "authenticationFault",
                            $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" ) );

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: addActiveRole
  // Input: intSid / xsd:string
  //        username / xsd:string
  //        sid / xsd:string
  //        role / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Adds an active role to the session. This is
  //   possible without having authenticated.
  // -----------------------------------------------------
  function addActiveRole( $inRequest ) {

    $result = new booleanResponse();  // The result of the session-creation
    $addActiveRoleResult = false;     // The result of the rbac-call


    // Try to add the role to the session
    try{

      $addActiveRoleResult = $this->rbac->addActiveRole( $inRequest->username, $inRequest->sid, $inRequest->role );

      $result->result = true;

    }
    catch( RBACException $e ) {

      return new SoapFault( "rbacFault", $e->getCode(), get_class( $rbac ), $e->getMessage() );

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: dropActiveRole
  // Input: intSid / xsd:string
  //        username / xsd:string
  //        sid / xsd:string
  //        role / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Adds an active role to the session. This is
  //   possible without having authenticated.
  // -----------------------------------------------------
  function dropActiveRole( $inRequest ) {

    $result = new booleanResponse();  // The result of the session-creation
    $dropActiveRoleResult = false;    // The result of the rbac-call


    // Try to add the role to the session
    try{

      $dropActiveRoleResult = $this->rbac->dropActiveRole( $inRequest->username, $inRequest->sid, $inRequest->role );

      $result->result = true;

    }
    catch( RBACException $e ) {

      return new SoapFault( "rbacFault", $e->getCode(), get_class( $rbac ), $e->getMessage() );

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: checkAccess
  // Input: intSid / xsd:string
  //        sid / xsd:string
  //        operation / xsd:string
  //        resource / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Makes the "checkAccess"-call and returns the information
  //   if the access is granted or denied.
  // -----------------------------------------------------
  function checkAccess( $inRequest ) {

    $result = new booleanResponse();  // The result of the session-creation
    $checkAccessResult = false;       // The result of the rbac-call


    try {

      $checkAccessResult = $this->rbac->checkAccess( $inRequest->sid, $inRequest->operation, $inRequest->resource );

      $result->result = $checkAccessResult;

    }
    catch( RBACException $e ) {

      return new SoapFault( "rbacFault", $e->getCode(), get_class( $rbac ), $e->getMessage() );

    }


    return $result;

  }

}
?>
