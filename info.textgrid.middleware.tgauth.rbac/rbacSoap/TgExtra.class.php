<?php
// ##############################################################
// Author: Markus Widmer & Martin Haase, DAASI International GmbH
// Creation date: 17.07.2007
// Modification date: 14.02.2020 --fu
// Version: 0.4.0
// ##############################################################

class TgExtra {

  // Global variables
  protected $rbac;
  protected $config;
  protected $connection;

  // -----------------------------------------------------
  // Constructor
  // Input: none
  // Output: object RBACcore
  // Description:
  //   Creates initial connections to the LDAP-server and
  //   sets some configuration parameters.
  // -----------------------------------------------------
  public function __construct( $inConfigurationFilename, $inRbacConfFile, $inRbacBase ) {
    	
    $this->rbac = new RBAC( $inRbacConfFile, $inRbacBase );


    $this->config = new SimpleConfig( $inConfigurationFilename );


    // Create connection
    $this->connection['user'] = new LDAP();
    $this->connection['user']->connect( $this->config->getValue( "authentication", "host" ),
                                        $this->config->getValue( "authentication", "port" ),
                                        $this->config->getValue( "authentication", "version" ),
                                        preg_match( "/yes/i", $this->config->getValue( "authentication", "tls" ) ) ? true : false );
    $this->connection['user']->bind( $this->config->getValue( "authentication", "binddn" ),
                                     $this->config->getValue( "authentication", "password" ) );


    $this->connection['counter'] = new LDAP();
    $this->connection['counter']->connect( $this->config->getValue( "counter", "host" ),
                                           $this->config->getValue( "counter", "port" ),
                                           $this->config->getValue( "counter", "version" ),
                                           preg_match( "/yes/i", $this->config->getValue( "counter", "tls" ) ) ? true : false );
    $this->connection['counter']->bind( $this->config->getValue( "counter", "binddn" ),
                                        $this->config->getValue( "counter", "password" ) );


    $this->connection['resource'] = $this->rbac->getConnection( "resource" );
    $this->connection['role'] = $this->rbac->getConnection( "role" );
    $this->connection['session'] = $this->rbac->getConnection( "session" );

  }


  // -----------------------------------------------------
  // Function: authenticate
  // Input: username / xsd:string
  //        password / xsd:string
  //        log / xsd:string
  // Output: sid / xsd:string
  // Description
  //   Tries to authenticate the user. If this is
  //   successful a session-ID is generated and a
  //   session is startet.
  // -----------------------------------------------------
  function authenticate( $inRequest ) {

    $filter = "";                          // search-filter
    $result = new authenticateResponse();  // service-resonse
    $arrUserEntry;                         // the users entry in the directory
    $intSid = "";                          // the generated session-ID
    $creationResult;                       // the result of the creation of the session
    $objFaultDetail = new stdClass();      // Fault details sent to the client



    // Construct the search-filter
    $filter .= "(&" . $this->config->getValue( "authentication", "filter" );
    $filter .= "(" . $this->config->getValue( "authentication", "namingattribute" ) . "=" . $inRequest->username . "))";

    // Search for the users entry
    $arrUserEntry = $this->connection['user']->search( $this->config->getValue( "authentication", "base" ), $filter, "sub",
                                                       Array( $this->config->getValue( "authentication", "namingattribute" ) ) );


    if(    isset( $arrUserEntry[0] )
        && isset( $arrUserEntry[0]['dn'] ) ) {

      // Try to bind with the given password
      $bindResult = $this->connection['user']->bind( $arrUserEntry[0]['dn'], $inRequest->password );


      if( $bindResult ) {

        $intSid = $this->createSessionID();


        // Try to create the session in the rbac-system
        $creationResult = $this->rbac->createSession( $inRequest->username, Array(), $intSid );
        if( $creationResult == $this->config->getValue( "errorCode", "OK" ) ) {

          $result->auth = $intSid;

        }
        else {

          $objFaultDetail->faultNo = $this->config->getValue( "errorCode", "AUTHENTICATION_ERROR" );
          $objFaultDetail->faultMessage = $this->config->getValue( "errorDescription", "AUTHENTICATION_ERROR" );
          $objFaultDetail->cause = "Unable to create new session. This should not have happened.";


          return new SoapFault( "authenticationFault",
                                $this->config->getValue( "errorCode", "AUTHENTICATION_ERROR" ),
                                get_class( $this ),
                                $objFaultDetail );

        }

      }
      else {

        $objFaultDetail->faultNo = $this->config->getValue( "errorCode", "AUTHENTICATION_ERROR" );
        $objFaultDetail->faultMessage = $this->config->getValue( "errorDescription", "AUTHENTICATION_ERROR" );
        $objFaultDetail->cause = "Wrong username or password.";


        return new SoapFault( "authenticationFault",
                              $this->config->getValue( "errorCode", "AUTHENTICATION_ERROR" ),
                              get_class( $this ),
                              $objFaultDetail );

      }

    }
    else {

      $objFaultDetail->faultNo = $this->config->getValue( "errorCode", "AUTHENTICATION_ERROR" );
      $objFaultDetail->faultMessage = $this->config->getValue( "errorDescription", "UNKNOWN_USER" );
      $objFaultDetail->cause = "Wrong username or password.";


      return new SoapFault( "authenticationFault",
                            $this->config->getValue( "errorCode", "UNKNOWN_USER" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "UNKNOWN_USER" ) );

    }


    return $result;

  }


  // -----------------------------------------------------
  // Function: userExists
  // Input: auth / xsd:string
  //        log / xsd:string
  //        username / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Checks if a given user exists in the LDAP directory.
  // -----------------------------------------------------
  function userExists( $inRequest ) {

    $result = new booleanResponse();  // The result


    $filter  = "(&" . $this->config->getValue( "authentication", "filter" );
    $filter .= "(" . $this->config->getValue( "authentication", "namingattribute" ) . "=" . $inRequest->username . "))";


    // Search for the users entry
    $arrUserEntry = $this->connection['user']->search( $this->config->getValue( "authentication", "base" ), $filter, "sub",
                                                       Array( $this->config->getValue( "authentication", "namingattribute" ) ) );


    if(    isset( $arrUserEntry[0] )
        && isset( $arrUserEntry[0]['dn'] ) ) {

      $result->result = true;

    }
    else {

      $result->result = false;

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: tgCheckAccess
  // Input: log / xsd:string
  //        session / xsd:string
  //        operation / xsd:string
  //        resource / xsd:string
  // Output: sid / xsd:string
  // Description
  //   Tries to authenticate the user. If this is
  //   successful a session-ID is generated and a
  //   session is startet.
  // -----------------------------------------------------
  public function tgCheckAccess( $inRequest ) {

    $result = new booleanResponse();
    $objFaultDetail = new stdClass();


    try {

      $this->rbac->roleOperationsOnObject( "TextGrid-Admin", $inRequest->resource );

    }
    catch( Exception $e ) {

      $objFaultDetail->faultNo = $this->config->getValue( "errorCode", "RESOURCENOTFOUND_ERROR" );
      $objFaultDetail->faultMessage = $this->config->getValue( "errorDescription", "RESOURCENOTFOUND_ERROR" );
      $objFaultDetail->cause = "Object does not exist";


      return new SoapFault( "unknownResourceFault",
                            $this->config->getValue( "errorDescription", "RESOURCENOTFOUND_ERROR" ),
                            get_class( $this ),
                            $objFaultDetail );

    }

    $result->result = $this->rbac->checkAccess( $inRequest->auth, $inRequest->operation, $inRequest->resource );


    return $result;

  }




  // -----------------------------------------------------
  // Function: getSid
  // Input: none
  // Output: sid / xsd:string
  // Description
  //   Creates a new session-ID.
  // -----------------------------------------------------
  public function getSid() {

    $result = new getSidResponse();


    $result->sid = $this->createSessionID();


    return $result;

  }


  // -----------------------------------------------------
  // Function: getSupportedUserAttributes
  // Input: none
  // Output: attribute[] / tns:userAttribute
  // Description
  //   describe User Attributes this RBAC understands
  // -----------------------------------------------------
  public function getSupportedUserAttributes() {

    $result = new stdClass();

    $attrlist = explode ( " ", $this->config->getValue( "userdetails", "00order" ));
    foreach ($attrlist as $attr) {
      $det = explode ("|", $this->config->getValue( "userdetails", $attr ));
      $result->attribute[] = new userAttribute(
				$attr, // name
				null,  // no value here
				$det[4], // description
				$det[0] ==="mandatory"? TRUE:FALSE ,
				$det[1], // ldapname (attrNameInLDAP)
				$det[2], // inclass (schemaNameInLDAP)
				$det[3] // displayname
				);
    }
    return $result;

  }

  // -----------------------------------------------------
  // Function: getMyUserAttributes
  // Input: auth / xsd:string
  // Output: attribute[] / tns:userAttribute
  // Description
  //   get User Attributes
  // -----------------------------------------------------
  public function getMyUserAttributes($inRequest) {

    $result = new stdClass();

    // Search for Session entry
    $filter = "(" . $this->rbac->getConfiguration()->getValue( "session", "namingattribute" ) .
      "=" . $inRequest->auth . ")";
    $arrSessionEntry = $this->connection['session']->search(
           $this->rbac->getConfiguration()->getValue( "session", "base" ),
           $filter,        "sub",           Array( "rbacSessionUser" ) );

    // retrieve rbacSessionUser attribute (ePPN)
    if(    isset( $arrSessionEntry[0] ) && isset( $arrSessionEntry[0]['dn'] ) ) {
      $eppn = $arrSessionEntry[0]['rbacsessionuser'][0];
    } else {
      // echo ("Could not find Session entry");
      return new SoapFault( "authenticationFault",
                            $this->config->getValue( "errorCode", "AUTHENTICATION_ERROR" ),
                            get_class( $this ),
                            $this->config->getValue( "errorDescription", "AUTHENTICATION_ERROR" ) );

    }

    // Search for user entry
    $filter = "(&" . $this->config->getValue( "authentication", "filter" );
    $filter .= "(" . $this->config->getValue( "authentication", "namingattribute" ) . "=" . $eppn . "))";

    $arrUserEntry = $this->connection['user']->search(
       $this->config->getValue( "authentication", "base" ), $filter, "sub", Array( ) );

    // Retrieve attributes
    $entry = $arrUserEntry[0];

    $attrlist = explode ( " ", $this->config->getValue( "userdetails", "00order" ));

    foreach ($attrlist as $attr) {

      $det = explode ("|", $this->config->getValue( "userdetails", $attr ));

      $result->attribute[] = new userAttribute(
				$attr, // name
				$entry[strtolower($det[1])][0], // value
				$det[4], // description
				$det[0] ==="mandatory"? TRUE:FALSE ,
				null, // do not need this in the client ldapname (attrNameInLDAP)
				null, // do not need this in the client inclass (schemaNameInLDAP)
				$det[3] // displayname
				);

    }

    return $result;

  }


  // -----------------------------------------------------
  // Function: setMyUserAttributes
  // Input: auth / xsd:string
  // Input: webAuthSecret / xsd:string
  // Input: attribute[] / tns:userAttribute
  // Output: result / xsd:boolean
  // Description
  //   set User Attributes
  // -----------------------------------------------------
  public function setMyUserAttributes($inRequest) {

    $arrModify = Array();
    $filter = "";
    $result = new booleanResponse();  // The return-result

    $ePPN = $this->rbac->sessionUser( $inRequest->auth );

    $filter = "(" . $this->config->getValue( "user", "namingattribute" ) . "=" . $ePPN . ")";

    $arrUserEntry = $this->connection['user']->search( $this->config->getValue( "user", "base" ), $filter, "sub" );

    $entry = $arrUserEntry[0];

    if( $arrUserEntry && sizeof( $arrUserEntry ) == 1 && isset( $arrUserEntry[0]) &&  isset( $arrUserEntry[0]['dn']) ) {

      $attrlist = explode ( " ", $this->config->getValue( "userdetails", "00order" ));

      $existing = Array();
      $objectclasses = Array();
      $ldapattr = Array();
      foreach ($attrlist as $attr) {
        $det = explode ("|", $this->config->getValue( "userdetails", $attr ));
        $existing[$attr] = $entry[strtolower($det[1])][0];
        $ldapattr[$attr] = $det[1];
        $objectclasses[] = $det[2];
      }
      $objectclasses = array_unique( $objectclasses);
      $notyetadded = array_diff($objectclasses, $entry['objectclass']);

      if (count($notyetadded) > 0) {
        $arrModify['objectclass'] = array_values($notyetadded);
        ldap_mod_add ($this->connection['user']->getConnection(), $entry['dn'], $arrModify);
        unset ( $arrModify );
      }

      $provided = Array();
      foreach ($inRequest->attribute as $prov) {
	if (!in_array($prov->name, $attrlist)) {
	  trigger_error("Attribute '".$prov->name."' provided with value '".$prov->value."', however, this RBAC instance cannot understand it.", E_USER_WARNING);
        } else {
          $provided[$prov->name] = $prov->value;
        }
      }

      // only assert that these data are correct if they came from the IdP AND the IdP had sent at least the name (surname and givenname) and one mail address
      if( $inRequest->webAuthSecret === $this->config->getValue( "webAuth", "secret" )
            && strlen($provided['surname']) > 0
            && strlen($provided['givenname']) > 0
            && strlen ($provided['mail']) > 0  ) {
        $arrModify['tgusersupplieddata'][] = "FALSE";

	// only set the agreesearch flag automatically if it was not there before, i.e. on very first login
	//        if (! isset ($entry['tgagreesearch'] )) {
	//          $arrModify['tgagreesearch'][] = "TRUE";
	//        }
//      } elseif (isset ($entry['tgusersupplieddata']) && $entry['tgusersupplieddata'][0] === "FALSE") {
//        // once data came from the IdP, the flag will always remain on FALSE and only the agreesearch Flag can be set
//        if ( $inRequest->agreeSearch ) { $arrModify['tgagreesearch'][] = "TRUE"; } else { $arrModify['tgagreesearch'][] = "FALSE"; }
//        $this->connection['user']->modify( $entry['dn'], $arrModify);
//        $result->result = true;
//        return $result;

      } else  {
	// we cannot check everything, but when user changes their mail address, then we do not have a safe user handle anymore
	if (! ($provided['mail'] === $existing['mail'])) {
	  $arrModify['tgusersupplieddata'][] = "TRUE";
        }
//	// only set the agreesearch flag if it came from the user
//	if ( $inRequest->agreeSearch ) { $arrModify['tgagreesearch'][] = "TRUE"; } else { $arrModify['tgagreesearch'][] = "FALSE"; }
      }

      foreach ($provided as $attr => $value) {
	if (is_string($value) && strlen($value) > 0 ) {
          $arrModify[$ldapattr[$attr]][] = $value;
	} else if (is_bool($value)) {  // this will never hold as we only have strings in the WSDL
	  if ($value) {
            $arrModify[$ldapattr[$attr]][] = "TRUE";
          } else {
            $arrModify[$ldapattr[$attr]][] = "FALSE";
          }
  	}
      }

      $this->connection['user']->modify( $entry['dn'], $arrModify);

      $result->result = true;

    } else {
      // no unique user found
      	$result->result = false;
    }

    return $result;

  }





  // -----------------------------------------------------
  // Function: filterBySid
  // Input: auth / xsd:string
  //        log / xsd:string
  //        sid / xsd:string
  //        resource / xsd:string
  //        operation / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Tries to authorize the user. If this is
  //   successful the function filters all the ressources
  //   given by checking if the sid has appropriate access.
  // -----------------------------------------------------
  function filterBySid( $inRequest ) {

    $filterBySidResult = Array();    // The resources that pass the filter
    $arrRole = Array();
    $arrResource = Array();
    $arrFilter = Array();
    $arrHandle = Array();
    $result = new filterResponse();  // The result


    $res = $inRequest->resource;
    // Make sure it is an array
    if( !is_array( $res ) ) {

      $res = Array( $res );

    }


    // If we do not free some memory we might
    // consume enormous resources
    unset( $inResuest->resource );


    try {

      // Get all roles activated in the session
      // (internal representation)
      $arrRole = $this->rbac->sessionRoles( $inRequest->auth, false );


    }
    catch( Exception $e ) {

      $arrRole[] = "_NO_ROLE_GIVEN_";

    }


    $arrResTmp = Array();

    for( $i = 0; $i < sizeof( $res ); $i++ ) {

      $arrResTmp[] = $res[$i];


      if( ($i > 0) && ($i % 400 === 0) ) {

        $arrResource[] = $arrResTmp;
        $arrResTmp = Array();

      }

    }


    $arrResource[] = $arrResTmp;
    unset( $res );


    for( $loParSearch = 0; $loParSearch < sizeof( $arrResource ); $loParSearch++ ) {

      // Construct a filter with all resources and
      // all roles/permissions
      $filter  = "(&(objectClass=rbacResource)(|";


      for( $i = 0; $i < sizeof( $arrResource[$loParSearch] ); $i++ ) {

        $filter .= "(" . $this->rbac->getConfiguration()->getValue( "resource", "aliasattribute" ) . "=" . $arrResource[$loParSearch][$i] . ")";

      }


      $filter .= ")(|";


      for( $i = 0; $i < sizeof( $arrRole ); $i++ ) {

        $filter .= "(rbacPermission=" . $arrRole[$i] . ":-:" . $inRequest->operation . ")";

      }


      $filter .= "(TgIsPublic=TRUE)(TgIsProjectFile=TRUE)))";


      $arrFilter[] = $filter;
      $arrHandle[] = $this->connection['resource']->getConnection();

    }


    // If we do not free some memory we might
    // consume enormous resources
    unset( $filter );
    unset( $arrRole );
    unset( $arrResource );


//    $arrResult = $this->connection['resource']->search( $this->rbac->getConfiguration()->getValue( "resource", "base" ),
//                                                        $filter, "sub", Array( "tgResourceURI" ) );

    $arrParSearchResult = ldap_search( $arrHandle, $this->rbac->getConfiguration()->getValue( "resource", "base" ), $arrFilter, Array( "tgResourceURI" ) );


    unset( $arrFilter );


    if( !is_array( $arrParSearchResult ) ) {

      $arrParSearchResult = Array( $arrParSearchResult );

    }


    for( $loParSearch = 0; $loParSearch < sizeof( $arrParSearchResult ); $loParSearch++ ) {

      $arrResult = ldap_get_entries( $arrHandle[$loParSearch], $arrParSearchResult[$loParSearch] );


      for( $i = 0; $i < $arrResult['count']; $i++ ) {

        $result->resource[] = $arrResult[$i]['tgresourceuri'][0];

      }

    }


    return $result;

  }



// this does not activate the role in the user's session(s), see below for revised addMember function
//  // -----------------------------------------------------
//  // Function: addMember
//  // Input: auth / xsd:string
//  //        log / xsd:string
//  //        username / xsd:string
//  //        role / xsd:string
//  // Output: result / xsd:boolean
//  // Description
//  //   Tries to add a user to a project. This is only possible
//  //   if the user exists and the session has the permission
//  //   "delegate" on the project.
//  // -----------------------------------------------------
//  public function addMember( $inRequest ) {
//
//    $arrSplit = Array();
//    $project = false;
//    $result = new booleanResponse();  // The return-result
//
//
//    // Extract the project from the role
//    $arrSplit = preg_split( "/[,]/", $inRequest->role );
//
//    for( $i = 0; $i < sizeof( $arrSplit ); $i++ ) {
//
//      if( preg_match( "/^tgpr[0-9]+$/i", $arrSplit[$i] ) ) {
//
//        $project = $arrSplit[$i];
//
//      }
//
//    }
//
//
//    if(    $project
//        && $this->rbac->checkAccess( $inRequest->auth, "delegate", $project ) ) {
//
//      try {
//
//        if( $this->rbac->assignUser( $inRequest->username, $inRequest->role ) ) {
//
//
//          $result->result = true;
//
//        }
//        else {
//
//          $result->result = false;
//
//        }
//
//      }
//      catch( RBACException $e ) {
//
//        return new SoapFault( "rbacFault", $e->getCode(), get_class( $this->rbac ), $e->getMessage() );
//
//      }
//
//    }
//    else {
//
//      $result->result = false;
//
//    }
//
//
//    return $result;
//
//  }




  // -----------------------------------------------------
  // Function: tgGrantPermission
  // Input: auth / xsd:string
  //        log / xsd:string
  //        role / xsd:string
  //        resource / xsd:string
  //        operation / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Grants a permission to a resource if the user
  //   given by the auth parameter has the right to
  //   do this.
  // -----------------------------------------------------
  public function tgGrantPermission( $inRequest ) {

    $result = new booleanResponse();
    $objFaultDetail = new stdClass();


    try {

      if( $this->rbac->checkAccess( $inRequest->auth, "delegate", $inRequest->resource ) ) {

        $result->result = $this->rbac->grantPermission( $inRequest->resource, $inRequest->operation, $inRequest->role );

      }
      else {

        $objFaultDetail->faultNo = $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" );
        $objFaultDetail->faultMessage = $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" );
        $objFaultDetail->cause = "You do not have the permission to change the access to the resource " . $inRequest->resource . ".";


        return new SoapFault( "authenticationFault",
                               $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                               get_class( $this ),
                               $objFaultDetail );

      }

    }
    catch( RBACException $e ) {

      $objFaultDetail->faultNo = $e->getCode();
      $objFaultDetail->faultMessage = $e->getMessage();
      $objFaultDetail->cause = "There was an error while connecting to the underlying RBAC database. This should not have happend!";


      return new SoapFault( "rbacFault", $e->getCode(), get_class( $this->rbac ), $objFaultDetail );

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: tgRevokePermission
  // Input: auth / xsd:string
  //        log / xsd:string
  //        role / xsd:string
  //        resource / xsd:string
  //        operation / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Revokes a permission for a resource if the user
  //   given by the auth parameter has the right to
  //   do this.
  // -----------------------------------------------------
  public function tgRevokePermission( $inRequest ) {

    $result = new booleanResponse();
    $objFaultDetail = new stdClass();


    try {

      if( $this->rbac->checkAccess( $inRequest->auth, "delegate", $inRequest->resource ) ) {

        $result->result = $this->rbac->revokePermission( $inRequest->operation, $inRequest->resource, $inRequest->role );

      }
      else {

        $objFaultDetail->faultNo = $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" );
        $objFaultDetail->faultMessage = $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" );
        $objFaultDetail->cause = "You do not have the permission to change the access to the resource " . $inRequest->resource . ".";


        return new SoapFault( "authenticationFault",
                               $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                               get_class( $this ),
                               $objFaultDetail );

      }

    }
    catch( RBACException $e ) {

      $objFaultDetail->faultNo = $e->getCode();
      $objFaultDetail->faultMessage = $e->getMessage();
      $objFaultDetail->cause = "There was an error while connecting to the underlying RBAC database. This should not have happend!";


      return new SoapFault( "rbacFault", $e->getCode(), get_class( $this->rbac ), $objFaultDetail );

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: getObjects
  // Input: auth / xsd:string
  //        log / xsd:string
  //        project / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Returns a list of all resources that the user corresponding
  //   to the session-ID (auth) may read.
  // -----------------------------------------------------
  public function getObjects( $inRequest ) {

    $result = new resourcesetResponse();  // The return-result
    $filter = "";                         // RBAC-filter
    $arrResource = Array();               // Resoult of the RBAC-search
    $arrSessionRole = Array();            // The active roles of the session
    $i = 0;                               // Loop

    if( preg_match( "/.+/", $inRequest->auth ) ) {

      $arrSessionRole = $this->rbac->sessionRoles( $inRequest->auth, false );

    }


    $filter  = "(&" . $this->rbac->getConfiguration()->getValue( "resource", "filter" );
    $filter .= "(tgprojectid=" . $inRequest->project . ")";
    $filter .= "(|(tgispublic=TRUE)(tgisprojectfile=TRUE)";

    for( $i = 0; $i < sizeof( $arrSessionRole ); $i++ ) {

      $filter .= "(rbacPermission=" . $arrSessionRole[$i] . ":-:read)";

    }

    $filter .= "))";


    // If we do not free some memory we might
    // consume enormous resources
    unset( $arrSessionRole );


    $arrResource = $this->connection['resource']->search( $this->rbac->getConfiguration()->getValue( "resource", "base" ),
                                                          $filter, "sub",
                                                          Array( $this->rbac->getConfiguration()->getValue( "resource", "aliasattribute" ) ) );

    for( $i = 0; $i < sizeof( $arrResource ); $i++ ) {

      $result->resource[] = $arrResource[$i][$this->rbac->getConfiguration()->getValue( "resource", "aliasattribute" )][0];

    }


    if( !$arrResource || sizeof( $arrResource ) == 0 ) {

      unset( $result->resource );

    }

    return $result;

  }




  // -----------------------------------------------------
  // Function: isPublic
  // Input: auth / xsd:string
  //        log / xsd:string
  //        resource / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Returns true if the resource is public. In every
  //   other case there will be returned false.
  // -----------------------------------------------------
  public function isPublic( $inRequest ) {

    $result = new booleanResponse();
    $filter = "";
    $arrResource = Array();


    // Create a filter that searches for the
    // given resource.
    $filter  = "(&" . $this->rbac->getConfiguration()->getValue( "resource", "filter" );
    $filter .= "(|(" . $this->rbac->getConfiguration()->getValue( "resource", "namingattribute" ) . "=" . $inRequest->resource . ")";
    $filter .= "  (" . $this->rbac->getConfiguration()->getValue( "resource", "aliasattribute" ) . "=" . $inRequest->resource . ")))";


    $arrResource = $this->connection['resource']->search( $this->rbac->getConfiguration()->getValue( "resource", "base" ),
                                                          $filter, "sub",
                                                          Array( "tgispublic" ) );


    // If the attribute is not set or there was no
    // resource found, return false.
    if( !isset( $arrResource[0]['tgispublic'] ) ) {

      $result->result = false;

    }
    else {

      preg_match( "/^true$/i", $arrResource[0]['tgispublic'][0] ) ? $result->result = true : $result->result = false;

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: getOwner
  // Input: auth / xsd:string
  //        log / xsd:string
  //        resource / xsd:string
  // Output: result / xsd:string
  // Description
  //   Returns the owner of a resource. This owner has nothing
  //   to do with any permissions, it is just the owner.
  // -----------------------------------------------------
  public function getOwner( $inRequest ) {

    $result = new getOwnerResponse();  // The return-result
    $filter = "";                      // RBAC-filter
    $arrResource = Array();            // Resoult of the RBAC-search

    // Create a filter that searches for the
    // given resource.
    $filter  = "(&" . $this->rbac->getConfiguration()->getValue( "resource", "filter" );
    $filter .= "(|(" . $this->rbac->getConfiguration()->getValue( "resource", "namingattribute" ) . "=" . $inRequest->resource . ")";
    $filter .= "  (" . $this->rbac->getConfiguration()->getValue( "resource", "aliasattribute" ) . "=" . $inRequest->resource . ")))";


    if( $this->rbac->checkAccess( $inRequest->auth, "read", $inRequest->resource ) ) {

      $arrResource = $this->connection['resource']->search( $this->rbac->getConfiguration()->getValue( "resource", "base" ),
                                                            $filter, "sub",
                                                            Array( "tgresourceowner" ) );


      if($arrResource && sizeof( $arrResource ) == 1 ) {

        $result->owner = $arrResource[0]['tgresourceowner'][0];

      }

    }
    return $result;
  }








  // -----------------------------------------------------
  // Function: getMembers
  // Input: auth / xsd:string
  //        log / xsd:string
  //        project / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Returns a list of members in a project.
  // -----------------------------------------------------
  public function getMembers( $inRequest ) {
    $result = new usersetResponse();  // The return-result
    $arrMember = Array();             // Resoult of the RBAC-search
    
    $arrMember = $this->rbac->authorizedUsers( $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=" . $inRequest->project . ","
                                               . $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "="
                                               . $this->config->getValue( "project", "base" )
                                               . "," . $this->rbac->getConfiguration()->getValue( "role", "base" ) );
    // want to be case-independent
    $arrMember = array_map ( "mb_strtolower", $arrMember);
    $arrMember = $this->rbac->removeDuplicates($arrMember);


    // The user has to be in the project to be
    // allowed to display all other users
    if( in_array( mb_strtolower( $this->rbac->sessionUser( $inRequest->auth ) ), $arrMember ) ) {

      $result->username = $arrMember;

    }


    return $result;

  }


  // -----------------------------------------------------
  // Function: getUserRole
  // Input: auth / xsd:string
  //        log / xsd:string
  //        project / xsd:string
  // Output: UserRole[]: tns:UserRole
  // Description
  //   Returns members in a project with their roles.
  // -----------------------------------------------------
  public function getUserRole ( $inRequest ) {
    $result = new getUserRoleResponse(); // The return-result
    $arrMember = Array(); // Result of the RBAC-search

    $arrMember = $this->rbac->authorizedUsers(
        $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=" .
        $inRequest->project . "," .
        $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=" .
        $this->config->getValue( "project", "base" ) . "," .
        $this->rbac->getConfiguration()->getValue( "role", "base" )
    );


    $strFilter .= "(objectClass=rbacRole)";

    $arrRole = $this->rbac->getConnection( "role" )->search(
        "rbacname=" . $inRequest->project .
        ",rbacname=Projekt-Teilnehmer," .
        $this->rbac->getConfiguration()->getValue( "role", "base" ),
        $strFilter, "one" );
    if ($arrMember) {
    // want to be case-independent
    $arrMember = array_map ( "mb_strtolower", $arrMember);
    $arrMember = $this->rbac->removeDuplicates($arrMember);
    
    // The user has to be in the project to be allowed to see other's roles
    if( in_array( mb_strtolower( $this->rbac->sessionUser( $inRequest->auth ) ), $arrMember ) ) {
          $res = array();
	  if ($arrRole) {
            // Loop all array members (project members).
	    for( $i = 0; $i < sizeof ( $arrMember ) ; $i++ ) {

            // Loop all roles of each array member, construct response string.
            $roles = array();
	      for( $j = 0; $j < sizeof ( $arrRole ); $j++) {
	      	  
	        if (isset($arrRole[$j]["rbacperformer"]) && in_array($arrMember[$i], array_map ("mb_strtolower", $arrRole[$j]["rbacperformer"]))) {
	 	      array_push($roles,  $arrRole[$j]["rbacname"][0]);
              }
	      
            }

            // Set response item.
            $res[$i] = new userRole ( $arrMember[$i], $roles );
   	    }
          }	  
      $result->userRole = $res;
    }
  }
    return $result;
  }

  // -----------------------------------------------------
  // Function: getFriends
  // Input: auth / xsd:string
  //        log / xsd:string
  // Output: friend[] / tns:friend
  // Description
  //   Returns a list of operations allowed by the user
  //   on a specific resource.
  // -----------------------------------------------------
  public function getFriends( $inRequest ) {
    $result = new getFriendsResponse();
    $res = array();

    $sandboxstring = $this->config->getValue( "project", "sandBoxProjects" );
    $sandboxarr = preg_split ("/;/", $sandboxstring);

    $username = mb_strtolower ($this->rbac->sessionUser( $inRequest->auth ) );

    $strFilter .= "(&(objectClass=rbacRole)(rbacPerformer=". $username . "))";

    $arrRole = $this->rbac->getConnection( "role" )->search( $this->rbac->getConfiguration()->getValue( "role", "base" ), $strFilter, "sub" );

    $userscores = array();

    for( $i = 0; $i < sizeof( $arrRole ); $i++ ) {
/* Only old-style project IDs can be matched this way
      $m = preg_match ( '/(TGPR\d+)/', $arrRole[$i]['dn'], $matches );
*/
      $m = preg_match ( '/(TGPR.+)/', $arrRole[$i]['dn'], $matches );
      if ($m == 1) {
        $tgpr = $matches[1];
      } else {
        $tgpr = "XXXXX";
      }
      if (in_array ($tgpr, $sandboxarr)) {
	continue;
      }
      $users = $arrRole[$i]['rbacperformer'];

      for ($j=0; $j<sizeof($users); $j++) {
        $onefriend = mb_strtolower ($users[$j]);
        if ( $username == $onefriend ) {
          continue;
        }
        if (isset($userscores[$onefriend])) {
          $userscores[$onefriend] += 1;
	} else {
          $userscores[$onefriend] = 1;
        }
      }
    }

    foreach ($userscores as $name => $score) {
	$res[] = new friend ($name, $score);
    }


    $result->friends = $res;
    return $result;
  }


  // -----------------------------------------------------
  // Function: getRights
  // Input: auth / xsd:string
  //        log / xsd:string
  //        resource / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Returns a list of operations allowed by the user
  //   on a specific resource.
  // -----------------------------------------------------
  public function getRights( $inRequest ) {

    $arrOperation = Array();
    $result = new operationsetResponse();  // The return-result
    $objFaultDetail = new stdClass();


    if( preg_match( "/.+/", $inRequest->username ) ) {

      if( $this->rbac->checkAccess( $inRequest->auth, "delegate", $inRequest->resource ) ) {

        $arrOperation = $this->rbac->userOperationsOnObject( $inRequest->username, $inRequest->resource );

      }
      else {

        $objFaultDetail->faultNo = $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" );
        $objFaultDetail->faultMessage = $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" );
        $objFaultDetail->cause = "You do not have the permission to see the rights of the resource " . $inRequest->resource . ".";


        return new SoapFault( "authenticationFault",
                               $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                               get_class( $this ),
                               $objFaultDetail );

      }

    }
    else {

      $arrOperation = $this->rbac->userOperationsOnObject( $this->rbac->sessionUser( $inRequest->auth ), $inRequest->resource );

    }

    if( is_array( $arrOperation ) ) {

      $result->operation = $arrOperation;

    }
    else {
      $result->operation = Array();
    }

    return $result;

  }





  // -----------------------------------------------------
  // Function: getProjectDescription
  // Input: auth / xsd:string
  //        log / xsd:string
  //        project / xsd:string
  // Output: result / xsd:string
  // Description
  //   Returns the description of a project
  // -----------------------------------------------------
  public function getProjectDescription( $inRequest ) {

    $result = new getProjectDescriptionResponse();
    $filter = "";


    $filter  = "(&" . $this->rbac->getConfiguration()->getValue( "role", "filter" );
    $filter .= "(" . $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=" . $inRequest->project . "))";


    $arrProject = $this->connection['role']->getEntry( $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "="
                                                       . $inRequest->project . ","
                                                       . $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "="
                                                       . $this->config->getValue( "project", "base" )
                                                       . "," . $this->rbac->getConfiguration()->getValue( "role", "base" ) );


    if( isset( $arrProject['dn'] ) ) {

      $result->project = new stdClass();
      $result->project->description = $arrProject['tgprojectdescription'][0];
      $result->project->name = $arrProject['tgprojectname'][0];
      $result->project->id = $arrProject['tgprojectid'][0];
      $result->project->file = $arrProject['tgprojectfile'][0];

    }
    else {

      $result->project->description = "Not available";

    }


    return $result;

  }


  // -----------------------------------------------------
  // Function: getNames
  // Input: auth / xsd:string
  //        log / xsd:string
  //        ePPN[] / xsd:string
  // Output: userdetail[] / tns:userDetail
  // Description
  //   returns full user records for the requested ePPNs
  // -----------------------------------------------------
  public function getNames( $inRequest ) {
    // this will check the SID, throws an exception when SID does not exist
    $this->rbac->sessionUser( $inRequest->auth );

    $result = array();

    if (!is_array ($inRequest->ePPN)) {
	  $inRequest->ePPN = array ($inRequest->ePPN);
    }

    for ($i=0; $i < sizeof ($inRequest->ePPN); $i++) {
      $filter = "(" . $this->config->getValue( "authentication", "namingattribute" ) . "=" . $inRequest->ePPN[$i] . ")";
      $arrUserEntry = $this->connection['user']->search( $this->config->getValue( "authentication", "base" ), $filter, "sub" );

      if( $arrUserEntry && sizeof( $arrUserEntry ) == 1 && isset( $arrUserEntry[0]) &&  isset( $arrUserEntry[0]['dn']) ) {

        $entry = $arrUserEntry[0];
        $namingattr = mb_strtolower( $this->config->getValue( "authentication", "namingattribute" ) );
        $eppn = mb_strtolower( $entry[$namingattr][0] );

        $displayName = isset( $entry['cn'][0] ) ? $entry['cn'][0] : null;
  	    $name = isset( $entry['sn'][0] ) ? $entry['sn'][0] : null;
        $givenName = isset( $entry['givenName'][0] ) ? $entry['givenName'][0] : null;
  	    $altName = isset ( $givenName ) ? $givenName . " " : "";
        $altName = $altName . ( isset ( $name ) ? $name : "" );

        $result[] = new userDetail (
          $eppn,
          // displayName, alternatively givenName and name!
          isset ( $displayName ) ? $displayName : $altName,
          isset ( $entry['mail'][0] ) ? $entry['mail'][0] : null,
          // Organization
          isset ( $entry['o'][0] ) ? $entry['o'][0] : null,
          isset ( $entry['dariahissearchable'][0] ) ? ($entry['dariahissearchable'][0] === "TRUE" ? TRUE : FALSE) : null,
          null
        );
      } else {
	    // do nothing, this ePPN was not existing or not unique
      }
    }

    return $result;
  }


  // -----------------------------------------------------
  // Function: getIDs
  // Input: auth / xsd:string
  //        log / xsd:string
  //        name / xsd:string
  //        mail / xsd:string
  //        organisation / xsd:string
  // Output: userdetail[] / tns:userDetail
  // Description
  // Searches for name (alternatively mail or organisation) and returns matching users.
  // -----------------------------------------------------
  public function getIDs( $inRequest ) {

    // this will check the SID, throws an exception when SID does not exist
    $this->rbac->sessionUser( $inRequest->auth );

    $result = array();

    // keep from returning ALL users ;-)
    if (strlen ($inRequest->name) == 0 && strlen ($inRequest->mail) == 0 && strlen ($inRequest->organisation) == 0) {
       return result;
    }

    $filter = "(|(&";

    // only find those resources we really look for
    $filter .= $this->config->getValue( "authentication", "filter" );

    if (strlen ($inRequest->name) > 0) {
      // search also in sn or givenName if they are fully specified
      $filter .= "(|(displayname=" . $inRequest->name . ")(sn=" . $inRequest->name . ")(givenname=". $inRequest->name . "))";
    }
    if	(strlen	($inRequest->mail) > 0) {
      $filter .= "(mail=" . $inRequest->mail . ")";
    }
    if (strlen ($inRequest->organisation) > 0) {
      $filter .= "(o=" . $inRequest->organisation . ")";
    }
    $filter .= "(!(dariahissearchable=FALSE)))";

    // overload the name field for ePPN search in case someone has dariahissearchable=FALSE but client specified a UID completely
    $namingattr = mb_strtolower ($this->config->getValue( "authentication", "namingattribute" ));
    if ( strlen ($inRequest->name) > 0 && ! preg_match( "/\*/", $inRequest->name) ) {
      $filter .= "(". $namingattr . "=" . $inRequest->name . ")";
    }
    $filter .= ")";



    $arrUserEntry = $this->connection['user']->search( $this->config->getValue( "authentication", "base" ), $filter, "sub" );

    for ($i = 0; $i < sizeof( $arrUserEntry ); $i++) {
      if( isset( $arrUserEntry[$i]) &&  isset( $arrUserEntry[$i]['dn']) ) {
        $entry = $arrUserEntry[$i];

	$namingattr = mb_strtolower ($this->config->getValue( "authentication", "namingattribute" ));

        $result[] = new userDetail (
				     mb_strtolower ($entry[$namingattr][0]), // ePPN
				     isset ($entry['cn'][0]) ? $entry['cn'][0] : $entry[$namingattr][0],  // name, alternatively the ePPN residing in cn
				     isset ($entry['mail'][0]) ? $entry['mail'][0] : null,
				     isset ($entry['o'][0]) ? $entry['o'][0]: null,  // organisation
				     isset ($entry['dariahissearchable'][0]) ? ($entry['dariahissearchable'][0] === "TRUE" ? TRUE : FALSE) : null,  // bool
				     null   // bool
				   );

      } else {
	// do nothing, this entry was strange
      }
    }

    return $result;
  }


  // -----------------------------------------------------
  // Function: setName ... deprecated, see setMyUserAttributes
  // Input: auth / xsd:string
  //        log / xsd:string
  //        webAuthSecret / xsd:string
  //        name / xsd:string
  //        mail / xsd:string
  //        organisation / xsd:string
  //        agreeSearch / xsd:boolean
  // Output: result / xsd:boolean
  // Description
  //   Sets userdetails, either by Webauth or by a call from the Lab
  // -----------------------------------------------------
  public function setName( $inRequest ) {

    $arrModify = Array();
    $filter = "";
    $result = new booleanResponse();  // The return-result

    $ePPN = $this->rbac->sessionUser( $inRequest->auth );

    $filter = "(" . $this->config->getValue( "authentication", "namingattribute" ) . "=" . $ePPN . ")";

    $arrUserEntry = $this->connection['user']->search( $this->config->getValue( "authentication", "base" ), $filter, "sub" );

    if( $arrUserEntry && sizeof( $arrUserEntry ) == 1 && isset( $arrUserEntry[0]) &&  isset( $arrUserEntry[0]['dn']) ) {

      $entry = $arrUserEntry[0];

      if (!in_array ("TextGridUser", $entry['objectclass'])) {
        $arrModify['objectclass'][] = "TextGridUser";
        ldap_mod_add ($this->connection['user']->getConnection(), $entry['dn'], $arrModify);
        unset ( $arrModify );
      }


      // only assert that these data are correct if they came from the IdP AND the IdP had sent at least the name (cn or (sn and givenname)) and one mail address
      if( $inRequest->webAuthSecret === $this->config->getValue( "webAuth", "secret" ) && strlen($inRequest->name) > 0 && strlen ($inRequest->mail) > 0  ) {
        $arrModify['tgusersupplieddata'][] = "FALSE";

	// only set the agreesearch flag automatically if it was not there before, i.e. on very first login
        if (! isset ($entry['dariahissearchable'] )) {
          $arrModify['dariahissearchable'][] = "TRUE";
        }
      } elseif (isset ($entry['tgusersupplieddata']) && $entry['tgusersupplieddata'][0] === "FALSE") {

        // once data came from the IdP, the flag will always remain on FALSE and only the agreesearch Flag can be set
        if ( $inRequest->agreeSearch ) { $arrModify['dariahissearchable'][] = "TRUE"; } else { $arrModify['dariahissearchable'][] = "FALSE"; }
        $this->connection['user']->modify( $entry['dn'], $arrModify);
        $result->result = true;
        return $result;

      } else  {
	$arrModify['tgusersupplieddata'][] = "TRUE";

	// only set the agreesearch flag if it came from the user
	if ( $inRequest->agreeSearch ) { $arrModify['dariahissearchable'][] = "TRUE"; } else { $arrModify['dariahissearchable'][] = "FALSE"; }
      }

      if ( strlen($inRequest->name) > 0 ) { $arrModify['cn'][] = $inRequest->name; }
      if ( strlen($inRequest->organisation) > 0 ) { $arrModify['o'][] = $inRequest->organisation; }
      if ( strlen($inRequest->mail) > 0 ) { $arrModify['mail'][] = $inRequest->mail; }

      $this->connection['user']->modify( $entry['dn'], $arrModify);

      $result->result = true;

    } else {
      // no unique user found
      	$result->result = false;
    }

    return $result;
  }

  // -----------------------------------------------------
  // Function: setProjectFile
  // Input: auth / xsd:string
  //        log / xsd:string
  //        project / xsd:string
  //        file / xsd:string
  // Output: result / xsd:string
  // Description
  //   Sets the projectFile of a project
  // -----------------------------------------------------
  public function setProjectFile( $inRequest ) {

    $result = new booleanResponse();
    $objFaultDetail = new stdClass();

    $connection = $this->rbac->getConnection( "role" );

    if( $this->rbac->checkAccess( $inRequest->auth, "delegate", $inRequest->project ) ) {

      $projectDn  = $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=" . $inRequest->project . ",";
      $projectDn .= $this->rbac->getConfiguration()->getValue( "project", "base" );

      if( preg_match( "/.+/", $inRequest->file ) )  {
	if ( $arrResource = $connection->search( $this->rbac->getConfiguration()->getValue( "resource", "base" ),
                                                      "(tgresourceuri=" . $inRequest->file . ")" ) ) {
          // register the projectfile
          $boResult = $connection->modify( $projectDn, Array( "tgprojectfile" => Array( $inRequest->file ) ) );

           if( $boResult ) {
             $this->modifyProjectFilePermissions( $arrResource[0]['dn'], $projectDn );
           }
	} else {
           $boResult = false;
        }
      } else { // empty file string: unregister

        $boResult = $connection->modify( $projectDn, Array( "tgprojectfile" => Array( ) ) );

      }

    }
    else { // not delegate

      $objFaultDetail->faultNo = $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" );
      $objFaultDetail->faultMessage = $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" );
      $objFaultDetail->cause = "You do not have the permission to set the project file for project " . $inRequest->project . ".";


      return new SoapFault( "authenticationFault",
                             $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                             get_class( $this ),
                             $objFaultDetail );

    }


    $result->result = $boResult;

    return $result;

  }



  // -----------------------------------------------------
  // Function: tgAssignedProjects
  // Input: auth / xsd:string
  //        log / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Returns a list of all projects of the user corresponding
  //   to the session-ID (auth).
  // -----------------------------------------------------
  public function tgAssignedProjects( $inRequest ) {

    $filter = "";
    $username = "";
    $arrFound = Array();
    $result = new rolesetResponse();  // The return-result
    $i = 0;                           // Loop
    $j = 0;                           // Loop


    // By default the result is an empty array
    $result->role = Array();


    // The user corresponding to the session
    $username = $this->rbac->sessionUser( $inRequest->auth );


    // Search all roles in which the user is performer.
    $filter  = "(&" . $this->rbac->getConfiguration()->getValue( "role", "filter" );
    $filter .= "(rbacPerformer=" . $username . "))";


    // Get all the roles of the user
    $arrRole = $this->rbac->assignedRoles( $username );


    for( $i = 0; $i < sizeof( $arrRole ); $i++ ) {

      $arrSplit = preg_split( "/[,]/", $arrRole[$i] );
      $projectBelong = "";


      $theRole = preg_replace( "/^" . $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=/i", "", $arrSplit[0] );


      for( $j = 0; $j < sizeof( $arrSplit ); $j++ ) {

/* Old-style project IDs
        if( preg_match( "/^TGPR[0-9]+$/i",
                        preg_replace( "/^" . $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=/i", "", $arrSplit[$j] ) ) ) {
*/
        if( preg_match( "/^TGPR.+$/i",
                        preg_replace( "/^" . $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=/i", "", $arrSplit[$j] ) ) ) {

          $projectBelong = preg_replace( "/^" . $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=/i", "", $arrSplit[$j] );

        }

      }


      if( preg_match( "/.+/", $projectBelong ) ) {
//          && !isset( $arrFound[mb_strtolower( $projectBelong )] ) ) {

        if( $inRequest->level == 4 ) {

          if( preg_match( "/^projektleiter$/i", $theRole ) ) {

// why strtolower?            $arrFound[mb_strtolower( $projectBelong )][] = $theRole;
            $arrFound[$projectBelong][] = $theRole;

          }

        }
        elseif( $inRequest->level == 3 ) {

          if( preg_match( "/^bearbeiter$/i", $theRole ) ) {

// why strtolower?             $arrFound[mb_strtolower( $projectBelong )][] = $theRole;
            $arrFound[ $projectBelong ][] = $theRole;

          }

        }
        elseif( $inRequest->level == 2 || $inRequest->level == 1 ) {

          if( preg_match( "/(^bearbeiter$)|(^beobachter$)/i", $theRole ) ) {

// why strtolower?             $arrFound[mb_strtolower( $projectBelong )][] = $theRole;
		$arrFound[ $projectBelong ][] = $theRole;

          }

        }
        else {

// why strtolower?	          $arrFound[mb_strtolower( $projectBelong )][] = $theRole;
          $arrFound[$projectBelong ][] = $theRole;

        }

      }

    }


    // If the level is given and equals 1 than
    // all projects that have public resources
    // are needed later.
    if( $inRequest->level == 1 ) {

      $filter  = "(&" . $this->rbac->getConfiguration()->getValue( "resource", "filter" );
      $filter .= "(TGisPublic=TRUE)";


      // Projects that are already in the array are
      // not of any interest.
      foreach( $arrFound as $key => $value ) {

        $filter .= "(!(TGProjectId=$key))";

      }


      $filter .= ")";



      // Get the resource
      $arrResource = $this->connection['resource']->search( $this->rbac->getConfiguration()->getValue( "resource", "base" ), $filter, "sub",
                                                            Array( "tgprojectid" ) );


      for( $i = 0; $i < sizeof( $arrResource ); $i++ ) {

// no strtolower anymore!
//        if( !isset( $arrFound[mb_strtolower( $arrResource[$i]['tgprojectid'][0] )] ) ) {
//
//         $arrFound[mb_strtolower( $arrResource[$i]['tgprojectid'][0] )][] = "public";
        if( !isset( $arrFound[ $arrResource[$i]['tgprojectid'][0] ] ) ) {

          $arrFound[ $arrResource[$i]['tgprojectid'][0] ][] = "public";

        }

      }

    }


    foreach( $arrFound as $key => $value ) {

// strtoupper seems to be not needed here
//      $result->role[] = mb_strtoupper( $key );
      $result->role[] = $key;

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: deleteMember
  // Input: auth / xsd:string
  //        log / xsd:string
  //        username / xsd:string
  //        role / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Tries to remove a user from a role. This is only possible
  //   if the user exists and the session has the permission
  //   "delegate" on the project.
  // -----------------------------------------------------
  public function deleteMember( $inRequest ) {

    $arrSplit = Array();
    $project = false;
    $result = new booleanResponse();  // The return-result


    // Get the username from the session.
    $strUsername = $this->rbac->sessionUser( $inRequest->auth );


    // Extract the project from the role
    $arrSplit = preg_split( "/[,]/", $inRequest->role );

    for( $i = 0; $i < sizeof( $arrSplit ); $i++ ) {

      if( preg_match( "/^TGPR/i", $arrSplit[$i] ) ) {

        $project = $arrSplit[$i];
      }
    }

    // Allow to remove a member from a role if the authenticated
    // user has the RBAC right to do so or if the user removes
    // itself.
    if(    $project
        && (    $this->rbac->checkAccess( $inRequest->auth, "delegate", $project )
             || $strUsername === $inRequest->username ) ) {

      if( $this->rbac->deassignUser( $inRequest->username, $inRequest->role ) ) {

        $result->result = true;

      }
      else {

        $result->result = false;

      }

    }
    else {

      $result->result = false;

    }


    return $result;

  }



  // -----------------------------------------------------
  // Function: getLeader
  // Input: log / xsd:string
  // Output: username[] / tns:xsd:string
  // Description
  //   Searches for the leader(s) of a project.
  // -----------------------------------------------------
  public function getLeader( $inRequest ) {

    $arrUser = Array();
    $result = new usersetResponse();


    $result->username = $this->rbac->assignedUsers( "Projektleiter," . $inRequest->project . ","
                                                    . $this->config->getValue( "project", "base" ) );

    return $result;

  }




  // -----------------------------------------------------
  // Function: getAllProjects
  // Input: log / xsd:string
  // Output: project[] / tns:projectInfo
  // Description
  //   Searches for all projects and returns them as a list.
  // -----------------------------------------------------
  public function getAllProjects( $inRequest ) {

    $arrProject = Array();                    // All project-entries found
    $connection = false;                    // The connection for roles
    $result = new getAllProjectsResponse();  // Return


    // The role-connection is needed because projects
    // are representated as roles.
    $connection = $this->rbac->getConnection( "resource" );


    // Search the directory
    $arrProject = $connection->search( $this->rbac->getConfiguration()->getValue( "project", "base" ),
                                       "(&(objectClass=rbacRole)(!(tgprojectdeactivated=TRUE)))",
                                       "one", $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) );


    for( $i = 0; $i < sizeof( $arrProject ); $i++ ) {

      $result->project[] = new ProjectInfo( $arrProject[$i][$this->rbac->getConfiguration()->getValue( "role", "namingattribute" )][0],
                                            $arrProject[$i]['tgprojectname'][0], $arrProject[$i]['tgprojectdescription'][0],
                                            $arrProject[$i]['tgprojectfile'][0] );

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: createProject
  // Input: auth / xsd:string
  //        log / xsd:string
  //        name / xsd:string
  //        description / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Tries to authorize the user. If this is
  //   successful the given role is added to the system.
  //   This function creates roles without a hirarchy, so it
  //   only has to check if there is access to the "role_base".
  // -----------------------------------------------------
  function createProject( $inRequest ) {

    $createProjectResult = false;           // The result of the process
    $connection = false;                    // The connection to the projects
    $arrProjectName = Array();              // All present projects
    $projectName = "";                      // At last Holds the project-name
    $maxNumber = 0;                         // The highest free project-number
    $result = new createProjectResponse();  // The result
    $i = 0;                                 // Loop
    $username = "";                         // Reply from sessionUser
    $is_authenticated = false;              // test if session exists in rbac
    $objFaultDetail = new stdClass();

    try {
      $username = $this->rbac->sessionUser($inRequest->auth);
      $is_authenticated = true;

    } catch (Exception $e) {
      $is_authenticated = false;
    }


    // Create a new uuid for the new project
    $strUuid = $this->uuidV4();


    if( $is_authenticated ) {

      // The role-connection is needed because projects
      // are representated as roles.
      $connection = $this->rbac->getConnection( "role" );


/* Old-style enumeration of project ID
      // Search the directory
      $arrProjectName = $connection->search( $this->rbac->getConfiguration()->getValue( "project", "base" ), "(objectClass=rbacRole)", "one",
                                             $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) );


      // Every returned project has to be examined for
      // its number to get the next free one.
      for( $i = 0; $i < sizeof( $arrProjectName ); $i++ ) {

        $projectName = preg_replace( "/^TGPR/i", "", $arrProjectName[$i][$this->rbac->getConfiguration()->getValue( "role", "namingattribute" )][0] );
        $maxNumber = max( $maxNumber, intval( $projectName ) );

      }
*/

      if(    (    preg_match( "/.+/", $inRequest->file )
               && $arrResource = $connection->search( $this->rbac->getConfiguration()->getValue( "resource", "base" ),
                                                      "(tgresourceuri=" . $inRequest->file . ")" ) )
          || !preg_match( "/.+/", $inRequest->file ) ) {

      try {

/* Old-style enumeration of project ID
        // Create the project-role
        $createProjectResult = $this->rbac->addAscendant( "TGPR" . ($maxNumber + 1),
                                                          $this->rbac->getConfiguration()->getValue( "project", "base" ) );
*/
        // Create the project-role
        $createProjectResult = $this->rbac->addAscendant( "TGPR-" . $strUuid,
                                                          $this->rbac->getConfiguration()->getValue( "project", "base" ) );

/* Old-style enumeration of project ID
        // Create the project's DN
        $projectDn  = $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=TGPR";
        $projectDn .= ($maxNumber + 1) . "," . $this->rbac->getConfiguration()->getValue( "project", "base" );
*/
        // Create the project's DN
        $projectDn  = $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=TGPR-";
        $projectDn .= $strUuid . "," . $this->rbac->getConfiguration()->getValue( "project", "base" );


        // The user of the session-ID will be the Leader
        // of this new project.
        $this->rbac->assignUser( $username,
                                 $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=Projektleiter," . $projectDn );

        // Set the project-description
        $arrModify['tgprojectname'][] = $inRequest->name;
        preg_match( "/.+/", $inRequest->description ) ? $arrModify['tgprojectdescription'][] = $inRequest->description : false;


        // Set the project-file and modify the
        // file's permission set
        if( preg_match( "/.+/", $inRequest->file ) ) {

          $arrModify['tgprojectfile'][] = $inRequest->file;


          $this->modifyProjectFilePermissions( $arrResource[0]['dn'], $projectDn );

        }


        $connection->modify( $projectDn, $arrModify );


	$this->rbac->addActiveRole( $username, $inRequest->auth,
                                 $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=Projektleiter," . $projectDn );

/* Old-style enumeration of project ID
        $result->projectId = "TGPR" . ($maxNumber + 1);
*/
        $result->projectId = "TGPR-" . $strUuid;

      }
      catch( RBACException $e ) {

        $objFaultDetail->faultNo = $e->getCode();
        $objFaultDetail->faultMessage = $e->getMessage();
        $objFaultDetail->cause = "There was an error while connecting to the underlying RBAC database. This should not have happend!";


        return new SoapFault( "rbacFault", $e->getCode(), get_class( $this->rbac ), $objFaultDetail );

      }

      }
      else {

        // The file that had to be set as project file
        // does not exist. Stop everything and return
        // an empty projectId.
        $result->projectId = "";

      }

    }
    else {

      $objFaultDetail->faultNo = $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" );
      $objFaultDetail->faultMessage = $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" );
      $objFaultDetail->cause = "You have to login to be able to create a new project.";


      return new SoapFault( "authenticationFault",
                            $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                            get_class( $this ),
                            $objFaultDetail );

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: deleteProject
  // Input: auth / xsd:string
  //        log / xsd:string
  //        project / xsd:string
  // Output: result / xsd:boolean
  // Description:
  //    Removes a project/role from the database if there are
  //    no resources containing any role that belongs to
  //    this project/role.
  // -----------------------------------------------------
  public function deleteProject( $inRequest ) {

    $result = new booleanResponse();
    $objFaultDetail = new stdClass();


    // Search for any resources within the project. If
    // there are any the project cannot be deleted.
    $strFilter .= "(&(objectClass=TextGridResource)(TGProjectId=" . $inRequest->project . "))";


    $arrResource = $this->connection['resource']->search( $this->rbac->getConfiguration()->getValue( "resource", "base" ), $strFilter, "sub" );


    if( $arrResource && sizeof( $arrResource ) != 0 ) {

      $objFaultDetail->faultNo = 7;
      $objFaultDetail->faultMessage = "Project could not be deleted";
      $objFaultDetail->cause = "There are still " . sizeof( $arrResource ) . " resources belonging to this project.";

      return new SoapFault( "notEmptyFault", 7, get_class( $this ), $objFaultDetail );
//      return new SoapFault( Array( "http://textgrid.info/namespaces/middleware/tgauth", "notEmptyFault" ), 7, get_class( $this ), "There are still " . sizeof( $arrResource ) . " resources belonging to this project." );

    }
    else {

      $strFilter = "(&(objectClass=rbacRole)(rbacName=" . $inRequest->project . "))";


      $arrRole = $this->rbac->getConnection( "role" )->search( $this->rbac->getConfiguration()->getValue( "role", "base" ), $strFilter, "sub" );


      if( $arrRole && sizeof( $arrRole ) == 1 ) {

        if( $this->rbac->checkAccess( $inRequest->auth, "delegate", $inRequest->project ) ) {

          $result->result = $this->rbac->getConnection( "role" )->delete( $arrRole[0]['dn'], true );

        }
        else {

          $objFaultDetail->faultNo = $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" );
          $objFaultDetail->faultMessage = $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" );
          $objFaultDetail->cause = "You do not have permission to delete project " . $inRequest->project . ".";

          return new SoapFault( "authenticationFault",
                                $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                                get_class( $this ),
                                $objFaultDetail );

        }

      }
      else {

        $result->result = false;

      }

    }

    return $result;

  }



  // -----------------------------------------------------
  // Function: getNumberOfResources
  // Input: auth / xsd:string
  //        log / xsd:string
  //        project / xsd:string
  // Output: allresources / xsd:int
  //         publicresources / xsd:int
  // Description
  //   Checks if a given project has any public resources.
  // -----------------------------------------------------
  public function getNumberOfResources ( $inRequest ) {

    $result = new stdClass();

    // Filter for ALL resources...
    $strFilter = "(&(objectClass=TextGridResource)(TGProjectId=" . $inRequest->project . "))";

    $arrResource = $this->connection['resource']->search( $this->rbac->getConfiguration()->getValue( "resource", "base" ), $strFilter, "sub", array("TGisPublic" ));

    $result->allresources = sizeof( $arrResource );

    // Construct a filter that searches for the public
    // resources of a project.
    $strFilter = "(&(objectClass=TextGridResource)(TGisPublic=TRUE)(TGProjectId=" . $inRequest->project . "))";

    $arrResource = $this->connection['resource']->search( $this->rbac->getConfiguration()->getValue( "resource", "base" ), $strFilter, "sub", array("TGisPublic" ) );

    $result->publicresources = sizeof( $arrResource );


    return $result;

  }




  public function getDeactivatedProjects( $inRequest ) {

    $strFilter = "";
    $arrDeactivatedProject = Array();
    $result = new getAllProjectsResponse();  // Return


    $username = $this->rbac->sessionUser( $inRequest->auth );


    $strFilter .= "(&(objectClass=rbacRole)(rbacName=Projektleiter)(rbacPerformer=" . $username . "__deactivated))";


    $arrRole = $this->rbac->getConnection( "role" )->search( $this->rbac->getConfiguration()->getValue( "role", "base" ), $strFilter, "sub" );

    for( $i = 0; $i < sizeof( $arrRole ); $i++ ) {

      if( preg_match( "/Projektleiter/", $arrRole[$i]["dn"] ) ) {

/* Old-style project IDs can be found this way but not IDs using a UUID
	preg_match ( '/(TGPR\d+)/', $arrRole[$i]["dn"], $tgpr);
*/
	preg_match ( '/(TGPR[^,]+)/', $arrRole[$i]["dn"], $tgpr);

        $arrProject = $this->rbac->getConnection( "role" )->getEntry(
			"rbacName=" . $tgpr[1] .
			",rbacName=Projekt-Teilnehmer," .
			$this->rbac->getConfiguration()->getValue( "role", "base" ) );

        $result->project[] = new ProjectInfo( $arrProject[$this->rbac->getConfiguration()->getValue( "role", "namingattribute" )][0],
                                                $arrProject['tgprojectname'][0], $arrProject['tgprojectdescription'][0],
                                                $arrProject['tgprojectfile'] );

      }

    }

    return $result;

  }




  // -----------------------------------------------------
  // Function: reactivateProject
  // Input: auth / xsd:string
  //        log / xsd:string
  //        project / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Reactivates a project by renaming the users that
  //   perform the various roles. The old permissions are
  //   restored this way.
  // -----------------------------------------------------
  public function reactivateProject( $inRequest ) {

    $strDn = "";
    $strFilter = "";
    $result = new booleanResponse();  // Return
    $objFaultDetail = new stdClass();


    $username = $this->rbac->sessionUser( $inRequest->auth );


    $strDn .= $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=Projektleiter,";
    $strDn .= $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=" . $inRequest->project . ",";
    $strDn .= $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=" . $this->config->getValue( "project", "base" ) . ",";
    $strDn .= $this->rbac->getConfiguration()->getValue( "role", "base" );


    $arrProject = $this->connection['role']->getEntry( $strDn );


    if( isset( $arrProject['dn'] ) ) {

      $boAccess = false;


      for( $i = 0; $i < sizeof( $arrProject['rbacperformer'] ); $i++ ) {

        $boAccess = $boAccess || preg_match( "/^" . $username . "__deactivated$/i", $arrProject['rbacperformer'][$i] );

      }


      if( $boAccess ) {

        $strDn  = $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=" . $inRequest->project . ",";
        $strDn .= $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=" . $this->config->getValue( "project", "base" ) . ",";
        $strDn .= $this->rbac->getConfiguration()->getValue( "role", "base" );


        $renameResult = $this->renameRbacPerformers( $strDn, true, true );


        if( $renameResult ) {

          // Set the flag to "FALSE"
          $arrModify['tgprojectdeactivated'][] = "FALSE";


          $result->result = $this->connection['role']->modify( $strDn, $arrModify );

        }
        else {

          $result->result = false;

        }

      }
      else {

        $objFaultDetail->faultNo = $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" );
        $objFaultDetail->faultMessage = $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" );
        $objFaultDetail->cause = "You are not allowed to reactivate the project " . $inRequest-project . ".";


        return new SoapFault( "authenticationFault",
                              $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                              get_class( $this ),
                              $objFaultDetail );

      }

    }
    else {

      $objFaultDetail->faultNo = $this->config->getValue( "errorCode", "UNKNOWN_PROJECT" );
      $objFaultDetail->faultMessage = $this->config->getValue( "errorDescription", "UNKNOWN_PROJECT" );
      $objFaultDetail->cause = "The project " . $inRequest-project . " is unknown.";


      return new SoapFault( "unknownProjectFault",
                            $this->config->getValue( "errorCode", "UNKNOWN_PROJECT" ),
                            get_class( $this ),
                            $objFaultDetail );

    }


    return $result;

  }



  // -----------------------------------------------------
  // Function: deactivateProject
  // Input: auth / xsd:string
  //        log / xsd:string
  //        project / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Deactivates a project by renaming the users that
  //   perform the various roles. This way it is impossible
  //   to activate these roles/projects anymore.
  // -----------------------------------------------------
  public function deactivateProject( $inRequest ) {

    $renameResult = false;            // The result of the rename process
    $flagResult = false;              // The result of setting the flag to the project role
    $result = new booleanResponse();  // Return
    $arrProject = "";                 // The username corresponding to the session-ID
    $objFaultDetail = new stdClass();
    $arrModify = Array();


    if( $this->rbac->checkAccess( $inRequest->auth, "delegate", $inRequest->project ) ) {

      $arrProject = $this->connection['role']->getEntry( $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "="
                                                         . $inRequest->project . ","
                                                         . $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "="
                                                         . $this->config->getValue( "project", "base" )
                                                         . "," . $this->rbac->getConfiguration()->getValue( "role", "base" ) );
      // Mark all users as deactivated that are directly
      // assigned to the project role.
      $arrModify['rbacperformer'] = Array();

      if( is_array( $arrProject ) && array_key_exists( "rbacperformer", $arrProject ) ) {

        for( $i = 0; $i < sizeof( $arrProject['rbacperformer'] ); $i++ ) {

          $arrModify['rbacperformer'][] = $arrProject['rbacperformer'][$i] . "__deactivated";

        }
      }


      // Try to rename all users so they cannot
      // activate any roles from this project
      // anymore.
      $renameResult = $this->renameRbacPerformers( $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "="
                                   . $inRequest->project . ","
                                   . $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "="
                                   . $this->config->getValue( "project", "base" )
                                   . "," . $this->rbac->getConfiguration()->getValue( "role", "base" ) );

      if( $renameResult ) {

        // Set the flag to "TRUE"
        $arrModify['tgprojectdeactivated'][] = "TRUE";


        $flagResult = $this->connection['role']->modify( $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "="
                                         . $inRequest->project . ","
                                         . $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "="
                                         . $this->config->getValue( "project", "base" )
                                         . "," . $this->rbac->getConfiguration()->getValue( "role", "base" ),
                                         $arrModify );
      }


      $result->result = $renameResult && $flagResult;

    }
    else {

      $objFaultDetail->faultNo = $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" );
      $objFaultDetail->faultMessage = $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" );
      $objFaultDetail->cause = "You are not allowed to deactivate the project " . $inRequest->project . ".";

      return new SoapFault( "authenticationFault",
                            $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                            get_class( $this ),
                            $objFaultDetail );

    }


    return $result;

  }



  // -----------------------------------------------------
  // Function: renameRbacPerformers
  // Input: inBase / string
  //        inRecursive / boolean
  // Output: result / xsd:boolean
  // Description
  //   The users stored in the rbacPerformer attribute are
  //   recursively renamed to <username>__deactivated.
  // -----------------------------------------------------
  private function renameRbacPerformers( $inBase, $inRecursive = true, $inRemoveDeactivation = false ) {

    $arrEntry = Array();
    $arrModify = Array();
    $i = 0;
    $result = true;


    // Get the entry
    $arrEntry = $this->connection['role']->getEntry( $inBase );

    if( is_array($arrEntry) && array_key_exists("rbacperformer", $arrEntry) && is_array($arrEntry['rbacperformer']) ) {
      // Mark all users as deactivated that are directly
      // assigned to the project role.
      for( $i = 0; $i < sizeof( $arrEntry['rbacperformer'] ); $i++ ) {

        if( $inRemoveDeactivation ) {

          $arrModify['rbacperformer'][] = preg_replace( "/__deactivated$/", "", $arrEntry['rbacperformer'][$i] );

        }
        else {

          $arrModify['rbacperformer'][] = $arrEntry['rbacperformer'][$i] . "__deactivated";

        }
      }
    }

    // If there are any users present in the entry,
    // send the modifications.
    if(is_array($arrModify) && array_key_exists("rbacperformer", $arrModify) && $arrModify['rbacperformer'] && sizeof( $arrModify['rbacperformer'] ) > 0 ) {

      $this->connection['role']->modify( $inBase, $arrModify );

    }


    // Query all sub entries that still need to be
    // processed.
    $arrSub = $this->connection['role']->search( $inBase, $this->rbac->getConfiguration()->getValue( "role", "filter" ), "one" );


    for( $i = 0; $i < sizeof( $arrSub ); $i++ ) {

      $result = $result && $this->renameRbacPerformers( $arrSub[$i]['dn'], $inRecursive, $inRemoveDeactivation );

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: tgAddActiveRole
  // Input: auth / xsd:string
  //        log / xsd:string
  //        role / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Adds an assigned role to the list of active session
  //   roles.
  // -----------------------------------------------------
  public function tgAddActiveRole( $inRequest ) {

    $result = new booleanResponse();  // Return
    $username = "";                   // The username corresponding to the session-ID
    $objFaultDetail = new stdClass();


    try {

      $username = $this->rbac->sessionUser( $inRequest->auth );

    }
    catch( Exception $e ) {

      $objFaultDetail->faultNo = $e->getCode();
      $objFaultDetail->faultMessage = $e->getMessage;
      $objFaultDetail->cause = "There was an error while connecting to the underlying RBAC database. This should not have happend!";


      return new SoapFault( "rbacFault", $e->getCode(), get_class( $this->rbac ), $objFaultDetail );

    }


    if( preg_match( "/.+/", $username ) ) {

      $result->result = $this->rbac->addActiveRole( $username, $inRequest->auth, $inRequest->role );

    }
    else {

      $result->result = false;

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: tgDropActiveRole
  // Input: auth / xsd:string
  //        log / xsd:string
  //        role / xsd:string
  // Output: sid / string
  // Description
  //   Removes a role from the list of active session roles.
  // -----------------------------------------------------
  public function tgDropActiveRole( $inRequest ) {

    $result = new booleanResponse();  // Return
    $username = "";                   // The username corresponding to the session-ID


    $username = $this->rbac->sessionUser( $inRequest->auth );


    if( preg_match( "/.+/", $username ) ) {

      $result->result = $this->rbac->dropActiveRole( $username, $inRequest->auth, $inRequest->role );

    }


    return $result;

  }




  // -----------------------------------------------------
  // Function: tgAssignedRoles
  // Input: auth / xsd:string
  //        log / xsd:string
  //        username / xsd:string
  // Output: sid / string
  // Description
  //   Creates a random string containing characters and
  //   numbers.
  // -----------------------------------------------------
  public function tgAssignedRoles( $inRequest ) {

    $result = new rolesetResponse();
    $arrActiveRole = Array();
    $arrProject = Array();
    $arrAllreadyChecked = Array();
    $connection = false;
    $i = 0;  // Loop


    try {
      $eppn = $this->rbac->sessionUser( $inRequest->auth );

      if(    preg_match( "/.+/", $inRequest->username )
          && !preg_match( "/^" . $inRequest->username . "$/i", $eppn) ) {

        // The roles of the user
        $arrRole = $this->rbac->assignedRoles( $inRequest->username );

        // Extract the different projects the user is
        // assigned by his roles.
        for( $i = 0; $i < sizeof( $arrRole ); $i++ ) {

          $arrSplit = preg_split( "/[,]/", $arrRole[$i] );

          for( $j = 0; $j < sizeof( $arrSplit ); $j++ ) {

            if(    preg_match( "/TGPR.+/i", $arrSplit[$j] )
                && !in_array( trim( $arrSplit[$j] ), $arrProject ) ) {

              $arrProject[] = trim( $arrSplit[$j] );
              break;

            }

          }

        }

        // For each project the username is in, check if the
        // session-ID has the right to display the assigned roles.
        for( $i = 0; $i < sizeof( $arrProject ); $i++ ) {

          if( $this->rbac->checkAccess( $inRequest->auth, "delegate", $arrProject[$i] ) ) {

            for( $j = 0; $j < sizeof( $arrRole ); $j++ ) {

              if( preg_match( "/" . $arrProject[$i] . "/i", $arrRole[$j] ) ) {

                $result->role[] = $arrRole[$j];

              }

            }

          }

        }

      }
      else {

        $result->role = $this->rbac->assignedRoles( $eppn);

      }

    } catch (RBACException $f) {
      // return empty roleset if session does not exist
      $result->role = Array();
    }

    return $result;

  }




  // -----------------------------------------------------
  // Function: createSessionID
  // Input: none
  // Output: sid / string
  // Description
  //   Creates a random string containing characters and
  //   numbers.
  // -----------------------------------------------------
  private function createSessionID() {

    $sid = "";  // Session-ID
    $tmp = "";  // Temporaere Session-ID


    // Setzen eines Startwerts fuer den Zufallsgenerator
    mt_srand( (double)microtime(true) * 1234560);

    // Erzeugen eines Zufallsstrings
    for( $i = 0; $i < 256; $i++ ) {

      $tmp = chr( mt_rand( 0, 255 ) );


      if( preg_match( "/[a-zA-Z0-9]/", $tmp ) ) {

        $sid .= $tmp;

      }

    }
    $sid .= (integer) ( microtime(true) * 1000000) ;

    return $sid;

  }




  private function modifyProjectFilePermissions( $inDn, $inProjectDn ) {

    // The resource-connection is needed.
    $connection = $this->rbac->getConnection( "resource" );


    $arrModify = Array( "rbacpermission" =>
                        Array( $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=Projektleiter," . $inProjectDn . ":-:write",
                               $this->rbac->getConfiguration()->getValue( "role", "namingattribute" ) . "=Projektleiter," . $inProjectDn . ":-:delete" ),
                        "tgisprojectfile" => "TRUE" );


    $connection->modify( $inDn, $arrModify );

  }




  // -----------------------------------------------------
  // Function: addMember // tgAssignUser
  // Input: auth / xsd:string
  //        username / xsd:string
  //        role / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Tries to authorize the user. If this is
  //   successful the given user is assigned to the
  //   role. In addition the new role is activated in
  //   all of the users sessions.
  // -----------------------------------------------------
  function addMember ( $inRequest ) {

    $assignUserResult = false;        // The result of the RBAC-call
    $result = new booleanResponse();  // The result
    $objFaultDetail = new stdClass();


    $arrRoleSplit = preg_split( "/[,]/", $inRequest->role );

    for( $i = 0; $i < sizeof( $arrRoleSplit ); $i++ ) {

/* Only old-style projects IDs can be found this way
      if( preg_match( "/^TGPR[0-9]+$/i", $arrRoleSplit[$i] ) ) {
*/
      if( preg_match( "/^TGPR.+$/i", $arrRoleSplit[$i] ) ) {

        $projectName = $arrRoleSplit[$i];
        break;

      }

    }


    // Test if the user has apropriate rights to assign a user
    // to the given role. The user may have directly the right
    // to modify the given role or the user may have the right to
    // modify the whole role-tree.
    if(    $this->rbac->checkAccess( $inRequest->auth, "delegate", $projectName )
        || $this->rbac->checkAccess( $inRequest->auth, "administer", "role_base" ) ) {

      try {

        $assignUserResult = $this->rbac->assignUser( $inRequest->username, $inRequest->role );


        $result->result = $assignUserResult;

      }
      catch( RBACException $e ) {

        $objFaultDetail->faultNo = $e->getCode();
        $objFaultDetail->faultMessage = $e->getMessage;
        $objFaultDetail->cause = "There was an error while connecting to the underlying RBAC database. This should not have happend!";


        return new SoapFault( "rbacFault", $e->getCode(), get_class( $this->rbac ), $objFaultDetail );

      }

    }
    else {

      $objFaultDetail->faultNo = $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" );
      $objFaultDetail->faultMessage = $this->config->getValue( "errorDescription", "INSUFFICIENT_ACCESS" );
      $objFaultDetail->cause = "You are not allowed to assign the member " . $inRequest->username . " to the role " . $inRequest->role . ".";


      return new SoapFault( "rbacFault",
                            $this->config->getValue( "errorCode", "INSUFFICIENT_ACCESS" ),
                            get_class( $this ),
                            $objFaultDetail );

    }


    if( $result->result ) {

      // Now activate the new role in all of the user's
      // sessions.
      $filter = "(&(objectClass=rbacSession)(rbacSessionUser=" . $inRequest->username . "))";
      $arrSession = $this->connection['session']->search( $this->rbac->getConfiguration()->getValue( "session", "base" ), $filter, "one" );


      for( $i = 0; $i < sizeof( $arrSession ); $i++ ) {

        try {

          $this->rbac->addActiveRole( $inRequest->username, $arrSession[$i]['rbacname'][0], $inRequest->role );

        }
        catch( RBACException $e ) {

          // Do nothing

        }

      }

    }


    return $result;

  }


  function uuidV4() {
    $strResult = "";
    $strRandom = "";
    $strRandom = uniqid( md5( rand() ) );
    $strResult .= substr( $strRandom, 0, 8 ) . "-";
    $strResult .= substr( $strRandom, 8, 4 ) . "-";
    $strResult .= substr( $strRandom, 12, 4 ) . "-";
    $strResult .= substr( $strRandom, 16, 4 ) . "-";
    $strResult .= substr( $strRandom, 32, 12 );

    return $strResult;
  }

}
?>
