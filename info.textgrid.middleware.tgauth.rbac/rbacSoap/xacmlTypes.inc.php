<?php
class dumpResponse {
  public $dump;
}


class XACMLAuthzDecisionQuery {
  public $id;
  public $Version;
  public $issueInstant;
  public $inputContextOnly;
  public $returnContext;
  public $request;
}


class Request {
  public $subject;
  public $resource;
  public $action;
}
?>
