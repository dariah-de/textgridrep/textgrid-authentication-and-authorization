<?php 
/* Set internal character encoding to UTF-8 */
mb_internal_encoding("UTF-8");
// #######################################################
// Author: Markus Widmer
// Creation date: 05.09.2011
// Modification date: 05.09.2011
// Version: 1.0.0
// #######################################################


require_once( "soapTypes.inc.php" );
require_once( "../rbac/RBAC.class.php" );
require_once( "TgExtraCrud.class.php" );

// Dont be so verbose with messages and notices.
error_reporting( E_ERROR | E_USER_ERROR );

// #############################################################
// Starting SOAP-Server
// #############################################################
$server = new SoapServer( "./wsdl/tgextra-crud.wsdl" );
$server->setClass( "TgExtraCrud", "../conf/rbacSoap.conf", "../conf/system.conf", "../rbac/" );


$server->handle();
/*
$tge = new TgExtra( "../conf/rbacSoap.conf.xml", "../conf/system.conf.xml", "../rbac/" );
$createProjectRequest = new CreateProjectRequest();
$tge->createProject( $createProjectRequest );
*/
?>
