<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 24.09.2010
// Modification date: 24.09.2010
// Version: 0.0.1
// #######################################################



class XACMLGrid extends XACML {

  // Global variables
  protected $rbac;
  protected $config;
  protected $connection;



  // -----------------------------------------------------
  // Constructor
  // Input: none
  // Output: object RBACcore
  // Description:
  //   Creates initial connections to the LDAP-server and
  //   sets some configuration parameters.
  // -----------------------------------------------------
  public function __construct( $inConfigurationFilename, $inRbacConfFile, $inRbacBase ) {

/*
    $this->rbac = new RBAC( $inRbacConfFile, $inRbacBase );
    $this->config = new SimpleConfig( $inConfigurationFilename );
*/

    parent::__construct( $inConfigurationFilename, $inRbacConfFile, $inRbacBase );


    $this->connection['user'] = new LDAP();
    $this->connection['user']->connect( $this->config->getValue( "authentication", "host" ),
                                        $this->config->getValue( "authentication", "port" ),
                                        $this->config->getValue( "authentication", "version" ),
                                        preg_match( "/yes/i", $this->config->getValue( "authentication", "tls" ) ) ? true : false );
    $this->connection['user']->bind( $this->config->getValue( "authentication", "binddn" ),
                                     $this->config->getValue( "authentication", "password" ) );
    $this->connection['resource'] = $this->rbac->getConnection( "resource" );
    $this->connection['session'] = $this->rbac->getConnection( "session" );

  }




  // -----------------------------------------------------
  // Function: checkXACMLaccess
  // Input: auth / xsd:string
  //        log / xsd:string
  //        username / xsd:string
  // Output: result / xsd:boolean
  // Description
  //   Checks if a given user exists in the LDAP directory.
  // -----------------------------------------------------
  function checkXACMLaccess( $inRequest ) {

    $strFilter = "";           // An LDAP filter string
    $result = new stdClass();  // The response


    try {

      $strDnUsername = $inRequest->Request->Subject->Attribute->AttributeValue->any;


      // Get the user's real name
      $strFilter = "(" . $this->config->getValue( "xacmlGrid", "certDnAttribute" ) . "=" . $strDnUsername . ")";
      $arrUserEntry = $this->connection['user']->search( $this->config->getValue( "authentication", "base" ), $strFilter, "sub" );


      if( $arrUserEntry && sizeof( $arrUserEntry ) > 0 ) {

        // Set the username from the first available result
        // (there should only be one).
        $strRealUsername = $arrUserEntry[0][strtolower($this->config->getValue( "authentication", "namingattribute" ))][0];

        fwrite( $file, "strRealUsername: " . $strRealUsername . "\n" );

        // Get the user's roles
        $arrRole = $this->rbac->authorizedRoles( $strRealUsername );


        // Search for an existing session for the cert DN.
        $strFilter = "(rbacSession=" . $inRequest->Request->Subject->Attribute->AttributeValue->any . ")";
        $arrSession = $this->connection['session']->search( $this->rbac->getConfiguration()->getValue( "session", "base" ), "sub", $strFilter );


        // Create a temporary session for the user that will
        // be removed after the check has been completed.
        $strSessionId = "temp_" . $this->createSessionID();

        $this->rbac->createSession( $strRealUsername, $arrRole, $strSessionId );


        // Replace the user's cert DN by the temporary session ID
        $inRequest->Request->Subject->Attribute->AttributeValue->any = $strSessionId;


        // Check the access...
        $result = parent::checkXACMLaccess( $inRequest );


        // Re-replace the user's temporary session ID by the user's cert DN
        $inRequest->Request->Subject->Attribute->AttributeValue->any = $strDnUsername;


        // Remove the temporary session.
        $this->rbac->deleteSession( $strRealUsername, $strSessionId );

      }

    }
    catch( Exception $e ) {

      $result->Response->Result->Decision = "NotApplicable";

    }


    fclose( $file );

    return $result;

  }


  private function createSessionID() {

    $sid = "";  // Session-ID
    $tmp = "";  // Temporaere Session-ID


    // Setzen eines Startwerts fuer den Zufallsgenerator
    mt_srand( (double)microtime(true) * 1234560);

    // Erzeugen eines Zufallsstrings
    for( $i = 0; $i < 256; $i++ ) {

      $tmp = chr( mt_rand( 0, 255 ) );


      if( preg_match( "/[a-zA-Z0-9]/", $tmp ) ) {

        $sid .= $tmp;

      }

    }
    $sid .= (integer) ( microtime(true) * 1000000) ;

    return $sid;

  }

}
?>
