<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 07.07.2007
// Modification date: 13.11.2007
// Version: 1.0.0
// #######################################################


require_once( "xacmlTypes.inc.php" );
require_once( "../rbac/RBAC.class.php" );
require_once( "XACML.class.php" );


// Dont be so verbose with messages and notices.
error_reporting( E_ERROR | E_USER_ERROR );


// #############################################################
// Starting SOAP-Server
// #############################################################
$server = new SoapServer( "./wsdl/xacml.wsdl.local" );
$server->setClass( "XACML", "../conf/rbacSoap.conf", "../conf/system.conf", "../rbac/" );


$server->handle();
?>
