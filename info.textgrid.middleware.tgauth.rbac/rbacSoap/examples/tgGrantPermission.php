<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 04.04.2008
// Modification date: 04.04.2008
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra = new SoapClient( "../wsdl/tgextra.wsdl" );


echo "<BODY><HTML>";




if( isset( $_POST['auth'] ) ) {

  // -----------------------------------------------------
  // If this was successfull you can add a the user you
  // wish to create
  // -----------------------------------------------------
  $graReq = new tgGrantPermissionRequest();
  $graReq->auth = $_POST['auth'];
  $graReq->log = "";
  $graReq->role = $_POST['role'];
  $graReq->resource = $_POST['resource'];
  $graReq->operation = $_POST['operation'];


  echo "<HR/>";
  echo "Trying to grant permission...<BR/>";

  try {

    $response = $soapExtra->tgGrantPermission( $graReq );

    if( $response->result ) {

      echo "DONE";

    }
    else {

     echo "UNABLE TO COMMIT...";

    }

  }
  catch( SoapFault $f ) {

    echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

  }

}


echo "<FORM action=\"tgGrantPermission.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
echo "Auth: <INPUT type=\"text\" name=\"auth\" value=\"\"><BR>\n";
echo "Role: <INPUT type=\"text\" name=\"role\" value=\"\"><BR>\n";
echo "Resource: <INPUT type=\"text\" name=\"resource\" value=\"\"><BR>\n";
echo "Operation: <INPUT type=\"text\" name=\"operation\" value=\"\"><BR>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";

?>
