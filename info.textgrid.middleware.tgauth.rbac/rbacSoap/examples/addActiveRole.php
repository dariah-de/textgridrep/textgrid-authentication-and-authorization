<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 08.07.2007
// Modification date: 17.07.2007
// Version: 0.1.1
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra = new SoapClient( "../wsdl/tgextra.wsdl", Array( "trace" => true ) );


echo "<BODY><HTML>";


if( isset( $_POST['auth'] ) ) {


  // -----------------------------------------------------
  // Now you can try to drop the active role from your session
  // -----------------------------------------------------
  $addRoleReq = new addActiveRoleRequest();
  $addRoleReq->role = $_POST['role'];
  $addRoleReq->auth = $_POST['auth'];

  echo "Adding active role...<BR/>";


  try {

    $addRoleResponse = $soapExtra->tgAddActiveRole( $addRoleReq );


    echo $soapExtra->__getLastRequest();

    if( $addRoleResponse->result ) {

      echo "DONE.<BR/>";

    }
    else {

      echo "FAILED.<BR/>";

    }

  }
  catch( SoapFault $f ) {

    echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

  }

}

echo "<FORM action=\"addActiveRole.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
echo "Auth: <INPUT type=\"text\" name=\"auth\" value=\"\"><BR>\n";
echo "Role: <INPUT type=\"text\" name=\"role\" value=\"\"><BR/>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";

?>
