<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 14.04.2008
// Modification date: 14.04.2008
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra = new SoapClient( "../wsdl/tgextra.wsdl" );


echo "<BODY><HTML>";




if( isset( $_POST['project'] ) ) {

  // -----------------------------------------------------
  // If this was successfull you can add a the user you
  // wish to create
  // -----------------------------------------------------
  $regReq = new stdClass();
  $regReq->auth = "";
  $reqReq->log = "";
  $regReq->project = $_POST['project'];


  echo "<HR/>";
  echo "Checking for resources...<BR/>";

  try {

    $checkResponse = $soapExtra->getNumberOfResources( $regReq );

    echo "All resources: " . $checkResponse->allresources;	
    echo "<br/>Public resources: " . $checkResponse->publicresources;	

  }
  catch( SoapFault $f ) {

    echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

  }

}


echo "<FORM action=\"getNumberOfResources.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
//echo "Auth: <INPUT type=\"text\" name=\"auth\" value=\"\"><BR>\n";
echo "Project: <INPUT type=\"text\" name=\"project\" value=\"\"><BR>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";

?>
