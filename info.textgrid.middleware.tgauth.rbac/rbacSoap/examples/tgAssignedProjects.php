<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 18.07.2007
// Modification date: 18.07.2007
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra = new SoapClient( "../wsdl/tgextra.wsdl" );


echo "<BODY><HTML>";




if( isset( $_POST['auth'] ) ) {

  // -----------------------------------------------------
  // If this was successfull you can add a the user you
  // wish to create
  // -----------------------------------------------------
  $regReq = new tgAssignedProjectsRequest();
  $regReq->auth = $_POST['auth'];
  $regReq->log = "";
  $regReq->level = $_POST['level'];


  echo "<HR/>";
  echo "Searching...<BR/>";

  try {

    $assignedProjectsResponse = $soapExtra->tgAssignedProjects( $regReq );

    if( is_array( $assignedProjectsResponse->role ) ) {

      for( $i = 0; $i < sizeof( $assignedProjectsResponse->role ); $i++ ) {

        echo $assignedProjectsResponse->role[$i] . "<BR>";

      }

    }
    elseif( preg_match( "/.+/", $assignedProjectsResponse->role ) ) {

      echo $assignedProjectsResponse->role;

    }
    else {

      echo "No assigned roles!<BR>";

    }

  }
  catch( SoapFault $f ) {

    echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

  }

}


echo "<FORM action=\"tgAssignedProjects.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
echo "Auth: <INPUT type=\"text\" name=\"auth\" value=\"\"><BR>\n";
echo "Level: <SELECT name=\"level\">\n";
echo "       <OPTION value=\"4\">Alle Projekte, deren Projektleiter ich bin</OPTION>\n";
echo "       <OPTION value=\"3\">Alle Projekte in denen Schreibrechte bestehen</OPTION>\n";
echo "       <OPTION value=\"2\">Alle Projekte mit lesbaren Dateien</OPTION>\n";
echo "       <OPTION value=\"1\">Alle Projekte mit lesbbaren (inkl. öffentliche) Dateien</OPTION>\n";
echo "       <OPTION value=\"0\">Alle Projekte, in denen der Benutzer eingetragen ist</OPTION>\n";
echo "       </SELECT>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";

?>
