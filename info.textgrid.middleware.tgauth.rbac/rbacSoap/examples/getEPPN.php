<?php
// #######################################################
// Author: Martin Haase / DAASI / TextGrid
// Creation date: 17.11.2010
// Modification date: 17.11.2010
// Version: 0.0.1
// #######################################################


$soapExtra = new SoapClient( "../wsdl/tgextra.wsdl" );


echo "<BODY><HTML>";




if( isset( $_POST['auth'] ) ) {

  // -----------------------------------------------------
  // If this was successfull you can add a the user you
  // wish to create
  // -----------------------------------------------------
  $Req = new StdClass();
  $Req->auth = $_POST['auth'];
  $Req->log = "";
  $Req->secret = $_POST['secret'];


  echo "<HR/>";
  echo "Looking for ePPN...<BR/>";

  try {

    $response = $soapExtra->getEPPN( $Req );

    if( $response->eppn ) {

      echo "Owner of this session: " . $response->eppn;

    }
    else {

     echo "UNABLE TO COMMIT...";

    }

  }
  catch( SoapFault $f ) {

    echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

  }

}


echo "<FORM action=\"getEPPN.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
echo "Auth: <INPUT type=\"text\" name=\"auth\" value=\"\"><BR>\n";
echo "Session Check Secret: <INPUT type=\"text\" name=\"secret\" value=\"\"><BR>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";

?>
