<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 18.07.2007
// Modification date: 18.07.2007
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapSystem = new SoapClient( "../wsdl/tgextra.wsdl", Array( "trace" => 1 ) );


echo "<BODY><HTML>";




if( isset( $_POST['project'] ) ) {

  // -----------------------------------------------------
  // If this was successfull you can add a the user you
  // wish to create
  // -----------------------------------------------------
  $regReq = new getObjectsRequest();
  $regReq->auth = $_POST['auth'];
  $reqReq->log = "";
  $regReq->project = $_POST['project'];


  echo "<HR/>";
  echo "Searching resources for project...<BR/>";

  try {

    $resourceResponse = $soapSystem->getObjects( $regReq );


    if( is_array( $resourceResponse->resource ) ) {

      for( $i = 0; $i < sizeof( $resourceResponse->resource ); $i++ ) {

        echo $resourceResponse->resource[$i] . "<BR>";

      }

    }
    elseif( preg_match( "/.+/", $resourceResponse->resource ) ) {

      echo $resourceResponse->resource;

    }
    else {

      echo "No resources found!<BR>";

    }

  }
  catch( SoapFault $f ) {

    echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

  }

}


echo "<FORM action=\"getObjects.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
echo "Auth: <INPUT type=\"text\" name=\"auth\" value=\"\"><BR>\n";
echo "project: <INPUT type=\"text\" name=\"project\" value=\"\"><BR>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";

?>
