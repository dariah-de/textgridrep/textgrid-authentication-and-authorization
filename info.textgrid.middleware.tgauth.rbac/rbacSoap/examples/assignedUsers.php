<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 23.07.2007
// Modification date: 23.07.2007
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra = new SoapClient( "http://textgrid.regengedanken.de/rbacSoap/wsdl/tgextra.wsdl" );
$soapSystem = new SoapClient( "http://textgrid.regengedanken.de/rbacSoap/wsdl/tgsystem.wsdl" );
$soapReview = new SoapClient( "http://textgrid.regengedanken.de/rbacSoap/wsdl/tgreview.wsdl" );


echo "<BODY><HTML>";



// -----------------------------------------------------
// Before you can create a session you have to
// authenticate. If this was successful you get a
// session-ID that you should keep
// -----------------------------------------------------
$authReq = new authenticateRequest();
$authReq->username = "sp00001@textgrid.de";
$authReq->password = "secret";


echo "<HR/>";
echo "Doing authentication...<BR/>";

try {

  $authResponse = $soapExtra->authenticate( $authReq );

  if( preg_match( "/[0-9a-z]{2,}/i", $authResponse->sid ) ) {

    echo "DONE: " . $authResponse->sid . "<BR/>";

  }

}
catch( SoapFault $f ) {

  echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

}



// -----------------------------------------------------
// Now you can try to add an active role to your session
// -----------------------------------------------------
$addRoleReq = new addActiveRoleRequest();
$addRoleReq->username = "sp00001@textgrid.de";
$addRoleReq->role = "serviceProvider";
$addRoleReq->sid = $authResponse->sid;

echo "<HR/>";
echo "Adding active role...<BR/>";

try {

  $addRoleResponse = $soapSystem->addActiveRole( $addRoleReq );

  if( $addRoleResponse->result ) {

    echo "DONE.<BR/>";

  }

}
catch( SoapFault $f ) {

  echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

}


// -----------------------------------------------------
// If this was successfull you have to add a apropriate
// role to your active session that allows you to create
// a session for someone else.
// -----------------------------------------------------
$auReq = new assignedUsersRequest();
$auReq->intSid = $authResponse->sid;
$auReq->role = "Projekt-1,Projekt-Teilnehmer";

echo "<HR/>";
echo "The assigned users for role serviceProvider...<BR/>";

try {

  $usersetResponse = $soapReview->assignedUsers( $auReq );

  if( is_array( $usersetResponse->username ) ) {

    for( $i = 0; $i < sizeof( $usersetResponse->username ); $i++ ) {

      echo "User " . $i . ": " . $usersetResponse->username[$i] . "<BR/>";

    }

  }
  else {

    echo "User 0: " . $usersetResponse->username . "<BR/>";

  }

}
catch( SoapFault $f ) {

  echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

}


echo "</BODY></HTML>";

?>
