<?php
// #######################################################
// Author: Martin Haase
// Creation date: 15.10.2010
// Modification date: XX
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra = new SoapClient( "../wsdl/tgextra.wsdl" );


echo "<BODY><HTML>";


if( isset( $_POST['auth'] ) ) {


$x = new StdClass();
$x->auth=$_POST['auth'];
echo "<HR/>";
echo "asking for my attributes...<BR/>";


try {

  $getMyUserAttributesResponse = $soapExtra->getMyUserAttributes($x);

  $attrs = $getMyUserAttributesResponse->attribute; 	

  echo "<table>\n";
  foreach ($attrs as $a) {
	echo "<tr><td>".$a->name."</td><td>".$a->value."</td></tr>\n";
  }
  echo "</table>\n";
	
}	
catch( SoapFault $f ) {

  echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

}

}
echo "<FORM action=\"getMyUserAttributes.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
echo "Auth: <INPUT type=\"text\" name=\"auth\" value=\"\"><BR>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";


echo "</BODY></HTML>";

?>
