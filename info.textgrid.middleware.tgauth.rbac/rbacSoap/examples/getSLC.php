<?php
// #######################################################
// Author: Martin Haase
// Creation date: 13.10.2009
// Modification date: 13.10.2009
// Version: 0.1.1
// #######################################################


require_once( "../soapTypes.inc.php" );

// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra = new SoapClient( "../wsdl/tgextra.wsdl" );


echo "<BODY><HTML>";

if( isset( $_POST['auth'] ) ) {

  // -----------------------------------------------------
  // If this was successfull you can add a the user you
  // wish to create
  // -----------------------------------------------------
  $getReq = new getSLCRequest();
  $getReq->auth = $_POST['auth'];
  $getReq->log = "";
  $getReq->secret = $_POST['secret'];


  echo "<HR/>";
  echo "Getting SLC...<BR/>";

  try {

    $getSLCResponse = $soapExtra->getSLC( $getReq );

    if( $getSLCResponse->slc ) {

      echo "DONE. . $getSLCResponse->slc .<BR>";

    }
    else {

      echo "UNABLE to get SLC!<BR>";

    }

  }
  catch( SoapFault $f ) {

    echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

  }

}


echo "<FORM action=\"getSLC.php\" method=\"post\" enctype=\"multipart/form-data\">\n";

echo "</SELECT><BR>\n";
echo "Auth: <INPUT type=\"text\" name=\"auth\" value=\"\"><BR>\n";
echo "CRUD secret: <INPUT type=\"text\" name=\"secret\" value=\"\"><BR>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";

?>
