<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 23.10.2007
// Modification date: 23.10.2007
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra = new SoapClient( "../wsdl/tgextra.wsdl", Array( "trace" => 1 ) );
//$soapSystem = new SoapClient( "http://textgrid.regengedanken.de/rbacSoap/wsdl/tgsystem.wsdl" );


echo "<BODY><HTML>";

$filterReq = new filterBySidRequest();
$filterReq->auth = "HvO8kwcT81MH42QeHsZdPoqzcWGciHr8HjsSg41VZoDJFB9loUzHMWKVc4DzJSyK1308037097888871";

$filterReq->resource = Array( "textgrid:TGPR3:Die+Leiden+des+jungen+Werther+-+Zweyter+Theil:20080514T134649:xml%2Ftei:1",
			      "textgrid:6d5m.0" // a ProjectFile
 );
$filterReq->operation = "read";

echo "<HR/>";
echo "Filtering resources...<BR/>";

try {

  $filterResponse = $soapExtra->filterBySid( $filterReq );


  if( is_array( $filterResponse->resource ) ) {

    for( $i = 0; $i < sizeof( $filterResponse->resource ); $i++ ) {

      echo "Resource " . $i . ": " . $filterResponse->resource[$i] . "<BR/>";

    }

  }
  else {

    echo "Resource 0: " . $filterResponse->resource . "<BR/>";

  }

}
catch( SoapFault $f ) {

  echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

}


echo "</BODY></HTML>";

?>
