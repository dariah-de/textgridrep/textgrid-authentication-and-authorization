<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 18.07.2007
// Modification date: 18.07.2007
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra = new SoapClient( "../wsdl/tgextra.wsdl" );


echo "<BODY><HTML>";




if( isset( $_POST['auth'] ) ) {

  // -----------------------------------------------------
  // If this was successfull you can add a the user you
  // wish to create
  // -----------------------------------------------------
  $regReq = new tgAssignedRolesRequest();
  $regReq->auth = $_POST['auth'];
  $regReq->log = "";
  $regReq->username = $_POST['username'];


  echo "<HR/>";
  echo "Searching...<BR/>";

  try {

    $assignedRolesResponse = $soapExtra->tgAssignedRoles( $regReq );

    if( is_array( $assignedRolesResponse->role ) ) {

      for( $i = 0; $i < sizeof( $assignedRolesResponse->role ); $i++ ) {

        echo $assignedRolesResponse->role[$i] . "<BR>";

      }

    }
    elseif( preg_match( "/.+/", $assignedRolesResponse->role ) ) {

      echo $assignedRolesResponse->role;

    }
    else {

      echo "No assigned roles!<BR>";

    }

  }
  catch( SoapFault $f ) {

    echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

  }

}


echo "<FORM action=\"tgAssignedRoles.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
echo "Auth: <INPUT type=\"text\" name=\"auth\" value=\"\"><BR>\n";
echo "Username (may be empty): <INPUT type=\"text\" name=\"username\" value=\"\"><BR>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";

?>
