<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 08.04.2008
// Modification date: 08.04.2008
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra = new SoapClient( "../wsdl/tgextra.wsdl" );


echo "<BODY><HTML>";




if( isset( $_POST['auth'] ) ) {

  // -----------------------------------------------------
  // If this was successfull you can add a the user you
  // wish to create
  // -----------------------------------------------------
  $deaReq = new deactivateProjectRequest();
  $deaReq->auth = $_POST['auth'];
  $deaReq->log = "";
  $deaReq->project = $_POST['project'];


  echo "<HR/>";
  echo "Deactivating project...<BR/>";

  try {

    $response = $soapExtra->deactivateProject( $deaReq );

    if( $response->result ) {

      echo "DONE";

    }
    else {

     echo "UNABLE TO COMMIT...";

    }

  }
  catch( SoapFault $f ) {

    echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

  }

}


echo "<FORM action=\"deactivateProject.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
echo "Auth: <INPUT type=\"text\" name=\"auth\" value=\"\"><BR>\n";
echo "Project: <INPUT type=\"text\" name=\"project\" value=\"\"><BR>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";

?>
