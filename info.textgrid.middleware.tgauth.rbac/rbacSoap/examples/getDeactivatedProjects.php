<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 07.04.2008
// Modification date: 07.04.2008
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra =  new SoapClient( "../wsdl/tgextra.wsdl" );


echo "<BODY><HTML>";




if( isset( $_POST['auth'] ) ) {

  // -----------------------------------------------------
  // If this was successfull you can add a the user you
  // wish to create
  // -----------------------------------------------------
  $opReq = new getDeactivatedProjectsRequest();
  $opReq->auth = $_POST['auth'];
  $opReq->log = "";

  echo "<HR/>";
  echo "Searching for deactivated Projects where YOU were leader...<BR/>";

  try {

    $getProResponse = $soapExtra->getDeactivatedProjects($opReq);


   if( is_array( $getProResponse->project ) ) {

    for( $i = 0; $i < sizeof( $getProResponse->project ); $i++ ) {

      echo $getProResponse->project[$i]->id . " / " . $getProResponse->project[$i]->name . " / " . $getProResponse->project[$i]->description . "<BR>";

    }

  }
  elseif( $getProResponse->project instanceof project ) {

    echo $getProResponse->project->id . " / " . $getProResponse->project->name . " / " . $getProResponse->project->description . "<BR>";

  }
  else {

    echo "No projects!<BR/>";

  }

  }
    catch( SoapFault $f ) {

      echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

  }
}

echo "<FORM action=\"getDeactivatedProjects.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
echo "Auth: <INPUT type=\"text\" name=\"auth\" value=\"\"><BR>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";

?>
