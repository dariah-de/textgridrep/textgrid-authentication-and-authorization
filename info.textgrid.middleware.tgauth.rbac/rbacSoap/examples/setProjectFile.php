<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 18.07.2007
// Modification date: 18.07.2008
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------


echo "<HTML><BODY>";
echo "<HEAD><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></HEAD>\n";

$soapExtra = new SoapClient( "../wsdl/tgextra.wsdl" );



if( isset( $_POST['auth'] ) ) {

  // -----------------------------------------------------
  // If this was successfull you can add a the user you
  // wish to create
  // -----------------------------------------------------
  $setPFReq = new setProjectFileRequest();
  $setPFReq->auth = $_POST['auth'];
  $setPFReq->log = "";
  $setPFReq->project = $_POST['project'];
  $setPFReq->file = $_POST['file'];


  echo "<HR/>";
  echo "Setting project file...<BR/>";

  try {

    $setPFResponse = $soapExtra->setProjectFile( $setPFReq );

    if( $setPFResponse ) {

      echo "DONE.";

    }
    else {

      echo "UNABLE to register project file!<BR>";

    }

  }
  catch( SoapFault $f ) {

    echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

  }

}


echo "<FORM action=\"setProjectFile.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
echo "Target: ";
echo "<SELECT name=\"wsdl\">\n";

for( $i = 0; $i < sizeof( $arrExtraWsdl ); $i++ ) {

  echo "<OPTION value=\"" . $i . "\">" . $arrExtraWsdl[$i]['name'] . "</OPTION>\n";

}

echo "</SELECT><BR>\n";
echo "Auth: <INPUT type=\"text\" name=\"auth\" value=\"\"><BR>\n";
echo "Project ID: <INPUT type=\"text\" name=\"project\" value=\"\"><BR>\n";
echo "Project File URI: <INPUT type=\"text\" name=\"file\" value=\"\"><BR>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";

?>
