<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 07.04.2008
// Modification date: 07.04.2008
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapSystem = new SoapClient( "../wsdl/tgextra.wsdl" );


echo "<BODY><HTML>";




if( isset( $_POST['project'] ) ) {

  // -----------------------------------------------------
  // If this was successfull you can add a the user you
  // wish to create
  // -----------------------------------------------------
  $memReq = new getMembersRequest();
  $memReq->auth = $_POST['auth'];
  $memReq->log = "";
  $memReq->project = $_POST['project'];


  echo "<HR/>";
  echo "Searching UserRoles for project...<BR/>";

  try {

    $memResponse = $soapSystem->getUserRole( $memReq );

    if( is_array( $memResponse->userRole ) ) {

      for( $i = 0; $i < sizeof( $memResponse->userRole ); $i++ ) {

        echo serialize( $memResponse->userRole[$i]) . "<BR>";

      }

    }
    else {
      echo serialize ($memResponse->userRole);

    }

  }
  catch( SoapFault $f ) {

    echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

  }

}


echo "<FORM action=\"getUserRole.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
echo "Auth: <INPUT type=\"text\" name=\"auth\" value=\"\"><BR>\n";
echo "project: <INPUT type=\"text\" name=\"project\" value=\"\"><BR>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";

?>
