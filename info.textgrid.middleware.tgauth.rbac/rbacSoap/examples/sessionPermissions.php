<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 08.08.2007
// Modification date: 08.08.2007
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra = new SoapClient( "http://textgrid.regengedanken.de/rbacSoap/wsdl/tgextra.wsdl" );
$soapSystem = new SoapClient( "http://textgrid.regengedanken.de/rbacSoap/wsdl/tgsystem.wsdl" );
$soapReview = new SoapClient( "http://textgrid.regengedanken.de/rbacSoap/wsdl/tgreview.wsdl" );


echo "<BODY><HTML>";



// -----------------------------------------------------
// Before you can create a session you have to
// authenticate. If this was successful you get a
// session-ID that you should keep
// -----------------------------------------------------
$authReq = new authenticateRequest();
$authReq->username = "sp00001@textgrid.de";
$authReq->password = "secret";


echo "<HR/>";
echo "Doing authentication...<BR/>";

try {

  $authResponse = $soapExtra->authenticate( $authReq );

  if( preg_match( "/[0-9a-z]{2,}/i", $authResponse->sid ) ) {

    echo "DONE: " . $authResponse->sid . "<BR/>";

  }

}
catch( SoapFault $f ) {

  echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

}



// -----------------------------------------------------
// Now you can try to add an active role to your session
// -----------------------------------------------------
$addRoleReq = new addActiveRoleRequest();
$addRoleReq->username = "sp00001@textgrid.de";
$addRoleReq->role = "serviceProvider";
$addRoleReq->sid = $authResponse->sid;

echo "<HR/>";
echo "Adding active role...<BR/>";

try {

  $addRoleResponse = $soapSystem->addActiveRole( $addRoleReq );

  if( $addRoleResponse->result ) {

    echo "DONE.<BR/>";

  }

}
catch( SoapFault $f ) {

  echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

}


// -----------------------------------------------------
// If this was successfull you can ask for a session's
// permissions.
// -----------------------------------------------------
$permReq = new sessionPermissionsRequest();
$permReq->intSid = $authResponse->sid;
$permReq->sid = $authResponse->sid;

echo "<HR/>";
echo "Query the permissions of my own session...<BR/>";

try {

  $permResponse = $soapReview->sessionPermissions( $permReq );
  echo serialize( $permResponse );

  for( $i = 0; $i < sizeof( $permResponse->permissionset ); $i++ ) {

    echo   $permResponse->permissionset[$i]->resource . " / "
         . $permResponse->permissionset[$i]->operation . "<BR/>";

  }

}
catch( SoapFault $f ) {

  echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

}


echo "</BODY></HTML>";

?>
