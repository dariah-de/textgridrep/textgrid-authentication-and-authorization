<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 14.04.2008
// Modification date: 14.04.2008
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapSystem = new SoapClient( "../wsdl/tgextra.wsdl" );


echo "<BODY><HTML>";




if( isset( $_POST['resource'] ) ) {

  // -----------------------------------------------------
  // If this was successfull you can add a the user you
  // wish to create
  // -----------------------------------------------------
  $regReq = new isPublicRequest();
  $regReq->auth = "";
  $reqReq->log = "";
  $regReq->resource = $_POST['resource'];


  echo "<HR/>";
  echo "Checking if resource is public...<BR/>";

  try {

    $checkResponse = $soapSystem->isPublic( $regReq );

    if( $checkResponse->result ) {

      echo "YES.<BR>";

    }
    else {

      echo "NO<BR>";

    }

  }
  catch( SoapFault $f ) {

    echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

  }

}


echo "<FORM action=\"isPublic.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
echo "Auth: <INPUT type=\"text\" name=\"auth\" value=\"\"><BR>\n";
echo "Resource: <INPUT type=\"text\" name=\"resource\" value=\"\"><BR>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";

?>
