<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 18.07.2007
// Modification date: 18.07.2008
// Version: 0.1.1
// #######################################################


require_once( "../soapTypes.inc.php" );


// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra = new SoapClient( "../wsdl/tgextra.wsdl" );


echo "<BODY><HTML>";



if( isset( $_POST['auth'] ) ) {

  // -----------------------------------------------------
  // If this was successfull you can add a the user you
  // wish to create
  // -----------------------------------------------------
  $regReq = new registerResourceRequest();
  $regReq->auth = $_POST['auth'];
  $regReq->secret = $_POST['secret'];
  $regReq->log = "";
  $regReq->project = $_POST['project'];
  $regReq->uri = $_POST['uri'];


  echo "<HR/>";
  echo "Adding resource...<BR/>";

  try {

    $registerResourceResponse = $soapExtra->registerResource( $regReq );
    echo $soapExtra->__getLastRequest();

    if( $registerResourceResponse->operation ) {

      echo "Operations: " . serialize( $registerResourceResponse->operation );

    }
    else {

      echo $soapExtra->__getLastResponse();
      echo "UNABLE to commit!<BR>";

    }

  }
  catch( SoapFault $f ) {

    echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . serialize( $f->detail );

  }

}


echo "<FORM action=\"registerResource.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
echo "Auth: <INPUT type=\"text\" name=\"auth\" value=\"\"><BR>\n";
echo "Project-Name: <INPUT type=\"text\" name=\"project\" value=\"\"><BR>\n";
echo "CRUD secret: <INPUT type=\"text\" name=\"secret\" value=\"\"><BR>\n";
echo "URI: <INPUT type=\"text\" name=\"uri\" value=\"\"><BR>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";

?>
