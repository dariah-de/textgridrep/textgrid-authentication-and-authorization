<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 18.07.2007
// Modification date: 18.07.2007
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapSystem = new SoapClient( "../wsdl/tgextra.wsdl", Array( "trace" => 1 ) );


echo "<BODY><HTML>";




if( isset( $_POST['sid'] ) ) {

  // -----------------------------------------------------
  // If this was successfull you can add a the user you
  // wish to create
  // -----------------------------------------------------
  $regReq = new deleteProjectRequest();
  $regReq->auth = $_POST['sid'];
  $regReq->project = $_POST['project'];


  echo "<HR/>";
  echo "Delete project " . $_POST['project'] . "...<BR/>";

  try {

    $deleteResponse = $soapSystem->deleteProject( $regReq );


    if( $deleteResponse->result ) {

      echo "YES.<BR>";

    }
    else {

      echo "NO<BR>";

    }

  }
  catch( SoapFault $f ) {

    echo "SOAP FAULT!: " . serialize( $f );

  }

}


echo "<FORM action=\"deleteProject.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
echo "SID: <INPUT type=\"text\" name=\"sid\" value=\"\"><BR>\n";
echo "Operation: <INPUT type=\"text\" name=\"project\" value=\"\"><BR>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";

?>
