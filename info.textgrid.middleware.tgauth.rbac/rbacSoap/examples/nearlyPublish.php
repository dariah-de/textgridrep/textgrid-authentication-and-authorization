<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 05.09.2011
// Modification date: 05.09.2011
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra = new SoapClient( "../wsdl/tgextra-crud.wsdl" );


echo "<BODY><HTML>";




if( isset( $_POST['auth'] ) ) {

  // -----------------------------------------------------
  // If this was successfull you can add a the user you
  // wish to create
  // -----------------------------------------------------
  $pubReq = new stdClass();
  $pubReq->auth = $_POST['auth'];
  $pubReq->log = "";
  $pubReq->resource = $_POST['resource'];


  echo "<HR/>";
  echo "Publishing resource...<BR/>";

  try {

    $response = $soapExtra->nearlyPublish( $pubReq );

    if( $response->result ) {

      echo "DONE";

    }
    else {

     echo "UNABLE TO COMMIT...";

    }

  }
  catch( SoapFault $f ) {

    echo serialize( $f );
    echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

  }

}


echo "<FORM action=\"nearlyPublish.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
echo "Auth: <INPUT type=\"text\" name=\"auth\" value=\"\"><BR>\n";
echo "Resource: <INPUT type=\"text\" name=\"resource\" value=\"\"><BR>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";

?>
