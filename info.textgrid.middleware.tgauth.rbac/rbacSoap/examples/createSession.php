<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 08.07.2007
// Modification date: 02.08.2007
// Version: 0.1.2
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra = new SoapClient( "https://textgridlab.org/1.0/tgauth/wsdl/tgextra.wsdl" );
$soapSystem = new SoapClient( "https://textgridlab.org/1.0/tgauth/wsdl/tgsystem.wsdl" );

echo "<BODY><HTML>";



// -----------------------------------------------------
// Before you can create a session you have to
// authenticate. If this was successful you get a
// session-ID that you should keep
// -----------------------------------------------------
$authReq = new authenticateRequest();
$authReq->username = "shibConnector@application.int";
$authReq->password = "secret";


echo "<HR/>";
echo "Doing authentication...<BR/>";

try {

  $authResponse = $soapExtra->authenticate( $authReq );

  if( preg_match( "/[0-9a-z]{2,}/i", $authResponse->sid ) ) {

    echo "DONE: " . $authResponse->sid . "<BR/>";

  }

}
catch( SoapFault $f ) {

  echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

}



// -----------------------------------------------------
// Now you can try to add an active role to your session
// -----------------------------------------------------
$addRoleReq = new addActiveRoleRequest();
$addRoleReq->username = "shibConnector@application.int";
$addRoleReq->role = "sessionCreator,Anwendung";
$addRoleReq->sid = $authResponse->sid;

echo "<HR/>";
echo "Adding active role...<BR/>";

try {

  $addRoleResponse = $soapSystem->addActiveRole( $addRoleReq );

  if( $addRoleResponse->result ) {

    echo "DONE.<BR/>";

  }

}
catch( SoapFault $f ) {

  echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

}


// -----------------------------------------------------
// If this was successfull you have to add a apropriate
// role to your active session that allows you to create
// a session for someone else.
// -----------------------------------------------------
$creReq = new createSessionRequest();
$creReq->intSid = $authResponse->sid;
$creReq->username = "mhaase@uni-tuebingen.de";
$creReq->roleset = Array( "Projekt-Teilnehmer" );
$creReq->sid = "ABcDEFG";

echo "<HR/>";
echo "Creating the session...<BR/>";

try {

  $creResponse = $soapSystem->createSession( $creReq );

  if( $creResponse->result ) {

    echo "DONE.<BR/>";

  }

}
catch( SoapFault $f ) {

  echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

}


echo "</BODY></HTML>";

?>
