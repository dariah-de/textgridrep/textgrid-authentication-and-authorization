<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 18.07.2007
// Modification date: 18.07.2007
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra = new SoapClient( "../wsdl/tgextra.wsdl" );


echo "<BODY><HTML>";




if( isset( $_POST['auth'] ) ) {

  // -----------------------------------------------------
  // If this was successfull you can add a the user you
  // wish to create
  // -----------------------------------------------------
  $leaReq = new getLeaderRequest();
  $leaReq->auth = $_POST['auth'];
  $leaReq->log = "";
  $leaReq->project = $_POST['project'];


  echo "<HR/>";
  echo "Searching leader...<BR/>";

  try {

    $response = $soapExtra->getLeader( $leaReq );

    if( is_array( $response->username ) ) {

      for( $i = 0; $i < sizeof( $response->username ); $i++ ) {

        echo $response->username[$i] . "<BR>";

      }

    }
    elseif( preg_match( "/.+/", $response->username ) ) {

     echo $response->username;

    }
    else {

      echo "No leader found!<BR>";

    }

  }
  catch( SoapFault $f ) {

    echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

  }

}


echo "<FORM action=\"getLeader.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
echo "Auth: <INPUT type=\"text\" name=\"auth\" value=\"\"><BR>\n";
echo "Project-Name: <INPUT type=\"text\" name=\"project\" value=\"\"><BR>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";

?>
