<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 18.07.2007
// Modification date: 18.07.2008
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );

// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra = new SoapClient( "../wsdl/tgextra.wsdl" );




echo "<HTML><BODY>";
echo "<HEAD><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></HEAD>\n";


if( isset( $_POST['auth'] ) ) {

  // -----------------------------------------------------
  // If this was successfull you can add a the user you
  // wish to create
  // -----------------------------------------------------
  $setNameReq = new setNameRequest();
  $setNameReq->auth = $_POST['auth'];
  $setNameReq->log = "";
  $setNameReq->webAuthSecret = $_POST['webAuthSecret'];
  $setNameReq->name = $_POST['name'];
  $setNameReq->mail = $_POST['mail'];
  $setNameReq->organisation = $_POST['organisation'];
  $setNameReq->agreeSearch = isset($_POST['agreeSearch']) ? TRUE : FALSE;

  echo serialize($_POST);

  echo "<HR/>";
  echo "Setting name...<BR/>";

  try {

    $setNameResponse = $soapExtra->setName( $setNameReq );

    if( $setNameResponse ) {

      echo "DONE.";

    }
    else {

      echo "UNABLE to set Name!<BR>";

    }

  }
  catch( SoapFault $f ) {

    echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

  }

}


echo "<FORM action=\"setName.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
echo "Auth: <INPUT type=\"text\" name=\"auth\" value=\"\"><BR>\n";
echo "WebAuth Secret: <INPUT type=\"text\" name=\"webAuthSecret\" value=\"\"><BR>\n";
echo "Name: <INPUT type=\"text\" name=\"name\" value=\"\"><BR>\n";
echo "Mail: <INPUT type=\"text\" name=\"mail\" value=\"\"><BR>\n";
echo "Organisation: <INPUT type=\"text\" name=\"organisation\" value=\"\"><BR>\n";
echo "Agree to be Searchable: <INPUT type=\"checkbox\" name=\"agreeSearch\" value=\"yes\"><BR>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";

?>
