<?php
// #######################################################
// Author: Martin Haase
// Creation date: 15.10.2010
// Modification date: XX
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra = new SoapClient( "../wsdl/tgextra.wsdl" );


echo "<BODY><HTML>";



// -----------------------------------------------------
// How to get a session-ID from the RBAC-system
// -----------------------------------------------------
echo "<HR/>";
echo "Asking for attributes...<BR/>";

try {

  $getSupportedUserAttributesResponse = $soapExtra->getSupportedUserAttributes();

  $a = $getSupportedUserAttributesResponse->attribute; 	

  echo serialize ($a);	
}
catch( SoapFault $f ) {

  echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

}


echo "</BODY></HTML>";

?>
