<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 18.07.2007
// Modification date: 18.07.2007
// Version: 0.1.0
// #######################################################


require_once( "../xacmlTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapXACML = new SoapClient( "../wsdl/xacml.wsdl", Array( "trace" => 1 ) );


echo "<BODY><HTML>";




if( isset( $_POST['session'] ) ) {

  // -----------------------------------------------------
  // If this was successfull you can add a the user you
  // wish to create
  // -----------------------------------------------------

  $regReq = new stdClass();

  $regReq->Version = "2.0";
  $regReq->ID = "abcde1234";
  $regReq->ReturnContext = true;
  $regReq->Request = new stdClass();
  $regReq->Request->Subject = new stdClass();
  $regReq->Request->Resource = new stdClass();
  $regReq->Request->Action = new stdClass();
  $regReq->Request->Environment = new stdClass();

  $regReq->Request->Subject->Attribute = new stdClass();
  $regReq->Request->Subject->Attribute->AttributeId = "urn:oasis:names:tc:xacml:1.0:subject:subject-id";
  $regReq->Request->Subject->Attribute->DataType = "http://www.w3.org/2001/XMLSchema#string";
  $regReq->Request->Subject->Attribute->AttributeValue = new stdClass();
  $regReq->Request->Subject->Attribute->AttributeValue->any = $_POST['session'];

  $regReq->Request->Resource->Attribute = new stdClass();
  $regReq->Request->Resource->Attribute->AttributeId = "urn:oasis:names:tc:xacml:1.0:resource:resource-id";
  $regReq->Request->Resource->Attribute->DataType = "http://www.w3.org/2001/XMLSchema#anyURI";
  $regReq->Request->Resource->Attribute->AttributeValue = new stdClass();
  $regReq->Request->Resource->Attribute->AttributeValue->any = $_POST['resource'];

  $regReq->Request->Action->Attribute = new stdClass();
  $regReq->Request->Action->Attribute->AttributeId = "urn:oasis:names:tc:xacml:1.0:action:action-id";
  $regReq->Request->Action->Attribute->DataType = "http://www.w3.org/2001/XMLSchema#string";
  $regReq->Request->Action->Attribute->AttributeValue = new stdClass();
  $regReq->Request->Action->Attribute->AttributeValue->any = $_POST['operation'];


  echo "<HR/>";
  echo "checking access...<BR/>";
  echo "Look at the code to see what happens!<BR/>";


  try {

    $caResponse = $soapXACML->checkXACMLaccess( $regReq );

    echo "\n\n" . $soapXACML->__getLastRequest();
    echo "\n\n" . $soapXACML->__getLastResponse() . "\n\n";

    if( preg_match( "/^permit$/i", $caResponse->Response->Result->Decision ) ) {

      echo "<BR><HR><BR>Granted: YES.<BR><HR><BR>";

    }
    else {

      echo "<BR><HR><BR>Granted: NO.<BR><HR><BR>";

    }

  }
  catch( SoapFault $f ) {

    echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

  }

}


echo "<FORM action=\"xacmlCheckAccess.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
echo "Session: <INPUT type=\"text\" name=\"session\" value=\"\"><BR>\n";
echo "Resource: <INPUT type=\"text\" name=\"resource\" value=\"\"><BR>\n";
echo "Operation: <INPUT type=\"text\" name=\"operation\" value=\"\"><BR>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";
?>
