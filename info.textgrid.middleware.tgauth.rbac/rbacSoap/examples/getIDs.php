<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 07.04.2008
// Modification date: 07.04.2008
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra =  new SoapClient( "../wsdl/tgextra.wsdl" );


echo "<BODY><HTML>";




if( isset( $_POST['auth'] ) ) {

  // -----------------------------------------------------
  // If this was successfull you can add a the user you
  // wish to create
  // -----------------------------------------------------
  $opReq = new getIDsRequest();
  $opReq->auth = $_POST['auth'];
  $opReq->name = $_POST['name'];
  $opReq->mail = $_POST['mail'];
  $opReq->organisation = $_POST['o'];
  $opReq->log = "";

  echo "<HR/>";
  echo "Searching for User Records...<BR/>";

  try {

    $getFrResponse = $soapExtra->getIDs($opReq);
//    echo serialize ($getFrResponse);


   if( is_array( $getFrResponse->userdetails ) ) {
    

    echo "ePPN / Name / Mail / Affiliation / Searchable? / Voluntary Data? <BR>";
    for( $i = 0; $i < sizeof( $getFrResponse->userdetails ); $i++ ) {
      $ud = $getFrResponse->userdetails[$i];

      echo $ud->ePPN . " / ".$ud->name . " / ".$ud->mail . " / ".$ud->organisation . " / ". (isset($ud->agreesearch) ?( $ud->agreesearch?"yes":"no"):"") . " / ". (isset( $ud->usersupplieddata)?($ud->usersupplieddata?"yes":"no"):"") . "<BR>";

    }

  }
  elseif( $getFrResponse->userdetails instanceof stdClass ) {
      $ud = $getFrResponse->userdetails;
      echo "ePPN / Name / Mail / Affiliation / Searchable? / Voluntary Data? <BR>";
      echo $ud->ePPN . " / ".$ud->name . " / ".$ud->mail . " / ".$ud->organisation . " / ". (isset($ud->agreesearch) ?( $ud->agreesearch?"yes":"no"):"") . " / ". (isset( $ud->usersupplieddata)?($ud->usersupplieddata?"yes":"no"):"") . "<BR>";
  }
  else {

    echo "No Records :( !<BR/>";

  }

  }
    catch( SoapFault $f ) {

      echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

  }
}

echo "<FORM action=\"getIDs.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
echo "Auth: <INPUT type=\"text\" name=\"auth\" value=\"\"><BR>\n";
echo "Name (optional): <INPUT type=\"text\" name=\"name\" value=\"\"><BR>\n";
echo "Mail (optional): <INPUT type=\"text\" name=\"mail\" value=\"\"><BR>\n";
echo "Organisation (optional): <INPUT type=\"text\" name=\"o\" value=\"\"><BR>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";

?>
