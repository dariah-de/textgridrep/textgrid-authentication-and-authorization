<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 07.04.2008
// Modification date: 07.04.2008
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapSystem = new SoapClient( "../wsdl/tgextra.wsdl" );


echo "<BODY><HTML>";




if( isset( $_POST['auth'] ) ) {

  // -----------------------------------------------------
  // If this was successfull you can add a the user you
  // wish to create
  // -----------------------------------------------------
  $opReq = new getMembersRequest();
  $opReq->auth = $_POST['auth'];
  $opReq->username = $_POST['username'];
  $opReq->log = "";
  $opReq->resource = $_POST['resource'];


  echo "<HR/>";
  echo "Searching allowed operations...<BR/>";

  try {

    $opResponse = $soapSystem->getRights( $opReq );

    if( is_array( $opResponse->operation ) ) {

      for( $i = 0; $i < sizeof( $opResponse->operation ); $i++ ) {

        echo $opResponse->operation[$i] . "<BR>";

      }

    }
    elseif( preg_match( "/.+/", $opResponse->operation ) ) {

      echo $opResponse->operation;

    }
    else {

      echo "No operations found!<BR>";

    }

  }
  catch( SoapFault $f ) {

    echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

  }

}


echo "<FORM action=\"getRights.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
echo "Auth: <INPUT type=\"text\" name=\"auth\" value=\"\"><BR>\n";
echo "Resource: <INPUT type=\"text\" name=\"resource\" value=\"\"><BR>\n";
echo "Username: <INPUT type=\"text\" name=\"username\" value=\"\"><BR/>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";

?>
