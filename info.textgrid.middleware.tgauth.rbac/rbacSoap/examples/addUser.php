<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 18.07.2007
// Modification date: 18.07.2007
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra = new SoapClient( "http://rbac.textgrid.daasi.de/wsdl/tgextra.wsdl" );
$soapSystem = new SoapClient( "http://rbac.textgrid.daasi.de/wsdl/tgsystem.wsdl" );
$soapAdministration = new SoapClient( "http://rbac.textgrid.daasi.de/wsdl/tgadministration.wsdl" );


echo "<BODY><HTML>";



// -----------------------------------------------------
// Before you can add a user you have to
// authenticate. If this was successful you get a
// session-ID that you should keep
// -----------------------------------------------------
$authReq = new authenticateRequest();
$authReq->username = "sp00001@textgrid.de";
$authReq->password = "secret";


echo "<HR/>";
echo "Doing authentication...<BR/>";

try {

  $authResponse = $soapExtra->authenticate( $authReq );

  if( preg_match( "/[0-9a-z]{2,}/i", $authResponse->sid ) ) {

    echo "DONE: " . $authResponse->sid . "<BR/>";

  }

}
catch( SoapFault $f ) {

  echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

}



// -----------------------------------------------------
// Now you can try to add an active role to your session
// -----------------------------------------------------
$addRoleReq = new addActiveRoleRequest();
$addRoleReq->username = "sp00001@textgrid.de";
$addRoleReq->role = "serviceProvider";
$addRoleReq->sid = $authResponse->sid;

echo "<HR/>";
echo "Adding active role...<BR/>";

try {

  $addRoleResponse = $soapSystem->addActiveRole( $addRoleReq );

  if( $addRoleResponse->result ) {

    echo "DONE.<BR/>";

  }

}
catch( SoapFault $f ) {

  echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

}


// -----------------------------------------------------
// If this was successfull you can add a the user you
// wish to create
// -----------------------------------------------------
$addUserReq = new addUserRequest();
$addUserReq->intSid = $authResponse->sid;
//$addUserReq->username = "mwidmer@uni-tuebingen.de";
$addUserReq->username = "ShibConnector@application.int";
$addUserReq->password = "secret";

echo "<HR/>";
echo "Adding user...<BR/>";

try {

  $addUserResponse = $soapAdministration->addUser( $addUserReq );

  if( $addUserResponse->result ) {

    echo "DONE.<BR/>";

  }

}
catch( SoapFault $f ) {

  echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

}


echo "</BODY></HTML>";

?>
