<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 18.07.2007
// Modification date: 18.07.2007
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra = new SoapClient( "../wsdl/tgextra.wsdl" );


echo "<BODY><HTML>";


// -----------------------------------------------------
// If this was successfull you have to add a apropriate
// role to your active session that allows you to create
// a session for someone else.
// -----------------------------------------------------
echo "<HR/>";
echo "Listing all projects...<BR/>";

try {

  $getProResponse = $soapExtra->getAllProjects();

  if( is_array( $getProResponse->project ) ) {

    for( $i = 0; $i < sizeof( $getProResponse->project ); $i++ ) {

      echo $getProResponse->project[$i]->id . " / " . $getProResponse->project[$i]->name . " / " . $getProResponse->project[$i]->description . "<BR>";

    }

  }
  elseif( $getProResponse->project instanceof project ) {

    echo $getProResponse->project->id . " / " . $getProResponse->project->name . " / " . $getProResponse->project->description . "<BR>";

  }
  else {

    echo "No projects!<BR/>";

  }

}
catch( SoapFault $f ) {

  echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

}


echo "</BODY></HTML>";

?>
