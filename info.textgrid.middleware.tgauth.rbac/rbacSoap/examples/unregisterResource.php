<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 17.03.2008
// Modification date: 17.03.2008
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra = new SoapClient( "http://rbac.textgrid.daasi.de/wsdl/tgextra.wsdl" );


echo "<BODY><HTML>";




if( isset( $_POST['auth'] ) ) {

  // -----------------------------------------------------
  // If this was successfull you can add a the user you
  // wish to create
  // -----------------------------------------------------
  $regReq = new unregisterResourceRequest();
  $regReq->auth = $_POST['auth'];
  $regReq->log = "";
  $regReq->uri = $_POST['uri'];


  echo "<HR/>";
  echo "Unregistering resource...<BR/>";

  try {

    $registerResourceResponse = $soapExtra->unregisterResource( $regReq );

    if( $registerResourceResponse->result ) {

      echo "DONE.<BR>";

    }
    else {

      echo "UNABLE to commit!<BR>";

    }

  }
  catch( SoapFault $f ) {

    echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

  }

}


echo "<FORM action=\"unregisterResource.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
echo "Auth: <INPUT type=\"text\" name=\"auth\" value=\"\"><BR>\n";
echo "URI: <INPUT type=\"text\" name=\"uri\" value=\"\"><BR>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";

?>
