<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 07.04.2008
// Modification date: 07.04.2008
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapSystem = new SoapClient( "../wsdl/tgextra.wsdl" );


echo "<BODY><HTML>";




if( isset( $_POST['project'] ) ) {

  // -----------------------------------------------------
  // If this was successfull you can add a the user you
  // wish to create
  // -----------------------------------------------------
  $memReq = new getMembersRequest();
  $memReq->auth = $_POST['auth'];
  $memReq->log = "";
  $memReq->project = $_POST['project'];


  echo "<HR/>";
  echo "Searching members for project...<BR/>";

  try {

    $memResponse = $soapSystem->getMembers( $memReq );

    if( is_array( $memResponse->username ) ) {

      for( $i = 0; $i < sizeof( $memResponse->username ); $i++ ) {

        echo $memResponse->username[$i] . "<BR>";

      }

    }
    elseif( preg_match( "/.+/", $memResponse->username ) ) {

      echo $memResponse->username;

    }
    else {

      echo "No members found!<BR>";

    }

  }
  catch( SoapFault $f ) {

    echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

  }

}


echo "<FORM action=\"getMembers.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
echo "Auth: <INPUT type=\"text\" name=\"auth\" value=\"\"><BR>\n";
echo "project: <INPUT type=\"text\" name=\"project\" value=\"\"><BR>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";

?>
