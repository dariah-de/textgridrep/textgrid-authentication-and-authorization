<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 05.08.2007
// Modification date: 05.08.2007
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra = new SoapClient( "../wsdl/tgextra.wsdl" );
//$soapExtra = new SoapClient( "http://textgrid.regengedanken.de/rbacSoap/wsdl/tgextra.wsdl",
//                           Array( 'proxy_host' => "134.2.217.67", 'proxy_port' => 7777 ) );


echo "<BODY><HTML>";



// -----------------------------------------------------
// How to get a session-ID from the RBAC-system
// -----------------------------------------------------
echo "<HR/>";
echo "Asking for a session-ID...<BR/>";

try {

  $getSidResponse = $soapExtra->getSid();

  if( $getSidResponse->sid ) {

    echo "DONE: " . $getSidResponse->sid . "<BR/>";

  }

}
catch( SoapFault $f ) {

  echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

}


echo "</BODY></HTML>";

?>
