<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 18.03.2008
// Modification date: 18.03.2008
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra = new SoapClient( "../wsdl/tgextra.wsdl" );


echo "<BODY><HTML>";




if( isset( $_POST['auth'] ) ) {

  // -----------------------------------------------------
  // If this was successfull you can add a the user you
  // wish to create
  // -----------------------------------------------------
  $regReq = new deleteMemberRequest();
  $regReq->auth = $_POST['auth'];
  $regReq->log = "";
  $regReq->role = $_POST['role'];
  $regReq->username = $_POST['username'];


  echo "<HR/>";
  echo "Removing member...<BR/>";

  try {

    $addMemberResponse = $soapExtra->deleteMember( $regReq );

    if( $addMemberResponse->result ) {

      echo "DONE.<BR>";

    }
    else {

      echo "UNABLE to commit!<BR>";

    }

  }
  catch( SoapFault $f ) {

    echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

  }

}


echo "<FORM action=\"deleteMember.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
echo "Auth: <INPUT type=\"text\" name=\"auth\" value=\"\"><BR>\n";
echo "Rolle: <INPUT type=\"text\" name=\"role\" value=\"\"><BR>\n";
echo "Benutzer (eppn): <INPUT type=\"text\" name=\"username\" value=\"\"><BR>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";

?>
