<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 18.07.2007
// Modification date: 18.07.2007
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapSystem = new SoapClient( "../wsdl/tgextra.wsdl", Array( "trace" => true ) );


echo "<BODY><HTML>";




if( isset( $_POST['auth'] ) ) {

  // -----------------------------------------------------
  // If this was successfull you can add a the user you
  // wish to create
  // -----------------------------------------------------
  $regReq = new tgCheckAccessRequest();
  $regReq->auth = $_POST['auth'];
  $reqReq->log = "";
//  $regReq->sid = $_POST['sid'];
  $regReq->resource = $_POST['resource'];
  $regReq->operation = $_POST['operation'];


  echo "<HR/>";
  echo "Checking access...<BR/>";

  try {

    $checkResponse = $soapSystem->tgCheckAccess( $regReq );

    echo $soapSystem->__getLastRequest() . "\n\n";
    echo $soapSystem->__getLastResponse();

    if( $checkResponse->result ) {

      echo "YES.<BR>";

    }
    else {

      echo "NO<BR>";

    }

  }
  catch( SoapFault $f ) {

    echo "SOAP FAULT!: " . serialize ($f);

  }

}


echo "<FORM action=\"tgCheckAccess.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
echo "Auth: <INPUT type=\"text\" name=\"auth\" value=\"\"><BR>\n";
echo "Operation: <INPUT type=\"text\" name=\"operation\" value=\"\"><BR>\n";
echo "Resource: <INPUT type=\"text\" name=\"resource\" value=\"\"><BR>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";

?>
