<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 18.07.2007
// Modification date: 18.07.2008
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------


echo "<HTML><BODY>";
echo "<HEAD><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></HEAD>\n";



  $soapExtra = new SoapClient( "../wsdl/tgextra.wsdl" );


if( isset( $_POST['auth'] ) ) {

  // -----------------------------------------------------
  // If this was successfull you can add a the user you
  // wish to create
  // -----------------------------------------------------
  $creReq = new createProjectRequest();
  $creReq->auth = $_POST['auth'];
  $creReq->log = "";
  $creReq->name = $_POST['name'];
  $creReq->description = $_POST['description'];


  echo "<HR/>";
  echo "Creating project...<BR/>";

  try {

    $creResponse = $soapExtra->createProject( $creReq );

    if( $creResponse->projectId ) {

      echo "DONE: " . $creResponse->projectId . "<BR>";

    }
    else {

      echo "UNABLE to create new project!<BR>";

    }

  }
  catch( SoapFault $f ) {

    echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

  }

}


echo "<FORM action=\"createProject.php\" method=\"post\" enctype=\"multipart/form-data\">\n";

echo "</SELECT><BR>\n";
echo "Auth: <INPUT type=\"text\" name=\"auth\" value=\"\"><BR>\n";
echo "Project name: <INPUT type=\"text\" name=\"name\" value=\"\"><BR>\n";
echo "Project description: <INPUT type=\"text\" name=\"description\" value=\"\"><BR>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";

?>
