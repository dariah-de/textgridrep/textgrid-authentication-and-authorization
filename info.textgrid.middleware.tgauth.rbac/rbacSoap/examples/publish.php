<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 07.04.2008
// Modification date: 07.04.2008
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra = new SoapClient( "../wsdl/tgextra.wsdl" );


echo "<BODY><HTML>";




if( isset( $_POST['auth'] ) ) {

  // -----------------------------------------------------
  // If this was successfull you can add a the user you
  // wish to create
  // -----------------------------------------------------
  $pubReq = new publishRequest();
  $pubReq->auth = $_POST['auth'];
  $pubReq->log = "";
  $pubReq->resource = $_POST['resource'];


  echo "<HR/>";
  echo "Publishing resource...<BR/>";

  try {

    $response = $soapExtra->publish( $pubReq );

    if( $response->result ) {

      echo "DONE";

    }
    else {

     echo "UNABLE TO COMMIT...";

    }

  }
  catch( SoapFault $f ) {

    echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

  }

}


echo "<FORM action=\"publish.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
echo "Auth: <INPUT type=\"text\" name=\"auth\" value=\"\"><BR>\n";
echo "Resource: <INPUT type=\"text\" name=\"resource\" value=\"\"><BR>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";

?>
