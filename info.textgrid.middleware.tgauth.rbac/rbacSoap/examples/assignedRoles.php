<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 23.07.2007
// Modification date: 23.07.2007
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra = new SoapClient( "http://rbac.textgrid.daasi.de/wsdl/tgextra.wsdl" );
$soapSystem = new SoapClient( "http://rbac.textgrid.daasi.de/wsdl/tgsystem.wsdl" );
$soapReview = new SoapClient( "http://rbac.textgrid.daasi.de/wsdl/tgreview.wsdl" );


echo "<BODY><HTML>";



// -----------------------------------------------------
// Before you can create a session you have to
// authenticate. If this was successful you get a
// session-ID that you should keep
// -----------------------------------------------------
$authReq = new authenticateRequest();
$authReq->username = "shibconnector@application.int";
$authReq->password = "secret";


echo "<HR/>";
echo "Doing authentication...<BR/>";

try {

  $authResponse = $soapExtra->authenticate( $authReq );

  if( preg_match( "/[0-9a-z]{2,}/i", $authResponse->sid ) ) {

    echo "DONE: " . $authResponse->sid . "<BR/>";

  }

}
catch( SoapFault $f ) {

  echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

}



// -----------------------------------------------------
// Now you can try to add an active role to your session
// -----------------------------------------------------
$addRoleReq = new addActiveRoleRequest();
$addRoleReq->username = "shibconnector@application.int";
$addRoleReq->role = "sessionCreator,Anwendung";
$addRoleReq->auth = $authResponse->auth;

echo "<HR/>";
echo "Adding active role...<BR/>";

try {

  $addRoleResponse = $soapExtra->tgAddActiveRole( $addRoleReq );

  if( $addRoleResponse->result ) {

    echo "DONE.<BR/>";

  }

}
catch( SoapFault $f ) {

  echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

}


// -----------------------------------------------------
// If this was successfull you have to add a apropriate
// role to your active session that allows you to create
// a session for someone else.
// -----------------------------------------------------
$arReq = new assignedRolesRequest();
$arReq->intSid = $authResponse->auth;
$arReq->username = "testuser@textgrid.de";

echo "<HR/>";
echo "The roles of mhaase@uni-tuebingen.de...<BR/>";

try {

  $rolesetResponse = $soapReview->assignedRoles( $arReq );

  if( is_array( $rolesetResponse->role ) ) {

    for( $i = 0; $i < sizeof( $rolesetResponse->role ); $i++ ) {

      echo "Role " . $i . ": " . $rolesetResponse->role[$i] . "<BR/>";

    }

  }
  else {

    echo "Role 0: " . $rolesetResponse->role . "<BR/>";

  }

}
catch( SoapFault $f ) {

  echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

}


echo "</BODY></HTML>";

?>
