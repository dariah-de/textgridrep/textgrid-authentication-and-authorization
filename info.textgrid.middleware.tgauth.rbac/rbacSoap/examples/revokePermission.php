<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 06.08.2007
// Modification date: 06.08.2007
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra = new SoapClient( "http://textgrid.regengedanken.de/rbacSoap/wsdl/tgextra.wsdl" );
$soapSystem = new SoapClient( "http://textgrid.regengedanken.de/rbacSoap/wsdl/tgsystem.wsdl" );
$soapAdministration = new SoapClient( "http://textgrid.regengedanken.de/rbacSoap/wsdl/tgadministration.wsdl" );


echo "<BODY><HTML>";



// -----------------------------------------------------
// Before you can create a role you have to
// authenticate. If this was successful you get a
// session-ID that you should keep
// -----------------------------------------------------
$authReq = new authenticateRequest();
$authReq->username = "sp00001@textgrid.de";
$authReq->password = "secret";


echo "<HR/>";
echo "Doing authentication...<BR/>";

try {

  $authResponse = $soapExtra->authenticate( $authReq );

  if( preg_match( "/[0-9a-z]{2,}/i", $authResponse->sid ) ) {

    echo "DONE: " . $authResponse->sid . "<BR/>";

  }

}
catch( SoapFault $f ) {

  echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

}



// -----------------------------------------------------
// Now you can try to add an active role to your session
// -----------------------------------------------------
$addRoleReq = new addActiveRoleRequest();
$addRoleReq->username = "sp00001@textgrid.de";
$addRoleReq->role = "serviceProvider";
$addRoleReq->sid = $authResponse->sid;

echo "<HR/>";
echo "Adding active role...<BR/>";

try {

  $addRoleResponse = $soapSystem->addActiveRole( $addRoleReq );

  if( $addRoleResponse->result ) {

    echo "DONE.<BR/>";

  }

}
catch( SoapFault $f ) {

  echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

}


// -----------------------------------------------------
// If this was successfull you can try to grant a permission
// to a role.
// -----------------------------------------------------
$revPermReq = new revokePermissionRequest();
$revPermReq->intSid = $authResponse->sid;
$revPermReq->resource = "ingrid.daasi.de//demo/tg-demo.xml";
$revPermReq->operation = "read";
$revPermReq->role = "Testrolle";

echo "<HR/>";
echo "Revoking permission read for role testRole...<BR/>";

try {

  $revPermResponse = $soapAdministration->revokePermission( $revPermReq );

  if( $revPermResponse->result ) {

    echo "DONE.<BR/>";

  }

}
catch( SoapFault $f ) {

  echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

}


echo "</BODY></HTML>";

?>
