<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 17.08.2007
// Modification date: 30.08.2007
// Version: 0.1.1
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra = new SoapClient( "http://textgrid.regengedanken.de/rbacSoap/wsdl/tgextra.wsdl" );
$soapSystem = new SoapClient( "http://textgrid.regengedanken.de/rbacSoap/wsdl/tgsystem.wsdl" );
$soapReview = new SoapClient( "http://textgrid.regengedanken.de/rbacSoap/wsdl/tgreview.wsdl" );


echo "<BODY><HTML>";



// -----------------------------------------------------
// Before you can create a session you have to
// authenticate. If this was successful you get a
// session-ID that you should keep
// -----------------------------------------------------
$authReq = new authenticateRequest();
$authReq->username = "sp00001@textgrid.de";
$authReq->password = "secret";


echo "<HR/>";
echo "Doing authentication...<BR/>";

try {

  $authResponse = $soapExtra->authenticate( $authReq );

  if( preg_match( "/[0-9a-z]{2,}/i", $authResponse->sid ) ) {

    echo "DONE: " . $authResponse->sid . "<BR/>";

  }

}
catch( SoapFault $f ) {

  echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

}



// -----------------------------------------------------
// Now you can try to add an active role to your session
// -----------------------------------------------------
$addRoleReq = new addActiveRoleRequest();
$addRoleReq->username = "sp00001@textgrid.de";
$addRoleReq->role = "serviceProvider";
$addRoleReq->sid = $authResponse->sid;

echo "<HR/>";
echo "Adding active role...<BR/>";

try {

  $addRoleResponse = $soapSystem->addActiveRole( $addRoleReq );

  if( $addRoleResponse->result ) {

    echo "DONE.<BR/>";

  }

}
catch( SoapFault $f ) {

  echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

}


// -----------------------------------------------------
// If this was successfull you can ask the RBAC-system
// form the operations a role may do on a resource.
// -----------------------------------------------------
$roooReq = new roleOperationsOnObjectRequest();
$roooReq->intSid = $authResponse->sid;
$roooReq->role = "Projektleiter,Projekt-1,Projekt-Teilnehmer";
$roooReq->resource = "ingrid.daasi.de//demo/tg-demo.xml";

echo "<HR/>";
echo "The allowed operations of roles Projektleiter,Projekt-1,Projekt-Teilnehmer on resource ingrid.daasi.de//demo/tg-demo.xml...<BR/>";

try {

  $operationsetResponse = $soapReview->roleOperationsOnObject( $roooReq );

  if( is_array( $operationsetResponse->operationset ) ) {

    for( $i = 0; $i < sizeof( $operationsetResponse->operationset ); $i++ ) {

      echo "Operation " . $i . ": " . $operationsetResponse->operationset[$i] . "<BR/>";

    }

  }
  else {

    echo "Operation 0: " . $operationsetResponse->operationset . "<BR/>";

  }

}
catch( SoapFault $f ) {

  echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

}


echo "</BODY></HTML>";

?>
