<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 18.10.2007
// Modification date: 18.10.2007
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );


// -----------------------------------------------------
// // You'll need these services
// -----------------------------------------------------
//$soapExtra = new SoapClient( "http://rbac.textgrid.daasi.de/wsdl/tgextra.wsdl" );
$soapExtra = new SoapClient( "https://test.textgridlab.org/1.0/tgauth/wsdl/tgextra.wsdl",
[
	'features'=>SOAP_USE_XSI_ARRAY_TYPE,
]
);


$soapSystem = new SoapClient( "https://test.textgridlab.org/1.0/tgauth/wsdl/tgsystem.wsdl",
[
        'features'=>SOAP_USE_XSI_ARRAY_TYPE,
]
);

//$soapSystem = new SoapClient( "http://rbac.textgrid.daasi.de/wsdl/tgsystem.wsdl" );
$soapReview = new SoapClient( "https://test.textgridlab.org/1.0/tgauth/wsdl/tgreview.wsdl",
[
        'features'=>SOAP_USE_XSI_ARRAY_TYPE,
]
);

//$soapReview = new SoapClient( "http://rbac.textgrid.daasi.de/wsdl/tgreview.wsdl" );


echo "<BODY><HTML>";



// -----------------------------------------------------
// Before you can create a session you have to
// authenticate. If this was successful you get a
// session-ID that you should keep
// -----------------------------------------------------
$authReq = new authenticateRequest();
$authReq->username = "shibconnector@application.int";
$authReq->password = "secret";


echo "<HR/>";
echo "Doing authentication...<BR/>";

try {

  $authResponse = $soapExtra->authenticate( $authReq );

  if( preg_match( "/[0-9a-z]+/i", $authResponse->auth ) ) {

    echo "DONE: " . $authResponse->auth . "<BR/>";

  }
  else {

    echo "FAILED!: " . serialize( $authResponse ) . "<BR/>";

  }

}
catch( SoapFault $f ) {

	echo "SOAP FAULT!0: " . $f->faultcode . " / " .$f->faultstring ;
	if (is_object($f->detail)) {
            echo " / Detail: " . json_encode($f->detail);
           } else {
            echo " / Detail: " . $f->detail;
          }   

}



// -----------------------------------------------------
// Now you can try to add an active role to your session
// -----------------------------------------------------
$addRoleReq = new addActiveRoleRequest();
$addRoleReq->username = "shibconnector@application.int";
$addRoleReq->role = "sessionCreator,Anwendung";
$addRoleReq->auth = $authResponse->auth;
print_r($addRoleReq);
echo "<HR/>";
echo "Adding active role...<BR/>";

try {

  $addRoleResponse = $soapExtra->tgAddActiveRole( $addRoleReq );

  if( $addRoleResponse->result ) {

    echo "DONE.<BR/>";

  }

}
catch( SoapFault $f ) {

  echo "SOAP FAUL !1: " . $f->faultcode . " / " . $f->faultstring ;
  if (is_object($f->detail)) {
            echo " / Detail: " . json_encode($f->detail);
           } else {
            echo " / Detail: " . $f->detail;
          }

}


// -----------------------------------------------------
// If this was successfull you have to add a apropriate
// role to your active session that allows you to create
// a session for someone else.
// -----------------------------------------------------
$arReq = new authorizedRolesRequest();

$arReq->intSid = $authResponse->auth;
$arReq->username = $remote_user;


echo "<HR/>";
echo "The roles of testuser@textgrid.de...<BR/>";

try {

  $rolesetResponse = $soapReview->authorizedRoles( $arReq );

  if( is_array( $rolesetResponse->role ) ) {

    for( $i = 0; $i < sizeof( $rolesetResponse->role ); $i++ ) {

      echo "Role " . $i . ": " . $rolesetResponse->role[$i] . "<BR/>";

    }

  }
  else {

    echo "Role 0: " . $rolesetResponse->role . "<BR/>";

  }

}
catch( SoapFault $f ) {

	echo "SOAP FAULT!:2 " . $f->faultcode . " / " . $f->faultstring ;
	 if (is_object($f->detail)) {
            echo " / Detail: " . json_encode($f->detail);
           } else {
            echo " / Detail: " . $f->detail;
          }


}


echo "</BODY></HTML>";

?>
