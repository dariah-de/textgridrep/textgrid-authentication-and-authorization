<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 07.04.2008
// Modification date: 07.04.2008
// Version: 0.1.0
// #######################################################


require_once( "../soapTypes.inc.php" );



// -----------------------------------------------------
// You'll need these services
// -----------------------------------------------------
$soapExtra = new SoapClient( "../wsdl/tgextra.wsdl" );


echo "<BODY><HTML>";




if( isset( $_POST['project'] ) ) {

  // -----------------------------------------------------
  // If this was successfull you can add a the user you
  // wish to create
  // -----------------------------------------------------
  $proReg = new getProjectDescriptionRequest();
  $proReg->auth = $_POST['auth'];
  $proReg->log = "";
  $proReg->project = $_POST['project'];


  echo "<HR/>";
  echo "Looking for description...<BR/>";

  try {

    $response = $soapExtra->getProjectDescription( $proReg );

    if( $response->project ) {

      echo "ID: " . $response->project->id . "<br>";
      echo "Name: " . $response->project->name . "<br>";
      echo "Description: " . $response->project->description . "<br>";
      echo "File: " . $response->project->file;

    }
    else {

     echo "UNABLE TO COMMIT...";

    }

  }
  catch( SoapFault $f ) {

    echo "SOAP FAULT!: " . $f->faultcode . " / " . $f->faultstring . " / " . $f->detail;

  }

}


echo "<FORM action=\"getProjectDescription.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
echo "Auth: <INPUT type=\"text\" name=\"auth\" value=\"\"><BR>\n";
echo "Project: <INPUT type=\"text\" name=\"project\" value=\"\"><BR>\n";
echo "<INPUT type=\"submit\" value=\"Commit...\">\n";
echo "</FORM>\n";

echo "</BODY></HTML>";

?>
