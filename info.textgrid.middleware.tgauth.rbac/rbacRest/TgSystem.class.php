<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 17.07.2007
// Modification date: 09.10.2007
// Version: 0.2.2
// #######################################################



class TgSystem {

  // Global variables
  protected $rbac;
  protected $config;



  // -----------------------------------------------------
  // Constructor
  // Input: none
  // Output: object RBACcore
  // Description:
  //   Creates initial connections to the LDAP-server and
  //   sets some configuration parameters.
  // -----------------------------------------------------
  public function __construct( $inConfigurationFilename, $inRbacConfFile, $inRbacBase ) {

    $this->rbac = new RBAC( $inRbacConfFile, $inRbacBase );


    $this->config = new SimpleConfig( $inConfigurationFilename );

  }




  public function checkAccess( $inSid, $inOperation, $inResource ) {

    try {

      $checkAccessResult = $this->rbac->checkAccess( $inSid, $inOperation, $inResource );


      if( $checkAccessResult ) {

        $checkAccessResult = "TRUE";

      }
      else {

        $checkAccessResult = "FALSE";

      }

    }
    catch( Exception $e ) {

      $checkAccessResult = "FALSE";

    }


    return $checkAccessResult;

  }

}
?>
