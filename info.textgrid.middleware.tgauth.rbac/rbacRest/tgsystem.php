<?php
// #######################################################
// Author: Markus Widmer
// Creation date: 13.10.2008
// Modification date: 13.10.2008
// Version: 1.0.0
// #######################################################


require_once( "../rbac/RBAC.class.php" );
require_once( "TgSystem.class.php" );


// Dont be so verbose with messages and notices.
error_reporting( E_ERROR | E_USER_ERROR );

$tgsystem = new TgSystem( "../conf/rbacSoap.conf", "../conf/system.conf", "../rbac/" );


header( "Content-Type: text/plain; charset=UTF-8" );

if( preg_match( "/checkaccess/i", $_REQUEST['method'] ) ) {


  $arrQuery = Array( "sid" => $_REQUEST['the_session_id'],
                     "operation" => $_REQUEST['the_operation'],
                     "resource" => base64_decode( $_REQUEST['the_resource'] ) );

  $arrQuery = base64_encode( serialize( $arrQuery ) );



  $sock = socket_create( AF_INET, SOCK_STREAM, SOL_TCP );
  socket_connect( $sock, '127.0.0.1', 6643 );
  socket_write( $sock, $arrQuery, strlen( $arrQuery ) );
  socket_write( $sock, "\r\n", strlen( "\r\n" ) );


  $result = socket_read( $sock, 1024 );

  socket_shutdown( $sock, 2 );
  socket_close( $sock );


}
else {

  echo "FALSE";

}
?>
