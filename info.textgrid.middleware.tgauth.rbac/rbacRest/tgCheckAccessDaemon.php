<?php
// Create socket with 50 connections in queue
//$sock = socket_create_listen( 6643, 50 );
//socket_set_nonblock( $sock );

$sock = socket_create( AF_INET, SOCK_STREAM, SOL_TCP );
socket_bind( $sock, "127.0.0.1", 6643 );
socket_listen( $sock, 50 );

require_once( "../rbac/RBAC.class.php" );
require_once( "TgSystem.class.php" );


// Dont be so verbose with messages and notices.
error_reporting( E_ERROR | E_USER_ERROR );

$tgsystem = new TgSystem( "../conf/rbacSoap.conf", "../conf/system.conf", "../rbac/" );


while( $con = socket_accept( $sock ) ) {

  socket_getpeername( $con, $raddr, $rport );


  $query = socket_read( $con, 2048 );
  $arrQuery = unserialize( base64_decode( $query ) );


  $result = $tgsystem->checkAccess( $arrQuery['sid'], $arrQuery['operation'], $arrQuery['resource'] );
  $result = base64_encode( serialize( $result ) );
  socket_write( $con, $result, strlen( $result ) );
  socket_write( $con, "\r\n", strlen( "\r\n" ) );

}

socket_close( $sock );
?>
