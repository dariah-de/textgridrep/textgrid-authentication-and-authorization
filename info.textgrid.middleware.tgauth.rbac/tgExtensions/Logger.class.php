<?php
// ####################################################################
// Version: 0.2.0
// Autor: Markus Widmer
// Erstellungsdatum: 02.11.2007
// Letzte Aenderung: 11.06.2024

$LOG_DIR = "/var/log/dhrep/tgauth/"; // This sould be configured via Puppet in dhrep module with write permission for www-data (apache2)!

class Logger extends RBACExtension {

  // ## Klassenvariablen ##############################################




  // ## Konstruktor ###################################################
  public function __construct( $inRBAC ) {

    // Let the extension do all the things
    // we dont't want to do
    parent::__construct( $inRBAC );

  }



  // ## registerEvents ################################################
  public function registerEvents( RBAC $inRegistrar ) {

    $inRegistrar->registerEventListener( "addUser", "write", $this, "logAddUserEvent" );
    $inRegistrar->registerEventListener( "checkAccess", "finish", $this, "logCheckAccessEvent" );
    $inRegistrar->registerEventListener( "assignUser", "write", $this, "logAssignUserEvent" );
    $inRegistrar->registerEventListener( "addAscendant", "finished", $this, "logAddAscendantEvent" );


    // These functions for debugging-informations
    $inRegistrar->registerEventListener( "checkAccess", "finish", $this, "debugCheckAccessResultEvent" );
    $inRegistrar->registerEventListener( "grantPermission", "filter", $this, "debugGrantPermissionEvent" );
    $inRegistrar->registerEventListener( "addAscendant", "write", $this, "debugAddAscendantEvent" );

  }




  // ## logAddUserEvent ###############################################
  public function logAddUserEvent( Context $inContext ) {

    $file = fopen( $LOG_DIR . "addUser.log", "a+" );

    fwrite( $file, date( "Y-m-d h:i:s", time() ) . " Adding user: " . $inContext->getValue( "dn" ) . "\n" );

    fclose( $file );


    return $inContext;

  }




  // ## logCheckAccessEvent ###########################################
  public function logCheckAccessEvent( Context $inContext ) {

    $arrParameter = $inContext->getParameters();  // The parameters of the checkAccess-function
    $arrSessionRole = Array();                    // The active roles of the session
    $file = false;                                // File-handler

    $file = fopen( $LOG_DIR . "checkAccess.log", "a+" );


    // The active roles of the session
    try {


      $arrSessionRole = $this->rbac->sessionRoles( $arrParameter[0], false );

      fwrite( $file, date( "Y-m-d h:i:s", time() ) . " " );
      fwrite( $file, "Operation: \"" . $arrParameter[1] . "\", Resource: " . $arrParameter[2] . "\"" );
      fwrite( $file, ", SID: \"" . $inContext->getValue( "sid" ) . "\", Result: \"" . serialize( $inContext->getValue( "decision" ) ) . "\"\n---------------\n" );

    }
    catch( Exception $e ) {
    	  
      fwrite( $file, date( "Y-m-d h:i:s", time() ) . " " );
      fwrite( $file, "Exception: " . $e->__toString() );

    }


    fclose( $file );

    return $inContext;

  }




  // ## logAddUserEvent ###############################################
  public function logAssignUserEvent( Context $inContext ) {

    $arrParameter = $inContext->getParameters();  // The parameters of the checkAccess-function
    $entry = $inContext->getValue( "entry" );


    $file = fopen(  $LOG_DIR . "assignUser.log", "a+" );

    fwrite( $file, date( "Y-m-d h:i:s", time() ) );
    fwrite( $file, " Assigning user \"" . $arrParameter[0] . "\"" );
    fwrite( $file, " to role \"" . $arrParameter[1] . "\"\n" );

    fclose( $file );


    return $inContext;

  }




  // ## logAddAscendantEvent ##########################################
  public function logAddAscendantEvent( Context $inContext ) {

    $arrParameter = $inContext->getParameters();  // The parameters of the checkAccess-function
    $file = false;                                // File-handler


    $file = fopen( $LOG_DIR . "addAscendant.log", "a+" );

    fwrite( $file, date( "Y-m-d h:i:s", time() ) );
    fwrite( $file, " Added ascendant: " . $arrParameter[0] . " to descendant: " . $arrParameter[1] . "\n---------------\n" );

    fclose( $file );


    return $inContext;

  }




  // ## debugCheckAccessResultEvent ###################################
  public function debugCheckAccessResultEvent( Context $inContext ) {

    $arrParameter = $inContext->getParameters();  // The parameters of the checkAccess-function
    $file = false;                                // File-handler


    $file = fopen( $LOG_DIR . "checkAccess.debug", "a+" );

    fwrite( $file, date( "Y-m-d h:i:s", time() ) . " Unmodified Result: " . serialize( $inContext->getValue( "decision" ) ) . "\n---------------\n" );
    fwrite( $file, date( "Y-m-d h:i:s", time() ) . " Parameter 1/2/3: " . serialize( $arrParameter[0] ) . "/" . serialize( $arrParameter[1] ) . "/" . serialize( $arrParameter[2] ) . "\n---------------\n" );

    fclose( $file );


    return $inContext;

  }




  // ## debugGrantPermissionEvent #####################################
  public function debugGrantPermissionEvent( Context $inContext ) {

    $arrParameter = $inContext->getParameters();  // The parameters of the checkAccess-function
    $file = false;                                // File-handler


    $file = fopen( $LOG_DIR . "grantPermission.debug", "a+" );

    fwrite( $file, date( "Y-m-d h:i:s", time() ) . " Filter-string: " . $inContext->getValue( "filter" ) . "\n---------------\n" );

    fclose( $file );


    return $inContext;

  }




  // ## debugAddAscendantEvent ########################################
  public function debugAddAscendantEvent( Context $inContext ) {

    $arrParameter = $inContext->getParameters();  // The parameters of the checkAccess-function
    $file = false;                                // File-handler


    $file = fopen( $LOG_DIR . "addAscendant.debug", "a+" );

    fwrite( $file, date( "Y-m-d h:i:s", time() ) . "Trying to add ascendant: " . $arrParameter[0] . " to descendant: " . $arrParameter[1] );
    fwrite( $file, " / DN: " . $inContext->getValue( "dn" ) );
    fwrite( $file, "\n---------------\n" );

    fclose( $file );


    return $inContext;

  }

}
?>
