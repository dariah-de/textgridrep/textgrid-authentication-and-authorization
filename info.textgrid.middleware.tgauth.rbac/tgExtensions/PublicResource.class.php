<?php
// ####################################################################
// Version: 0.1.1
// Autor: Markus Widmer
// Erstellungsdatum: 12.03.2008
// Letzte Aenderung: 15.05.2008



class PublicResource extends RBACExtension {

  // ## Klassenvariablen ##############################################




  // ## Konstruktor ###################################################
  public function __construct( $inRBAC ) {

    // Let the extension do all the things
    // we dont't want to do
    parent::__construct( $inRBAC );

  }



  // ## registerEvents ################################################
  public function registerEvents( RBAC $inRegistrar ) {

    $inRegistrar->registerEventListener( "checkAccess", "finish", $this, "publicCheckAccess" );
    $inRegistrar->registerEventListener( "checkAccess", "exception", $this, "publicCheckAccess" );
    $inRegistrar->registerEventListener( "userOperationsOnObject", "finish", $this, "publicUserOperationsOnObject" );

  }




  // ## publicCheckAccess #############################################
  public function publicCheckAccess( Context $inContext ) {

    $arrParameter = $inContext->getParameters();     // The parameters of the checkAccess-function
    $arrEntry = $inContext->getValue( "resource" );  // The resource-entry
    $operation = $arrParameter[1];                   // Die der Funktion checkAccess uebergebene Operatio
    $filter = "";


    if( preg_match( "/^read$/", $operation ) ) {

      // Filter to search for the resource
      $filter  = "(&" . $this->conf->getValue( "resource", "filter" );
      $filter .= "(|(" . $this->conf->getValue( "resource", "namingattribute" ) . "=" . $arrParameter[2] . ")";
      $filter .= "  (" . $this->conf->getValue( "resource", "aliasattribute" ) . "=" . $arrParameter[2] . ")))";


      // Search for the resource
      $arrResource = $this->conn['resource']->search( $this->conf->getValue( "resource", "base" ), $filter, "sub", Array( "tgispublic" ) );


      // Only if the flag "tgIsPublic" is true and the operation
      // is "read", we grant access to the resource even if
      // the read-operation would not be granted. Otherwise we
      // keep the decision of the checkAccess-function.
      if(    isset( $arrResource[0] )
          && preg_match( "/^true$/i", $arrResource[0]['tgispublic'][0] ) ) {
//          && preg_match( "/^read$/", $operation ) ) {

        $inContext->changeSecurityChain( true );
        $inContext->setValue( "decision", true );

      }
      else {

        $inContext->changeSecurityChain( $inContext->getValue( "decision" ) );

      }

    }
    else {

      $inContext->changeSecurityChain( $inContext->getValue( "decision" ) );

    }


    return $inContext;

  }




  // ## publicUserOperationsOnObject ##################################
  public function publicUserOperationsOnObject( Context $inContext ) {

    $arrParameter = $inContext->getParameters();             // The parameters of the userOperationsOnObject-function
    $arrOperation = $inContext->getValue( "arrOperation" );  // The operations already allowed


    if( is_array( $arrOperation ) && !in_array( "read", $arrOperation ) ) {

      $filter = "(&" . $this->conf->getValue( "resource", "filter" );
      $filter .= "(|(" . $this->conf->getValue( "resource", "namingattribute" ) . "=" . $arrParameter[1] . ")";
      $filter .= "  (" . $this->conf->getValue( "resource", "aliasattribute" ) . "=" . $arrParameter[1] . "))";
      $filter .= "(tgispublic=TRUE))";


      // Search for the resource
      $arrResource = $this->conn['resource']->search( $this->conf->getValue( "resource", "base" ), $filter, "sub", Array( "tgispublic" ) );

      if( is_array( $arrResource ) && sizeof( $arrResource ) > 0 ) {

        $arrOperation[] = "read";
        $inContext->setValue( "arrOperation", $arrOperation );

      }

    }


    return $inContext;

  }

}
?>
