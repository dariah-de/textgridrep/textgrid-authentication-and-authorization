<?php
// ####################################################################
// Version: 0.2.0
// Autor: Markus Widmer
// Erstellungsdatum: 18.11.2007
// Letzte Aenderung: 04.12.2007



class Project extends RBACExtension {

  // ## Klassenvariablen ##############################################
  //private $rbac;
  //private $conf;




  // ## Konstruktor ###################################################
  public function __construct( $inRBAC, $inRegistrar ) {

    // Save the instances of RBAC and grab the configuration
    // from it.
    $this->rbac = $inRBAC;
    $this->conf = $inRBAC->getConfiguration();


    // Get the user- and role connections from the
    // underlying RBAC-system
    $this->conn['role'] = $inRBAC->getConnection( "role" );


    // Let the extension do all the things
    // we dont't want to do
    parent::__construct( $inRBAC );


  }



  // ## registerEvents ################################################
  public function registerEvents( RBAC $inRegistrar ) {

    $inRegistrar->registerEventListener( "addAscendant", "write", $this, "upgradeToProject" );
    $inRegistrar->registerEventListener( "addAscendant", "finished", $this, "createMissingProjectRoleTree" );

  }




  // ## upgradeToProject ##############################################
  public function upgradeToProject( Context $inContext ) {

    $arrParameter = $inContext->getParameters();  // The parameters the addRole-function got
    $roleDn = $inContext->getValue( "dn" );


    // Extract the name of the role from the role-DN
    $roleName = preg_split( "/[,]/", $roleDn );
    $roleName = preg_split( "/[=]/", $roleName[0] );
    $roleName = $roleName[1];


    // If the roleName contains a DN that is directly under
    // the project-base-DN, then add the project-specific
    // permissions and operations
    if( preg_match( "/^rbacName=TGPR-[^,]+\s*,\s*" . $this->conf->getValue( "project", "base" ) . "/i", $roleDn ) ) {

      $arrEntry = $inContext->getValue( "entry" );


      $arrEntry['objectclass'][] = "TextGridProject";
      $arrEntry['objectclass'][] = "rbacResource";
      $arrEntry['tgprojectid'][] = $roleName;
      $arrEntry['rbacoperation'][] = "create";
      $arrEntry['rbacoperation'][] = "delegate";


      $inContext->setValue( "entry", $arrEntry );

    }


    return $inContext;

  }




  // ## createMissingProjectRoleTree ##################################
  public function createMissingProjectRoleTree( Context $inContext ) {

    $arrParameter = $inContext->getParameters();      // The parameters the addRole-function got
    $projectDn = $inContext->getValue( "dn" );        // The DN of the entry
    $projectEntry = $inContext->getValue( "entry" );  // The entry itself


    if( preg_match( "/^\s*rbacName=TGPR-[^,]+\s*,\s*" . $this->conf->getValue( "project", "base" ) . "/i", $projectDn ) ) {

      // Create the other roles
      $this->rbac->addAscendant( $this->conf->getValue( "project", "observerRoleName" ), $projectDn );
      $this->rbac->addAscendant( $this->conf->getValue( "project", "editorRoleName" ), $projectDn );
      $this->rbac->addAscendant( $this->conf->getValue( "project", "administratorRoleName" ), $projectDn );
      $this->rbac->addAscendant( $this->conf->getValue( "project", "leaderRoleName" ), $projectDn );


      // Add the default rights to the roles and the Project
      $this->rbac->grantPermission( $projectEntry['rbacname'][0], "delegate", "rbacName=Projektleiter," . $projectDn );
      $this->rbac->grantPermission( $projectEntry['rbacname'][0], "create", "rbacName=Bearbeiter," . $projectDn );

    }


    return $inContext;

  }

}
?>
