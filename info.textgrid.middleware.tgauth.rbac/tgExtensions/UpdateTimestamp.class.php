<?php
// ####################################################################
// Version: 0.1.0
// Autor: Markus Widmer
// Erstellungsdatum: 16.01.2009
// Letzte Aenderung: 16.01.2009



class UpdateTimestamp extends RBACExtension {

  // ## Klassenvariablen ##############################################




  // ## Konstruktor ###################################################
  public function __construct( $inRBAC ) {

    // Let the extension do all the things
    // we dont't want to do
    parent::__construct( $inRBAC );

  }



  // ## registerEvents ################################################
  public function registerEvents( RBAC $inRegistrar ) {

    $inRegistrar->registerEventListener( "checkAccess", "finish", $this, "timestampCheckAccess" );

  }




  // ## timestampCheckAccess ##########################################
  public function timestampCheckAccess( Context $inContext ) {

    $arrParameter = $inContext->getParameters();     // The parameters of the checkAccess-function
    $arrEntry = $inContext->getValue( "resource" );  // The resource-entry
    $operation = $arrParameter[1];                   // Die der Funktion checkAccess uebergebene Operatio
    $filter = "";


    // Filter to search for the resource
    $filter  = "(&" . $this->conf->getValue( "session", "filter" );
    $filter .= "(" . $this->conf->getValue( "session", "namingattribute" ) . "=" . $arrParameter[0] . "))";


    // Search for the session
    $arrSession = $this->conn['session']->search( $this->conf->getValue( "session", "base" ), $filter, "sub", Array( "rbacsessionchecktimestamp" ) );


    // If the session was found send an update of the
    // timestamp.
    if( $arrSession && sizeof( $arrSession ) === 1 ) {

      $arrModify['rbacsessionchecktimestamp'][] = date( "YmdHis", time() ) . "Z";

      $this->conn['session']->modify( $arrSession[0]['dn'], $arrModify );

    }


    return $inContext;

  }




  // ## publicUserOperationsOnObject ##################################
  public function publicUserOperationsOnObject( Context $inContext ) {

    $arrParameter = $inContext->getParameters();             // The parameters of the userOperationsOnObject-function
    $arrOperation = $inContext->getValue( "arrOperation" );  // The operations already allowed


    if( !in_array( "read", $arrOperation ) ) {

      $filter = "(&" . $this->conf->getValue( "resource", "filter" );
      $filter .= "(|(" . $this->conf->getValue( "resource", "namingattribute" ) . "=" . $arrParameter[1] . ")";
      $filter .= "  (" . $this->conf->getValue( "resource", "aliasattribute" ) . "=" . $arrParameter[1] . "))";
      $filter .= "(tgispublic=TRUE))";


      // Search for the resource
      $arrResource = $this->conn['resource']->search( $this->conf->getValue( "resource", "base" ), $filter, "sub", Array( "tgispublic" ) );


      if( $arrResource && sizeof( $arrResource ) > 0 ) {

        $arrOperation[] = "read";
        $inContext->setValue( "arrOperation", $arrOperation );

      }

    }


    return $inContext;

  }

}
?>
