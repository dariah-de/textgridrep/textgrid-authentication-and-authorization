import fnal.vox.vomrs.client.SoapClient; 
import java.util.Arrays;


/**
 * Class to Register Members at VOMRS automatically
 */
public class Autoreg {            
    public static void main(String [] args) throws Exception { 
	if (args.length < 5) {
	    System.out.println("Sorry, not enough information, cannot register");
	    System.out.println("I want: subject_dn issuer_dn serialNumber email 'Attr1name,Attr1value,Attr2name,Attr2value,...'");
	    return;
	}
	String r_dn = args[0]; // subject_dn
	String r_ca = args[1]; // issuer_dn
	String r_sn = args[2]; // serialNumber
	String r_in = "DAASI"; //"TextGridPortal"; // (institution)
	String r_repdn = "/C=DE/ST=Bremen/L=Bremerhaven/O=AWI/OU=SLC-Gap Project/CN=TextGrid VOrepresentative/Email=martin.haase@daasi.de"; //
	String r_repca = "/C=DE/ST=Bremen/L=Bremerhaven/O=SLC-Gap Project/OU=DAASI International GmbH/CN=SLC-Gap Test CA2"; //
	String r_rights = "full"; //
	String r_email = args[3]; // email
	String r_confirm = "N"; //
	String r_pi = args[4];

        SoapClient sc = new SoapClient("https://voms.awi.de:8443/vomrs/tgtest1/services/VOMRS");
	
	// getMembers
        Object o = sc.execute("getMembers",new String[]{}); 
	String members[] = (String[]) o;
	/*	for (String s : members) {
	    System.out.println(s);
	}*/

	if (Arrays.asList(members).contains(r_dn)) {
	    // TODO XXXXXX check if Details have changed
	    System.out.println("Member is already registered: " + r_dn);
	    return;
	}


	Object o2 = sc.execute("registerMember", new String[]{r_dn, r_ca, r_sn, r_in, r_repdn, r_repca, r_rights, r_email, r_confirm, r_pi});
	// o2 is null, this execute() returns nothing

	Object o3 = sc.execute("setMbrRegistrationStatus", new String[]{r_dn, r_ca, "Approved", "Representative", "TextGrid-SLC Portal Approval"});
	// o3 null as well


    } 
} 
