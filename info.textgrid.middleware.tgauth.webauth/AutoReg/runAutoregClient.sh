#!/bin/sh

cd /usr/local/bin/VOMRSclient/bin

export CLASSPATH="/usr/local/bin/VOMRSclient/lib/*:."

java -Daxis.socketSecureFactory=org.glite.security.trustmanager.axis.AXISSocketFactory -DsslConfigFile=auth.properties Autoreg "$@"
