<?php
// #######################################################
// Author: Martin Haase / DAASI International GmbH / TextGrid
// Creation date: 2010-09-23
// Modification date: 2017-02-27
// Version: 0.3 - user management is done in DARIAH now
// #######################################################

include("../tglib/RBAC.class.php");
include("../tglib/WebUtils.class.php");

$configfile = "/etc/dhrep/tgauth/conf/config_tgwebauth.xml";

$util = new WebUtils;

$authZinstance = $_REQUEST["authZinstance"];

if ( !(isset($authZinstance)) || strlen($authZinstance) <= 0 ) {
  $util->printAuthFailure("no_tgauth_instance_heading", 
		      "no_tgauth_instance_detail", 
		      null, 
		      null );
  exit;
}

$rbac = new RBAC ( $configfile, $authZinstance );


// Variant 1: Authentication at Community LDAP
// now unsused

// Variant 2: Shibboleth gave us the right REMOTE_USER. 
// We create a Session here in RBAC, also for Variant1

if (isset ($_SERVER["REMOTE_USER"])) {
  // now creating session, activating roles, etc, in RBAC
  $CSResult = $rbac->createSession( $_SERVER["REMOTE_USER"] );
  $CSResult["rbachash"]["identity_provider"] = $_SERVER["Shib-Identity-Provider"];

  

  if (!$CSResult["success"]) {
    $util->printAuthFailure("sid_create_failure_heading", 
			    $CSResult["detail"], 
			    $CSResult["loginname"], 
			    $CSResult["rbachash"]
			    );
    
    exit;
  }
} 

// not enough information, exiting. 
else 
{
     
    // check if we came via Shibboleth, but without an eduPersonPrincipalName 
    // (which would have been the REMOTE_USER)
      if (isset( $_SERVER['Shib-Session-ID'] )) {
	  $util->printAuthFailure("shib_login_failure_heading", 
				  "shib_login_failure_detail",
				  "(Shibboleth login, but no ePPN provided)", 
				  null ); 
	exit;
      }
    else
      {	
      	  $util->printAuthFailure("authn_failure_heading", 
				  "authn_failure_detail_nothing_to_do",
				  'XXXX', 
				  null ); 
	  trigger_error("WebAutnN: reached /secure, but no Shibboleth Session ID. This should not have happened.", E_USER_WARNING);
 	  exit;
      }
}


// print welcome screen causing the TextGridLab to take over the Sid
$util->printAuthSuccess("authn_succeeded_heading",
		         $_SERVER["REMOTE_USER"],
			 $CSResult["rbachash"] ); 

// Variant 3 unused now: No Session Creation, but just a desire to see (and update) User Attributes
?>
