<?php
// #######################################################
// Author: Martin Haase / DAASI International GmbH / TextGrid
// Creation date: 2010-09-23
// Modification date: 2010-09-03
// Version: 0.1
// based on authenticate.php
// #######################################################

mb_internal_encoding("UTF-8");

class LDAP {

  // Global variables
  protected $UserAttributes = array();
  protected $ldaphost;
  protected $ldapport;
  protected $binddn;
  protected $filter;
  protected $IDattribute;
  protected $LDAPname;
  public $availableAttributes = array("o", "sn", "givenName", "cn", "mail");
  public $AttributeMap = Array ('surname' => 'sn',
// StefanS will 'o' nicht User-Modifizierbar; daher wird 
// es nur in RBAC und nicht in DARIAH LDAP geschrieben
//				'organisation' => 'o',
				'givenname' => 'givenName',
				'displayname' => 'cn',
				'mail' => 'mail',
				'orgunit' => 'ou',
				'street' => 'street',
				'plz' => 'postalCode',
				'city' => 'l',
				'tel' => 'telephoneNumber',
				'interest' => 'dariahResearchInterests',
				'personid' => 'dariahResearcherId'
				); 
  
  public function __construct( $configfilepath ) {
    $config = new DOMDocument();
    $config->load($configfilepath);
    $xpath = new DOMXPath($config);
    $xpath->registerNamespace("c", "http://textgrid.info/namespaces/middleware/tgwebauth");

    $this->ldaphost = $xpath->query("/c:conf/c:authn[@type='community']/c:key[@name='host']")->item(0)->nodeValue;
    $this->ldapport = $xpath->query("/c:conf/c:authn[@type='community']/c:key[@name='port']")->item(0)->nodeValue;
    $this->binddn = $xpath->query("/c:conf/c:authn[@type='community']/c:key[@name='binddn']")->item(0)->nodeValue;
    $this->basedn = $xpath->query("/c:conf/c:authn[@type='community']/c:key[@name='basedn']")->item(0)->nodeValue;
    $this->filter = $xpath->query("/c:conf/c:authn[@type='community']/c:key[@name='filter']")->item(0)->nodeValue;
    $this->IDattribute = $xpath->query("/c:conf/c:authn[@type='community']/c:key[@name='IDattribute']")->item(0)->nodeValue;
    $this->LDAPname = $xpath->query("/c:conf/c:authn[@type='community']/c:key[@name='name']")->item(0)->nodeValue;
    $this->setAttributesDN = $xpath->query("/c:conf/c:authn[@type='community']/c:key[@name='setAttributesDN']")->item(0)->nodeValue;
    $this->setAttributesPW = $xpath->query("/c:conf/c:authn[@type='community']/c:key[@name='setAttributesPW']")->item(0)->nodeValue;

    $this->IDattribute = explode ( ";", $this->IDattribute );
  }

  public function authenticate ($login, $password) {
    $ldapconn = ldap_connect( $this->ldaphost, $this->ldapport );
    // ldap_connect always returns a handle, does not connect yet
    // or return array("success" => FALSE, "detail" => "Cannot connect to {$ldaphost}!");

    ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
//    $return = ldap_start_tls( $ldapconn );
//    $file = fopen ("/var/www/LOG/ssstls", "a+");
//    fwrite ($file, serialize($return));
//    fclose($file);

    $binddn = preg_replace ('/\${login}/', $login, $this->binddn);
    $bound = ldap_bind($ldapconn, $binddn , $password);
    if (!$bound) {
      return array("success" => FALSE, 
		   "detail" => "Authentication failed, reason: " . ldap_error ($ldapconn));
    } else {
      //echo "Could bind as user ${login}!";
      $filter = preg_replace ('/\${login}/', $login, $this->filter);
      $result = ldap_search( $ldapconn, $this->basedn, $filter);
      $entry  = ldap_first_entry( $ldapconn  , $result  );

      $this->UserAttributes = ldap_get_attributes ($ldapconn , $entry);

      foreach ( $this->IDattribute as $idattr ) {
      	   if ( isset ( $this->UserAttributes[$idattr] ) ) {
	      	$TGID = $this->UserAttributes[$idattr][0];
		break;
	   }     
      }

//      $TGID = $this->UserAttributes[$this->IDattribute][0];

      return array("success" => TRUE, "TGID" => $TGID, "LDAPname" => $this->LDAPname);
    }
  }

  public function getUserAttributes () {
    $rethash = array();
    foreach ($this->availableAttributes as $a) {
      if ( isset($this->UserAttributes[$a])) {
	$vals = array();
	for ($i=0; $i<$this->UserAttributes[$a]['count']; $i++) {
	  $vals[] = $this->UserAttributes[$a][$i];
	}
	$rethash[$a] =  implode (';', $vals);
      }
    }
    return $rethash;
  }

// Users will be modified via DARIAH SelfService at a later stage
  public function setUserAttributes ($attrHash, $remote_user) {

    $arrModify = Array();
    $needsModification = FALSE;
    $sendOutMail = FALSE;

    foreach ($attrHash as $a) {
      if (is_object($a) && in_array($a->name, array_keys ($this->AttributeMap))) {
	$arrModify[$this->AttributeMap[$a->name]][] = $a->value;
	$needsModification = TRUE;
	if ($a->name === "mail") {
	  $sendOutMail = $a->value;
	}
      }
    }

    if (! $needsModification ) {
      return array("success" => TRUE, 
		   "detail" => "Nothing to do");
    }


    $ldapconn = ldap_connect( $this->ldaphost, $this->ldapport );
    // ldap_connect always returns a handle, does not connect yet
    // or return array("success" => FALSE, "detail" => "Cannot connect to {$ldaphost}!");

    ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
    //ldap_start_tls( $ldapconn );

    $bound = ldap_bind($ldapconn, $this->setAttributesDN , $this->setAttributesPW);
    if (!$bound) {
      return array("success" => FALSE, 
		   "detail" => "Authentication failed, reason: " . ldap_error ($ldapconn));
    } else {

      $filter = "(|";
      foreach ( $this->IDattribute as $idattr ) {
      	 $filter .= "(". $idattr."=".$remote_user.")";
      }
      $filter .= ")";

      $result = ldap_search( $ldapconn, $this->basedn, $filter);
      if ($result === FALSE ) {
	return array("success" => FALSE, 
		     "detail" => "Could not find this user with the filter: ".$filter . ldap_error ($ldapconn));
      }
      $entry  = ldap_first_entry( $ldapconn  , $result  );
      $oldmailsArr = ldap_get_values ($ldapconn, $entry, "mail");
      if ($sendOutMail !== FALSE) {
	$this->sendmailOut($oldmailsArr, $sendOutMail );
      } 

      $modifyResult = ldap_modify($ldapconn, ldap_get_dn($ldapconn, $entry), $arrModify);

      if ($modifyResult == FALSE ) {
	return array("success" => FALSE, 
		     "detail" => "Could not modify this user:" . ldap_error ($ldapconn));
      }
      return array("success" => TRUE, "detail" => "Alles bestens");
    }
  }

  public function sendmailOut($oldmailsArr, $newMail ) {
  
  }

}

?>
