<?php
class authenticateRequest {

  public $username;
  public $password;
  public $log;

}

class setNameRequest {

  public $auth;
  public $log;
  public $webAuthSecret;
  public $name;
  public $mail;
  public $organisation;
  public $agreeSearch;

}

class authenticateResponse {

  public $sid;

}

class getSidResponse {

  public $sid;

}

class checkAccessRequest {

  public $intSid;
  public $operation;
  public $resource;
  public $sid;

}

class tgCheckAccessRequest {

  public $auth;
  public $log;
  public $operation;
  public $resource;
  public $sid;

}

class tgGrantPermissionRequest {

  public $auth;
  public $log;
  public $role;
  public $resource;
  public $operation;

}

class tgRevokePermissionRequest {

  public $auth;
  public $log;
  public $role;
  public $resource;
  public $operation;

}

class getOwnerRequest {

  public $auth;
  public $log;
  public $resource;

}

class getOwnerResponse {

  public $owner;

}

class getMembersRequest {

  public $auth;
  public $log;
  public $project;

}

class deactivateProjectRequest {

  public $auth;
  public $log;
  public $project;

}

class getRightsRequest {

  public $auth;
  public $log;
  public $resource;
  public $username;

}

class publishRequest {

  public $auth;
  public $log;
  public $resource;

}

class getProjectDescriptionRequest {

  public $auth;
  public $log;
  public $project;

}

class getProjectDescriptionResponse {

  public $description;

}

class createSessionRequest {

  public $intSid;
  public $username;
  public $roleset;
  public $sid;

}

class tgAddActiveRoleRequest {

  public $auth;
  public $log;
  public $role;

}

class tgAssignedRolesRequest {

  public $auth;
  public $log;
  public $username;

}

class tgAssignedProjectsRequest {

  public $auth;
  public $log;

}

class deleteSessionRequest {

  public $intSid;
  public $username;
  public $sid;

}

class addActiveRoleRequest {

  public $intSid;
  public $username;
  public $role;
  public $sid;
  public $auth;
}

class addUserRequest {

  public $intSid;
  public $username;
  public $password;

}

class deleteUserRequest {

  public $intSid;
  public $username;

}

class addInheritanceRequest {

  public $intSid;
  public $ascendant;
  public $descendant;

}

class deleteInheritanceRequest {

  public $intSid;
  public $ascendant;
  public $descendant;

}

class addAscendantRequest {

  public $intSid;
  public $ascendant;
  public $descendant;

}

class addDescendantRequest {

  public $intSid;
  public $ascendant;
  public $descendant;

}

class addRoleRequest {

  public $intSid;
  public $role;

}

class deleteRoleRequest {

  public $intSid;
  public $role;

}

class grantPermissionRequest {

  public $intSid;
  public $resource;
  public $operation;
  public $role;

}

class revokePermissionRequest {

  public $intSid;
  public $resource;
  public $operation;
  public $role;

}

class assignUserRequest {

  public $intSid;
  public $username;
  public $role;

}

class deassignUserRequest {

  public $intSid;
  public $username;
  public $role;

}

class sessionRolesRequest {

  public $intSid;
  public $sid;

}

class assignedRolesRequest {

  public $intSid;
  public $username;

}

class authorizedRolesRequest {

  public $intSid;
  public $username;

}

class roleOperationsOnObjectRequest {

  public $intSid;
  public $role;
  public $resource;

}

class userOperationsOnObjectRequest {

  public $intSid;
  public $user;
  public $resource;

}

class operationsetResponse {

  public $operationset;

}

class assignedUsersRequest {

  public $intSid;
  public $role;

}

class authorizedUsersRequest {

  public $intSid;
  public $role;

}

class usersetResponse {

  public $username;

}

class rolePermissionsRequest {

  public $intSid;
  public $role;

}

class userPermissionsRequest {

  public $intSid;
  public $username;

}

class getLeaderRequest {

  public $auth;
  public $log;
  public $project;

}

class getObjectsRequest {

  public $auth;
  public $log;
  public $project;

}

class sessionPermissionsRequest {

  public $intSid;
  public $sid;

}

class rolesetResponse {

  public $role;

}

class permissionsetResponse {

  public $permissionset;

}

class resourcesetResponse {

  public $resource;

}

class createProjectRequest {

  public $auth;
  public $log;
  public $name;
  public $description;

}

class registerResourceRequest {

  public $auth;
  public $log;
  public $project;
  public $uri;

}

class unregisterResourceRequest {

  public $auth;
  public $log;
  public $uri;

}

class addMemberRequest {

  public $auth;
  public $log;
  public $role;
  public $username;

}

class deleteMemberRequest {

  public $auth;
  public $log;
  public $role;
  public $username;

}

class createProjectResponse {

  public $projectId;

}

class getAllProjectsResponse {

  public $project;

}

class getAllProjectsRequest {

  public $log;

}

class userExistsRequest {

  public $auth;
  public $log;
  public $username;

}


class booleanResponse {

  public $result;
  public $errorCode;
  public $errorDescription;

}

class filterBySidRequest {

  public $auth;
  public $log;
  public $resource;
  public $operation;

}

class filterResponse {

  public $resource;

}


class permission {

  public $resource;
  public $operation;


  public function __construct( $inOperation, $inResource ) {

    $this->operation = $inOperation;
    $this->resource = $inResource;

  }

}

class projectInfo {

  public $id;
  public $description;
  public $name;


  public function __construct( $inId, $inName, $inDescription ) {

    $this->id = $inId;
    $this->description = $inDescription;
    $this->name = $inName;

  }

}

class checkXACMLaccessRequest {

  public $request;

}
?>
