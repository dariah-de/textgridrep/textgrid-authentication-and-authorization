<?php
// #######################################################
// Author: Martin Haase / DAASI International GmbH / TextGrid
// Creation date: 2010-09-23
// Modification date: 2019-02-13 --fu
// Version: 0.1
// #######################################################

mb_internal_encoding("UTF-8");

class WebUtils {
  // Global variables

  public $t;
  public function __construct() {
    ob_start();
    require_once '../i18n_inc/class.I18Nbase.inc.php';
    $this->t = new I18Ntranslator();
  }

  public function printAuthSuccess($heading,$loginname,$rbachash,$slc="") {
   $this->printHeader($heading);

    echo "<meta name=\"remote_principal\" content=\"".$rbachash["remote_user"]."\"/>\n";
    echo "<meta name=\"rbac_session_status\" content=\"".$rbachash["scstatus"]."\"/>\n";
    echo "<meta name=\"rbac_sessionid\" content=\"".$rbachash["Sid"]."\"/>\n";
    echo "<meta name=\"Shib-Identity-Provider\" content=\"".$rbachash["identity_provider"]."\"/>\n";

    if (!$slc || !$slc["slcMode"]) {
    	echo "<meta name=\"ePPNplusSID\" content=\"".$rbachash["remote_user"]."|".$rbachash["Sid"]."\"/>\n";
    }
    echo "<style type=\"text/css\">";
    echo "#d123 {display:none;}";
    echo "</style>";
    echo "<script type=\"text/javascript\">\n";
    echo "<!--\n";
    echo "function toggle (target) {\n";
    echo "var obj=document.getElementById(target);\n";
    echo "obj.style.display=\"block\";\n";
    echo "}\n";
    echo "-->\n";
    echo "</script>\n";

    echo "</head>\n<body>\n";
    echo "<div style=\"font-family:Helvetica,Arial,sans-serif;\">";
    echo "<h2>".$this->t->_('authn_succeeded_heading')."</h2>\n";

    echo "<p>".$this->t->_('auth_success_text','',array($rbachash["remote_user"]))."</p>\n";

    // Details -----------------------------------------------
    echo "<p>".$this->t->_('more_details','',array("javascript:toggle('d123')"))."</p>\n";
    echo "<div id=\"d123\"> <h2>".$this->t->_('authn_details_heading')."</h2>\n";
    echo "<table><tr><td>".$this->t->_('tgauth_instance')."</td><td>". $rbachash["rbacbase"] ."</td></tr>\n";
    echo "<tr><td>".$this->t->_('idp_entityid')."</td><td>". $rbachash["identity_provider"]  ."</td></tr>\n";
    echo "<tr><td>".$this->t->_('login_name')."</td><td>". (isset($loginname) ? $loginname : $this->t->_('only_shib_knowns')) ."</td></tr>\n";
    echo "<tr><td>".$this->t->_('authn_method')."</td><td>". $rbachash["identified_user"]['authnmethod'] ."</td></tr>\n";
    echo "<tr><td>".$this->t->_('user_id')." </td><td>".$rbachash["remote_user"]."</td></tr>\n";
    //    echo "<tr><td>Given Name(s)</td><td>".$givennames."</td></tr>\n";
    //    echo "<tr><td>Surname(s)</td><td>".$surnames."</td></tr>\n";
    //    echo "<tr><td>Common Name(s)</td><td>".$cns."</td></tr>\n";
    //    echo "<tr><td>Mail(s)</td><td>".$mails."</td></tr>\n";
    //    echo "<tr><td>Organisation(s)</td><td>".$organisations."</td></tr>\n";
    echo "<tr><td>".$this->t->_('sid')."</td><td>".$rbachash["Sid"]."</td></tr></table>\n";
    echo "</div>";

    if (is_array($slc) && $slc['slcMode']) {
      echo "SLCMODE YES";
      $isSLCScompatible = $this->scanEntitlements($slc);
      if ($isSLCScompatible) {
	echo "SLCcompatible YES";
	$this->showCertificateButtons($slc,$rbachash);
      } else {
	$this->showCertificateInfoButton($slc,$rbachash);
      }
    }

    echo "</div>\n";
    echo "\n</body>\n</html>";
  }


  function showCertificateButtons ($slc,$rbachash) {
    echo "<form method=\"get\" action=\"". $slc['slcPortalDelegationURL'] ."\">\n";
    echo "<input type=\"hidden\" name=\"userDetails\" value=\"" . $rbachash['remote_user'] . "|" .$rbachash['Sid'] ."|". $rbachash['authZinstance'] . "\">\n";
    echo "<input type=\"submit\" value=\"Request Certificate\">\n";
    echo "</form>\n\n";

    echo "<form method=\"post\" action=\"". $slc['slcNoDelegationURL'] ."\">\n";
    echo "<input type=\"hidden\" name=\"ePPNplusSID\" value=\"" . $rbachash['remote_user'] . "|" .$rbachash['Sid'] . "\">\n";
    echo "<input type=\"submit\" value=\"Use Existing Certificate\"/>\n";
    echo "</form>\n";
  }

  function showCertificateInfoButton ($slc,$rbachash) {
    echo "<br/><br/>Your account does not include certificate support.";
    echo "<form method=\"post\" action=\"". $slc['slcNoDelegationURL'] ."\">\n";
    echo "<input type=\"hidden\" name=\"ePPNplusSID\" value=\"" . $rbachash['remote_user'] . "|" .$rbachash['Sid'] . "\">\n";
    echo "<input type=\"submit\" value=\"Work without Certificate\"/>\n";
    echo "</form>\n";
  }


  function scanEntitlements ($slc) {
    if (isset($_SERVER[$slc['slcEntitlementAttributeName']])) {
      $entitlements = $_SERVER[$slc['slcEntitlementAttributeName']];
      $arrEntitlements = explode( ";", $entitlements);
      foreach ($arrEntitlements as $ent) {
	if ($ent === $slc['slcEntitlementAttributeValue']) {
	  return TRUE;
	}
      }
    }
    return FALSE;
  }


  public function printAuthFailure($heading,$detail,$loginname,$rbachash) {

    $this->printHeader("authn_failure_pagetitle");

    if (isset($rbachash)) {
      echo "<meta name=\"remote_principal\" content=\"". $rbachash["remote_user"]."\"/>\n";
      echo "<meta name=\"rbac_session_status\" content=\"". $rbachash["scstatus"]."\"/>\n";
      echo "<meta name=\"rbac_sessionid\" content=\"". $rbachash["Sid"]."\"/>\n";
    }

    echo "</head>\n\n<body>\n";
    echo "<div style=\"font-family:Helvetica,Arial,sans-serif;\">";
    echo "<h3>".$this->t->_($heading)."</h3>\n";
    echo "<p>". $this->t->_('authn_error_status') ." </p>\n";
    echo "<p>". $this->t->_($detail) ."</p>" ;
    echo "<h3>". $this->t->_('further_information') . "</h3>\n";
    echo "<table>\n";
    echo "<tr><td>".$this->t->_('login_name')."</td><td>". $loginname ."</td></tr>\n";
    if (isset($rbachash["rbacbase"])) {
      echo "<tr><td>".$this->t->_('tgauth_instance')."</td><td>". $rbachash["rbacbase"] ."</td></tr>\n";
    }
    if (isset($rbachash["identity_provider"])) {
      echo "<tr><td>".$this->t->_('idp_entityid')."</td><td>". $rbachash["identity_provider"]  ."</td></tr>\n";
    }
    if (isset($rbachash["identified_user"]['authnmethod'] )) {
      echo "<tr><td>".$this->t->_('authn_method')."</td><td>". $rbachash["identified_user"]['authnmethod'] ."</td></tr>\n";
    }
    if (isset($rbachash["remote_user"])) {
      echo "<tr><td>".$this->t->_('user_id')." </td><td>".$rbachash["remote_user"]."</td></tr>\n";
    }
    if (isset($rbachash["Sid"])) {
      echo "<tr><td>".$this->t->_('sid')."</td><td>".$rbachash["Sid"]."</td></tr>\n";
    }
    echo "</table>\n";
    //    echo "</div>";

    echo "<p>".$this->t->_('error_details_blabla_goback')."</p>";
    echo "<p>".$this->t->_('error_details_blabla_date','',array(date("Y-m-d H:i:s")))."</p>" ;
    echo "\n</div></body>\n</html>";
  }

  public function printHeader ($title) {
    header('Content-Type: text/html; charset=utf-8');
    echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n";
    echo "<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\">\n";
    echo "<head>\n";
    echo "<title>" . $this->t->_($title) . "</title>\n";
    echo "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" />\n";
  }

  public function printAttributeForm ( $existingAttrs, $providedAttrs, $attrMap, $Sid, $authZinstance, $remote_user, $ToUversion, $ToUtext) {
    if (isset ($providedAttrs)) {
      $loginmode = TRUE;
      $title = "update_attr_please_pagetitle";
    } else {
      $loginmode = FALSE;
      $title = "modify_attr_pagetitle";
    }

    $this->printHeader ($title);

    echo "<meta name=\"remote_principal\" content=\"".$remote_user."\"/>\n";
    echo "<meta name=\"rbac_sessionid\" content=\"".$Sid."\"/>\n";

    echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"./scripts/help.css\" media=\"all\"/>\n";
    echo "<script src=\"./scripts/jquery-1.5.2.min.js\" type=\"text/javascript\"></script>\n";
    echo "<script src=\"./scripts/help.js\" type=\"text/javascript\"></script>\n";

    echo "<style type=\"text/css\">";
    echo "#d456 {display:none;}";
    echo "</style>";
    echo "<script type=\"text/javascript\">\n";
    echo "<!--\n";
    echo "function toggleToU (target) {\n";
    echo "var obj=document.getElementById(target);\n";
    echo "obj.style.display=\"block\";\n";
    echo "}\n";
    echo "-->\n";
    echo "</script>\n";

    echo "<script type=\"text/javascript\">\n";
    echo "function chkFormular () {\n";
    echo "    if (!document.Formular.ToUversion.checked) {\n";
    echo "        alert(\"". $this->t->_('ToU_alert','',array("javascript:toggleToU('d456')"))."\");\n";
    echo "        document.Formular.ToUversion.focus();\n";
    echo "        return false;\n";
    echo "    }\n";

    foreach ($existingAttrs as $a) {
      if ($a->mandatory && $a->name != "ToUversion") {
	echo "    if (document.Formular.". $a->name . ".value == \"\") {\n";
	echo "        alert(\"". $this->t->_('mandatory_field_empty_alert','',array($this->t->_('attr_'.$a->name),$this->t->_('attr_'.$a->name.'_description')))   ."\");\n";
	echo "        document.Formular.". $a->name . ".focus();\n";
	echo "        return false;\n";
	echo "    }\n";
      }
    }
    echo "    if (document.Formular.mail.value.indexOf(\"@\") == -1 ){\n";
    echo "        alert(\"". $this->t->_('email_missing_alert')."\");\n";
    echo "        document.Formular.mail.focus();\n";
    echo "        return false;\n";
    echo "    }\n";

    echo "}\n"; // end of chkFormular

    echo "</script>";

    echo "</head>\n<body>\n";
    echo "<div style=\"font-family:Helvetica,Arial,sans-serif;\">";
    echo "<h2 style=\"text-align: center;\">".$this->t->_($title)."</h2>\n";
    //    echo "This is what we got for sure:". serialize ($providedAttrs);
    //    echo $remote_user .serialize ( $attrMap );

    echo "<form name=\"Formular\" action=\"PutAttributes.php\" method=\"post\" onsubmit=\"return chkFormular()\">\n";
    echo "<table style=\"margin-left: auto; margin-right: auto;\">\n";
    echo "<colgroup> <col align=\"left\" span=\"3\"></col></colgroup>\n";
    $oldToUversion = "";
    foreach ($existingAttrs as $a) {
      if ($a->name == "ToUversion") {
	$oldToUversion = $a->value;
	continue;
      }
      $oldval = "";
      if (isset ($a->value)) {
	$oldval = $a->value;
      }
      $providedreadonly = "";
      if (isset($attrMap[$a->name]) && isset($providedAttrs[$attrMap[$a->name]])) {
	$oldval = $providedAttrs[$attrMap[$a->name]];
	if (stripos($remote_user, "@textgrid.de") === FALSE) {
	  $providedreadonly = " readonly=\"readonly\"";
	}
      }
      if ($this->isBoolean ($a)) {
	// pre-check "Searchable" box, cf. TG-1217
 	if (($a->name === 'agreesearch' && $oldval === "") ||
	    $oldval === "TRUE") {
	  $checked = "checked=\"checked\"";
	} else {
	  $checked = "";
	}
      }
      if ($a->mandatory) {
	$mandatory = "<span style=\"color:red\">*</span>";
      } else {
	$mandatory = "";
      }

      echo "<tr><td>".$this->t->_('attr_'.$a->name)." ${mandatory}</td>";
      if ($this->isBoolean ($a)) {
	echo "<td><input type=\"checkbox\" name=\"". $a->name . "\" value=\"on\" ${checked}/></td>\n";
      } else if ($this->isISO3166format ($a)) {
	if ($oldval === "") {$oldval = "DE";} // pre-assume German Scholars
	echo "<td>";
	$this->formatISO3166list( $a->name, $oldval);
	echo "</td>\n";
      } else { // assume string
	echo "<td><input style=\"-webkit-appearance:none;\" type=\"text\" size=\"50\" name=\"". $a->name . "\" value=\"${oldval}\"${providedreadonly}/></td>\n";
      }
      echo "<td><a href=\"javascript:displayhelp('".$this->t->_('attr_'.$a->name)."','". $this->t->_('attr_'.$a->name.'_description') ."')\"><img src=\"./scripts/help.png\" border=\"0\" height=\"25px\" alt=\"?\"></a>";
      echo "</td></tr>\n";
    }

    $checked = "";
    if ($ToUversion == $oldToUversion) {
      $checked = "checked=\"checked\"";
    }
    echo "<tr><td></td><td colspan=\"2\"><div style=\"text-align: right;\"><input type=\"checkbox\" name=\"ToUversion\" value=\"${ToUversion}\" ${checked}/>\n";

    echo $this->t->_('ToU_check_label','',array("javascript:toggleToU('d456')")) ."</div></td></tr>";
    echo "<tr><td colspan=\"3\"><div id=\"d456\">";

    echo $ToUtext;

    echo "</div></td></tr>\n";

    echo "<input type=\"hidden\" name=\"loginmode\" value=\"" . $loginmode . "\" />\n";
    echo "<input type=\"hidden\" name=\"remote_user\" value=\"" . $remote_user . "\" />\n";
    echo "<input type=\"hidden\" name=\"Sid\" value=\"" . $Sid . "\" />\n";
    echo "<input type=\"hidden\" name=\"authZinstance\" value=\"" . $authZinstance . "\" />\n";
    echo "<tr><td></td><td colspan=\"2\"><div style=\"text-align: right;\"><input type=\"reset\" value=\"". $this->t->_('cancel_button')."\">";
    echo "<input type=\"submit\" value=\"".$this->t->_('submit_button')."\"></div></td></tr>";
    echo "</table>\n";
    echo "</form>\n";
    echo "</div>";
    echo "\n</body>\n</html>";
  }

  public function printSetAttributesSuccess ($thedisplayname) {
    $this->printHeader("update_attr_success_pagetitle");
    echo "</head><body>\n";
    echo "<div style=\"font-family:Helvetica,Arial,sans-serif;\">";
    echo $this->t->_('update_attr_thank_you','',array($thedisplayname));
    echo "\n</div></body>\n</html>";
  }


  public function isBoolean ($attr) {
    // TODO this is a hack yet
    if ($attr->name === "agreesearch") {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  public function isISO3166format ($attr) {
    // TODO this is a hack yet
    if ($attr->name === "citizenship" || $attr->name === "country") {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  public function formatISO3166list ( $listname, $preselected ) {
    $list = $this->list_iso3166();

    echo "<select name=\"${listname}\" style=\"width: 200px;\" size=\"1\">\n";
    foreach ($list as $code => $name) {
      if ($code === $preselected) {
	$selected = "selected=\"selected\"";
      }	else {
	$selected = "";
      }
      echo "<option ${selected} value=\"${code}\">${name}</option>\n";
    }
    echo "</select>";
  }

  // taken from http://code.google.com/p/hm2k/source/browse/trunk/code/php/functions/iso3166.php
  function list_iso3166($key=false) { // Returns a list of ISO 3166 alpha 2 country codes and names in an array

    // Make a static copy of this file on your server
    $url='http://www.iso.org/iso/list-en1-semic-3.txt';

    // Path/Filename of the local copy (if present)
    $file='iso3166_en_code_lists.txt';

    // Can we find a local copy?
    if (!file_exists($file)) { $file=$url; }

    // Put the data into a list...
    $list=@file_get_contents($file);

    // No list? Return
    if (empty($list)) { return; }

    // Process list
    $lines=explode("\n",$list);
    $list=array();
    foreach ($lines as $line) {
      $line=explode(';',$line);
      if (isset($line[1])) {
	$list[trim($line[1])]=trim($line[0]);
      }
    }

    // Shift first line (info line) off the array
    array_shift($list);

    //Return either entire list as array or just a key.
    return isset($list[$key])?$list[$key]:$list;
  }

}

?>
