<?php
// #######################################################
// Author: Martin Haase / DAASI International GmbH
// Creation date: 02.12.2008
// Modification date: 06/11/2015 (fu)
// Version: 3.0 (ab TextGridLab 2.2)
// #######################################################

ob_start();
require_once '../i18n_inc/class.I18Nbase.inc.php';
$t = new I18Ntranslator();

header("Content-Type: text/html; charset=UTF-8");

$authZinstance = $_REQUEST["authZinstance"];
if ($authZinstance == null) {
   echo $t->_('no_tgauth_instance_heading') . "\n";
   echo $t->_('no_tgauth_instance_detail');
   exit;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>TextGrid WebAuth</title>
      <!-- Bootstrap -->
      <link href="./css/app-bootstrap.css" rel="stylesheet" media="screen">
    </head>
    <body class="login">
    <header>
      <div class="container">
      	   <img src="./img/drunk-eule.png" height="62px"/>
      	   &nbsp;
      	   <img class="login-headline" src="./img/textgridlab-login.png" alt="TextGridLab-Login" />
      </div>
    </header>
    <div class="container">
    <div class="row">
      <div class="col-xs-3">&nbsp;</div>
      <div class="col-xs-6">
        <h3><?php echo $t->_('login_option_ldap');?></h3>
         <form action="TextGrid-WebAuth.php" method="post" name="textgriddeform">
            <div class="form-group">
              <label for="loginname"><?php echo $t->_('login_label_id');?></label>
              <input type="text" class="form-control" name="loginname" id="loginname"
                value="Login ID" onclick="MachLeer()">
            </div>
            <div class="form-group">
              <label for="password"><?php echo $t->_('login_label_password');?></label>
              <input type="password" class="form-control" name="password" id="password">
            </div>
	    <?php echo "<input name=\"authZinstance\" type=\"hidden\" value=\"". $authZinstance . "\">"; ?>
            <button type="submit" class="btn btn-primary btn-block primary-login">Login</button>
        </form>
	<script type="text/javascript">
	  document.textgriddeform.loginname.focus();
	  document.textgriddeform.loginname.select();
	  function MachLeer () {
	    if (document.textgriddeform.loginname.value == "Login ID") {
              document.textgriddeform.loginname.value = "";
	    }
	  }
	</script>
      </div>
      <div class="col-xs-3">&nbsp;</div>
    </div>
    </div>
    </body>
</html>
