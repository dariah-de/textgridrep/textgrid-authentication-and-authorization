<?php
// #######################################################
// Author: Martin Haase / DAASI International GmbH
// Creation date: 02.12.2008
// Modification date: 06/06/2012
// Version: 2.0
// #######################################################

ob_start();
require_once '../i18n_inc/class.I18Nbase.inc.php';
$t = new I18Ntranslator();


header("Content-Type: text/html; charset=UTF-8");

$authZinstance = $_REQUEST["authZinstance"];
if ($authZinstance == null) {
   echo $t->_('no_tgauth_instance_heading') . "\n";
   echo $t->_('no_tgauth_instance_detail');
   exit;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>TextGrid WebAuth</title>
</head>
<body>
<div style="font-family:Helvetica,Arial,sans-serif;">

<h2><?php echo $t->_('login_heading');?></h2>
<table border="1" rules="cols" cellpadding="4" style="background: #EEEEEE;">
<tr>
<td align="center" valign="top">
<h3><?php echo $t->_('login_option_ldap');?></h3>
</td>

<td align="center" valign="top">
<h3><?php echo $t->_('login_option_shib');?></h3>
</td>
</tr>

<tr>
<td rowspan="2">
<form action="TextGrid-WebAuth.php" method="post" name="textgriddeform">
  <p><?php echo $t->_('login_label_id');?><br><input style="-webkit-appearance:none;" name="loginname" type="text" size="30" maxlength="30" value="Login ID" onclick="MachLeer()"></p>
  <p><?php echo $t->_('login_label_password');?><br><input style="-webkit-appearance:none;" name="password" type="password" size="30" maxlength="30"></p>
<?php
   echo "<input name=\"authZinstance\" type=\"hidden\" value=\"". $authZinstance . "\">";
?>
  <input type="submit" value="log in">
</form>
<script type="text/javascript">
document.textgriddeform.loginname.focus();
document.textgriddeform.loginname.select();
function MachLeer () {
   if (document.textgriddeform.loginname.value == "Login ID")
 	document.textgriddeform.loginname.value = "";
}
</script>
</td>


<td align="center" valign="top">
<form action="../secure/TextGrid-WebAuth.php">
<?php
   echo "<input name=\"authZinstance\" type=\"hidden\" value=\"". $authZinstance . "\">";
   echo "<input type=\"submit\" value=\"".$t->_('login_button_shib')."\">";
?>
</form>
</td>
</tr>

<tr>
<td><?php
echo $t->_('login_shib_explanation');
?></td>
</tr>
</table>
</div>
</body>
</html>
