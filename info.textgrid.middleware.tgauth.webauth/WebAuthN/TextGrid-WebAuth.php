<?php
// #######################################################
// Author: Martin Haase / DAASI International GmbH / TextGrid
// Creation date: 2010-09-23
// Modification date: 2017-02-27
// Version: 0.2
// #######################################################

include("../tglib/LDAP.class.php");
include("../tglib/RBAC.class.php");
include("../tglib/WebUtils.class.php");

$configfile = "/etc/dhrep/tgauth/conf/config_tgwebauth.xml";

$util = new WebUtils;

$authZinstance = $_REQUEST["authZinstance"];

if ( !(isset($authZinstance)) || strlen($authZinstance) <= 0 ) {
  $util->printAuthFailure("no_tgauth_instance_heading", 
		      "no_tgauth_instance_detail", 
		      null, 
		      null );
  exit;
}

$rbac = new RBAC ( $configfile, $authZinstance );

// Variant 1: Authentication at Community LDAP
if (isset ($_REQUEST["loginname"]) && strlen($_REQUEST["loginname"]) > 0
    && isset ($_REQUEST["password"]) && strlen($_REQUEST["password"]) > 0) {
  // now authenticating
  $ldap = new LDAP ( $configfile );
  $AuthNResult = $ldap->authenticate($_REQUEST["loginname"], $_REQUEST["password"]);
  if (! $AuthNResult["success"]) {
    $util->printAuthFailure("authn_failure_heading", 
			$AuthNResult["detail"], 
			$_REQUEST["loginname"], 
			null ); 
    exit;
  }
  $ProvidedAttributes = $ldap->getUserAttributes();
  $_SERVER["REMOTE_USER"] = $AuthNResult["TGID"];

}


// Variant 2: Shibboleth gave us the right REMOTE_USER. 
// We create a Session here in RBAC, also for Variant1
if (isset ($_SERVER["REMOTE_USER"])) { // this holds for both shib and ldap authN

  // now creating session, activating roles, etc, in RBAC

  $CSResult = $rbac->createSession( $_SERVER["REMOTE_USER"] );
  if (isset ($AuthNResult)) {
    $CSResult["rbachash"]["identity_provider"] = $AuthNResult["LDAPname"];
  } else {
    $CSResult["rbachash"]["identity_provider"] = $_SERVER["Shib-Identity-Provider"];
  }

  if (!$CSResult["success"]) {
    $util->printAuthFailure("sid_create_failure_heading", 
			    $CSResult["detail"], 
			    $_REQUEST["loginname"], 
			    $CSResult["rbachash"]
			    ); 
    exit;
  }
  $Sid = $CSResult["rbachash"]["Sid"];

  $AttributeMap = Array ('surname' => 'sn',
			 'organisation' => 'o',
			 'givenname' => 'givenName',
			 'displayname' => 'cn',
			 'mail' => 'mail'
			 );
  if (!isset ($ldap)) {
    $ProvidedAttributes = Array();
    // this is the list of attributes Shibboleth might give to us except from remote_user
    foreach (array ("o", "sn", "givenName", "cn", "mail") as $a) {
      if (isset($_SERVER[$a])) { $ProvidedAttributes[$a] = $_SERVER[$a];}
    }
  }
} 
// This is Variant 3: No Session Creation, but just a desire to see (and update) User Attributes
else if (isset ($_REQUEST["Sid"]) && strlen($_REQUEST["Sid"]) > 0 )  {
// we might have come directly here using the sid and use an earlier session
  $Sid = $_REQUEST["Sid"];
}
// not enough information, exiting. 
  else 
  {
     
    // check if we came via Shibboleth, but without an eduPersonPrincipalName 
    // (which would have been the REMOTE_USER)
      if (isset( $_SERVER['Shib-Session-ID'] )) {
	  $util->printAuthFailure("shib_login_failure_heading", 
				  "shib_login_failure_detail",
				  "(Shibboleth login, but no ePPN provided)", 
				  null ); 
	exit;
      }
    else
      {	
	$missing = 0;
	if (!isset($_REQUEST["loginname"]) || strlen($_REQUEST["loginname"]) == 0) {
	  $missing = 1;
	}
	if (!isset($_REQUEST["password"]) || strlen($_REQUEST["password"]) == 0) {
	  $missing = $missing + 2;
	}
  
	if ($missing == 0) {
	  $util->printAuthFailure("authn_failure_heading", 
				  "authn_failure_detail_nothing_to_do",
				  $_REQUEST["loginname"], 
				  null ); 
	  trigger_error("WebAuth does not know what to do (no login or password provided, no remote user, and no session Id), exiting.", E_USER_WARNING);
	} else if ($missing == 1) {
	  $util->printAuthFailure("authn_failure_heading", 
				  "authn_failure_detail_id_missing",
				  '(null)', 
				  null ); 
	} else if ($missing == 2) {
	  $util->printAuthFailure("authn_failure_heading", 
				  "authn_failure_detail_password_missing",
				  $_REQUEST["loginname"], 
				  null ); 
	} else if ($missing == 3) {
	  $util->printAuthFailure("authn_failure_heading", 
				  "authn_failure_detail_both_missing",
				  '(null)', 
				  null ); 
	}
	exit;
      }
  }

// no matter where we came from we need to retrieve attributes from RBAC
$attributes = $rbac->getUserAttributes( $Sid );

// if we already have enough attributes and just created a session, possibly update
// them if there came different ones, and then finally print welcome screen causing 
// the TextGridLab to take over the Sid
// 2015-11-09 fu: We must not ask for attributes, we do that in DARIAH now! Just print auth access! Yeah!
// if ($rbac->enoughUserAttributes( $Sid ) && isset ($_SERVER["REMOTE_USER"])) {
  $util->printAuthSuccess("authn_succeeded_heading",
			  isset($_REQUEST["loginname"]) ? $_REQUEST["loginname"] : $_SERVER["REMOTE_USER"],
			  $CSResult["rbachash"],
			  $rbac->slcData()
			  );
			  
  $rbac->updateAttributes ( $ProvidedAttributes, $AttributeMap, $Sid ); //  not vital and second-order
// } else {
//   // now presenting the form, let JavaScript take care for the non-empty-check and the help
//   // the form will return either displaying the Sid or just an ACK
//   if (isset ($_SERVER["REMOTE_USER"])) {
//     $util->printAttributeForm( $attributes, $ProvidedAttributes, $AttributeMap, $Sid, $authZinstance, $_SERVER["REMOTE_USER"], $rbac->ToUversion, $rbac->ToUtext);
//   } else if (isset ($_REQUEST["ePPN"]))  { // direct invocation of userdata modification dialogue
//     $util->printAttributeForm( $attributes, null, null, $Sid, $authZinstance, $_REQUEST["ePPN"], $rbac->ToUversion, $rbac->ToUtext);
//   } else {
//     echo "Could not modify attributes, not enough information";
//   }
// }

?>
