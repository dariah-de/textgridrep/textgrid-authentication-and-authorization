/** @class IdP Selector UI */
function IdPSelectUIParms(){

    //
    // Adjust the following to fit into your local configuration
    //
    this.alwaysShow = true;          // If true, this will show results as soon as you start typing
    this.dataSource = '/1.0/Shibboleth.sso/DiscoFeed';    // Where to get the data from
    this.defaultLanguage = 'de';     // Language to use if the browser local doesnt have a bundle
    this.defaultLogo = 'flyingpiglogo.jpg';
    this.defaultLogoWidth = 90;
    this.defaultLogoHeight = 80 ;
    this.defaultReturn = null;       // If non null, then the default place to send users who are not
                                     // Approaching via the Discovery Protocol for example
    //this.defaultReturn = "https://example.org/Shibboleth.sso/DS?SAMLDS=1&target=https://example.org/secure";
    this.defaultReturnIDParam = null;
    //    this.helpURL = 'https://wiki.shibboleth.net/confluence/display/SHIB2/DSRoadmap';
    this.ie6Hack = null;             // An array of structures to disable when drawing the pull down (needed to 
                                     // handle the ie6 z axis problem
    this.insertAtDiv = 'idpSelect';  // The div where we will insert the data
    this.maxResults = 10;            // How many results to show at once or the number at which to
                                     // start showing if alwaysShow is false
    this.myEntityID = null;          // If non null then this string must match the string provided in the DS parms
    this.preferredIdP = [];          //'https://ldap-dariah.esc.rzg.mpg.de/idp/shibboleth']; // Array of entityIds to always show
    this.hiddenIdPs = null;          // Array of entityIds to delete
    this.ignoreKeywords = false;     // Do we ignore the <mdui:Keywords/> when looking for candidates
    this.samlIdPCookieTTL = 730;     // in days
    this.testGUI = false;

    //
    // Globalization stuff
    //
    this.langBundles = {
	'en': {
	    'fatal.divMissing': '<div> specified  as "insertAtDiv" could not be located in the HTML',
	    'fatal.noXMLHttpRequest': 'Browser does not support XMLHttpRequest, unable to load IdP selection data',
	    'fatal.wrongProtocol' : 'Policy supplied to DS was not "urn:oasis:names:tc:SAML:profiles:SSO:idpdiscovery-protocol:single"',
	    'fatal.wrongEntityId' : 'entityId supplied by SP did not match configuration',
	    'fatal.noData' : 'Metadata download returned no data',
	    'fatal.loadFailed': 'Failed to download metadata from ',
	    'fatal.noparms' : 'No parameters to discovery session and no defaultReturn parameter configured',
	    'fatal.noReturnURL' : 'No URL return parameter provided',
	    'fatal.badProtocol' : 'Return request must start with https:// or http://',
	    // 'idpPreferred.label': 'Selection of your Home Organization',
	    'idpEntry.label': 'Or enter your organization\'s name',
	    'idpEntry.NoPreferred.label': 'Enter your organization\'s name',
	    'idpList.label': 'Or select your organization from the list below',
	    'idpList.NoPreferred.label': 'Select your organization from the list below',
	    'idpList.defaultOptionLabel': 'Please select your organization...',
	    'idpList.showList' : 'Allow me to pick from a list',
	    'idpList.showSearch' : 'Allow me to specify the site',
	    'submitButton.label': 'OK',
	    'helpText': ' ',
	    'defaultLogoAlt' : 'DefaultLogo'
	},
	'de': {
	    'fatal.divMissing': 'Das notwendige Div-Element fehlt',
	    'fatal.noXMLHttpRequest': 'Ihr Webbrowser unterstützt keine XMLHttpRequests, IdP-Auswahl kann nicht geladen werden',
	    'fatal.wrongProtocol' : 'DS bekam eine andere Policy als "urn:oasis:names:tc:SAML:profiles:SSO:idpdiscovery-protocol:single"',
	    'fatal.wrongEntityId' : 'Die entityId ist nicht korrekt',
	    'fatal.loadFailed': 'Metadaten konnten nicht heruntergeladen werden: ',
	    'fatal.noparms' : 'Parameter für den Discovery Service oder "defaultReturn" fehlen',
	    'fatal.noReturnURL' : 'URL return Parmeter fehlt',
	    'fatal.badProtocol' : 'Return Request muss mit https:// oder http:// beginnen',
	    'idpOtherHome.label' : 'Other Home Institution',
	    // 'idpPreferred.label': 'Auswahl Ihrer Heimat-Organisation',
	    'idpEntry.label': 'Oder geben Sie den Namen (oder Teile davon) an:',
	    'idpEntry.NoPreferred.label': 'Institutions-Namen (oder Teile davon) angeben:',
	    'idpList.label': 'Oder wählen Sie Ihre Institution aus einer Liste:',
	    'idpList.NoPreferred.label': 'Institution aus folgender Liste wählen:',
	    'idpList.defaultOptionLabel': 'Institution auswählen...',
	    'idpList.showList' : 'Institution aus einer Liste wählen',
	    'idpList.showSearch' : 'Institution selbst angeben',
	    'submitButton.label': 'OK',
	    'helpText': ' ',
	    'defaultLogoAlt' : 'Standard logo'
	},
	'es': {
	    'fatal.divMissing': '<div> determinado como "insertAtDiv" no pudo ser localizado en el HTML',
	    'fatal.noXMLHttpRequest': 'El navegador no soporta XMLHttpRequest, no hasido posible cargar los datos de selección de IdP',
	    'fatal.wrongProtocol' : 'Las políticas suministradas a DS no fueron "urn:oasis:names:tc:SAML:profiles:SSO:idpdiscovery-protocol:single"',
	    'fatal.wrongEntityId' : 'la configuración de entityId proporcionada por SP no coincide',
	    'fatal.noData' : 'La descarga de los metadatos no ha devuelto información',
	    'fatal.loadFailed': 'Ha habido un error descargando los metadatos desde',
	    'fatal.noparms' : 'No hay parámetros para la sesión de descubrimiento ni está configurado el parámetro defaultReturn',
	    'fatal.noReturnURL' : 'No se ha proporcionado ningún parámetro de devolución URL',
	    'fatal.badProtocol' : 'La solicitud de devolución debe empezar con https:// o http://',
	    // 'idpPreferred.label': 'Selecciona tu institución',
	    'idpEntry.label': 'O introduce el nombre de tu organización',
	    'idpEntry.NoPreferred.label': 'Introduce el nombre de tu organización',
	    'idpList.label': 'O selecciona tu institución desde la lista de abajo',
	    'idpList.NoPreferred.label': 'Selecciona tu institución desde la lista de abajo',
	    'idpList.defaultOptionLabel': 'Por favor, selecciona tu organización...',
	    'idpList.showList' : 'Permíteme escoger desde una lista.',
	    'idpList.showSearch' : 'Permíteme indicar el lugar',
	    'submitButton.label': 'OK',
	    'helpText': ' ',
	    'defaultLogoAlt' : 'Logo por defecto'
	}
    };

    //
    // The following should not be changed without changes to the css. Consider them as mandatory defaults
    //
    this.maxPreferredIdPs = 0;
    this.maxIdPCharsButton = 33;
    this.maxIdPCharsDropDown = 58;
    this.minWidth = 20;
    this.minHeight = 20;
    this.maxWidth = 115;
    this.maxHeight = 69;
    this.bestRatio = Math.log(80 / 60);
}
