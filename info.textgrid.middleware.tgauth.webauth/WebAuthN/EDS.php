<?php
// #######################################################
// Author: Martin Haase / DAASI International GmbH
// Creation date: 02.12.2008
// Modification date: 06.05.2015 (fu)
// Version: 3.0 (ab TextGridLab 3.0)
// #######################################################

ob_start();
require_once '../i18n_inc/class.I18Nbase.inc.php';
$t = new I18Ntranslator();

header("Content-Type: text/html; charset=UTF-8");
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>TextGrid WebAuth</title>
      <meta name="description" content="TextGrid WebAuth">
      <meta name="viewport" content="width=device-width,initial-scale=1">
      <link rel="stylesheet" href="./css/idpselect.css">
      <link rel="stylesheet" href="./css/app-bootstrap.css" media="screen">
    </head>

    <body class="login">

    <header>
      <div class="container">
        <img class="login-headline" src="./img/textgridlab-login.png"
	alt="TextGridLab-Login"></img>
      </div>
    </header>

    <div class="container">
      <div class="row">
    
        <!-- TextGrid/DARIAH-Account BEGINN -->
      
        <div class="col-xs-6">
          <h3><?php echo $t->_('login_option_ldap');?></h3>   
	  <p><?php echo $t->_('login_text_ldap');?></p>

	  <form method="get" action="" id="dform">
	    <button class="btn btn-primary btn-block primary-login" type="submit"
	    value="DARIAH"><?php echo $t->_('login_button_ldap');?></button>
          </form>
        </div>

        <!-- TextGrid/DARIAH-Account ENDE -->

        <!-- Anmelden an Heimatinstitution BEGINN -->

        <div class="col-xs-6">
          <h3><?php echo $t->_('login_option_shib');?></h3>
          <div id="idpOtherHome"></div>
          <div id="idpSelect"></div>
        </div>

        <!-- Anmelden an Heimatinstitution ENDE -->
    
      </div>
    </div>

    <p>&nbsp;</p>
    <div class="main-area"><div class="container" style="font-size:90%;line-height:1.15;">
    	<?php echo $t->_('login_explanation'); ?>
    </div></div>

    <script type="text/javascript" src="./js/vendor/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="./js/vendor/bootstrap.min.js"></script>
    <script type="text/javascript" src="./js/idpselect_config.js"></script>
    <script type="text/javascript" src="./js/vendor/idpselect.js"></script>
    <script type="text/javascript" src="./js/jquery.i18n.properties-min-1.0.9.js"></script>
    <script type="text/javascript" src="./js/dariah.js"></script>

  </body>
</html>
