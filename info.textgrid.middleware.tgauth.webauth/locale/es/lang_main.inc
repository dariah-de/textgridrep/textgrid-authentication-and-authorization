authn_error_status = DARIAH no ha podido identificarte.

further_information = Más información

login_name = Nombre de usuario

only_shib_knowns = solo se conoce en tu institución

tgauth_instance = Instancia de TgAuth

idp_entityid = Proveedor de identidad de Shibboleth

authn_method = Nombre de atributo del ID del usuario

user_id = Valor del ID del usuario

sid = ID de la sesión de TgAuth

error_details_blabla_goback = Si no se indica otra cosa en los detalles de arriba, es posible que algún seervicio no esté respondiendo temporalmente. En ese caso, por favor, <a href="javascript:history.back()">regresa</a> o vuelve a abrir TextGridLab e inténtalo de nuevo.

error_details_blabla_date = Si el problema persiste, por favor, informa del error junto con el momento del hecho (%1$s). En TextGridLab, elige 'Ayuda -&gt; Informar de un error ...'.

no_tgauth_instance_heading = No se ha proporcionado ninguna instancia de TgAuth

no_tgauth_instance_detail = Por favor, proporciona una cadena válida en la variable authZinstance.

authn_failure_heading = Ha habido un error en la identificación en el servidor de cuentas de la comunidad DARIAH.

authn_failure_detail_id_missing = No se ha podido realizar la identificación: no se ha proporcionado ningún ID. En caso de que hayas olvidado tu contraseña, por favor, haz clic en el botón inferior.

authn_failure_detail_password_missing = No se ha podido realizar la identificación: no se ha proporcionado ninguna contraseña. En caso de que hayas olvidado tu contraseña, por favor, haz clic en el botón inferior.

authn_failure_detail_both_missing = No se ha podido realizar la identificación: no se ha proporcionado ningún ID ni contraseña. En caso de que hayas olvidado tu contraseña, por favor, haz clic en el botón inferior.

authn_failure_detail_nothing_to_do = WebAuth no sabe qué hacer (no se ha proporcionado ningún nombre de usuario ni contraseña, ningún usuario remoto y ningún ID de sesión). En caso de que hayas olvidado tu contraseña, por favor, haz clic en el botón inferior.

sid_create_failure_heading = Ha habido un error creando la sesión en RBAC

shib_login_failure_heading = La identificación en Shibboleth vía DFN-AAI falló

shib_login_failure_detail = No se ha proporcionado ningún identificador. TextGrid necesita el atributo <b>eduPersonPrincipalName</b> para identificarte, pero tu institución no lo ha proporcionado. Por favor, contacta con la administración informática de tu centro para proporcionar este atributo a TextGrid. Mira <a href="https://auth.dariah.eu/cgi-bin/selfservice/ldapportal.pl?mode=selfreg&initialgroup=textgrid-stats">https://auth.dariah.eu/cgi-bin/selfservice/ldapportal.pl?mode=selfreg&initialgroup=textgrid-stats</a>.

authn_succeeded_heading = Éxito en la identificación

authn_failure_pagetitle = La identificación ha fallado

update_attr_please_pagetitle = Por favor, completa tus atributos de usuario

modify_attr_pagetitle = Tus atributos de usuario para su modificación

update_attr_success_pagetitle = Éxito actualizando los atributos de usuario

update_attr_thank_you = Gracias, %1$s, tu atributos de usuario están ahora actualizados.<br/>Puedes cerrar este diálogo.

auth_success_text = Has sido identificado correctamente con el ID de usuario '<b>%1$s</b>'. Ahora puedes acceder a recursos remotos utilizando TextGridLab. Puedes cerrar esta ventana.

more_details = Más <a href="%1$s">detalles</a>.

authn_details_heading = Detalles de identificación

login_heading = Identificación de TextGridLab

login_option_ldap = Cuenta de DARIAH

login_text_ldap = Aquí serás dirigido directamente a la página de identifiación de DARIAH.

login_button_ldap = Identificación vía DARIAH

login_option_shib = Tu cuenta

login_label_id = Nombre de usuario:

login_label_password = Contraseña:

login_button_shib = Identificarse vía eduGAIN/DFN-AAI

login_explanation = Si no tienes una cuenta de DARIAH aún, o si tu institución no está en la lista de organizaciones de la página de DFN-AAI, puedes <a href="https://auth.dariah.eu/cgi-bin/selfservice/ldapportal.pl?mode=selfreg&initialgroup=textgrid-stats"><b>solicitar una cuenta de DARIAH</b></a> utilizando este formulario.

attr_givenname = Nombre

attr_givenname_description = Tu nombre

attr_surname = Apellido

attr_surname_description = Tu apellido

attr_displayname = Nombre completo

attr_displayname_description = El nombre que se mostrará.

attr_mail = Correo electrónico

attr_mail_description = Tu dirección de correo electrónico.

attr_organisation = Institución

attr_organisation_description = Institución u organización.

attr_orgunit = Departamento

attr_orgunit_description = Tu unidad dentro de la organización.

attr_street = Calle

attr_street_description = La calle de la institución, con número.

attr_plz = Código postal

attr_plz_description = Código postal, Zipcode o PLZ

attr_city = Ciudad

attr_city_description = Localidad de tu organización.

attr_country = País

attr_country_description = País en el que se encuentra la organización.

attr_tel = Teléfono

attr_tel_description = Número de teléfono (formato internacional)

attr_citizenship = Nacionalidad

attr_citizenship_description = Tu país de nacionalidad

attr_interest = Campos de interés

attr_interest_description = Especificación opcional de los campos de interés en investigación en colaboración con DARIAH.

attr_personid = ID de persona

attr_personid_description = PND / URI de persona, p. ej. http://xyz.org/~johndoe

attr_agreesearch = Consultable

attr_agreesearch_description = Si esta opción está marcada, otros usuarios de TextGrid pueden encontrarte con tu nombre, institución o dirección de correo electrónico

attr_ToUversion = Términos de uso

attr_ToUversion_description = La última versión de los términos de uso ha sido aceptada por el usuario

ToU_check_label =  Acepto los <a href="%1$s">términos de uso de TextGrid</a>

ToU_alert = Debes aceptar los términos de uso para poder proceder.

mandatory_field_empty_alert = Por favor, especifica tu %1$s \n (%2$s).

email_missing_alert = Por favor, introduce una dirección de correo electrónico válida.

cancel_button = Cancelar

submit_button = Enviar
