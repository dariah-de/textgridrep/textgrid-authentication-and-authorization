authn_error_status = DARIAH could not authenticate you.

further_information = Further Information

login_name = Login Name

only_shib_knowns = only known at your home organization

tgauth_instance = TgAuth Instance

idp_entityid = Shibboleth Identity ProviderID

authn_method = User ID Attribute Name

user_id = User ID Value

sid = TgAuth Session ID

error_details_blabla_goback = If not indicated otherwise in the above details, it is possible that some service is not responding temporarily. In this case, please <a href="javascript:history.back()">go back</a> or re-open the TextGridLab and try again.

error_details_blabla_date = If the problem persists, please report this bug together with its time of occurrence (%1$s). In the TextGridLab, choose 'Help -&gt; Report Bug ...'.

no_tgauth_instance_heading = No TgAuth Instance provided

no_tgauth_instance_detail = Please provide a valid string in the authZinstance variable.

authn_failure_heading = Failure authenticating at DARIAH Community Account Server

authn_failure_detail_id_missing = Could not authenticate, no login ID provided. In case you forgot your password, please click the button below.

authn_failure_detail_password_missing = Could not authenticate, no password provided. In case you forgot your password, please click the button below.

authn_failure_detail_both_missing = Could not authenticate, no login ID and password provided. In case you forgot your password, please click the button below.

authn_failure_detail_nothing_to_do = WebAuth does not know what to do (no login or password provided, no remote user, and no session Id). In case you forgot your password, please click the button below.

sid_create_failure_heading = Failure Creating Session in RBAC

shib_login_failure_heading = Shibboleth Login via DFN-AAI failed

shib_login_failure_detail = No identifier provided. TextGrid needs the attribute <b>eduPersonPrincipalName</b> to identify you, but your organisation did not provide it. Please inquire with your organisation's computing centre to release this attribute to TextGrid. See <a href="https://auth.dariah.eu/cgi-bin/selfservice/ldapportal.pl?mode=selfreg&initialgroup=textgrid-stats">https://auth.dariah.eu/cgi-bin/selfservice/ldapportal.pl?mode=selfreg&initialgroup=textgrid-stats</a>.

authn_succeeded_heading = Authentication succeeded

authn_failure_pagetitle = Authentication failed

update_attr_please_pagetitle = Please Complete your User Attributes

modify_attr_pagetitle = Your User Attributes for Modification

update_attr_success_pagetitle = Successfully updated User Attributes

update_attr_thank_you = Thank you, %1$s, your user attributes are now up to date.<br/>You can close this dialogue.

auth_success_text = You were successfully authenticated with User ID '<b>%1$s</b>'. You may now access remote resources using the TextGridLab. This window can be closed.

more_details = More <a href="%1$s">Details</a>.

authn_details_heading = Authentication Details

login_heading = TextGridLab Login

login_option_ldap = DARIAH Account

login_text_ldap = Here you will directly be forwarded to the DARIAH login page.

login_button_ldap = Login via DARIAH

login_option_shib = Home Account

login_label_id = Username:

login_label_password = Password:

login_button_shib = Login via eduGAIN/DFN-AAI

login_explanation = If you do not have a DARIAH account yet, or if your school is not listed among the organizations on the DFN-AAI page, you are welcome to <a href="https://auth.dariah.eu/cgi-bin/selfservice/ldapportal.pl?mode=selfreg&initialgroup=textgrid-stats"><b>request a DARIAH account</b></a> using this form.

attr_givenname = First Name

attr_givenname_description = Your Given Name

attr_surname = Surname

attr_surname_description = Your Last Name

attr_displayname = Full Name

attr_displayname_description = Your Name for Display Purposes

attr_mail = E-Mail

attr_mail_description = Your Electronic Mail Address

attr_organisation = Institution

attr_organisation_description = Institute or Organisation

attr_orgunit = Department

attr_orgunit_description = Organisational Unit

attr_street = Street

attr_street_description = Institution\'s Street Address incl. Number

attr_plz = Zip Code

attr_plz_description = Postal Code, Zipcode or PLZ

attr_city = City

attr_city_description = Location of Institution

attr_country = Country

attr_country_description = Institution's Country of Residence

attr_tel = Phone

attr_tel_description = Phone Number (International Format)

attr_citizenship = Citizenship

attr_citizenship_description = Your Country of Citizenship

attr_interest = Field of Interest

attr_interest_description = Optional specification of fields of research interests in conjuction with DARIAH

attr_personid = Person ID

attr_personid_description = PND / URI for Person, e.g. http://xyz.org/~johndoe

attr_agreesearch = Searchable

attr_agreesearch_description = If this option is set, other TextGrid users can find you by Name, Institution or E-Mail-Address

attr_ToUversion = Terms Of Use

attr_ToUversion_description = Latest Version of Terms Of Use that has been accepted by user

ToU_check_label =  I accept the <a href="%1$s">TextGrid Terms of Use</a>

ToU_alert = You must accept the Terms of Use in order to proceed.

mandatory_field_empty_alert = Please specify your %1$s \n (%2$s).

email_missing_alert = Please specify your valid E-Mail-Address.

cancel_button = Cancel

submit_button = Submit
