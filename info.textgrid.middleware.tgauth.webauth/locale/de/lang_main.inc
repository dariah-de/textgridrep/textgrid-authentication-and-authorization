authn_error_status = DARIAH konnte Sie nicht authentifizieren.

further_information = Weitere Angaben

login_name = Nutzername

only_shib_knowns = nur an Ihrer Heimatuniversit&auml;t bekannt

tgauth_instance = TgAuth-Instanz

idp_entityid = Shibboleth Identity ProviderID

authn_method = Benutzer-ID Merkmal-Name

user_id = Benutzer-ID Wert

sid = TgAuth-Sitzungs-ID

error_details_blabla_goback = Wenn nicht anders in obigen Informationen angegeben, k&ouml;nnte es sein, dass ein Dienst vor&uuml;bergehend nicht antwortet. In diesem Fall <a href="javascript:history.back()">gehen Sie bitte zur&uuml;ck</a> oder &ouml;ffnen Sie das TextGridLab erneut und versuchen Sie es noch einmal.

error_details_blabla_date = Wenn das Problem bestehen bleibt, melden Sie bitte diesen Fehler zusammen mit dem Zeitpunkt seines Auftretens (%1$s). W&auml;hlen Sie 'Hilfe -&gt; Bug-Report ...' im TextGridLab.

no_tgauth_instance_heading = Keine TgAuth-Instanz angegeben

no_tgauth_instance_detail = Bitte geben Sie eine g&uuml;ltige Zeichenkette in der authZinstance-Variable an.

authn_failure_heading = Authentifizierung am DARIAH-Community-Account-Server fehlgeschlagen

authn_failure_detail_id_missing = Authentifizierung nicht m&ouml;glich, keine Benutzer-ID angegeben. Wenn Sie Ihr Passwort vergessen haben, klicken Sie bitte auf die nachstehende Schaltfl&auml;che.

authn_failure_detail_password_missing = Authentifizierung nicht m&ouml;glich, kein Passwort angegeben. Wenn Sie Ihr Passwort vergessen haben, klicken Sie bitte auf die nachstehende Schaltfl&auml;che.

authn_failure_detail_both_missing = Authentifizierung nicht m&ouml;glich, keine Benutzer-ID und kein Passwort angegeben. Wenn Sie Ihr Passwort vergessen haben, klicken Sie bitte auf die nachstehende Schaltfl&auml;che.

authn_failure_detail_nothing_to_do = WebAuth wei&szlig; nicht, was zu tun ist (kein Benutzername oder Passwort angegeben, kein Remote-Nutzer und keine Sizungs-ID). Wenn Sie Ihr Passwort vergessen haben, klicken Sie bitte auf die nachstehende Schaltfl&auml;che.

sid_create_failure_heading = Sitzungserstellung in RBAC fehlgeschlagen

shib_login_failure_heading = Shibboleth-Anmeldung &uuml;ber DFN-AAI fehlgeschlagen

shib_login_failure_detail = Kein Identifizierer angegeben. TextGrid ben&ouml;tigt das Attribut <b>eduPersonPrincipalName</b>, um Sie zu identifizieren, aber Ihre Organisation hat dieses nicht angegeben. Bitte setzen Sie sicht mit dem Rechenzentrum Ihrer Organisation in Verbindung, um dieses Attribut an TextGrid zu &uuml;bermitteln. Siehe dazu <a href="https://auth.dariah.eu/cgi-bin/selfservice/ldapportal.pl?mode=selfreg&initialgroup=textgrid-stats">https://auth.dariah.eu/cgi-bin/selfservice/ldapportal.pl?mode=selfreg&initialgroup=textgrid-stats</a>.

authn_succeeded_heading = Authentifizierung erfolgreich

authn_failure_pagetitle = Authentifizierung fehlgeschlagen

update_attr_please_pagetitle = Bitte vervollst&auml;ndigen Sie Ihre Nutzermerkmale

modify_attr_pagetitle = Ihre Nutzermerkmale zur Bearbeitung

update_attr_success_pagetitle = Nutzermerkmale erfolgreich aktualisiert

update_attr_thank_you = Vielen Dank, %1$s, Ihre Nutzermerkmale wurden jetzt aktualisiert.<br/>Sie k&ouml;nnen diesen Dialog schlie&szlig;en.

auth_success_text = Sie wurden erfolgreich mit der Benutzer-ID '<b>%1$s</b>' authentifiziert. Sie haben jetzt durch das TextGridLab Zugriff auf entfernte Ressourcen. Dieses Fenster kann geschlossen werden.

more_details = N&auml;here <a href="%1$s">Informationen</a>.

authn_details_heading = Authentifizierungsmerkmale

login_heading = TextGridLab-Anmeldung

login_option_ldap = DARIAH-Account

login_text_ldap = Hier gelangen Sie direkt zur Anmeldung &uuml;ber Ihren DARIAH-Account.

login_button_ldap = Anmelden &uuml;ber DARIAH

login_option_shib = Account an Heimatinstitution

login_text_shib = urgl

login_label_id = Nutzername:

login_label_password = Passwort:

login_button_shib = Anmelden &uuml;ber eduGAIN/DFN-AAI

login_explanation = Sollten Sie &uuml;ber keinen DARIAH-Account verf&uuml;gen oder Ihre Heimatinstitution nicht in der Liste der Organisationen gef&uuml;hrt werden, k&ouml;nnen Sie gerne &uuml;ber das folgende Formular einen <a href="https://auth.dariah.eu/cgi-bin/selfservice/ldapportal.pl?mode=selfreg&initialgroup=textgrid-stats"><b>DARIAH-Account beantragen</b></a>.

attr_givenname = Vorname

attr_givenname_description = Ihr Vorname

attr_surname = Nachname

attr_surname_description = Ihr Familienname

attr_displayname = Vollst&auml;ndiger Name

attr_displayname_description = Ihr in TextGrid angezeigter Name

attr_mail = E-Mail

attr_mail_description = Ihre Elektronische Mail-Addresse

attr_organisation = Institution

attr_organisation_description = Institut oder Organisation

attr_orgunit = Fachbereich

attr_orgunit_description = Organisationseinheit

attr_street = Stra&szlig;e

attr_street_description = Stra&szlig;enname der Institution inkl. Nummer

attr_plz = PLZ

attr_plz_description = Postleitzahl oder Zipcode

attr_city = Stadt

attr_city_description = Heimatort Ihrer Institution

attr_country = Land

attr_country_description = Heimatland Ihrer Institution

attr_tel = Telefon

attr_tel_description = Telefonnummer (internationales Format)

attr_citizenship = Nationalit&auml;t

attr_citizenship_description = Ihre Nationalit&auml;t

attr_interest = Interessengebiet

attr_interest_description = Optionale Spezifizierung Ihrer Forschungsinteressen in Verbindung mit DARIAH

attr_personid = Personen-ID

attr_personid_description = PND / URI f&uuml;r Person, z.B. http://xyz.org/~johndoe

attr_agreesearch = Suchbar

attr_agreesearch_description = Wenn diese Option gew&auml;hlt wird, k&ouml;nnen andere TextGrid-Nutzer Sie mittels Name, Institution oder E-Mail-Adresse finden

attr_ToUversion = Nutzungsbedingungen

attr_ToUversion_description = Aktuellste Version der Nutzungsbedingungen, die vom Nutzer akzeptiert wurde

ToU_check_label =  Ich akzeptiere die <a href="%1$s">TextGrid-Nutzungsbedingungen</a>

ToU_alert = Sie m&uuml;ssen die Nutzungsbedingungen akzeptieren, um fortfahren zu k&ouml;nnen.

mandatory_field_empty_alert = Bitte f&uuml;llen Sie das Feld %1$s aus \n (%2$s).

email_missing_alert = Bitte geben Sie Ihre g&uuml;ltige E-Mail-Adresse an.

cancel_button = Abbrechen

submit_button = Speichern
