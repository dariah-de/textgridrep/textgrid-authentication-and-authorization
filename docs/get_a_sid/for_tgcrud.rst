Getting a TextGrid Session ID for Web Applications that Use TG-crud
===================================================================

This document describes how to get a TextGrid Session ID (SID) for web applications that perform operations on the TextGrid repository via TG-crud. Note: this is not suitable for automated scripts, there must be a user present which enters a web page and logs into the DARIAH AAI using the credentials.

Adding the Domain Name to the Whitelist (admins only)
-----------------------------------------------------

The web applications cannot use the service if it is not listed on the whitelist. To add your domain to the whitelist, make sure that you checked out the latest version of dariah_de_puppet (https://gitlab.gwdg.de/dariah-de-puppet/dariah_de_puppet). Now open 

    * hieradata/textgrid-esx1.gwdg.de.yaml (TextGrid dev instance)
    * hieradata/textgrid-esx2.gwdg.de.yaml (TextGrid production instance)

Find

::

    dhrep::services::tgauth::sid_redirect_domains_allowed:

add your domain name / ip to the end of this list via

::

    - 'example.com' # responsible -> John Doe (john.doe@bloodybaron.com), project Foo


It is a good practice to add a comment and note who is responsible for the service and for which project this is used.

Do this for both files (textgrid-esx1.gwdg.de.yaml and textgrid-esx1.gwdg.de.yaml). Save the changes, commit, and push the changes to the dariah_de_puppet git repository. There is NO need to update the libraries (no need to execute librarian-puppet or change Puppetfile.lock).

**Important**: Depending on the check in time, Puppet may take up to one hour to apply the configuration change to the TextGrid servers.


Using the TG-sid Service from a Web Application
-----------------------------------------------

It does not matter if your web application is server side or not. Typically you will implement a mechanism like this:

    1. Check if a TGSID exists
    2. No? Then send the user to the TGSID service. If the user is not logged in yet via Shibboleth, the user must enter the credentials. If logged in, it will get automatically redirected.
    3. After successful authentication the TGSID service will redirect you back to your application. The URL is up to you, but the start of the URL (domain) must be in the whitelist.
    4. Create a session for the user or store the TGSID temporary until the operation / app has finished.


(Optional) Getting More Information about the User besides the TG-sid
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you need more information about the user (such as username information) then you have to configure Shibboleth for you application like you would normally do. Create a URL that is protected via Shibboleth (Apache), such as

::

    <Location /yourapp/shiboleth>
        AuthType shibboleth
        ShibRequestSetting requireSession 1
        require valid-user
    </Location>

Set any Shibboleth group that you want, this is only an example. Then on your app you need to explicitly set the attributes you need in your application. Example:

::

    <Location /yourapp/>
        AuthType shibboleth
        Require shibboleth
        ShibUseHeaders On
        RequestHeader set eppn %{eppn}e env=eppn
        RequestHeader set givenName %{givenName}e
   </Location>


This will pass the ePPN and the givenName to the application via the Apache environment variables. See all available attributes on the DARIAH AAI documentation. How to use the server environment variables depends on the programming language. For some examples see https://shibboleth.atlassian.net/wiki/spaces/SP3/pages/2065335257/AttributeAccess#Custom-SP-Variables 

After you have done the Shibboleth authentication and creating a session for the user, continue with the steps described in “Using the TG-sid Service from a Web Application“. Since the user is already logged in, there will be only a couple of automated redirects without a login form.


URL of the TG-sid Service
^^^^^^^^^^^^^^^^^^^^^^^^^

To get a SID, perform a redirect to:


**Development Instance of TextGrid**

::

    https://dev.textgridlab.org/1.0/secure/TextGrid-WebAuth-GetSid.php?authZinstance=textgrid-esx1.gwdg.de&return=https://example.com/myapp/auth


**Production Instance of TextGrid**

::

    https://textgridlab.org/1.0/secure/TextGrid-WebAuth-GetSid.php?authZinstance=textgrid-esx2.gwdg.de&return=https://example.com/myapp/auth


Replace *https://example.com/myapp/auth* with your domain name and change the path to the web application / view you want to retrieve the TGSID.


Getting the SID
"""""""""""""""

After successful authentication the TGSID service will create a redirect to

::

    https://example.com/myapp/auth?sid=THEMIGHTYTGSIDFORTHEUSERTORULETHEMALL


As you see you get the SID with the HTTP request parameter “sid“ and the contents is the TGSID for the user.

Now it is up to you to parse the request parameter with you server side application or via Javascript.


Strong Recommendation
"""""""""""""""""""""

After you have received a SID it is good practise to create a session for the user, store the SID and perform a redirect to clean URL of your application so that the ..?sid=THEMIGHTYTGSIDFORTHEUSERTORULETHEMALL is not visible in the browser any longer (of course it is also possible to do this in pure Javascript, too).

**Why is this important?**

If the user shares the URL with others by copy and paste, then someone else may be able to perform changes to files in the TextGrid repository that the user can access. Typically the SID is longer valid the the Shibboleth session ID.
