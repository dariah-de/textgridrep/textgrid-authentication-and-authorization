.. tgauth* documentation master file, created by
   sphinx-quickstart on Thu May 21 16:45:28 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


TG-auth*
========

The TG-auth* system consists of two main components:

* openRBAC, a system to maintain, modify, and enforce authorization policies using the Role-Based Access Control framework. See http://www.openrbac.de/, however, the basic software has been heavily customized for use with TextGrid.
* WebAuthN, a system offering authentication functionalities, both direct using a community-managed user directory and the Shibboleth-based DFN-AAI. WebAuthN is embedded in TextGridLab offering a Login Screen and registers the user in RBAC.

The two components *PWchange* and *PWreset* are deprecated, please use the `DARIAH Self Service Portal <https://auth.dariah.eu/cgi-bin/selfservice/ldapportal.pl>`_ to change DARIAH-AAI passwords.


Technical Information
---------------------


Response
^^^^^^^^

Implementation: PHP, consisting of

* openRBAC core: RBAC implementation backed up by an LDAP directory, e.g. openLDAP
* openRBAC Web Service layer: for accessing openRBAC functions via SOAP
* tgextra (also a SOAP Web Service): additional functions implemented for TextGrid needs, either aggregating basic RBAC functions or introducing unrelated functions that leverage the underlying LDAP server as storage
* Storage: an OpenLDAP server


There are two additional schemas: for RBAC core and for TextGrid-specific attributes

* Branches:
    * ou=people for users
    * ou=roles for the roles users can activate. TextGrid projects are treated like roles, with sub-roles for the actual roles visible in the TextGridLab. e.g. Administrator or Editor
    * ou=resources for the TextGridObjects and their role-right assignments
    * ou=sessions for the Session IDs that users have in the TextGridLab and the roles they activated in their sessions


WebAuthN
^^^^^^^^

* Implementation: PHP
    * Dual Login on the first page:
        * direct authentication in the community LDAP server or via
        * Shibboleth Login with DFN-AAI-Basic
    * Both Login methods populate the Server variable $REMOTE_USER
* In Login Mode, the following happens:
    1. authentication
    2. registration of a user session with activation of all available roles in RBAC
    3. check if user has filled out all required personal information and accepted the Terms of use
    4. exposure of the newly assigned Session ID for use in further activities with the TextGridLab and the TG-Utilities
* In User Details mode (no authentication, just see and modify user's attributes), only 3. happens.
* One WebAuthN installation with one community LDAP server can interact with multiple RBAC instances.
* HTTP GET or POST arguments for TextGrid-WebAuth.php:
    * authZinstance – string identifying the RBAC instance to be used. Always needed.
    * loginname and password – for authentication at community LDAP. Only in Login mode and with HTTP POST.
    * Sid – Session ID known from some earlier authentication. Necessary for User Details mode.
    * ePPN - User ID of the user. Necessary in User Details mode.
* TextGrid-WebAuth.php is being called from WebAuthN2.php, which presents both the community login form and the Shibboleth Login Button.
* For Shibboleth login, the Shibboleth Service Provider (Apache module) guarantees the provision of a correct User ID delivered from some home organisation.

If you are looking for a service that delivers TextGrid Session IDs for a web applications that uses TG-crud, please see: :doc:`Getting a TextGrid Session ID for Web Applications that Use TG-crud <get_a_sid/for_tgcrud>`.


PWchange (deprecated)
^^^^^^^^^^^^^^^^^^^^^

* PHP Web application
* Authenticates and changes passwords against an LDAP directory (community LDAP server)
* Source currently not in SVN, but available upon request
* Please use the `DARIAH Self Service Portal <https://auth.dariah.eu/cgi-bin/selfservice/ldapportal.pl>`_ to change passwords.


PWreset (deprecated)
^^^^^^^^^^^^^^^^^^^^

* Perl Web application
* sends out links for verification of the user's email address
* must be used with the system's Web browser, not the TextGridLab-internal one, because of the use of cookies to remember the user
* Please use the `DARIAH Self Service Portal <https://auth.dariah.eu/cgi-bin/selfservice/ldapportal.pl>`_ to change passwords.


URLs
----


Repository
^^^^^^^^^^

* Current development is in GIT repository of GWDG Gitlab: https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization


WSDL
^^^^

OpenRBAC SOAP WSDL locations on the productive TextGridRep TG-auth* server:

* Most relevant for Lab/User interaction: https://textgridlab.org/1.0/tgauth/wsdl/tgextra.wsdl  >>  Documentation of TGextra WSDL Methods
* Relevant for Server access: https://textgridlab.org/1.0/tgauth/wsdl/tgextra-crud.wsdl  >>  Documentation of TGextra-crud Methods
* Administrative functions:
    * https://textgridlab.org/1.0/tgauth/wsdl/tgadministration.wsdl
    * https://textgridlab.org/1.0/tgauth/wsdl/tgreview.wsdl
    * https://textgridlab.org/1.0/tgauth/wsdl/tgsystem.wsdl


API Documentation
-----------------

.. toctree::
   :maxdepth: 1

   api/tg-authApi.rst
   api/tg-authCrudApi.rst


Sources
-------

See tgauth_sources_


License
-------

See LICENCE_

.. _LICENCE: https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization/-/blob/main/info.textgrid.middleware.tgauth.passwordReset/License.txt
.. _tgauth_sources: https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization/-/tree/main
