TG-auth* API
============


userExists
----------

Checks whether this UserID (eduPersonPrincipalName) exists in RBAC.

* Input Parameters userExistsRequest, with elements

  * auth - String, SessionID of application or user asking
  * log - String for log information, optional
  * username - String containing the ID (ePPN) of the user

* Output Parameters booleanResponse, with element

  * result - boolean, true if user exists, false otherwise

* Faults

  * authenticationFault


getNames
--------

Returns user records for a list of ePPNs

* Input Parameters getNamesRequest, with elements

  * auth - String, SessionID of user that wants to query for names
  * log - String,
  * ePPN - List of Strings


getIDs
------

Returns user records for a name or mail address.

* Input Parameters getIDsRequest, with elements

  * auth - String, SessionID of user that wants to query for names
  * log - String,
  * name - String with Name
  * mail - String with E-Mail Address
  * organisation - String with Organisation

* Output Parameters getIDsResponse. It contains 0...n userDetails, which themselves have elements

  * ePPN - String holding ePPN of user
  * name - String holding Name (cn) of user
  * mail - String holding E-Mail address of user
  * organisation - String holding user' affiliation
  * agreesearch - Boolean, whether user wants his ID to be found be a search for name or mail address
  * usersupplieddata - Boolean, whether user himself or his organisation supplied these data

* Faults

  * none


getObjects
----------

Returns URIs of all resources in project. Caller must have some role in the project.

* Input Parameters getObjectsRequest, with elements

  * auth - String, SessionID of user
  * log - String for log information, optional
  * project - String with projectID

* Output Parameters resourcesetResponse, with element

  * resource - String with URI of resource in project. Can occur multiple times


tgCheckAccess
-------------

Returns access decision for given operation on given resource for session.

* Input Parameters tgCheckAccessRequest, with elements

  * auth - String, SessionID of user (or service) that wants to intitiate the check
  * log - String for log information, optional
  * sid - String, SessionID of user for whom the check is to be done. Leave empty if check is to be done for user's own session given under auth
  * operation - String, such as "read", "write"
  * resource - String, name of project or role, or URI of TgObject resource

* Output Parameters booleanResponse, with element

  * result - boolean, true if operation was successful, false otherwise

* Faults

  * unknownResourceFault


tgAssignedRoles
---------------

Returns the roles the requesting user has, in any project. Can be called for another user by project leaders, then roles will be limited to the intersection of those projects where auth is leader AND username has some role in.

* Input Parameters tgAssignedRolesRequest, with elements

  * auth - String, SessionID of user that wants to query for roles
  * log - String for log information, optional
  * username - String, ePPN of user for whom the query is to be done. Can be empty.

* Output Parameters rolesetResponse, with element

  * role - String with full-qualified role name of the role. The full-qualified role name has the form "Administrator,TGPR3,Projekt-Teilnehmer". Can occur multiple times.


tgAssignedProjects
------------------

Returns projectIDs of all projects the user has some role in.

* Input Parameters tgAssignedProjectsRequest, with elements

  * auth - String, SessionID of user that wants to query for roles
  * log - String for log information, optional
  * level - Integer. Can be either empty (= level 0) or one of the following:

    * 0 - Returns all projects the user has any role in (default).
    * 1 - Returns only projects where the user has read access, i.e.

      * she is Beobachter (observer) or
      * she is Bearbeiter (editor) or


getAllProjects
--------------

Returns all projects stored in this RBAC instance with ID, name, and description. See also getProjectDescription().

* Input Parameters getAllProjectsRequest, with elements

  * auth - String, SessionID of user that wants to query for projects. Can be omitted as this information can be reviewed publicly
  * log - String for log information, optional

* Output Parameters getAllProjectsResponse, with element

  * project - projectInfo, can occur multiple times
  * The type projectInfo consists of the following elements:

    * id - String, project ID, such as "TGPR123"
    * description - String, project description
    * name - String, project name
    * file - String with URI of project file for project-specific settings.


getDeactivatedProjects
----------------------

Returns all projects stored in this RBAC instance with ID, name, and description that are deactivated and can be reactivated by the current user.

* Input Parameters getAllProjectsRequest, with elements

  * auth - String, SessionID of user that wants to query for projects. Can be omitted as this information can be reviewed publicly
  * log - String for log information, optional

* Output Parameters getDeactivatedProjectsResponse, with element

  * project - projectInfo, can occur multiple times.
  * The type projectInfo consists of the following elements:

    * id - String, project ID, such as "TGPR123"
    * description - String, project description
    * name - String, project name
    * file - String with URI of project file for project-specific settings.


getLeader
---------

Returns Project Leader/s (i.e. who have delegate right on the respective project resource).

* Input Parameters getLeaderRequest, with elements

  * auth - String, SessionID of user that wants to query for projects. Can be omitted as this information can be reviewed publicly
  * log - String for log information, optional
  * project - String with project ID, such as "TGPR123"

* Output Parameters usersetResponse, with element

  * username - String holding ePPN of user, can occur multiple times.


filterBySid
-----------

This is tgCheckAccess in batch operation: Returns a list of the resources where a given operation with this SessionID is allowed. The resource parameter must contain a list of resources, and the subset of this list that matches will be returned.

* Input Parameters filterBySidRequest, with elements

  * auth - String, SessionID of user that wants to do the check
  * log - String for log information, optional
  * resource - String with projectID, or URI of TgObject resource. Can occur multiple times.
  * operation - String, such as "read", "write"

* Output Parameters filterResponse, with element

  * resource - String with projectID, or URIs that match. Can occur multiple times.

* Faults

  * authenticationFault


getOwner
--------

Returns ePPN of owner that was set in registerResource. User must have read permission on the resource, or resource must be public.

* Input Parameters getOwnerRequest, with elements

  * auth - String, SessionID of user that wants to query for the owner
  * log - String for log information, optional
  * resource - String, URI of TgObject

* Output Parameters getOwnerResponse, with element

  * owner - String, ePPN of owner.

* Faults

  * authenticationFault


getMembers
----------

Returns ePPNs of all members in the project, caller must be member herself.

* Input Parameters getMembersRequest, with elements

  * auth - String, SessionID of user that wants to query for members
  * log - String for log information, optional
  * project - String, ProjectID, such as "TGPR123"

* Output Parameters usersetResponse, with element

  * username - String holding ePPN of user, can occur multiple times.

* Faults

  * authenticationFault


getUserRole
-----------

Returns ePPNs plus Array of Roles of all members in the project, caller must be member herself.

* Input Parameters getUserRoleRequest, with elements

  * auth - String, SessionID of user that wants to query for roles
  * log - String for log information, optional
  * project - String, ProjectID, such as "TGPR123"

* Output Parameters getUserRoleResponse. It contains 0...n userRoles, which themselves have elements

  * username - String holding ePPN of user
  * roles - array of Strings with roles the user has (e.g. "Bearbeiter").

* Faults

  * authenticationFault
  * unknownProjectFault


getFriends
----------

Returns ePPNs of the users the authenticated user has relations with, i.e. they are signed into the same project role. Each user name has a score which shows how many roles the requester shares with that user.

* Input Parameters getFriendsRequest, with element

  * auth - String, SessionID of user that wants to query for friends

* Output Parameters getFriendsResponse. It contains 0...n friends, which themselves have elements

  * username - String holding ePPN of user
  * score - Integer with the users' number of common roles with the requester

* Faults none


getRights
---------

Returns permissions for given resource or projectID. Can be used by project leaders to check permissions for another project member.

* Input Parameters getRightsRequest, with elements

  * auth - String, SessionID of user that wants to query the rights
  * log - String for log information, optional
  * resource - String, URI of TgObject or ProjectID
  * username - ePPN of user whose rights shall be queried. Leave empty to get rights of user authenticated by the auth parameter.

* Output Parameters operationsetResponse, with element

  * operation - String with a right, such as "read" or "delegate". Can occur multiple times.

* Faults

  * authenticationFault


isPublic
--------

Returns status of isPublic flag for a TextGridObject.

* Input Parameters isPublicRequest, with elements

  * auth - String, SessionID of user that wants to query. Can be empty.
  * log - String for log information, optional
  * resource - String, URI of TgObject

* Output Parameters booleanResponse, with element

  * result - boolean, true if resource has the isPublic flag set to TRUE, i.e. has been published, false otherwise


getNumberOfResources
--------------------

Returns the total number of resources and the number of public ones in this project. Can be called by anyone.

* Input Parameters getNumberOfResourcesRequest, with elements

  * auth - String, SessionID of user that wants to query. Can be empty.
  * log - String for log information, optional
  * project - String

* Output Parameters getNumberOfResourcesResponse, with elements

  * allresources - integer
  * publicresources - integer


getProjectDescription
---------------------

Returns name and description of project identified by ID. See also getAllProjects().

* Input Parameters getProjectDescriptionRequest, with elements

  * auth - String, SessionID of user. Can be empty.
  * log - String for log information, optional
  * project - String, ProjectID

* Output Parameters getProjectDescriptionResponse, with element

  * project - projectInfo
  * The type projectInfo consists of the following elements:

    * id - String, project ID, such as "TGPR123"
    * description - String, project description
    * name - String, project name
    * file - String with URI of project file for project-specific settings.


getSid
------

Returns some random SessionID suitable for RBAC.

* Input Parameters empty
* Output Parameters getSidResponse, with element

  * sid - String, approximately 60-70 bytes long, consisting of random characters taken from a-zA-Z0-9.


getSupportedUserAttributes
--------------------------

TODO


getMyUserAttributes
-------------------

TODO


authenticate
------------

Internal function used by applications to identify themselves (via shared secret between RBAC and WebAuth).

* Input Parameters authenticateRequest, with elements

  * username - String with name of application
  * password - String with password for this application
  * log - String for log information, optional

* Output Parameters authenticateResponse, with element

  * auth - String, SessionID of authenticated application (See getSid() for format.)

* Faults

  * authenticationFault


createProject
-------------

Creates a project. Projects are roles with various sub-roles, i.e Project Leader, Administrator... Creates default roles with default permissions:

===================================  =========
Role                                 Right on Project
===================================  =========
Projektleiter (Project Leader)       delegate
Administrator (Authority to delete)  -none-
Bearbeiter (Editor)                  create
Beobachter (Observer)                -none-
===================================  =========

These permissions can be adapted afterwards. For the rights on resources, see registerResource().

* Input Parameters createProjectRequest, with elements

  * auth - String, SessionID of future project leader
  * log - String for log information, optional
  * name - String with name of project
  * description - String with description of project
  * file - String with URI of project file - currently unused, omit or leave empty and use setProjectFile( ) once the file resource has been created

* Output Parameters createProjectResponse, with element

  * projectId - String with the newly assigned project ID, such as "TGPR123"


setProjectFile
--------------

Registers the URI of a TextGridObject to be the project file for project-specific settings. The URI must be registered at RBAC beforehand.

* Input Parameters setProjectFileRequest, with elements

  * auth - String, SessionID of Project Leader
  * log - String for log information, optional
  * project - String with projectID
  * file - String with URI of project file

* Output Parameters booleanResponse, with element

  * result - boolean, true if operation was successful, false otherwise

* Faults

  * authenticationFault


setName
-------

Supply user-specific information for display instead of the ePPN.

* Input Parameters setNameRequest, with elements

  * auth - String, SessionID of User who wants to set their name
  * log - String for log information, optional
  * webAuthSecret - String that is known by the Web Authentication which will assure that the data are correct. If the secret is not given, the user entry will be marked as a volunteered one, where the users gave their details themselves.
  * name - String with User Name, preferredly in the form of "givenname surname"
  * mail - String with User's E-Mail address
  * organisation - String with Affiliation of the User
  * agreeSerach - Boolean telling whether the user agrees to be searchable by the getIDs function.

* Output Parameters booleanResponse, with element

  * result - boolean, true if operation was successful, false otherwise

* Faults


addMember
---------

Project leaders can assign users into specific roles. This still does not effect activation of the role in the user(s)' session(s).

* Input Parameters addMemberRequest, with elements

  * auth - String, SessionID of project leader
  * log - String for log information, optional
  * username - String with ePPN of user to be added
  * role - String with full-qualified role name of the role the user is to be signed into. The full-qualified role name has the form "Administrator,TGPR3,Projekt-Teilnehmer"

* Output Parameters booleanResponse, with element

  * result - boolean, true if operation was successful, false otherwise

* Faults

  * rbacFault


deleteMember
------------

Project leaders can delete a role from a user. This still does not effect (de-)activation of the role in the user(s)' session(s).

* Input Parameters deleteMemberRequest, with elements

  * auth - String, SessionID of project leader
  * log - String for log information, optional
  * username - String with ePPN of user to be signed off
  * role - String with full-qualified role name of the role the user is to be signed off. The full-qualified role name has the form "Administrator,TGPR3,Projekt-Teilnehmer"

* Output Parameters booleanResponse, with element

  * result - boolean, true if operation was successful, false otherwise


tgAddActiveRole
---------------

Activates a role for a session. NB, currently the login process activates all available roles of the user.

* Input Parameters tgAddActiveRoleRequest, with elements

  * auth - String, SessionID of user
  * log - String for log information, optional
  * role - String with full-qualified role name of the role the user wants to activate. The full-qualified role name has the form "Administrator,TGPR3,Projekt-Teilnehmer".

* Output Parameters booleanResponse, with element

  * result - boolean, true if operation was successful, false otherwise


tgDropActiveRole
----------------

De-activates a role for a session.

* Input Parameters tgDropActiveRoleRequest, with elements

  * auth - String, SessionID of user
  * log - String for log information, optional
  * role - String with full-qualified role name of the role the user wants to de-activate. The full-qualified role name has the form "Administrator,TGPR3,Projekt-Teilnehmer".

* Output Parameters booleanResponse, with element

  * result - boolean, true if operation was successful, false otherwise


tgGrantPermission
-----------------

Enables given operation for given role on given resource. Resource may be an URI of a TgObject, or a projectID. Users need delegate right on resource or project (i.e. be project leader).

* Input Parameters tgGrantPermissionRequest, with elements

  * auth - String, SessionID of user
  * log - String for log information, optional
  * role - String with full-qualified role name of the role the user wants to grant a right. The full-qualified role name has the form "Administrator,TGPR3,Projekt-Teilnehmer".
  * operation - String, operation to be granted, such as "read", "write".
  * resource - String, URI of TgObject, or projectID

* Output Parameters booleanResponse, with element

  * result - boolean, true if operation was successful, false otherwise

* Faults

  * authenticationFault
  * rbacFault


tgRevokePermission
------------------

Disables this permission, see tgGrantPermission for limitations.

* Input Parameters tgRevokePermissionRequest, with elements

  * auth - String, SessionID of user
  * log - String for log information, optional
  * role - String with full-qualified role name of the role the user wants to revoke a right. The full-qualified role name has the form "Administrator,TGPR3,Projekt-Teilnehmer".
  * operation - String, operation to be granted, such as "read", "write".
  * resource - String, URI of TgObject, or projectID

* Output Parameters booleanResponse, with element

  * result - boolean, true if operation was successful, false otherwise

* Faults

  * authenticationFault


deactivateProject
-----------------

Hides a project. User must be project leader of the project to be deactivated. Hidden projects cannot be modified or read anymore (except published resources). However, information is preserved so that the project can be re-activated by manual modification of the database.

* Input Parameters deactivateProjectRequest, with elements

  * auth - String, SessionID of user that wants publish
  * log - String for log information, optional
  * project - String, ProjectID

* Output Parameters booleanResponse, with element

  * result - boolean, true if operation was successful, false otherwise

* Faults

  * authenticationFault


reactivateProject
-----------------

Reactivate a hidden and deactivated project. The user must be the former project leader of the project to be able to reactivate it. All rights will be restored.

* Input Parameters reactivateProjectRequest, with elements

  * auth - String, SessionID of user that wants publish
  * log - String for log information, optional
  * project - String, ProjectID

* Output Parameters booleanResponse, with element

  * result - boolean, true if operation was successful, false otherwise

* Faults

  * authenticationFault


deleteProject
-------------

Delete a project. User must be project leader of the project that is deleted. Before a project can be deleted there must not be any resources associated with the project. A deleted project cannot be restored.

* Input Parameters deleteProjectRequest, with elements

  * auth - String, SessionID of user that wants publish
  * log - String for log information, optional
  * project - String, ProjectID

* Output Parameters booleanResponse, with element

  * result - boolean, true if operation was successful, false otherwise

* Faults

  * authenticationFault
  * notEmptyFault
