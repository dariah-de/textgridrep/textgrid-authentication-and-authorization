TG-auth* API (crud)
===================


registerResource
----------------

Registers a resource in the database. Assigns standard permissions to roles in project:

==============================  ============================
Role                            Right on Resource
==============================  ============================
Projektleiter (project leader)  delegate, publish
Administrator                   delete
Bearbeiter (editor)             read, write
Beobachter (observer)           read
==============================  ============================

These permissions can be adapted afterwards. User needs create right on project resource, see createProject().

This function is **only for use by TG-crud**. Clients should call TG-crud#create() instead.

* Input Parameters registerResorceRequest, with elements

  * auth - String, SessionID of future owner of this resource
  * log - String for log information, optional
  * secret - String known by TG-crud to authenticate itself
  * project - String with project ID, such as "TGPR123"
  * uri - String holding URI of TgObject resource to be registered in RBAC

* Output Parameters operationsSetResponse, with element

  * operation (0..n) Strings with permissible operations on the newly created resource

* Faults

  * authenticationFault
  * rbacFault - if resource could not be registered successfully


unregisterResource
------------------

Removes resource from database. User needs delete right on resource. **Only for use by TG-crud**. Clients should call TG-crud#delete() instead.

* Input Parameters unregisterResourceRequest, with elements

  * auth - String, SessionID of requester
  * log - String for log information, optional
  * secret - String known by TG-crud to authenticate itself
  * uri - String holding URI of TgObject resource to be removed from RBAC

* Output Parameters booleanResponse, with element

  * result - boolean, true if operation was successful, false otherwise

* Faults

  * unknownResourceFault
  * authenticationFault


tgCrudCheckAccess
-----------------

Returns access decision for given operation on given resource for session. In addition the owner of the session and informations on the project is returned. **Only for use by TG-crud**.

* Input Parameters tgCrudCheckAccessRequest, with elements

  * auth - String, SessionID of user (or service) that wants to intitiate the check
  * log - String for log information, optional
  * secret - String known by TG-crud to authenticate itself
  * sid - String, SessionID of user for whom the check is to be done. Leave empty if check is to be done for user's own session given under **auth**
  * operation - String, such as "read", "write"
  * resource - String, name of project or role, or URI of TgObject resource

* Output Parameters tgCrudCheckAccessResponse, with elements

  * public
  * project
  * username
  * operation

* Faults

  * unknownResourceFault


putCRT
------

Puts the signed Certificate into the RBAC's accompanying daemon that holds the key.

* Input Parameters putCRTRequest, with elements

  * auth - String, SessionID of user
  * log - String for log information, optional
  * crt - the Certificate as a String

* Output Parameters putCRTResponse, with element

  * success, Boolean

* Faults

  * authenticationFault
  * unknownResourceFault


getEPPN
-------

Returns ePPN (aka TextGrid ID) of authenticated user. As this function could be used for SID guessing, the caller must know a secret.

* Input Parameters getEPPNRequest, with elements

  * auth - String, SessionID of user that wants to query his ePPN
  * log - String for log information, optional
  * secret - String known by TG-crud to authenticate itself

* Output Parameters getEPPNResponse, with element

  * eppn - String, ePPN of user

* Faults

  * authenticationFault


getUUID
-------

Returns UUID (full path including server address) of the resource. User must have read permission on the resource, or resource must be public.

* Input Parameters getUUIDRequest, with elements

  * auth - String, SessionID of user that wants to query for the UUID
  * log - String for log information, optional
  * secret - String known by TG-crud to authenticate itself
  * resource - String, URI of TgObject

* Output Parameters getUUIDResponse, with element

  * uuid - String, UUID if defined

* Faults

  * authenticationFault


publish
-------

Sets the isPublic flag of a TextGridObject identified by its URI to TRUE. The resource will then be world-readable. This is irreversible (only by manual modification of the database) and needs the publish right, which by default is granted to project leaders.

* Input Parameters publishRequest, with elements

  * auth - String, SessionID of user that wants publish
  * log - String for log information, optional
  * secret - String known by TG-crud to authenticate itself
  * resource - String, URI of TgObject

* Output Parameters booleanResponse, with element

  * result - boolean, true if operation was successful, false otherwise

* Faults

  * authenticationFault


getSLC
------

CRUD may use this to retrieve a Short-Lived Certificate Key pair in PEM format for the user.

* Input Parameters getSLCRequest, with elements

  * auth - String, SessionID of user
  * log - String for log information, optional
  * secret - String known by TG-crud to authenticate itself

* Output Parameters getSLCResponse, with element

  * slc - Base64binary String with the key pair. Contains only printable ASCII Character.

* Faults

  * authenticationFault
  * unknownResourceFault


nearlyPublish
-------------

* Input Parameters nearlyPublishRequest, with elements

  * auth - String, SessionID of user
  * log - String for log information, optional
  * secret - String known by TG-crud to authenticate itself
  * resource - String, URI of TgObject

* Output Parameters booleanResponse, with element

  * result - boolean, true if operation was successful, false otherwise

* Faults

  * authenticationFault
  * unknownResourceFault


getCSR
------

The Portal may use this to retrieve a Dummy Certificate Signing Request (CSR) in PEM format, for which the RBAC's accompanying daemon will hold the real key.

* Input Parameters getCSRRequest, with elements

  * auth - String, SessionID of user
  * log - String for log information, optional

* Output Parameters getCSRResponse, with element

  * csr - Base64binary String with the CSR. Contains only printable ASCII Character

* Faults

  * authenticationFault
  * unknownResourceFault
